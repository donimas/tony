import React, {useEffect, useState} from 'react';
import { connect } from 'react-redux';
import {IRootState} from "app/shared/reducers";
import CreateDictForm from "app/shared/form/create-dict";
import {Button, Row, Col, Label, Table} from 'reactstrap';
import { Translate, translate } from 'react-jhipster';
import CreateCompanyForm from "app/shared/form/create-company";
import {AvForm, AvGroup, AvInput, AvField} from 'availity-reactstrap-validation';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {DICT_PARTNERSHIP_TYPE} from "app/shared/util/constants";

const PartnersUpdate = (props) => {
	
	const [partners, setPartners] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [workTitle, setWorkTitle] = useState("");
	const [shareType, setShareType] = useState('SUM_FROM_CONTRACT');
	const [share, setShare] = useState('0');
	
	const customerInitial = null;
	const typeDictInitial = null;
	
	const [company, setCompany] = useState(customerInitial);
	const [typeDict, setTypeDict] = useState(typeDictInitial);
	
	useEffect(() => {
		props.onChange(partners);
		setCompany(customerInitial);
		setTypeDict(typeDictInitial);
		setWorkTitle("");
		setShareType('SUM_FROM_CONTRACT');
		setShare('0');
	}, [partners]);
	
	const handleAddPartner = () => {
		const companyCopy = Object.assign({}, company);
		const typeDictCopy = Object.assign({}, typeDict);
		
		const newPartner = {
			typeId: typeDictCopy && typeDictCopy.id || null,
			contractorId: companyCopy && companyCopy.id || null,
			share,
			shareType,
			workTitle,
			
			company: companyCopy,
			typeDict: typeDictCopy
		};
		setPartners(prevState => [...prevState, newPartner]);
	};
	
	const handleRemovePartner = (partner, i) => {
		setPartners(prevData => prevData.filter(p => p !== partner));
	};
	
	const handleOnWorkTitleChange = (e) => {
		e.persist();
		setWorkTitle(e.target.value);
	};
	
	const handleOnShareChange = (e) => {
		e.persist();
		setShare(e.target.value);
	};
	const handleOnShareTypeChange = (e) => {
		e.persist();
		setShareType(e.target.value);
	};
	
	return (
		<div>
			<Table responsive>
				<thead>
				<tr>
					<th style={{width: '20%'}}>
						<Translate contentKey="ronyApp.partnership.contractor">Contractor</Translate>
					</th>
					<th style={{width: '15%'}}>
						<Translate contentKey="ronyApp.partnership.type">Type</Translate>
					</th>
					<th style={{width: '25%'}}>
						<Translate
							contentKey="ronyApp.partnership.description">Description</Translate>
					</th>
					<th style={{width: '15%'}}>
						<Translate contentKey="ronyApp.partnership.shareType">Share type</Translate>
					</th>
					<th style={{width: '15%'}}>
						<Translate
							contentKey="ronyApp.partnership.share">Share</Translate>
					</th>
					<th style={{width: '10%'}}/>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td>
						
						<CreateCompanyForm
							company={company}
							isResetable={true}
							onChange={(option) => setCompany(option)}
						/>
					
					</td>
					<td>
						<CreateDictForm
							dict={typeDict}
							entityName={DICT_PARTNERSHIP_TYPE}
							onChange={(option) => setTypeDict(option)}
						/>
					</td>
					<td>
						<AvField id="partners-update-description" type="text" name="workTitle" value={workTitle}
								 onChange={handleOnWorkTitleChange}/>
					</td>
					<td>
						<AvInput
							id="share-type"
							type="select"
							className="form-control"
							name="shareType"
							value={shareType}
							onChange={handleOnShareTypeChange}
						>
							<option value="PERCENTAGE_FROM_CONTRACT">{translate('ronyApp.shareType.PERCENTAGE_FROM_CONTRACT')}</option>
							<option value="SUM_FROM_CONTRACT">{translate('ronyApp.shareType.SUM_FROM_CONTRACT')}</option>
						</AvInput>
					</td>
					<td>
						<AvField id="share" type="text" name="share" value={share} onChange={handleOnShareChange}/>
					</td>
					<td>
						<Button color="primary" className="float-right" type="button" onClick={handleAddPartner}>
							{!isLoading ? <FontAwesomeIcon icon="plus"/> : <FontAwesomeIcon icon="plus" spin={true} />}
						</Button>
					</td>
				</tr>
				{partners.map((row, i) => (
					<tr key={`entity-${i}`}>
						<td>{row.company?.name}</td>
						<td>{row.typeDict?.name}</td>
						<td>{row.workTitle}</td>
						<td>{translate('ronyApp.shareType.'+row.shareType)}</td>
						<td>{row.share}</td>
						<td>
							<Button color="danger" size="sm" className="float-right" type="button" onClick={() => handleRemovePartner(row, i)}>
								<FontAwesomeIcon icon="trash" />
							</Button>
						</td>
					</tr>
				))}
				</tbody>
			</Table>
		</div>
	);
	
};

const mapStateToProps = (storeState: IRootState) => ({

});

const  mapDispatchToProps = {

};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PartnersUpdate);