export const DICT_PROJECT_STATUS_HISTORY = 'ProjectStatusHistory';
export const DICT_AGREEMENT_STATUS_HISTORY = 'AgreementStatusHistory';
export const DICT_CONTRACT_STATUS_HISTORY = 'ContractStatusHistory';
export const DICT_CURRENCY = 'Currency';
export const DICT_STACK = 'TechStack';
export const DICT_CONTRACT_SALES_TYPE = 'ContractSalesType';
export const DICT_PARTNERSHIP_TYPE = 'PartnershipType';

export const ENTITY_NAME_USER = 'User';
export const ENTITY_NAME_AGREEMENT = 'Agreement';
export const ENTITY_NAME_PROJECT = 'Project';
export const ENTITY_NAME_MAIN_CONTACT = 'MainContact';

export const CURRENCY_API_KEY = '5f81c65224d33e16be8bc77331c402dc';
export const CURRENCY_API_URL = 'http://data.fixer.io/api';
