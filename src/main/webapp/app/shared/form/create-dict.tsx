import React, {useState, useEffect} from 'react';
import CreatableSelect from 'react-select/creatable';
import {createDict, getEntitiesByName} from "app/entities/dictionary/dictionary.reducer";
import {IRootState} from "app/shared/reducers";
import { connect } from 'react-redux';
import {DICT_STACK, ENTITY_NAME_MAIN_CONTACT} from "app/shared/util/constants";
import {createTechStack, getEntities as fetchTechStacks} from "app/entities/tech-stack/tech-stack.reducer";
import makeAnimated from 'react-select/animated';

import {getEntities as fetchMainContacts, createMainContact} from "app/entities/main-contact/main-contact.reducer";
import { toast } from 'react-toastify';
import { translate } from 'react-jhipster';

const animatedComponents = makeAnimated();

const CreateDictForm = (props) => {
	
	const {stacks, mainContacts} = props;
	
	const [data, setData] = useState([]);
	const [selectedItem, setSelectedItem] = useState(undefined);
	const [isLoading, setIsLoading] = useState(false);
	
	const [optionLabelTitle, setOptionLabelTitle] = useState('name');
	const [optionValueTitle, setOptionValueTitle] = useState('id');
	
	useEffect(() => {
		if(DICT_STACK === props.entityName) {
			props.fetchTechStacks();
		} else if(ENTITY_NAME_MAIN_CONTACT === props.entityName) {
			props.fetchMainContacts();
		} else {
			getEntitiesByName(props.entityName).payload.then((resp) => {
				const tmp:any = resp.data;
				setData([...tmp]);
				
				setOptionLabelTitle('name');
			});
		}
	}, []);
	useEffect(() => {
		if(ENTITY_NAME_MAIN_CONTACT === props.entityName) {
			setData([...mainContacts]);
			setOptionLabelTitle('fullName');
		} else if(DICT_STACK === props.entityName) {
			setData([...stacks]);
			setOptionLabelTitle('label');
		}
	}, [stacks, mainContacts]);
	
	useEffect(() => {
		props.onChange(selectedItem);
		if(ENTITY_NAME_MAIN_CONTACT === props.entityName) {
			setSelectedItem(null);
		}
	}, [selectedItem]);
	
	useEffect(() => {
		setSelectedItem(props.dict);
	}, [props.dict]);
	
	const handleOnChange = (inputValue) => {
		setSelectedItem(inputValue);
	};
	
	const releaseLoading = (resp) => {
		if(DICT_STACK === props.entityName) {
			setSelectedItem(prevData => [...prevData, resp]);
		} else {
			setSelectedItem(resp);
		}
		setData(prevData => [...prevData, resp]);
		setIsLoading(false);
	};
	
	const handleOnCreate = (name) => {
		setIsLoading(true);
		
		if(DICT_STACK === props.entityName) {
			createTechStack({label: name}).payload.then((resp) => {
				releaseLoading(resp.data);
			});
		} else if(ENTITY_NAME_MAIN_CONTACT === props.entityName) {
			createMainContact({fullName: name, companyId: props.companyId, flagDeleted: false}).payload.then((resp) => {
				releaseLoading(resp.data);
			}).catch((err) => {
				toast.error(translate(err.response.data.message));
				setIsLoading(false);
			});
		} else {
			createDict({entityName: props.entityName, name, flagDeleted: false}).payload.then((resp) => {
				releaseLoading(resp.data);
			});
		}
	};
	
	return(
		<CreatableSelect
			isMulti={props.isMulty}
			components={animatedComponents}
			isClearable={!props.isMulty}
			options={data}
			getOptionLabel={(option) => option[optionLabelTitle]}
			getOptionValue={(option) => option.id}
			onChange={handleOnChange}
			onCreateOption={handleOnCreate}
			getNewOptionData={(inputValue, optionLabel) => ({
				name: optionLabel,
				label: optionLabel,
				fullName: optionLabel,
				id: inputValue,
				__isNew__: true
			})}
			value={selectedItem}
			isLoading={isLoading}
			isDisabled={isLoading}
			className="basic-multi-select"
			classNamePrefix="select"
			menuPortalTarget={document.body}
			styles={{ menuPortal: base => ({ ...base, zIndex: 9999 }) }}
		/>
	);
	
};
const mapStateToProps = (storeState: IRootState) => ({
	stacks: storeState.techStack.entities,
	mainContacts: storeState.mainContact.entities
});

const mapDispatchToProps = {
	getEntitiesByName,
	fetchTechStacks,
	fetchMainContacts
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CreateDictForm);