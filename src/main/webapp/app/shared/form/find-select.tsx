import React, {useEffect, useState} from 'react';
import AsyncSelect from 'react-select/async';
import {findUsers} from "app/modules/administration/user-management/user-management.reducer";
import {findAgreements, getEntities as fetchAgreements} from "app/entities/agreement/agreement.reducer";
import {ENTITY_NAME_AGREEMENT, ENTITY_NAME_USER} from "app/shared/util/constants";
import {connect} from "react-redux";
import {IRootState} from "app/shared/reducers";

const FindSelect = (props) => {
	
	const {agreements} = props;
	
	const [data, setData] = useState([]);
	
	useEffect(() => {
		if(props.entityName === ENTITY_NAME_AGREEMENT) {
			props.fetchAgreements()
		}
	}, []);
	
	useEffect(() => {
		if(props.entityName === ENTITY_NAME_AGREEMENT) {
			if(agreements.length > 0) {
				const tmp: any = agreements;
				setData([...tmp]);
			}
		}
	}, [agreements]);
	
	const promiseOptions = inputValue =>
		new Promise(resolve => {
			if (inputValue.trim().length < 3) {
				return [];
			}
			
			if(props.entityName && props.entityName === ENTITY_NAME_AGREEMENT) {
				findAgreements(inputValue).payload.then((resp) => {
					resolve(resp.data);
				}).then((err) => {
					resolve([]);
				});
			} else {
				findUsers(inputValue).payload.then((response) => {
					resolve(response.data);
				}).catch((error) => {
					resolve([]);
				});
			}
		});
	
	const handleLabel = (option: any) => {
		if(props.entityName === ENTITY_NAME_USER) {
			return option.firstName + ' ' + option.lastName;
		} else if(props.entityName === ENTITY_NAME_AGREEMENT) {
			return `${option.projectLabel}: ${option.customerLabel}`;
		}
		return 'label not found';
	};
	
	return (
		
		<AsyncSelect
			cacheOptions
			isClearable={true}
			defaultOptions={data}
			loadOptions={promiseOptions}
			getOptionLabel={(option) => handleLabel(option)}
			getOptionValue={(option) => option.id}
			onChange={(option) => props.onChange(option)}
		/>
	);
};

const mapStateToProps = (storeState: IRootState) => ({
	agreements: storeState.agreement.entities
});

const mapDispatchToProps = {
	fetchAgreements
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FindSelect);