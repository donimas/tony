import {connect} from 'react-redux';
import {IRootState} from "app/shared/reducers";
import React, {useState, useEffect} from 'react';
import Select from 'react-select';

import {DICT_CURRENCY, ENTITY_NAME_AGREEMENT, ENTITY_NAME_PROJECT} from "app/shared/util/constants";
import {createDict as createDictionary} from "app/entities/dictionary/dictionary.reducer";
import {getAllSymbols as fetchCurrencies, createCurrency} from "app/entities/currency/currency.reducer";

import {getEntities as fetchProjects} from "app/entities/project/project.reducer";

const SelectEntityForm = (props) => {
	
	const {currencies, projects} = props;
	
	const [data, setData] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [selectedItem, setSelectedItem] = useState(undefined);
	
	const [optionLabel, setOptionLabel] = useState('formula');
	const [optionValue, setOptionValue] = useState('formula');
	
	useEffect(() => {
		if(DICT_CURRENCY === props.entityName) {
			props.fetchCurrencies();
		} else if(ENTITY_NAME_PROJECT === props.entityName) {
			props.fetchProjects();
		}
	}, []);
	
	useEffect(() => {
		if(DICT_CURRENCY === props.entityName) {
			setData([...currencies]);
		} else if(ENTITY_NAME_PROJECT === props.entityName) {
			setData([...projects]);
			
			setOptionLabel('name');
			setOptionValue('id');
		}
	}, [currencies, projects]);
	
	useEffect(() => {
		props.onChange(selectedItem);
	}, [selectedItem]);
	
	const createCurrencyByFormula = (inputValue: string) => {
		setIsLoading(true);
		createCurrency(inputValue).payload.then((resp) => {
			const tmp: any = resp.data;
			setSelectedItem(tmp);
			setIsLoading(false);
		});
	};
	
	const handleOnChange = (inputValue: string) => {
		if(!inputValue) {
			setSelectedItem(null);
			return;
		}
		if(DICT_CURRENCY === props.entityName) {
			createCurrencyByFormula(inputValue);
		} else if(ENTITY_NAME_PROJECT === props.entityName) {
			setSelectedItem(inputValue);
		}
	};
	
	return (
		<Select
			isClearable={true}
			options={data}
			getOptionLabel={(option) => option[optionLabel]}
			getOptionValue={(option) => option[optionValue]}
			onChange={handleOnChange}
			value={selectedItem}
			isLoading={isLoading}
			isDisabled={isLoading}
		/>
	);
	
};

const mapStateToProps = (storeState: IRootState) => ({
	currencies: storeState.currency.symbols,
	createdDictionary: storeState.dictionary.entity,
	
	projects: storeState.project.entities
});

const mapDispatchToProps = {
	fetchCurrencies,
	createDictionary,
	createCurrency,
	
	fetchProjects
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SelectEntityForm);