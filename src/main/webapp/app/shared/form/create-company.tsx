import {connect} from 'react-redux';
import React, {useEffect, useState} from 'react';
import CreatableSelect from 'react-select/creatable';
import {createCustomer} from "app/entities/company-info/company-info.reducer";

import {getEntities as fetchCompanies} from "app/entities/company-info/company-info.reducer";
import {IRootState} from "app/shared/reducers";

const CreateCompanyForm = (props) => {
	
	const {companies} = props;
	
	const [data, setData] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [selectedCustomer, setSelectedCustomer] = useState(undefined);
	
	useEffect(() => {
		props.fetchCompanies();
	}, []);
	
	useEffect(() => {
		setSelectedCustomer(props.company);
	}, [props.company]);
	
	useEffect(() => {
		if(companies.length) {
			setData([...companies]);
		}
	}, [companies]);
	
	useEffect(() =>  {
		props.onChange(selectedCustomer);
	}, [selectedCustomer]);
	
	const handleOnChange = (inputValue) => {
		setSelectedCustomer(inputValue);
	};
	
	const handleOnCreate = (name) => {
		if(!name.trim().length) {
			return;
		}
		setIsLoading(true);
		createCustomer({name, flagDeleted: false}).payload.then((resp) => {
			setSelectedCustomer(resp.data);
			setData(prevData => [...prevData, resp.data]);
			setIsLoading(false);
		});
	};
	
	return (
		<CreatableSelect
			isClearable={true}
			options={data}
			getOptionLabel={(option) => option.name}
			getOptionValue={(option) => option.id}
			name={'customerId'}
			onChange={handleOnChange}
			onCreateOption={handleOnCreate}
			getNewOptionData={(inputValue, optionLabel) => ({
				name: optionLabel,
				id: inputValue,
				__isNew__: true
			})}
			value={selectedCustomer}
			isLoading={isLoading}
			isDisabled={isLoading}
			menuPortalTarget={document.body}
			styles={{ menuPortal: base => ({ ...base, zIndex: 9999 }) }}
		/>
	);
};

const mapStateToProps = (storeState: IRootState) => ({
	companies: storeState.companyInfo.entities
});

const mapDispatchToProps = {
	fetchCompanies
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CreateCompanyForm);