import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import project, {
  ProjectState
} from 'app/entities/project/project.reducer';
// prettier-ignore
import structure, {
  StructureState
} from 'app/entities/structure/structure.reducer';
// prettier-ignore
import companyInfo, {
  CompanyInfoState
} from 'app/entities/company-info/company-info.reducer';
// prettier-ignore
import mainContact, {
  MainContactState
} from 'app/entities/main-contact/main-contact.reducer';
// prettier-ignore
import address, {
  AddressState
} from 'app/entities/address/address.reducer';
// prettier-ignore
import projectTeam, {
  ProjectTeamState
} from 'app/entities/project-team/project-team.reducer';
// prettier-ignore
import teamMember, {
  TeamMemberState
} from 'app/entities/team-member/team-member.reducer';
// prettier-ignore
import contract, {
  ContractState
} from 'app/entities/contract/contract.reducer';
// prettier-ignore
import transaction, {
  TransactionState
} from 'app/entities/transaction/transaction.reducer';
// prettier-ignore
import techStack, {
  TechStackState
} from 'app/entities/tech-stack/tech-stack.reducer';
// prettier-ignore
import dictionary, {
  DictionaryState
} from 'app/entities/dictionary/dictionary.reducer';
// prettier-ignore
import partnership, {
  PartnershipState
} from 'app/entities/partnership/partnership.reducer';
// prettier-ignore
import currency, {
  CurrencyState
} from 'app/entities/currency/currency.reducer';
// prettier-ignore
import agreement, {
  AgreementState
} from 'app/entities/agreement/agreement.reducer';
// prettier-ignore
import agreementStatusHistory, {
  AgreementStatusHistoryState
} from 'app/entities/agreement-status-history/agreement-status-history.reducer';
// prettier-ignore
import projectStatusHistory, {
  ProjectStatusHistoryState
} from 'app/entities/project-status-history/project-status-history.reducer';
// prettier-ignore
import contractStatusHistory, {
  ContractStatusHistoryState
} from 'app/entities/contract-status-history/contract-status-history.reducer';
// prettier-ignore
import attachment, {
  AttachmentState
} from 'app/entities/attachment/attachment.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly project: ProjectState;
  readonly structure: StructureState;
  readonly companyInfo: CompanyInfoState;
  readonly mainContact: MainContactState;
  readonly address: AddressState;
  readonly projectTeam: ProjectTeamState;
  readonly teamMember: TeamMemberState;
  readonly contract: ContractState;
  readonly transaction: TransactionState;
  readonly techStack: TechStackState;
  readonly dictionary: DictionaryState;
  readonly partnership: PartnershipState;
  readonly currency: CurrencyState;
  readonly agreement: AgreementState;
  readonly agreementStatusHistory: AgreementStatusHistoryState;
  readonly projectStatusHistory: ProjectStatusHistoryState;
  readonly contractStatusHistory: ContractStatusHistoryState;
  readonly attachment: AttachmentState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  project,
  structure,
  companyInfo,
  mainContact,
  address,
  projectTeam,
  teamMember,
  contract,
  transaction,
  techStack,
  dictionary,
  partnership,
  currency,
  agreement,
  agreementStatusHistory,
  projectStatusHistory,
  contractStatusHistory,
  attachment,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;
