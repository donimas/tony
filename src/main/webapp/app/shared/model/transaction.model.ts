import { Moment } from 'moment';
import { TransactionStatus } from 'app/shared/model/enumerations/transaction-status.model';
import { DirectionType } from 'app/shared/model/enumerations/direction-type.model';

export interface ITransaction {
  id?: number;
  planningSum?: number;
  actualSum?: number;
  convertedSum?: number;
  payDate?: string;
  note?: string;
  status?: TransactionStatus;
  direction?: DirectionType;
  createDate?: string;
  flagDeleted?: boolean;
  currencyId?: number;
  contractId?: number;
}

export const defaultValue: Readonly<ITransaction> = {
  flagDeleted: false,
};
