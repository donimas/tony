import { Moment } from 'moment';

export interface IProjectStatusHistory {
  id?: number;
  comment?: string;
  createDate?: string;
  statusId?: number;
  projectId?: number;
  statusType?: string;
}

export const defaultValue: Readonly<IProjectStatusHistory> = {};
