export interface IAttachment {
  id?: number;
  fileContentType?: string;
  file?: any;
  type?: string;
  uri?: string;
  unicode?: string;
  name?: string;
  entityName?: string;
  entityId?: number;
  flagDeleted?: boolean;
}

export const defaultValue: Readonly<IAttachment> = {
  flagDeleted: false,
};
