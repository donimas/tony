import { Moment } from 'moment';

export interface ITeamMember {
  id?: number;
  involvement?: number;
  startDate?: string;
  finishDate?: string;
  flagLeft?: boolean;
  createDate?: string;
  employeeId?: number;
  teamId?: number;
}

export const defaultValue: Readonly<ITeamMember> = {
  flagLeft: false,
};
