import { Moment } from 'moment';

export interface IAgreementStatusHistory {
  id?: number;
  createDate?: string;
  comment?: string;
  statusId?: number;
  agreementId?: number;
  statusDictLabel?: string;
}

export const defaultValue: Readonly<IAgreementStatusHistory> = {};
