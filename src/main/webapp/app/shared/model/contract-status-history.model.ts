import { Moment } from 'moment';

export interface IContractStatusHistory {
  id?: number;
  comment?: string;
  createDate?: string;
  statusId?: number;
  contractId?: number;
}

export const defaultValue: Readonly<IContractStatusHistory> = {};
