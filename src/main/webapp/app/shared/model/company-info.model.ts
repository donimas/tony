export interface ICompanyInfo {
  id?: number;
  name?: string;
  website?: string;
  requisites?: string;
  flagDeleted?: boolean;
  addressId?: number;
}

export const defaultValue: Readonly<ICompanyInfo> = {
  flagDeleted: false,
};
