export interface IDictionary {
  id?: number;
  code?: string;
  name?: string;
  entityName?: string;
  flagDeleted?: boolean;
}

export const defaultValue: Readonly<IDictionary> = {
  flagDeleted: false,
};
