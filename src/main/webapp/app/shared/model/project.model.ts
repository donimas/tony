import { Moment } from 'moment';
import { IAgreement } from 'app/shared/model/agreement.model';
import { ITechStack } from 'app/shared/model/tech-stack.model';

export interface IProject {
  id?: number;
  name?: string;
  description?: string;
  goal?: string;
  startDate?: string;
  finishDate?: string;
  uri?: string;
  createDate?: string;
  flagDeleted?: boolean;
  agreements?: IAgreement[];
  statusHistoryId?: number;
  directionId?: number;
  techStacks?: ITechStack[];
  teamId?: number;
  statusDictLabel?: string;
  directionDictLabel?: string;
  teamLeader?: string;
  teamLeadId?: number;
}

export const defaultValue: Readonly<IProject> = {
  flagDeleted: false,
};
