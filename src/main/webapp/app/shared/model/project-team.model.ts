import { IProject } from 'app/shared/model/project.model';
import { ITeamMember } from 'app/shared/model/team-member.model';

export interface IProjectTeam {
  id?: number;
  flagDeleted?: boolean;
  projects?: IProject[];
  teamMembers?: ITeamMember[];
  leadId?: number;
}

export const defaultValue: Readonly<IProjectTeam> = {
  flagDeleted: false,
};
