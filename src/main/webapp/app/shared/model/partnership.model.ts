import { PartnerShareType } from 'app/shared/model/enumerations/partner-share-type.model';

export interface IPartnership {
  id?: number;
  typeId?: number;
  contractorId?: number;
  contractId?: number;
  shareType?: PartnerShareType;
  share?: number;
}

export const defaultValue: Readonly<IPartnership> = {};
