export interface ICurrency {
  id?: number;
  formula?: string;
  titleId?: number;
  titleLabel?: string;
}

export const defaultValue: Readonly<ICurrency> = {};
