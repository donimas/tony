import { IProject } from 'app/shared/model/project.model';

export interface ITechStack {
  id?: number;
  titleId?: number;
  projects?: IProject[];
  label?: string;
}

export const defaultValue: Readonly<ITechStack> = {};
