export interface IAddress {
  id?: number;
  country?: string;
  city?: string;
  fullAddress?: string;
  cabinet?: string;
  lat?: string;
  lon?: string;
  postalCode?: string;
  flagDeleted?: boolean;
}

export const defaultValue: Readonly<IAddress> = {
  flagDeleted: false,
};
