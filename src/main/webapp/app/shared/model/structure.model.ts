export interface IStructure {
  id?: number;
  code?: string;
  name?: string;
  description?: string;
  flagDeleted?: boolean;
  parentId?: number;
}

export const defaultValue: Readonly<IStructure> = {
  flagDeleted: false,
};
