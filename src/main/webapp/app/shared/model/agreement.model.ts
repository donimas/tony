import { Moment } from 'moment';
import { IContract } from 'app/shared/model/contract.model';
import { IMainContact } from 'app/shared/model/main-contact.model';

export interface IAgreement {
  id?: number;
  createDate?: string;
  comment?: string;
  progressBar?: number;
  startDate?: string;
  finishDate?: string;
  flagDeleted?: boolean;
  contracts?: IContract[];
  statusHistoryId?: number;
  customerId?: number;
  mainContacts?: IMainContact[];
  projectId?: number;
  statusDictId?: number;
  statusDictLabel?: number;
  projectLabel?: number;
  customerLabel?: number;
}

export const defaultValue: Readonly<IAgreement> = {
  flagDeleted: false,
};
