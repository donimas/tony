export const enum DirectionType {
  EMPTY = 'EMPTY',

  INCOME = 'INCOME',

  EXPENSE = 'EXPENSE',
}
