export const enum TransactionStatus {
  PLANNING = 'PLANNING',

  DECLINED = 'DECLINED',

  PAID = 'PAID',
}
