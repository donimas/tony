import { Moment } from 'moment';
import { IPartnership } from 'app/shared/model/partnership.model';
import { ITransaction } from 'app/shared/model/transaction.model';
import { IMainContact } from 'app/shared/model/main-contact.model';

export interface IContract {
  id?: number;
  name?: string;
  number?: string;
  sum?: number;
  signDate?: string;
  finishDate?: string;
  comment?: string;
  createDate?: string;
  flagDeleted?: boolean;
  partnerships?: IPartnership[];
  transactions?: ITransaction[];
  responsiblePmId?: number;
  salesTypeId?: number;
  statusHistoryId?: number;
  vendorId?: number;
  customerId?: number;
  currencyId?: number;
  agreementId?: number;
  statusDictId?: number;
  mainContacts?: IMainContact[];

  responsiblePmLabel?: string;
  salesTypeLabel?: string;
  statusHistoryLabel?: string;
  vendorLabel?: string;
  customerLabel?: string;
  currencyLabel?: string;
  agreementLabel?: string;
}

export const defaultValue: Readonly<IContract> = {
  flagDeleted: false,
};
