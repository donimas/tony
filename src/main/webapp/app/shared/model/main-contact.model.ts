import { Moment } from 'moment';
import { IAgreement } from 'app/shared/model/agreement.model';

export interface IMainContact {
  id?: number;
  fullName?: string;
  telephone?: string;
  email?: string;
  dob?: string;
  position?: string;
  comment?: string;
  flagDeleted?: boolean;
  companyId?: number;
  agreements?: IAgreement[];
  companyLabel?: string;
}

export const defaultValue: Readonly<IMainContact> = {
  flagDeleted: false,
};
