import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

export const DictionariesMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.dict.main')}
    id="crm-menu"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="asterisk" to="/project-status-history">
      <Translate contentKey="global.menu.dict.project" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/agreement-status-history">
      <Translate contentKey="global.menu.dict.agreement" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contract-status-history">
      <Translate contentKey="global.menu.dict.contract" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/tech-stack">
      <Translate contentKey="global.menu.dict.stack" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/dictionary">
      <Translate contentKey="global.menu.dict.dictionary" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/currency">
      <Translate contentKey="global.menu.dict.currency" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
