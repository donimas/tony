import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.entities.main')}
    id="entity-menu"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="asterisk" to="/project">
      <Translate contentKey="global.menu.entities.project" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/structure">
      <Translate contentKey="global.menu.entities.structure" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/company-info">
      <Translate contentKey="global.menu.entities.companyInfo" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/main-contact">
      <Translate contentKey="global.menu.entities.mainContact" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/address">
      <Translate contentKey="global.menu.entities.address" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/project-team">
      <Translate contentKey="global.menu.entities.projectTeam" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/team-member">
      <Translate contentKey="global.menu.entities.teamMember" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contract">
      <Translate contentKey="global.menu.entities.contract" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/transaction">
      <Translate contentKey="global.menu.entities.transaction" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/tech-stack">
      <Translate contentKey="global.menu.entities.techStack" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/dictionary">
      <Translate contentKey="global.menu.entities.dictionary" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/partnership">
      <Translate contentKey="global.menu.entities.partnership" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/currency">
      <Translate contentKey="global.menu.entities.currency" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/agreement">
      <Translate contentKey="global.menu.entities.agreement" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/agreement-status-history">
      <Translate contentKey="global.menu.entities.agreementStatusHistory" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/project-status-history">
      <Translate contentKey="global.menu.entities.projectStatusHistory" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contract-status-history">
      <Translate contentKey="global.menu.entities.contractStatusHistory" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/attachment">
      <Translate contentKey="global.menu.entities.attachment" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
