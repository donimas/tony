import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

export const CrmMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.crm.main')}
    id="crm-menu"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="asterisk" to="/project">
      <Translate contentKey="global.menu.crm.project" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/agreement">
      <Translate contentKey="global.menu.crm.agreement" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/main-contact">
      <Translate contentKey="global.menu.crm.mainContact" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contract">
      <Translate contentKey="global.menu.crm.contract" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/transaction">
      <Translate contentKey="global.menu.crm.transaction" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/company-info">
      <Translate contentKey="global.menu.crm.companyInfo" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
