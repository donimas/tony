import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Col, Label, Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';
import {AvFeedback, AvForm, AvGroup, AvInput, AvField} from 'availity-reactstrap-validation';
import {Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';

import {IUser} from 'app/shared/model/user.model';
import {getUsers} from 'app/modules/administration/user-management/user-management.reducer';
import {IDictionary} from 'app/shared/model/dictionary.model';
import {
	getEntitiesContractSalesType,
	getEntitiesContractStatus as fetchContractStatusDictionary,
	getEntitiesCurrency
} from 'app/entities/dictionary/dictionary.reducer';
import {IContractStatusHistory} from 'app/shared/model/contract-status-history.model';
import {getEntities as getContractStatusHistories} from 'app/entities/contract-status-history/contract-status-history.reducer';
import {ICompanyInfo} from 'app/shared/model/company-info.model';
import {getEntities as getCompanyInfos} from 'app/entities/company-info/company-info.reducer';
import {ICurrency} from 'app/shared/model/currency.model';
import {getEntities as getCurrencies} from 'app/entities/currency/currency.reducer';
import {IAgreement} from 'app/shared/model/agreement.model';
import {getEntities as getAgreements} from 'app/entities/agreement/agreement.reducer';
import {getEntity, updateEntity, createEntity, reset} from './contract.reducer';
import {IContract} from 'app/shared/model/contract.model';
import {convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime} from 'app/shared/util/date-utils';
import {mapIdList} from 'app/shared/util/entity-utils';
import CreateDictForm from "app/shared/form/create-dict";
import {
	DICT_CONTRACT_SALES_TYPE,
	DICT_CONTRACT_STATUS_HISTORY, DICT_CURRENCY,
	ENTITY_NAME_AGREEMENT, ENTITY_NAME_MAIN_CONTACT, ENTITY_NAME_PROJECT,
	ENTITY_NAME_USER
} from "app/shared/util/constants";
import SelectEntityForm from "app/shared/form/select-entity";

import FindSelect from "app/shared/form/find-select";
import CreateCompanyForm from "app/shared/form/create-company";
import classnames from 'classnames';
import PartnersUpdate from "app/shared/costs/partners-update";

export interface IContractUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const ContractUpdate = (props: IContractUpdateProps) => {
	const [responsiblePmId, setResponsiblePmId] = useState(undefined);
	const [salesTypeId, setSalesTypeId] = useState(undefined);
	const [statusHistoryId, setStatusHistoryId] = useState(undefined);
	const [vendorId, setVendorId] = useState(undefined);
	const [customerId, setCustomerId] = useState(undefined);
	const [currencyId, setCurrencyId] = useState(undefined);
	const [projectId, setProjectId] = useState(undefined);
	const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);
	const [selectedCustomer, setSelectedCustomer] = useState(undefined);
	
	const [statusDictId, setStatusDictId] = useState(undefined);
	const [mainContacts, setMainContacts] = useState([]);
	
	const [partnerships, setPartnerships] = useState([]);
	
	const {
		contractEntity, users, statusDict, salesTypeDict,
		contractStatusHistories, loading, updating
	} = props;
	
	const handleClose = () => {
		props.history.push('/contract' + props.location.search);
	};
	
	useEffect(() => {
		if (isNew) {
			props.reset();
		} else {
			props.getEntity(props.match.params.id);
		}
	}, []);
	
	useEffect(() => {
		if (props.updateSuccess) {
			handleClose();
		}
	}, [props.updateSuccess]);
	
	const saveEntity = (event, errors, values) => {
		values.signDate = convertDateTimeToServer(values.signDate);
		values.finishDate = convertDateTimeToServer(values.finishDate);
		values.createDate = convertDateTimeToServer(values.createDate);
		
		if (errors.length === 0) {
			const entity = {
				...contractEntity,
				...values,
				currencyId,
				statusDictId,
				salesTypeId,
				customerId,
				projectId,
				vendorId,
				responsiblePmId,
				mainContacts: mapIdList(mainContacts && mainContacts.map(e => e.id)),
				
				partnerships
			};
			
			if (isNew) {
				props.createEntity(entity);
			} else {
				props.updateEntity(entity);
			}
		}
	};
	
	const handleUserOption = (option: any) => {
		setResponsiblePmId(option ? option.id : null);
	};
	
	const addToMainContacts = (option: any) => {
		if(option) {
			setMainContacts(prevState => [...prevState, option]);
		}
	};
	const removeMainContact = (mainContact: any) => {
		const filteredArray = mainContacts.filter((m) => m.id !== mainContact.id);
		setMainContacts([...filteredArray]);
	};
	
	const [activeTab, setActiveTab] = useState('1');
	
	const toggle = tab => {
		if(activeTab !== tab) setActiveTab(tab);
	};
	
	return (
		<div>
			<Row className="justify-content-center">
				<Col md="8">
					<h2 id="ronyApp.contract.home.createOrEditLabel">
						<Translate contentKey="ronyApp.contract.home.createOrEditLabel">Create or edit a
							Contract</Translate>
					</h2>
				</Col>
			</Row>
			<Row className="justify-content-center">
				<Col md="8">
					{loading ? (
						<p>Loading...</p>
					) : (
						<AvForm model={isNew ? {} : contractEntity} onSubmit={saveEntity}>
							{!isNew ? (
								<AvGroup>
									<Label for="contract-id">
										<Translate contentKey="global.field.id">ID</Translate>
									</Label>
									<AvInput id="contract-id" type="text" className="form-control" name="id" required
											 readOnly/>
								</AvGroup>
							) : null}
							<Row>
								<Col md="6">
									<AvGroup>
										<Label id="nameLabel" for="contract-name">
											<Translate contentKey="ronyApp.contract.name">Name</Translate>
										</Label>
										<AvField id="contract-name" type="text" name="name"/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label id="numberLabel" for="contract-number">
											<Translate contentKey="ronyApp.contract.number">Number</Translate>
										</Label>
										<AvField id="contract-number" type="text" name="number"/>
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<Row>
										<Col md="8">
											<AvGroup>
												<Label id="sumLabel" for="contract-sum">
													<Translate contentKey="ronyApp.contract.sum">Sum</Translate>
												</Label>
												<AvField id="contract-sum" type="text" name="sum"/>
											</AvGroup>
										</Col>
										<Col md="4">
											<AvGroup>
												<Label for="contract-currency">
													<Translate contentKey="ronyApp.contract.currency">Currency</Translate>
												</Label>
												
												<SelectEntityForm
													entityName={DICT_CURRENCY}
													onChange={(option) => setCurrencyId(option ? option.id : null)}/>
											
											</AvGroup>
										</Col>
									</Row>
								</Col>
								<Col md="6">
									<Row>
										<Col md="6">
											<AvGroup>
												<Label id="signDateLabel" for="contract-signDate">
													<Translate contentKey="ronyApp.contract.signDate">Sign Date</Translate>
												</Label>
												<AvInput
													id="contract-signDate"
													type="datetime-local"
													className="form-control"
													name="signDate"
													placeholder={'YYYY-MM-DD HH:mm'}
													value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.contractEntity.signDate)}
												/>
											</AvGroup>
										</Col>
										<Col md="6">
											<AvGroup>
												<Label id="finishDateLabel" for="contract-finishDate">
													<Translate contentKey="ronyApp.contract.finishDate">Finish Date</Translate>
												</Label>
												<AvInput
													id="contract-finishDate"
													type="datetime-local"
													className="form-control"
													name="finishDate"
													placeholder={'YYYY-MM-DD HH:mm'}
													value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.contractEntity.finishDate)}
												/>
											</AvGroup>
										</Col>
									</Row>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<AvGroup>
										<Label for="contract-responsiblePm">
											<Translate contentKey="ronyApp.contract.responsiblePm">Responsible Pm</Translate>
										</Label>
										
										<FindSelect
											entityName={ENTITY_NAME_USER}
											onChange={(option) => handleUserOption(option)}
										/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label id="commentLabel" for="contract-comment">
											<Translate contentKey="ronyApp.contract.comment">Comment</Translate>
										</Label>
										<AvField id="contract-comment" type="text" name="comment"/>
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<AvGroup>
										<Label for="agreement-project">
											<Translate contentKey="ronyApp.agreement.project">Project</Translate>
										</Label>
										
										<SelectEntityForm
											entityName={ENTITY_NAME_PROJECT}
											onChange={(option) => setProjectId(option ? option.id : null)}
										/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label for="contract-customer">
											<Translate contentKey="ronyApp.contract.customer">Customer</Translate>
										</Label>
										
										<CreateCompanyForm onChange={(option) => setCustomerId(option ? option.id : null)} />
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<AvGroup>
										<Label for="contract-vendor">
											<Translate contentKey="ronyApp.contract.vendor">Vendor</Translate>
										</Label>
										
										<CreateCompanyForm
											onChange={(option) => setVendorId(option ? option.id : null)}
										/>
									</AvGroup>
								</Col>
								<Col md="6">
									<Row>
										<Col md="6">
											<AvGroup>
												<Label for="contract-salesType">
													<Translate contentKey="ronyApp.contract.salesType">Sales Type</Translate>
												</Label>
												
												<CreateDictForm entityName={DICT_CONTRACT_SALES_TYPE}
																onChange={(option) => option ? setSalesTypeId(option.id) : setSalesTypeId(null)}/>
											
											</AvGroup>
										</Col>
										<Col md="6">
											<AvGroup>
												<Label for="contract-statusHistory">
													<Translate contentKey="ronyApp.contract.statusHistory">Status History</Translate>
												</Label>
												
												<CreateDictForm
													entityName={DICT_CONTRACT_STATUS_HISTORY}
													onChange={(option) => option ? setStatusDictId(option.id) : setStatusDictId(null)}
												/>
											
											</AvGroup>
										</Col>
									</Row>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<AvGroup>
										<Label for="contract-mainContact">
											<Translate contentKey="ronyApp.agreement.mainContact">Main
												Contact</Translate>
										</Label>
										
										<CreateDictForm
											entityName={ENTITY_NAME_MAIN_CONTACT}
											onChange={(option) => addToMainContacts(option)}
											companyId={customerId}
										/>
										
										{mainContacts.length ? mainContacts.map((mainContact, i) => (
											<div key={`entity-${i}`} style={{'clear': 'both', 'marginTop': '10px'}}>
												{mainContact?.fullName}
												<Button style={{'float': 'right'}}
														color="danger"
														size="sm"
														type="button"
														onClick={() => removeMainContact(mainContact)}><FontAwesomeIcon icon="trash"/></Button>
											</div>
										)) : null}
										
									</AvGroup>
								</Col>
							</Row>
							
							<Row>
								<Col md="12">
									
									<Nav tabs>
										<NavItem>
											<NavLink
												className={classnames({ active: activeTab === '1' })}
												onClick={() => { toggle('1'); }}
											>
												<Translate contentKey={'ronyApp.contract.tabs.partners'}>Partners</Translate>
											</NavLink>
										</NavItem>
										<NavItem>
											<NavLink
												className={classnames({ active: activeTab === '2' })}
												onClick={() => { toggle('2'); }}
											>
												<Translate contentKey={'ronyApp.contract.tabs.resourcePlan'}>Ресурсный план</Translate>
											</NavLink>
										</NavItem>
										<NavItem>
											<NavLink
												className={classnames({ active: activeTab === '3' })}
												onClick={() => { toggle('3'); }}
											>
												<Translate contentKey={'ronyApp.contract.tabs.checkList'}>Перечень работ</Translate>
											</NavLink>
										</NavItem>
										<NavItem>
											<NavLink
												className={classnames({ active: activeTab === '4' })}
												onClick={() => { toggle('4'); }}
											>
												<Translate contentKey={'ronyApp.contract.tabs.waste'}>Прочие расходы</Translate>
											</NavLink>
										</NavItem>
									</Nav>
									<TabContent activeTab={activeTab}>
										<TabPane tabId="1">
											<Row>
												<Col md="12">
													<PartnersUpdate onChange={(partners) => setPartnerships(partners)}/>
												</Col>
											</Row>
										</TabPane>
										<TabPane tabId="2">
											<Row>
												<Col md="12">
													ресурсный план
												</Col>
											</Row>
										</TabPane>
										<TabPane tabId="3">
											<Row>
												<Col md="12">
													перечень работ
												</Col>
											</Row>
										</TabPane>
										<TabPane tabId="4">
											<Row>
												<Col md="12">
													прочие расходы
												</Col>
											</Row>
										</TabPane>
									</TabContent>
								
								</Col>
							</Row>
							
							<Row className="detail-buttons">
								<Col md="12">
									<Button tag={Link} id="cancel-save" to="/contract" replace color="info">
										<FontAwesomeIcon icon="arrow-left"/>
										&nbsp;
										<span className="d-none d-md-inline">
										  <Translate contentKey="entity.action.back">Back</Translate>
										</span>
									</Button>
									&nbsp;
									<Button color="primary" id="save-entity" type="submit" disabled={updating}>
										<FontAwesomeIcon icon="save"/>
										&nbsp;
										<Translate contentKey="entity.action.save">Save</Translate>
									</Button>
								</Col>
							</Row>
						</AvForm>
					)}
				</Col>
			</Row>
		</div>
	);
};

const mapStateToProps = (storeState: IRootState) => ({
	users: storeState.userManagement.users,
	contractStatusHistories: storeState.contractStatusHistory.entities,
	agreements: storeState.agreement.entities,
	contractEntity: storeState.contract.entity,
	loading: storeState.contract.loading,
	updating: storeState.contract.updating,
	updateSuccess: storeState.contract.updateSuccess,
	
	statusDict: storeState.dictionary.contractStatus,
	salesTypeDict: storeState.dictionary.contractSalesType
});

const mapDispatchToProps = {
	getUsers,
	getContractStatusHistories,
	getAgreements,
	getEntity,
	updateEntity,
	createEntity,
	reset,
	
	fetchContractStatusDictionary,
	getEntitiesContractSalesType,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContractUpdate);