import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contract.reducer';
import { IContract } from 'app/shared/model/contract.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContractDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContractDetail = (props: IContractDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contractEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.contract.detail.title">Contract</Translate> [<b>{contractEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">
              <Translate contentKey="ronyApp.contract.name">Name</Translate>
            </span>
          </dt>
          <dd>{contractEntity.name}</dd>
          <dt>
            <span id="number">
              <Translate contentKey="ronyApp.contract.number">Number</Translate>
            </span>
          </dt>
          <dd>{contractEntity.number}</dd>
          <dt>
            <span id="sum">
              <Translate contentKey="ronyApp.contract.sum">Sum</Translate>
            </span>
          </dt>
          <dd>{contractEntity.sum}</dd>
          <dt>
            <span id="signDate">
              <Translate contentKey="ronyApp.contract.signDate">Sign Date</Translate>
            </span>
          </dt>
          <dd>{contractEntity.signDate ? <TextFormat value={contractEntity.signDate} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="finishDate">
              <Translate contentKey="ronyApp.contract.finishDate">Finish Date</Translate>
            </span>
          </dt>
          <dd>
            {contractEntity.finishDate ? <TextFormat value={contractEntity.finishDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="comment">
              <Translate contentKey="ronyApp.contract.comment">Comment</Translate>
            </span>
          </dt>
          <dd>{contractEntity.comment}</dd>
          <dt>
            <span id="createDate">
              <Translate contentKey="ronyApp.contract.createDate">Create Date</Translate>
            </span>
          </dt>
          <dd>
            {contractEntity.createDate ? <TextFormat value={contractEntity.createDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="flagDeleted">
              <Translate contentKey="ronyApp.contract.flagDeleted">Flag Deleted</Translate>
            </span>
          </dt>
          <dd>{contractEntity.flagDeleted ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="ronyApp.contract.responsiblePm">Responsible Pm</Translate>
          </dt>
          <dd>{contractEntity.responsiblePmId ? contractEntity.responsiblePmId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.contract.salesType">Sales Type</Translate>
          </dt>
          <dd>{contractEntity.salesTypeId ? contractEntity.salesTypeId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.contract.statusHistory">Status History</Translate>
          </dt>
          <dd>{contractEntity.statusHistoryId ? contractEntity.statusHistoryId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.contract.vendor">Vendor</Translate>
          </dt>
          <dd>{contractEntity.vendorId ? contractEntity.vendorId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.contract.customer">Customer</Translate>
          </dt>
          <dd>{contractEntity.customerId ? contractEntity.customerId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.contract.currency">Currency</Translate>
          </dt>
          <dd>{contractEntity.currencyId ? contractEntity.currencyId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.contract.agreement">Agreement</Translate>
          </dt>
          <dd>{contractEntity.agreementId ? contractEntity.agreementId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contract" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contract/${contractEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contract }: IRootState) => ({
  contractEntity: contract.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContractDetail);
