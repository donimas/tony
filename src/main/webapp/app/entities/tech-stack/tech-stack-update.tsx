import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDictionary } from 'app/shared/model/dictionary.model';
import { getEntities as getDictionaries } from 'app/entities/dictionary/dictionary.reducer';
import { IProject } from 'app/shared/model/project.model';
import { getEntities as getProjects } from 'app/entities/project/project.reducer';
import { getEntity, updateEntity, createEntity, reset } from './tech-stack.reducer';
import { ITechStack } from 'app/shared/model/tech-stack.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITechStackUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TechStackUpdate = (props: ITechStackUpdateProps) => {
  const [titleId, setTitleId] = useState('0');
  const [label, setLabel] = useState('0');
  const [projectId, setProjectId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { techStackEntity, dictionaries, projects, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/tech-stack');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }

    //props.getDictionaries();
    //props.getProjects();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...techStackEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ronyApp.techStack.home.createOrEditLabel">
            <Translate contentKey="ronyApp.techStack.home.createOrEditLabel">Create or edit a TechStack</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : techStackEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="tech-stack-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="tech-stack-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label for="tech-stack-title">
                  <Translate contentKey="ronyApp.techStack.title">Title</Translate>
                </Label>
                <AvInput id="tech-stack-title" type="text" className="form-control" name="label">
                </AvInput>
                {/*<AvInput id="tech-stack-title" type="select" className="form-control" name="titleId">
                  <option value="" key="0" />
                  {dictionaries
                    ? dictionaries.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>*/}
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/tech-stack" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  dictionaries: storeState.dictionary.entities,
  projects: storeState.project.entities,
  techStackEntity: storeState.techStack.entity,
  loading: storeState.techStack.loading,
  updating: storeState.techStack.updating,
  updateSuccess: storeState.techStack.updateSuccess,
});

const mapDispatchToProps = {
  getDictionaries,
  getProjects,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TechStackUpdate);
