import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import TechStack from './tech-stack';
import TechStackDetail from './tech-stack-detail';
import TechStackUpdate from './tech-stack-update';
import TechStackDeleteDialog from './tech-stack-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={TechStackUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={TechStackUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={TechStackDetail} />
      <ErrorBoundaryRoute path={match.url} component={TechStack} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={TechStackDeleteDialog} />
  </>
);

export default Routes;
