import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction,
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ITechStack, defaultValue } from 'app/shared/model/tech-stack.model';

export const ACTION_TYPES = {
  FETCH_TECHSTACK_LIST: 'techStack/FETCH_TECHSTACK_LIST',
  FETCH_TECHSTACK: 'techStack/FETCH_TECHSTACK',
  CREATE_TECHSTACK: 'techStack/CREATE_TECHSTACK',
  UPDATE_TECHSTACK: 'techStack/UPDATE_TECHSTACK',
  DELETE_TECHSTACK: 'techStack/DELETE_TECHSTACK',
  RESET: 'techStack/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ITechStack>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type TechStackState = Readonly<typeof initialState>;

// Reducer

export default (state: TechStackState = initialState, action): TechStackState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TECHSTACK_LIST):
    case REQUEST(ACTION_TYPES.FETCH_TECHSTACK):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_TECHSTACK):
    case REQUEST(ACTION_TYPES.UPDATE_TECHSTACK):
    case REQUEST(ACTION_TYPES.DELETE_TECHSTACK):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_TECHSTACK_LIST):
    case FAILURE(ACTION_TYPES.FETCH_TECHSTACK):
    case FAILURE(ACTION_TYPES.CREATE_TECHSTACK):
    case FAILURE(ACTION_TYPES.UPDATE_TECHSTACK):
    case FAILURE(ACTION_TYPES.DELETE_TECHSTACK):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_TECHSTACK_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_TECHSTACK):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_TECHSTACK):
    case SUCCESS(ACTION_TYPES.UPDATE_TECHSTACK):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_TECHSTACK):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/tech-stacks';

// Actions

export const getEntities: ICrudGetAllAction<ITechStack> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_TECHSTACK_LIST,
    payload: axios.get<ITechStack>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<ITechStack> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_TECHSTACK,
    payload: axios.get<ITechStack>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ITechStack> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_TECHSTACK,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  return result;
};
export const createTechStack = entity => {
  return {
    type: ACTION_TYPES.CREATE_TECHSTACK,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  };
};

export const updateEntity: ICrudPutAction<ITechStack> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_TECHSTACK,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ITechStack> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_TECHSTACK,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
