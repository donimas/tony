import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './company-info.reducer';
import { ICompanyInfo } from 'app/shared/model/company-info.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICompanyInfoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CompanyInfoDetail = (props: ICompanyInfoDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { companyInfoEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.companyInfo.detail.title">CompanyInfo</Translate> [<b>{companyInfoEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">
              <Translate contentKey="ronyApp.companyInfo.name">Name</Translate>
            </span>
          </dt>
          <dd>{companyInfoEntity.name}</dd>
          <dt>
            <span id="website">
              <Translate contentKey="ronyApp.companyInfo.website">Website</Translate>
            </span>
          </dt>
          <dd>{companyInfoEntity.website}</dd>
          <dt>
            <span id="requisites">
              <Translate contentKey="ronyApp.companyInfo.requisites">Requisites</Translate>
            </span>
          </dt>
          <dd>{companyInfoEntity.requisites}</dd>
          <dt>
            <span id="flagDeleted">
              <Translate contentKey="ronyApp.companyInfo.flagDeleted">Flag Deleted</Translate>
            </span>
          </dt>
          <dd>{companyInfoEntity.flagDeleted ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="ronyApp.companyInfo.address">Address</Translate>
          </dt>
          <dd>{companyInfoEntity.addressId ? companyInfoEntity.addressId : ''}</dd>
        </dl>
        <Button tag={Link} to="/company-info" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/company-info/${companyInfoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ companyInfo }: IRootState) => ({
  companyInfoEntity: companyInfo.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CompanyInfoDetail);
