import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CompanyInfo from './company-info';
import CompanyInfoDetail from './company-info-detail';
import CompanyInfoUpdate from './company-info-update';
import CompanyInfoDeleteDialog from './company-info-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CompanyInfoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CompanyInfoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CompanyInfoDetail} />
      <ErrorBoundaryRoute path={match.url} component={CompanyInfo} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={CompanyInfoDeleteDialog} />
  </>
);

export default Routes;
