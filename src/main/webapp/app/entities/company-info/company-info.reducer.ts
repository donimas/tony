import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ICompanyInfo, defaultValue } from 'app/shared/model/company-info.model';

export const ACTION_TYPES = {
  FETCH_COMPANYINFO_LIST: 'companyInfo/FETCH_COMPANYINFO_LIST',
  FETCH_COMPANYINFO: 'companyInfo/FETCH_COMPANYINFO',
  CREATE_COMPANYINFO: 'companyInfo/CREATE_COMPANYINFO',
  UPDATE_COMPANYINFO: 'companyInfo/UPDATE_COMPANYINFO',
  DELETE_COMPANYINFO: 'companyInfo/DELETE_COMPANYINFO',
  RESET: 'companyInfo/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICompanyInfo>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type CompanyInfoState = Readonly<typeof initialState>;

// Reducer

export default (state: CompanyInfoState = initialState, action): CompanyInfoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_COMPANYINFO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_COMPANYINFO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_COMPANYINFO):
    case REQUEST(ACTION_TYPES.UPDATE_COMPANYINFO):
    case REQUEST(ACTION_TYPES.DELETE_COMPANYINFO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_COMPANYINFO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_COMPANYINFO):
    case FAILURE(ACTION_TYPES.CREATE_COMPANYINFO):
    case FAILURE(ACTION_TYPES.UPDATE_COMPANYINFO):
    case FAILURE(ACTION_TYPES.DELETE_COMPANYINFO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_COMPANYINFO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_COMPANYINFO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_COMPANYINFO):
    case SUCCESS(ACTION_TYPES.UPDATE_COMPANYINFO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_COMPANYINFO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/company-infos';

// Actions

export const getEntities: ICrudGetAllAction<ICompanyInfo> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_COMPANYINFO_LIST,
    payload: axios.get<ICompanyInfo>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<ICompanyInfo> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_COMPANYINFO,
    payload: axios.get<ICompanyInfo>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ICompanyInfo> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_COMPANYINFO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};
export const createCustomer = entity => {
  return {
    type: ACTION_TYPES.CREATE_COMPANYINFO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  };
};

export const updateEntity: ICrudPutAction<ICompanyInfo> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_COMPANYINFO,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICompanyInfo> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_COMPANYINFO,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
