import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './structure.reducer';
import { IStructure } from 'app/shared/model/structure.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IStructureDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const StructureDetail = (props: IStructureDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { structureEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.structure.detail.title">Structure</Translate> [<b>{structureEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="code">
              <Translate contentKey="ronyApp.structure.code">Code</Translate>
            </span>
          </dt>
          <dd>{structureEntity.code}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="ronyApp.structure.name">Name</Translate>
            </span>
          </dt>
          <dd>{structureEntity.name}</dd>
          <dt>
            <span id="description">
              <Translate contentKey="ronyApp.structure.description">Description</Translate>
            </span>
          </dt>
          <dd>{structureEntity.description}</dd>
          <dt>
            <span id="flagDeleted">
              <Translate contentKey="ronyApp.structure.flagDeleted">Flag Deleted</Translate>
            </span>
          </dt>
          <dd>{structureEntity.flagDeleted ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="ronyApp.structure.parent">Parent</Translate>
          </dt>
          <dd>{structureEntity.parentId ? structureEntity.parentId : ''}</dd>
        </dl>
        <Button tag={Link} to="/structure" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/structure/${structureEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ structure }: IRootState) => ({
  structureEntity: structure.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(StructureDetail);
