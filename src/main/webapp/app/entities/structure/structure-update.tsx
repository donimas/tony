import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getStructures } from 'app/entities/structure/structure.reducer';
import { getEntity, updateEntity, createEntity, reset } from './structure.reducer';
import { IStructure } from 'app/shared/model/structure.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IStructureUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const StructureUpdate = (props: IStructureUpdateProps) => {
  const [parentId, setParentId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { structureEntity, structures, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/structure');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }

    props.getStructures();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...structureEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ronyApp.structure.home.createOrEditLabel">
            <Translate contentKey="ronyApp.structure.home.createOrEditLabel">Create or edit a Structure</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : structureEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="structure-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="structure-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="codeLabel" for="structure-code">
                  <Translate contentKey="ronyApp.structure.code">Code</Translate>
                </Label>
                <AvField id="structure-code" type="text" name="code" />
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="structure-name">
                  <Translate contentKey="ronyApp.structure.name">Name</Translate>
                </Label>
                <AvField
                  id="structure-name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="structure-description">
                  <Translate contentKey="ronyApp.structure.description">Description</Translate>
                </Label>
                <AvField id="structure-description" type="text" name="description" />
              </AvGroup>
              <AvGroup check>
                <Label id="flagDeletedLabel">
                  <AvInput id="structure-flagDeleted" type="checkbox" className="form-check-input" name="flagDeleted" />
                  <Translate contentKey="ronyApp.structure.flagDeleted">Flag Deleted</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label for="structure-parent">
                  <Translate contentKey="ronyApp.structure.parent">Parent</Translate>
                </Label>
                <AvInput id="structure-parent" type="select" className="form-control" name="parentId">
                  <option value="" key="0" />
                  {structures
                    ? structures.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/structure" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  structures: storeState.structure.entities,
  structureEntity: storeState.structure.entity,
  loading: storeState.structure.loading,
  updating: storeState.structure.updating,
  updateSuccess: storeState.structure.updateSuccess,
});

const mapDispatchToProps = {
  getStructures,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(StructureUpdate);
