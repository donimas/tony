import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import MainContact from './main-contact';
import MainContactDetail from './main-contact-detail';
import MainContactUpdate from './main-contact-update';
import MainContactDeleteDialog from './main-contact-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={MainContactUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={MainContactUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={MainContactDetail} />
      <ErrorBoundaryRoute path={match.url} component={MainContact} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={MainContactDeleteDialog} />
  </>
);

export default Routes;
