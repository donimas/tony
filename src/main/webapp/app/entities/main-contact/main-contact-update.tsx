import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Col, Label} from 'reactstrap';
import {AvFeedback, AvForm, AvGroup, AvInput, AvField} from 'availity-reactstrap-validation';
import {Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';

import {ICompanyInfo} from 'app/shared/model/company-info.model';
import {getEntities as getCompanyInfos} from 'app/entities/company-info/company-info.reducer';
import {IAgreement} from 'app/shared/model/agreement.model';
import {getEntities as getAgreements} from 'app/entities/agreement/agreement.reducer';
import {getEntity, updateEntity, createEntity, reset} from './main-contact.reducer';
import {IMainContact} from 'app/shared/model/main-contact.model';
import {convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime} from 'app/shared/util/date-utils';
import {mapIdList} from 'app/shared/util/entity-utils';

import CreateCompanyForm from "app/shared/form/create-company";

export interface IMainContactUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const MainContactUpdate = (props: IMainContactUpdateProps) => {
	const [companyId, setCompanyId] = useState(undefined);
	const [agreementId, setAgreementId] = useState(undefined);
	const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);
	
	const {mainContactEntity, companyInfos, agreements, loading, updating} = props;
	
	const handleClose = () => {
		props.history.push('/main-contact' + props.location.search);
	};
	
	useEffect(() => {
		if (isNew) {
			props.reset();
		} else {
			props.getEntity(props.match.params.id);
		}
		
		props.getCompanyInfos();
		props.getAgreements();
	}, []);
	
	useEffect(() => {
		if (props.updateSuccess) {
			handleClose();
		}
	}, [props.updateSuccess]);
	
	const saveEntity = (event, errors, values) => {
		values.dob = convertDateTimeToServer(values.dob);
		
		if (errors.length === 0) {
			const entity = {
				...mainContactEntity,
				...values,
				companyId
			};
			
			if (isNew) {
				props.createEntity(entity);
			} else {
				props.updateEntity(entity);
			}
		}
	};
	
	return (
		<div>
			<Row className="justify-content-center">
				<Col md="8">
					<h2 id="ronyApp.mainContact.home.createOrEditLabel">
						<Translate contentKey="ronyApp.mainContact.home.createOrEditLabel">Create or edit a
							MainContact</Translate>
					</h2>
				</Col>
			</Row>
			<Row className="justify-content-center">
				<Col md="8">
					{loading ? (
						<p>Loading...</p>
					) : (
						<AvForm model={isNew ? {} : mainContactEntity} onSubmit={saveEntity}>
							{!isNew ? (
								<AvGroup>
									<Label for="main-contact-id">
										<Translate contentKey="global.field.id">ID</Translate>
									</Label>
									<AvInput id="main-contact-id" type="text" className="form-control" name="id"
											 required readOnly/>
								</AvGroup>
							) : null}
							
							<Row>
								<Col md="6">
									<AvGroup>
										<Label id="fullNameLabel" for="main-contact-fullName">
											<Translate contentKey="ronyApp.mainContact.fullName">Full Name</Translate>
										</Label>
										<AvField
											id="main-contact-fullName"
											type="text"
											name="fullName"
											validate={{
												required: {
													value: true,
													errorMessage: translate('entity.validation.required')
												},
											}}
										/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label id="positionLabel" for="main-contact-position">
											<Translate contentKey="ronyApp.mainContact.position">Position</Translate>
										</Label>
										<AvField id="main-contact-position" type="text" name="position"/>
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<Row>
										<Col md="6">
											<AvGroup>
												<Label id="telephoneLabel" for="main-contact-telephone">
													<Translate
														contentKey="ronyApp.mainContact.telephone">Telephone</Translate>
												</Label>
												<AvField id="main-contact-telephone" type="text" name="telephone"/>
											</AvGroup>
										</Col>
										<Col md="6">
											<AvGroup>
												<Label id="emailLabel" for="main-contact-email">
													<Translate contentKey="ronyApp.mainContact.email">Email</Translate>
												</Label>
												<AvField id="main-contact-email" type="text" name="email"/>
											</AvGroup>
										</Col>
									</Row>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label id="dobLabel" for="main-contact-dob">
											<Translate contentKey="ronyApp.mainContact.dob">Dob</Translate>
										</Label>
										<AvInput
											id="main-contact-dob"
											type="datetime-local"
											className="form-control"
											name="dob"
											placeholder={'YYYY-MM-DD HH:mm'}
											value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.mainContactEntity.dob)}
										/>
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<AvGroup>
										<Label for="main-contact-company">
											<Translate contentKey="ronyApp.mainContact.company">Company</Translate>
										</Label>
										<CreateCompanyForm onChange={(option) => setCompanyId(option ? option.id : null)}/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label id="commentLabel" for="main-contact-comment">
											<Translate contentKey="ronyApp.mainContact.comment">Comment</Translate>
										</Label>
										<AvField id="main-contact-comment" type="text" name="comment"/>
									</AvGroup>
								</Col>
							</Row>
							<Row className="detail-buttons">
								<Col md="12">
									<Button tag={Link} id="cancel-save" to="/main-contact" replace color="info">
										<FontAwesomeIcon icon="arrow-left"/>
										&nbsp;
										<span className="d-none d-md-inline">
                                            <Translate contentKey="entity.action.back">Back</Translate>
                                        </span>
									</Button>
									&nbsp;
									<Button color="primary" id="save-entity" type="submit" disabled={updating}>
										<FontAwesomeIcon icon="save"/>
										&nbsp;
										<Translate contentKey="entity.action.save">Save</Translate>
									</Button>
								</Col>
							</Row>
						</AvForm>
					)}
				</Col>
			</Row>
		</div>
	);
};

const mapStateToProps = (storeState: IRootState) => ({
	companyInfos: storeState.companyInfo.entities,
	agreements: storeState.agreement.entities,
	mainContactEntity: storeState.mainContact.entity,
	loading: storeState.mainContact.loading,
	updating: storeState.mainContact.updating,
	updateSuccess: storeState.mainContact.updateSuccess,
});

const mapDispatchToProps = {
	getCompanyInfos,
	getAgreements,
	getEntity,
	updateEntity,
	createEntity,
	reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MainContactUpdate);
