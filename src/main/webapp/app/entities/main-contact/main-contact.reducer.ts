import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IMainContact, defaultValue } from 'app/shared/model/main-contact.model';

export const ACTION_TYPES = {
  FETCH_MAINCONTACT_LIST: 'mainContact/FETCH_MAINCONTACT_LIST',
  FETCH_MAINCONTACT_LIST_PLAIN: 'mainContact/FETCH_MAINCONTACT_LIST_PLAIN',
  FETCH_MAINCONTACT: 'mainContact/FETCH_MAINCONTACT',
  CREATE_MAINCONTACT: 'mainContact/CREATE_MAINCONTACT',
  CREATE_MAINCONTACT_FOR_PAYLOAD: 'mainContact/CREATE_MAINCONTACT_FOR_PAYLOAD',
  UPDATE_MAINCONTACT: 'mainContact/UPDATE_MAINCONTACT',
  DELETE_MAINCONTACT: 'mainContact/DELETE_MAINCONTACT',
  RESET: 'mainContact/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IMainContact>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type MainContactState = Readonly<typeof initialState>;

// Reducer

export default (state: MainContactState = initialState, action): MainContactState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_MAINCONTACT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_MAINCONTACT_LIST_PLAIN):
    case REQUEST(ACTION_TYPES.FETCH_MAINCONTACT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_MAINCONTACT):
    case REQUEST(ACTION_TYPES.CREATE_MAINCONTACT_FOR_PAYLOAD):
    case REQUEST(ACTION_TYPES.UPDATE_MAINCONTACT):
    case REQUEST(ACTION_TYPES.DELETE_MAINCONTACT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_MAINCONTACT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_MAINCONTACT_LIST_PLAIN):
    case FAILURE(ACTION_TYPES.FETCH_MAINCONTACT):
    case FAILURE(ACTION_TYPES.CREATE_MAINCONTACT):
    case FAILURE(ACTION_TYPES.CREATE_MAINCONTACT_FOR_PAYLOAD):
    case FAILURE(ACTION_TYPES.UPDATE_MAINCONTACT):
    case FAILURE(ACTION_TYPES.DELETE_MAINCONTACT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_MAINCONTACT_LIST):
    case SUCCESS(ACTION_TYPES.FETCH_MAINCONTACT_LIST_PLAIN):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_MAINCONTACT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_MAINCONTACT):
    case SUCCESS(ACTION_TYPES.CREATE_MAINCONTACT_FOR_PAYLOAD):
    case SUCCESS(ACTION_TYPES.UPDATE_MAINCONTACT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_MAINCONTACT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/main-contacts';

// Actions

export const getEntities: ICrudGetAllAction<IMainContact> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_MAINCONTACT_LIST,
    payload: axios.get<IMainContact>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};
export const fetchEntities = () => {
  // TODO: в справочник тянет максимум 20 строк. Надо логику добавить чтобы весь каталог подтянул либо поиск через апи
  const requestUrl = `${apiUrl}`;
  return {
    type: ACTION_TYPES.FETCH_MAINCONTACT_LIST_PLAIN,
    payload: axios.get<any>(`${requestUrl}?${null}&cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IMainContact> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_MAINCONTACT,
    payload: axios.get<IMainContact>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IMainContact> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_MAINCONTACT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const createMainContact = entity => {
  return {
    type: ACTION_TYPES.CREATE_MAINCONTACT_FOR_PAYLOAD,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  };
};

export const updateEntity: ICrudPutAction<IMainContact> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_MAINCONTACT,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IMainContact> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_MAINCONTACT,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
