import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Col} from 'reactstrap';
import {Translate, ICrudGetAction, TextFormat} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntity} from './main-contact.reducer';
import {IMainContact} from 'app/shared/model/main-contact.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT} from 'app/config/constants';

export interface IMainContactDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const MainContactDetail = (props: IMainContactDetailProps) => {
	useEffect(() => {
		props.getEntity(props.match.params.id);
	}, []);
	
	const {mainContactEntity} = props;
	return (
		<Row className="justify-content-center">
			<Col md="8">
				<h2>
					<b>{mainContactEntity.fullName}</b>
				</h2>
				
				<Row>
					<Col md="3">
						<span id="fullName">
							<b><Translate contentKey="ronyApp.mainContact.fullName">Full Name</Translate></b>
						</span>
					</Col>
					<Col md="9">
						{mainContactEntity.fullName}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.mainContact.telephone">Telephone</Translate></b>
					</Col>
					<Col md="9">
						{mainContactEntity.telephone}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.mainContact.email">Email</Translate></b>
					</Col>
					<Col md="9">
						{mainContactEntity.email}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.mainContact.dob">Dob</Translate></b>
					</Col>
					<Col md="9">
						{mainContactEntity.dob ?
							<TextFormat value={mainContactEntity.dob} type="date" format={APP_DATE_FORMAT}/> : null}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.mainContact.position">Position</Translate></b>
					</Col>
					<Col md="9">
						{mainContactEntity.position}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.mainContact.comment">Comment</Translate></b>
					</Col>
					<Col md="9">
						{mainContactEntity.comment}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.mainContact.company">Company</Translate></b>
					</Col>
					<Col md="9">
						{mainContactEntity.companyId ? mainContactEntity.companyLabel : ''}
					</Col>
				</Row>
				
				<Row className="detail-buttons">
					<Col md="12">
						<Button tag={Link} to="/main-contact" replace color="info">
							<FontAwesomeIcon icon="arrow-left"/>{' '}
							<span className="d-none d-md-inline">
								<Translate contentKey="entity.action.back">Back</Translate>
							</span>
						</Button>
						&nbsp;
						<Button tag={Link} to={`/main-contact/${mainContactEntity.id}/edit`} replace color="primary">
							<FontAwesomeIcon icon="pencil-alt"/>{' '}
							<span className="d-none d-md-inline">
								<Translate contentKey="entity.action.edit">Edit</Translate>
							</span>
						</Button>
					</Col>
				</Row>
			</Col>
		</Row>
	);
};

const mapStateToProps = ({mainContact}: IRootState) => ({
	mainContactEntity: mainContact.entity,
});

const mapDispatchToProps = {getEntity};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MainContactDetail);
