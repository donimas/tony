import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Project from './project';
import Structure from './structure';
import CompanyInfo from './company-info';
import MainContact from './main-contact';
import Address from './address';
import ProjectTeam from './project-team';
import TeamMember from './team-member';
import Contract from './contract';
import Transaction from './transaction';
import TechStack from './tech-stack';
import Dictionary from './dictionary';
import Partnership from './partnership';
import Currency from './currency';
import Agreement from './agreement';
import AgreementStatusHistory from './agreement-status-history';
import ProjectStatusHistory from './project-status-history';
import ContractStatusHistory from './contract-status-history';
import Attachment from './attachment';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}project`} component={Project} />
      <ErrorBoundaryRoute path={`${match.url}structure`} component={Structure} />
      <ErrorBoundaryRoute path={`${match.url}company-info`} component={CompanyInfo} />
      <ErrorBoundaryRoute path={`${match.url}main-contact`} component={MainContact} />
      <ErrorBoundaryRoute path={`${match.url}address`} component={Address} />
      <ErrorBoundaryRoute path={`${match.url}project-team`} component={ProjectTeam} />
      <ErrorBoundaryRoute path={`${match.url}team-member`} component={TeamMember} />
      <ErrorBoundaryRoute path={`${match.url}contract`} component={Contract} />
      <ErrorBoundaryRoute path={`${match.url}transaction`} component={Transaction} />
      <ErrorBoundaryRoute path={`${match.url}tech-stack`} component={TechStack} />
      <ErrorBoundaryRoute path={`${match.url}dictionary`} component={Dictionary} />
      <ErrorBoundaryRoute path={`${match.url}partnership`} component={Partnership} />
      <ErrorBoundaryRoute path={`${match.url}currency`} component={Currency} />
      <ErrorBoundaryRoute path={`${match.url}agreement`} component={Agreement} />
      <ErrorBoundaryRoute path={`${match.url}agreement-status-history`} component={AgreementStatusHistory} />
      <ErrorBoundaryRoute path={`${match.url}project-status-history`} component={ProjectStatusHistory} />
      <ErrorBoundaryRoute path={`${match.url}contract-status-history`} component={ContractStatusHistory} />
      <ErrorBoundaryRoute path={`${match.url}attachment`} component={Attachment} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
