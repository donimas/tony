import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Col} from 'reactstrap';
import {Translate, ICrudGetAction, TextFormat} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntity} from './agreement.reducer';
import {IAgreement} from 'app/shared/model/agreement.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT} from 'app/config/constants';

export interface IAgreementDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const AgreementDetail = (props: IAgreementDetailProps) => {
	useEffect(() => {
		props.getEntity(props.match.params.id);
	}, []);
	
	const {agreementEntity} = props;
	return (
		<Row className="justify-content-center">
			<Col md="8">
				<h2>
					<b>{agreementEntity.customerLabel}</b>
				</h2>
				<Row>
					<Col md="3">
						<span id="createDate">
							<b><Translate contentKey="ronyApp.agreement.createDate">Create Date</Translate></b>
						</span>
					</Col>
					<Col md="9">
						{agreementEntity.createDate ? <TextFormat value={agreementEntity.createDate} type="date"
																  format={APP_DATE_FORMAT}/> : null}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<span id="comment">
						  	<b><Translate contentKey="ronyApp.agreement.comment">Comment</Translate></b>
						</span>
					</Col>
					<Col md="9">
						{agreementEntity.comment}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<span id="progressBar">
						  	<b><Translate contentKey="ronyApp.agreement.progressBar">Progress Bar</Translate></b>
						</span>
					</Col>
					<Col md="9">
						{agreementEntity.progressBar}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<span id="startDate">
						  	<b><Translate contentKey="ronyApp.agreement.startDate">Start Date</Translate></b>
						</span>
					</Col>
					<Col md="9">
						{agreementEntity.startDate ?
							<TextFormat value={agreementEntity.startDate} type="date" format={APP_DATE_FORMAT}/> : null}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<span id="finishDate">
						  	<b><Translate contentKey="ronyApp.agreement.finishDate">Finish Date</Translate></b>
						</span>
					</Col>
					<Col md="9">
						{agreementEntity.finishDate ? <TextFormat value={agreementEntity.finishDate} type="date"
																  format={APP_DATE_FORMAT}/> : null}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<span id="flagDeleted">
						  	<b><Translate contentKey="ronyApp.agreement.flagDeleted">Flag Deleted</Translate></b>
						</span>
					</Col>
					<Col md="9">
						{agreementEntity.flagDeleted ? 'true' : 'false'}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.agreement.statusHistory">Status History</Translate></b>
					</Col>
					<Col md="9">
						{agreementEntity.statusHistoryId ? agreementEntity.statusDictLabel : ''}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.agreement.customer">Customer</Translate></b>
					</Col>
					<Col md="9">
						{agreementEntity.customerId ? agreementEntity.customerLabel : ''}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.agreement.mainContact">Main Contact</Translate></b>
					</Col>
					<Col md="9">
						{agreementEntity.mainContacts
							? agreementEntity.mainContacts.map((val, i) => (
								<span key={val.id}>
									<a>{val.fullName}</a>
									{agreementEntity.mainContacts && i === agreementEntity.mainContacts.length - 1 ? '' : ', '}
							  	</span>
							))
							: null}
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<b><Translate contentKey="ronyApp.agreement.project">Project</Translate></b>
					</Col>
					<Col md="9">
						{agreementEntity.projectId ? agreementEntity.projectLabel : ''}
					</Col>
				</Row>
				<Row className="detail-buttons">
					<Col md="12">
						<Button tag={Link} to="/agreement" replace color="info">
							<FontAwesomeIcon icon="arrow-left"/>{' '}
							<span className="d-none d-md-inline">
								<b><Translate contentKey="entity.action.back">Back</Translate></b>
							</span>
						</Button>
						&nbsp;
						<Button tag={Link} to={`/agreement/${agreementEntity.id}/edit`} replace color="primary">
							<FontAwesomeIcon icon="pencil-alt"/>{' '}
							<span className="d-none d-md-inline">
								<b><Translate contentKey="entity.action.edit">Edit</Translate></b>
							</span>
						</Button>
					</Col>
				</Row>
			</Col>
		</Row>
	);
};

const mapStateToProps = ({agreement}: IRootState) => ({
	agreementEntity: agreement.entity,
});

const mapDispatchToProps = {getEntity};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AgreementDetail);
