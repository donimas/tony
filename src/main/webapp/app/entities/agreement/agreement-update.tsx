import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Col, Label, Table} from 'reactstrap';
import {AvForm, AvGroup, AvInput, AvField} from 'availity-reactstrap-validation';
import {Translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';

import {getEntities as getAgreementStatusHistories} from 'app/entities/agreement-status-history/agreement-status-history.reducer';
import {getEntities as getCompanyInfos, createCustomer} from 'app/entities/company-info/company-info.reducer';
import {fetchEntities as getMainContacts} from 'app/entities/main-contact/main-contact.reducer';
import {getEntity, updateEntity, createEntity, reset} from './agreement.reducer';
import {convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime} from 'app/shared/util/date-utils';
import {mapIdList} from 'app/shared/util/entity-utils';

import {getEntitiesAgreement as getStatusDicts} from 'app/entities/dictionary/dictionary.reducer'
import {createMainContact, updateEntity as updateMainContact} from 'app/entities/main-contact/main-contact.reducer'
import CreateCompanyForm from "app/shared/form/create-company";
import SelectEntityForm from "app/shared/form/select-entity";
import {DICT_AGREEMENT_STATUS_HISTORY, ENTITY_NAME_MAIN_CONTACT, ENTITY_NAME_PROJECT} from "app/shared/util/constants";
import CreateDictForm from "app/shared/form/create-dict";

export interface IAgreementUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const AgreementUpdate = (props: IAgreementUpdateProps) => {
	const [statusHistoryId, setStatusHistoryId] = useState(undefined);
	const [customerId, setCustomerId] = useState(undefined);
	const [projectId, setProjectId] = useState(undefined);
	const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);
	const [addedContactList, setAddedContactList] = useState([]);
	
	const [isMainContactFormEnabled, setIsMainContactFormEnabled] = useState(false);
	const [isMainContactUpdating, setIsMainContactUpdating] = useState(false);
	
	const {agreementEntity, loading, updating} = props;
	
	const initialMainContactState = {
		id: null,
		fullName: "",
		telephone: "",
		email: "",
		position: "",
	};
	
	const [mainContact, setMainContact] = useState(initialMainContactState);
	const [telephone, setTelephone] = useState("");
	const [email, setEmail] = useState("");
	const [position, setPosition] = useState("");
	
	const handleClose = () => {
		props.history.push('/agreement' + props.location.search);
	};
	
	useEffect(() => {
		if (isNew) {
			props.reset();
		} else {
			props.getEntity(props.match.params.id);
		}
	}, []);
	
	useEffect(() => {
		if (props.updateSuccess) {
			handleClose();
		}
	}, [props.updateSuccess]);
	
	const saveEntity = (event, errors, values) => {
		values.createDate = convertDateTimeToServer(values.createDate);
		values.startDate = convertDateTimeToServer(values.startDate);
		values.finishDate = convertDateTimeToServer(values.finishDate);
		
		if (errors.length === 0) {
			const entity = {
				...agreementEntity,
				...values,
				mainContacts: mapIdList(addedContactList && addedContactList.map(e => e.id)),
				projectId,
				customerId,
				statusDictId: statusHistoryId
			};
			
			if (isNew) {
				props.createEntity(entity);
			} else {
				props.updateEntity(entity);
			}
		}
	};
	
	const resetMainContactInfo = () => {
		setTelephone("");
		setEmail("");
		setPosition("");
		setMainContact(undefined);
	};
	
	const initMainContact = (entity) => {
		setMainContact({...entity});
		setTelephone(entity.telephone);
		setEmail(entity.email);
		setPosition(entity.position);
	};
	
	const handleOnMainContactChange = (option) => {
		setIsMainContactFormEnabled(!!option);
		if(option) {
			initMainContact(option);
		} else {
			resetMainContactInfo();
		}
	};
	
	const updateContact = () => {
		setIsMainContactUpdating(true);
		props.updateMainContact({...mainContact, telephone, position, email});
	};
	
	useEffect(() => {
		if(props.updatedMainContact && props.updatedMainContact.id) {
			const updatedContact =  props.updatedMainContact;
			const tempList = addedContactList.filter((c) => c.id !== updatedContact.id);
			setAddedContactList([...tempList, updatedContact]);
			setIsMainContactUpdating(false);
			setIsMainContactFormEnabled(false);
			
			resetMainContactInfo();
		}
	}, [props.updatedMainContact]);
	
	const updateAddedContact = (contact: any) => {
		initMainContact(contact);
		setIsMainContactFormEnabled(true);
	};
	
	const onContactPhoneChange = (event) => {
		setTelephone(event.target.value);
	};
	const onContactEmailChange = (event) => {
		setEmail(event.target.value);
	};
	const onContactPositionChange = (event) => {
		setPosition(event.target.value);
	};
	
	return (
		<div>
			<Row className="justify-content-center">
				<Col md="8">
					<h2 id="ronyApp.agreement.home.createOrEditLabel">
						<Translate contentKey="ronyApp.agreement.home.createOrEditLabel">Create or edit a
							Agreement</Translate>
					</h2>
				</Col>
			</Row>
			<Row className="justify-content-center">
				<Col md="8">
					{loading ? (
						<p>Loading...</p>
					) : (
						<AvForm model={isNew ? {} : agreementEntity} onSubmit={saveEntity}>
							{!isNew ? (
								<AvGroup>
									<Label for="agreement-id">
										<Translate contentKey="global.field.id">ID</Translate>
									</Label>
									<AvInput id="agreement-id" type="text" className="form-control" name="id" required
											 readOnly/>
								</AvGroup>
							) : null}
							<Row>
								<Col md="6">
									<AvGroup>
										<Label for="agreement-project">
											<Translate contentKey="ronyApp.agreement.project">Project</Translate>
										</Label>
										
										<SelectEntityForm
											entityName={ENTITY_NAME_PROJECT}
											onChange={(option) => setProjectId(option ? option.id : null)}
										/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label for="agreement-customer">
											<Translate contentKey="ronyApp.agreement.customer">Customer</Translate>
										</Label>
										
										<CreateCompanyForm onChange={(option) => setCustomerId(option ? option.id : null)} />
										
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<AvGroup>
										<Label id="commentLabel" for="agreement-comment">
											<Translate contentKey="ronyApp.agreement.comment">Comment</Translate>
										</Label>
										<AvField id="agreement-comment" type="text" name="comment"/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label id="progressBarLabel" for="agreement-progressBar">
											<Translate contentKey="ronyApp.agreement.progressBar">Progress
												Bar</Translate>
										</Label>
										<AvField id="agreement-progressBar" type="text" name="progressBar"/>
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<Row>
										<Col md="6">
											<AvGroup>
												<Label id="startDateLabel" for="agreement-startDate">
													<Translate contentKey="ronyApp.agreement.startDate">Start
														Date</Translate>
												</Label>
												<AvInput
													id="agreement-startDate"
													type="datetime-local"
													className="form-control"
													name="startDate"
													placeholder={'YYYY-MM-DD HH:mm'}
													value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.agreementEntity.startDate)}
												/>
											</AvGroup>
										</Col>
										<Col md="6">
											<AvGroup>
												<Label id="finishDateLabel" for="agreement-finishDate">
													<Translate contentKey="ronyApp.agreement.finishDate">Finish
														Date</Translate>
												</Label>
												<AvInput
													id="agreement-finishDate"
													type="datetime-local"
													className="form-control"
													name="finishDate"
													placeholder={'YYYY-MM-DD HH:mm'}
													value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.agreementEntity.finishDate)}
												/>
											</AvGroup>
										</Col>
									</Row>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label for="agreement-statusHistory">
											<Translate contentKey="ronyApp.agreement.statusHistory">Status
												History</Translate>
										</Label>
										
										<CreateDictForm
											entityName={DICT_AGREEMENT_STATUS_HISTORY}
											onChange={(option) => setStatusHistoryId(option ? option.id : undefined)}
										/>
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<AvGroup>
										<Label for="agreement-mainContact">
											<Translate contentKey="ronyApp.agreement.mainContact">Main
												Contact</Translate>
										</Label>
									</AvGroup>
								</Col>
								<Col md="6">
								</Col>
							</Row>
							<Row>
								<Col md="12">
									<Table responsive>
										<thead>
										<tr>
											<th style={{width: '30%'}}>
												<Translate contentKey="ronyApp.mainContact.fullName">Full
													Name</Translate>
											</th>
											<th style={{width: '15%'}}>
												<Translate
													contentKey="ronyApp.mainContact.telephone">Telephone</Translate>
											</th>
											<th style={{width: '15%'}}>
												<Translate contentKey="ronyApp.mainContact.email">Email</Translate>
											</th>
											<th style={{width: '30%'}}>
												<Translate
													contentKey="ronyApp.mainContact.position">Position</Translate>
											</th>
											<th style={{width: '10%'}}/>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td>
												
												<CreateDictForm
													entityName={ENTITY_NAME_MAIN_CONTACT}
													companyId={customerId}
													onChange={(option) => handleOnMainContactChange(option)}
												/>
												
											</td>
											<td>
												<AvField id="main-contact-telephone" type="text" name="telephone" value={telephone || ""} onChange={onContactPhoneChange} disabled={!isMainContactFormEnabled}/>
											</td>
											<td>
												<AvField id="main-contact-email" type="text" name="email" value={email || ""} onChange={onContactEmailChange} disabled={!isMainContactFormEnabled}/>
											</td>
											<td>
												<AvField id="main-contact-position" type="text" name="position" value={position || ""} onChange={onContactPositionChange} disabled={!isMainContactFormEnabled}/>
											</td>
											<td>
												<Button color="primary" disabled={!isMainContactFormEnabled || isMainContactUpdating} className="float-right" onClick={updateContact}>
													{!isMainContactUpdating ? <FontAwesomeIcon icon="plus"/> : <FontAwesomeIcon icon="plus" spin={true} />}
												</Button>
											</td>
										</tr>
										{addedContactList.map((contact, i) => (
											<tr key={`entity-${i}`}>
												<td>{contact.fullName}</td>
												<td>{contact.telephone}</td>
												<td>{contact.email}</td>
												<td>{contact.position}</td>
												<td>
													<Button style={{'float': 'right'}} color="danger" size="sm" type="button" onClick={() => {setAddedContactList(prevData  => [...prevData.filter((e) => e.id !== contact.id)])}}><FontAwesomeIcon icon="trash"/></Button>
													<Button style={{'float': 'right', 'marginRight': '5px'}} color="warning" size="sm" type="button" onClick={() => updateAddedContact(contact)}><FontAwesomeIcon icon="pencil-alt"/></Button>
												</td>
											</tr>
										))}
										</tbody>
									</Table>
								</Col>
							</Row>
							
							<Row className="float-right">
								<Button tag={Link} id="cancel-save" to="/agreement" replace color="info">
									<FontAwesomeIcon icon="arrow-left"/>
									&nbsp;
									<span className="d-none d-md-inline">
										<Translate contentKey="entity.action.back">Back</Translate>
									</span>
								</Button>
								&nbsp;
								<Button color="primary" id="save-entity" type="submit" disabled={updating}>
									<FontAwesomeIcon icon="save"/>
									&nbsp;
									<Translate contentKey="entity.action.save">Save</Translate>
								</Button>
							</Row>
						</AvForm>
					)}
				</Col>
			</Row>
		</div>
	);
};

const mapStateToProps = (storeState: IRootState) => ({
	agreementEntity: storeState.agreement.entity,
	loading: storeState.agreement.loading,
	updating: storeState.agreement.updating,
	updateSuccess: storeState.agreement.updateSuccess,
	
	updatedMainContact: storeState.mainContact.entity
});

const mapDispatchToProps = {
	getAgreementStatusHistories,
	getEntity,
	updateEntity,
	createEntity,
	reset,
	updateMainContact
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AgreementUpdate);
