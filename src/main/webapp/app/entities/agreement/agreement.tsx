import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Col, Row, Table} from 'reactstrap';
import {
	Translate,
	ICrudGetAllAction,
	TextFormat,
	getSortState,
	IPaginationBaseState,
	JhiPagination,
	JhiItemCount
} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntities} from './agreement.reducer';
import {IAgreement} from 'app/shared/model/agreement.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT} from 'app/config/constants';
import {ITEMS_PER_PAGE} from 'app/shared/util/pagination.constants';
import {overridePaginationStateWithQueryParams} from 'app/shared/util/entity-utils';

export interface IAgreementProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {
}

export const Agreement = (props: IAgreementProps) => {
	const [paginationState, setPaginationState] = useState(
		overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE), props.location.search)
	);
	
	const getAllEntities = () => {
		props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
	};
	
	const sortEntities = () => {
		getAllEntities();
		const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`;
		if (props.location.search !== endURL) {
			props.history.push(`${props.location.pathname}${endURL}`);
		}
	};
	
	useEffect(() => {
		sortEntities();
	}, [paginationState.activePage, paginationState.order, paginationState.sort]);
	
	useEffect(() => {
		const params = new URLSearchParams(props.location.search);
		const page = params.get('page');
		const sort = params.get('sort');
		if (page && sort) {
			const sortSplit = sort.split(',');
			setPaginationState({
				...paginationState,
				activePage: +page,
				sort: sortSplit[0],
				order: sortSplit[1],
			});
		}
	}, [props.location.search]);
	
	const sort = p => () => {
		setPaginationState({
			...paginationState,
			order: paginationState.order === 'asc' ? 'desc' : 'asc',
			sort: p,
		});
	};
	
	const handlePagination = currentPage =>
		setPaginationState({
			...paginationState,
			activePage: currentPage,
		});
	
	const {agreementList, match, loading, totalItems} = props;
	return (
		<div>
			<h2 id="agreement-heading">
				<Translate contentKey="ronyApp.agreement.home.title">Agreements</Translate>
				<Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity"
					  id="jh-create-entity">
					<FontAwesomeIcon icon="plus"/>
					&nbsp;
					<Translate contentKey="ronyApp.agreement.home.createLabel">Create new Agreement</Translate>
				</Link>
			</h2>
			<div className="table-responsive">
				{agreementList && agreementList.length > 0 ? (
					<Table responsive>
						<thead>
						<tr>
							<th className="hand" onClick={sort('id')}>
								<Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort"/>
							</th>
							<th className="hand" onClick={sort('createDate')}>
								<Translate contentKey="ronyApp.agreement.createDate">Create Date</Translate>
								<FontAwesomeIcon icon="sort"/>
							</th>
							<th className="hand" onClick={sort('comment')}>
								<Translate contentKey="ronyApp.agreement.comment">Comment</Translate> <FontAwesomeIcon
								icon="sort"/>
							</th>
							<th className="hand" onClick={sort('progressBar')}>
								<Translate contentKey="ronyApp.agreement.progressBar">Progress Bar</Translate>
								<FontAwesomeIcon icon="sort"/>
							</th>
							<th className="hand" onClick={sort('startDate')}>
								<Translate contentKey="ronyApp.agreement.startDate">Start Date</Translate>
								<FontAwesomeIcon icon="sort"/>
							</th>
							<th className="hand" onClick={sort('finishDate')}>
								<Translate contentKey="ronyApp.agreement.finishDate">Finish Date</Translate>
								<FontAwesomeIcon icon="sort"/>
							</th>
							<th className="hand" onClick={sort('flagDeleted')}>
								<Translate contentKey="ronyApp.agreement.flagDeleted">Flag Deleted</Translate>
								<FontAwesomeIcon icon="sort"/>
							</th>
							<th>
								<Translate contentKey="ronyApp.agreement.statusHistory">Status History</Translate>
								<FontAwesomeIcon icon="sort"/>
							</th>
							<th>
								<Translate contentKey="ronyApp.agreement.customer">Customer</Translate> <FontAwesomeIcon
								icon="sort"/>
							</th>
							<th>
								<Translate contentKey="ronyApp.agreement.project">Project</Translate> <FontAwesomeIcon
								icon="sort"/>
							</th>
							<th>
								<Translate contentKey="ronyApp.mainContact.home.title">Main Contact</Translate>
								<FontAwesomeIcon
									icon="sort"/>
							</th>
							<th/>
						</tr>
						</thead>
						<tbody>
						{agreementList.map((agreement, i) => (
							<tr key={`entity-${i}`}>
								<td>
									<Button tag={Link} to={`${match.url}/${agreement.id}`} color="link" size="sm">
										{agreement.id}
									</Button>
								</td>
								<td>{agreement.createDate ? <TextFormat type="date" value={agreement.createDate}
																		format={APP_DATE_FORMAT}/> : null}</td>
								<td>{agreement.comment}</td>
								<td>{agreement.progressBar}</td>
								<td>{agreement.startDate ? <TextFormat type="date" value={agreement.startDate}
																	   format={APP_DATE_FORMAT}/> : null}</td>
								<td>{agreement.finishDate ? <TextFormat type="date" value={agreement.finishDate}
																		format={APP_DATE_FORMAT}/> : null}</td>
								<td>{agreement.flagDeleted ? 'true' : 'false'}</td>
								<td>
									{agreement.statusHistoryId ? (
										<Link
											to={`agreement-status-history/${agreement.statusHistoryId}`}>{agreement.statusDictLabel}</Link>
									) : (
										''
									)}
								</td>
								<td>{agreement.customerId ? <Link
									to={`company-info/${agreement.customerId}`}>{agreement.customerLabel}</Link> : ''}</td>
								<td>{agreement.projectId ? <Link
									to={`project/${agreement.projectId}`}>{agreement.projectLabel}</Link> : ''}</td>
								<td>
									{agreement.mainContacts
										? agreement.mainContacts.map((val, j) => (
											<span key={val.id}><a>{val.fullName}</a>
												{agreement.mainContacts && j === agreement.mainContacts.length - 1 ? '' : ', '}
											</span>
										))
										: null}
								</td>
								<td className="text-right">
									<div className="btn-group flex-btn-group-container">
										<Button tag={Link} to={`${match.url}/${agreement.id}`} color="info" size="sm">
											<FontAwesomeIcon icon="eye"/>{' '}
											<span className="d-none d-md-inline">
												<Translate contentKey="entity.action.view">View</Translate>
											</span>
										</Button>
										<Button
											tag={Link}
											to={`${match.url}/${agreement.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
											color="primary"
											size="sm"
										>
											<FontAwesomeIcon icon="pencil-alt"/>{' '}
											<span className="d-none d-md-inline">
												<Translate contentKey="entity.action.edit">Edit</Translate>
											</span>
										</Button>
										<Button
											tag={Link}
											to={`${match.url}/${agreement.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
											color="danger"
											size="sm"
										>
											<FontAwesomeIcon icon="trash"/>{' '}
											<span className="d-none d-md-inline">
												<Translate contentKey="entity.action.delete">Delete</Translate>
											</span>
										</Button>
									</div>
								</td>
							</tr>
						))}
						</tbody>
					</Table>
				) : (
					!loading && (
						<div className="alert alert-warning">
							<Translate contentKey="ronyApp.agreement.home.notFound">No Agreements found</Translate>
						</div>
					)
				)}
			</div>
			{props.totalItems ? (
				<div className={agreementList && agreementList.length > 0 ? '' : 'd-none'}>
					<Row className="justify-content-center">
						<JhiItemCount page={paginationState.activePage} total={totalItems}
									  itemsPerPage={paginationState.itemsPerPage} i18nEnabled/>
					</Row>
					<Row className="justify-content-center">
						<JhiPagination
							activePage={paginationState.activePage}
							onSelect={handlePagination}
							maxButtons={5}
							itemsPerPage={paginationState.itemsPerPage}
							totalItems={props.totalItems}
						/>
					</Row>
				</div>
			) : (
				''
			)}
		</div>
	);
};

const mapStateToProps = ({agreement}: IRootState) => ({
	agreementList: agreement.entities,
	loading: agreement.loading,
	totalItems: agreement.totalItems,
});

const mapDispatchToProps = {
	getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Agreement);
