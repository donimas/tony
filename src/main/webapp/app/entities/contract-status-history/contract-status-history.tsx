import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './contract-status-history.reducer';
import { IContractStatusHistory } from 'app/shared/model/contract-status-history.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContractStatusHistoryProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const ContractStatusHistory = (props: IContractStatusHistoryProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { contractStatusHistoryList, match, loading } = props;
  return (
    <div>
      <h2 id="contract-status-history-heading">
        <Translate contentKey="ronyApp.contractStatusHistory.home.title">Contract Status Histories</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="ronyApp.contractStatusHistory.home.createLabel">Create new Contract Status History</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {contractStatusHistoryList && contractStatusHistoryList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.contractStatusHistory.comment">Comment</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.contractStatusHistory.createDate">Create Date</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.contractStatusHistory.status">Status</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.contractStatusHistory.contract">Contract</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {contractStatusHistoryList.map((contractStatusHistory, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${contractStatusHistory.id}`} color="link" size="sm">
                      {contractStatusHistory.id}
                    </Button>
                  </td>
                  <td>{contractStatusHistory.comment}</td>
                  <td>
                    {contractStatusHistory.createDate ? (
                      <TextFormat type="date" value={contractStatusHistory.createDate} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {contractStatusHistory.statusId ? (
                      <Link to={`dictionary/${contractStatusHistory.statusId}`}>{contractStatusHistory.statusId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {contractStatusHistory.contractId ? (
                      <Link to={`contract/${contractStatusHistory.contractId}`}>{contractStatusHistory.contractId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${contractStatusHistory.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${contractStatusHistory.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${contractStatusHistory.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="ronyApp.contractStatusHistory.home.notFound">No Contract Status Histories found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ contractStatusHistory }: IRootState) => ({
  contractStatusHistoryList: contractStatusHistory.entities,
  loading: contractStatusHistory.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContractStatusHistory);
