import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contract-status-history.reducer';
import { IContractStatusHistory } from 'app/shared/model/contract-status-history.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContractStatusHistoryDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContractStatusHistoryDetail = (props: IContractStatusHistoryDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contractStatusHistoryEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.contractStatusHistory.detail.title">ContractStatusHistory</Translate> [
          <b>{contractStatusHistoryEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="comment">
              <Translate contentKey="ronyApp.contractStatusHistory.comment">Comment</Translate>
            </span>
          </dt>
          <dd>{contractStatusHistoryEntity.comment}</dd>
          <dt>
            <span id="createDate">
              <Translate contentKey="ronyApp.contractStatusHistory.createDate">Create Date</Translate>
            </span>
          </dt>
          <dd>
            {contractStatusHistoryEntity.createDate ? (
              <TextFormat value={contractStatusHistoryEntity.createDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="ronyApp.contractStatusHistory.status">Status</Translate>
          </dt>
          <dd>{contractStatusHistoryEntity.statusId ? contractStatusHistoryEntity.statusId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.contractStatusHistory.contract">Contract</Translate>
          </dt>
          <dd>{contractStatusHistoryEntity.contractId ? contractStatusHistoryEntity.contractId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contract-status-history" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contract-status-history/${contractStatusHistoryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contractStatusHistory }: IRootState) => ({
  contractStatusHistoryEntity: contractStatusHistory.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContractStatusHistoryDetail);
