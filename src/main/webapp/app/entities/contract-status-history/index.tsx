import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContractStatusHistory from './contract-status-history';
import ContractStatusHistoryDetail from './contract-status-history-detail';
import ContractStatusHistoryUpdate from './contract-status-history-update';
import ContractStatusHistoryDeleteDialog from './contract-status-history-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContractStatusHistoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContractStatusHistoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContractStatusHistoryDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContractStatusHistory} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContractStatusHistoryDeleteDialog} />
  </>
);

export default Routes;
