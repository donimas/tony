import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDictionary } from 'app/shared/model/dictionary.model';
import { getEntities as getDictionaries } from 'app/entities/dictionary/dictionary.reducer';
import { IContract } from 'app/shared/model/contract.model';
import { getEntities as getContracts } from 'app/entities/contract/contract.reducer';
import { getEntity, updateEntity, createEntity, reset } from './contract-status-history.reducer';
import { IContractStatusHistory } from 'app/shared/model/contract-status-history.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IContractStatusHistoryUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContractStatusHistoryUpdate = (props: IContractStatusHistoryUpdateProps) => {
  const [statusId, setStatusId] = useState('0');
  const [contractId, setContractId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { contractStatusHistoryEntity, dictionaries, contracts, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/contract-status-history');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getDictionaries();
    props.getContracts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.createDate = convertDateTimeToServer(values.createDate);

    if (errors.length === 0) {
      const entity = {
        ...contractStatusHistoryEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ronyApp.contractStatusHistory.home.createOrEditLabel">
            <Translate contentKey="ronyApp.contractStatusHistory.home.createOrEditLabel">Create or edit a ContractStatusHistory</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : contractStatusHistoryEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="contract-status-history-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="contract-status-history-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="commentLabel" for="contract-status-history-comment">
                  <Translate contentKey="ronyApp.contractStatusHistory.comment">Comment</Translate>
                </Label>
                <AvField id="contract-status-history-comment" type="text" name="comment" />
              </AvGroup>
              <AvGroup>
                <Label id="createDateLabel" for="contract-status-history-createDate">
                  <Translate contentKey="ronyApp.contractStatusHistory.createDate">Create Date</Translate>
                </Label>
                <AvInput
                  id="contract-status-history-createDate"
                  type="datetime-local"
                  className="form-control"
                  name="createDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.contractStatusHistoryEntity.createDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label for="contract-status-history-status">
                  <Translate contentKey="ronyApp.contractStatusHistory.status">Status</Translate>
                </Label>
                <AvInput id="contract-status-history-status" type="select" className="form-control" name="statusId">
                  <option value="" key="0" />
                  {dictionaries
                    ? dictionaries.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="contract-status-history-contract">
                  <Translate contentKey="ronyApp.contractStatusHistory.contract">Contract</Translate>
                </Label>
                <AvInput id="contract-status-history-contract" type="select" className="form-control" name="contractId">
                  <option value="" key="0" />
                  {contracts
                    ? contracts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/contract-status-history" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  dictionaries: storeState.dictionary.entities,
  contracts: storeState.contract.entities,
  contractStatusHistoryEntity: storeState.contractStatusHistory.entity,
  loading: storeState.contractStatusHistory.loading,
  updating: storeState.contractStatusHistory.updating,
  updateSuccess: storeState.contractStatusHistory.updateSuccess,
});

const mapDispatchToProps = {
  getDictionaries,
  getContracts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContractStatusHistoryUpdate);
