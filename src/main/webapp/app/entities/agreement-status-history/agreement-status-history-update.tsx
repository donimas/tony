import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDictionary } from 'app/shared/model/dictionary.model';
import { getEntities as getDictionaries } from 'app/entities/dictionary/dictionary.reducer';
import { IAgreement } from 'app/shared/model/agreement.model';
import { getEntities as getAgreements } from 'app/entities/agreement/agreement.reducer';
import { getEntity, updateEntity, createEntity, reset } from './agreement-status-history.reducer';
import { IAgreementStatusHistory } from 'app/shared/model/agreement-status-history.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IAgreementStatusHistoryUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AgreementStatusHistoryUpdate = (props: IAgreementStatusHistoryUpdateProps) => {
  const [statusId, setStatusId] = useState('0');
  const [agreementId, setAgreementId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { agreementStatusHistoryEntity, dictionaries, agreements, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/agreement-status-history');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getDictionaries();
    props.getAgreements();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.createDate = convertDateTimeToServer(values.createDate);

    if (errors.length === 0) {
      const entity = {
        ...agreementStatusHistoryEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ronyApp.agreementStatusHistory.home.createOrEditLabel">
            <Translate contentKey="ronyApp.agreementStatusHistory.home.createOrEditLabel">
              Create or edit a AgreementStatusHistory
            </Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : agreementStatusHistoryEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="agreement-status-history-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="agreement-status-history-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="createDateLabel" for="agreement-status-history-createDate">
                  <Translate contentKey="ronyApp.agreementStatusHistory.createDate">Create Date</Translate>
                </Label>
                <AvInput
                  id="agreement-status-history-createDate"
                  type="datetime-local"
                  className="form-control"
                  name="createDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.agreementStatusHistoryEntity.createDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="commentLabel" for="agreement-status-history-comment">
                  <Translate contentKey="ronyApp.agreementStatusHistory.comment">Comment</Translate>
                </Label>
                <AvField id="agreement-status-history-comment" type="text" name="comment" />
              </AvGroup>
              <AvGroup>
                <Label for="agreement-status-history-status">
                  <Translate contentKey="ronyApp.agreementStatusHistory.status">Status</Translate>
                </Label>
                <AvInput id="agreement-status-history-status" type="select" className="form-control" name="statusId">
                  <option value="" key="0" />
                  {dictionaries
                    ? dictionaries.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="agreement-status-history-agreement">
                  <Translate contentKey="ronyApp.agreementStatusHistory.agreement">Agreement</Translate>
                </Label>
                <AvInput id="agreement-status-history-agreement" type="select" className="form-control" name="agreementId">
                  <option value="" key="0" />
                  {agreements
                    ? agreements.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/agreement-status-history" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  dictionaries: storeState.dictionary.entities,
  agreements: storeState.agreement.entities,
  agreementStatusHistoryEntity: storeState.agreementStatusHistory.entity,
  loading: storeState.agreementStatusHistory.loading,
  updating: storeState.agreementStatusHistory.updating,
  updateSuccess: storeState.agreementStatusHistory.updateSuccess,
});

const mapDispatchToProps = {
  getDictionaries,
  getAgreements,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AgreementStatusHistoryUpdate);
