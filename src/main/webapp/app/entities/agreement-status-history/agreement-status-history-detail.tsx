import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './agreement-status-history.reducer';
import { IAgreementStatusHistory } from 'app/shared/model/agreement-status-history.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAgreementStatusHistoryDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AgreementStatusHistoryDetail = (props: IAgreementStatusHistoryDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { agreementStatusHistoryEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.agreementStatusHistory.detail.title">AgreementStatusHistory</Translate> [
          <b>{agreementStatusHistoryEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="createDate">
              <Translate contentKey="ronyApp.agreementStatusHistory.createDate">Create Date</Translate>
            </span>
          </dt>
          <dd>
            {agreementStatusHistoryEntity.createDate ? (
              <TextFormat value={agreementStatusHistoryEntity.createDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="comment">
              <Translate contentKey="ronyApp.agreementStatusHistory.comment">Comment</Translate>
            </span>
          </dt>
          <dd>{agreementStatusHistoryEntity.comment}</dd>
          <dt>
            <Translate contentKey="ronyApp.agreementStatusHistory.status">Status</Translate>
          </dt>
          <dd>{agreementStatusHistoryEntity.statusId ? agreementStatusHistoryEntity.statusId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.agreementStatusHistory.agreement">Agreement</Translate>
          </dt>
          <dd>{agreementStatusHistoryEntity.agreementId ? agreementStatusHistoryEntity.agreementId : ''}</dd>
        </dl>
        <Button tag={Link} to="/agreement-status-history" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/agreement-status-history/${agreementStatusHistoryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ agreementStatusHistory }: IRootState) => ({
  agreementStatusHistoryEntity: agreementStatusHistory.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AgreementStatusHistoryDetail);
