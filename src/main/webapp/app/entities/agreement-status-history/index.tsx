import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import AgreementStatusHistory from './agreement-status-history';
import AgreementStatusHistoryDetail from './agreement-status-history-detail';
import AgreementStatusHistoryUpdate from './agreement-status-history-update';
import AgreementStatusHistoryDeleteDialog from './agreement-status-history-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={AgreementStatusHistoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={AgreementStatusHistoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={AgreementStatusHistoryDetail} />
      <ErrorBoundaryRoute path={match.url} component={AgreementStatusHistory} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={AgreementStatusHistoryDeleteDialog} />
  </>
);

export default Routes;
