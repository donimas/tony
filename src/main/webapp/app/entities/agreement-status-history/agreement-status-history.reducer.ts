import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IAgreementStatusHistory, defaultValue } from 'app/shared/model/agreement-status-history.model';

export const ACTION_TYPES = {
  FETCH_AGREEMENTSTATUSHISTORY_LIST: 'agreementStatusHistory/FETCH_AGREEMENTSTATUSHISTORY_LIST',
  FETCH_AGREEMENTSTATUSHISTORY: 'agreementStatusHistory/FETCH_AGREEMENTSTATUSHISTORY',
  CREATE_AGREEMENTSTATUSHISTORY: 'agreementStatusHistory/CREATE_AGREEMENTSTATUSHISTORY',
  UPDATE_AGREEMENTSTATUSHISTORY: 'agreementStatusHistory/UPDATE_AGREEMENTSTATUSHISTORY',
  DELETE_AGREEMENTSTATUSHISTORY: 'agreementStatusHistory/DELETE_AGREEMENTSTATUSHISTORY',
  RESET: 'agreementStatusHistory/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IAgreementStatusHistory>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type AgreementStatusHistoryState = Readonly<typeof initialState>;

// Reducer

export default (state: AgreementStatusHistoryState = initialState, action): AgreementStatusHistoryState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_AGREEMENTSTATUSHISTORY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_AGREEMENTSTATUSHISTORY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_AGREEMENTSTATUSHISTORY):
    case REQUEST(ACTION_TYPES.UPDATE_AGREEMENTSTATUSHISTORY):
    case REQUEST(ACTION_TYPES.DELETE_AGREEMENTSTATUSHISTORY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_AGREEMENTSTATUSHISTORY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_AGREEMENTSTATUSHISTORY):
    case FAILURE(ACTION_TYPES.CREATE_AGREEMENTSTATUSHISTORY):
    case FAILURE(ACTION_TYPES.UPDATE_AGREEMENTSTATUSHISTORY):
    case FAILURE(ACTION_TYPES.DELETE_AGREEMENTSTATUSHISTORY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_AGREEMENTSTATUSHISTORY_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_AGREEMENTSTATUSHISTORY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_AGREEMENTSTATUSHISTORY):
    case SUCCESS(ACTION_TYPES.UPDATE_AGREEMENTSTATUSHISTORY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_AGREEMENTSTATUSHISTORY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/agreement-status-histories';

// Actions

export const getEntities: ICrudGetAllAction<IAgreementStatusHistory> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_AGREEMENTSTATUSHISTORY_LIST,
  payload: axios.get<IAgreementStatusHistory>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IAgreementStatusHistory> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_AGREEMENTSTATUSHISTORY,
    payload: axios.get<IAgreementStatusHistory>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IAgreementStatusHistory> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_AGREEMENTSTATUSHISTORY,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IAgreementStatusHistory> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_AGREEMENTSTATUSHISTORY,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IAgreementStatusHistory> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_AGREEMENTSTATUSHISTORY,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
