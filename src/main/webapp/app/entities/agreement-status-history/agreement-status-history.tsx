import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './agreement-status-history.reducer';
import { IAgreementStatusHistory } from 'app/shared/model/agreement-status-history.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAgreementStatusHistoryProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const AgreementStatusHistory = (props: IAgreementStatusHistoryProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { agreementStatusHistoryList, match, loading } = props;
  return (
    <div>
      <h2 id="agreement-status-history-heading">
        <Translate contentKey="ronyApp.agreementStatusHistory.home.title">Agreement Status Histories</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="ronyApp.agreementStatusHistory.home.createLabel">Create new Agreement Status History</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {agreementStatusHistoryList && agreementStatusHistoryList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.agreementStatusHistory.createDate">Create Date</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.agreementStatusHistory.comment">Comment</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.agreementStatusHistory.status">Status</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.agreementStatusHistory.agreement">Agreement</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {agreementStatusHistoryList.map((agreementStatusHistory, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${agreementStatusHistory.id}`} color="link" size="sm">
                      {agreementStatusHistory.id}
                    </Button>
                  </td>
                  <td>
                    {agreementStatusHistory.createDate ? (
                      <TextFormat type="date" value={agreementStatusHistory.createDate} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{agreementStatusHistory.comment}</td>
                  <td>
                    {agreementStatusHistory.statusId ? (
                      <Link to={`dictionary/${agreementStatusHistory.statusId}`}>{agreementStatusHistory.statusId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {agreementStatusHistory.agreementId ? (
                      <Link to={`agreement/${agreementStatusHistory.agreementId}`}>{agreementStatusHistory.agreementId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${agreementStatusHistory.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${agreementStatusHistory.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${agreementStatusHistory.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="ronyApp.agreementStatusHistory.home.notFound">No Agreement Status Histories found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ agreementStatusHistory }: IRootState) => ({
  agreementStatusHistoryList: agreementStatusHistory.entities,
  loading: agreementStatusHistory.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AgreementStatusHistory);
