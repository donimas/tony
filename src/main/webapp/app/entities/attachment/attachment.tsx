import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { openFile, byteSize, Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './attachment.reducer';
import { IAttachment } from 'app/shared/model/attachment.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAttachmentProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Attachment = (props: IAttachmentProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { attachmentList, match, loading } = props;
  return (
    <div>
      <h2 id="attachment-heading">
        <Translate contentKey="ronyApp.attachment.home.title">Attachments</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="ronyApp.attachment.home.createLabel">Create new Attachment</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {attachmentList && attachmentList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.attachment.file">File</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.attachment.type">Type</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.attachment.uri">Uri</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.attachment.unicode">Unicode</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.attachment.name">Name</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.attachment.entityName">Entity Name</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.attachment.entityId">Entity Id</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.attachment.flagDeleted">Flag Deleted</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {attachmentList.map((attachment, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${attachment.id}`} color="link" size="sm">
                      {attachment.id}
                    </Button>
                  </td>
                  <td>
                    {attachment.file ? (
                      <div>
                        {attachment.fileContentType ? (
                          <a onClick={openFile(attachment.fileContentType, attachment.file)}>
                            <Translate contentKey="entity.action.open">Open</Translate>
                            &nbsp;
                          </a>
                        ) : null}
                        <span>
                          {attachment.fileContentType}, {byteSize(attachment.file)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td>{attachment.type}</td>
                  <td>{attachment.uri}</td>
                  <td>{attachment.unicode}</td>
                  <td>{attachment.name}</td>
                  <td>{attachment.entityName}</td>
                  <td>{attachment.entityId}</td>
                  <td>{attachment.flagDeleted ? 'true' : 'false'}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${attachment.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${attachment.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${attachment.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="ronyApp.attachment.home.notFound">No Attachments found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ attachment }: IRootState) => ({
  attachmentList: attachment.entities,
  loading: attachment.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Attachment);
