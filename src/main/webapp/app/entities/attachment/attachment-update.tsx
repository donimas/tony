import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, setFileData, openFile, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, setBlob, reset } from './attachment.reducer';
import { IAttachment } from 'app/shared/model/attachment.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IAttachmentUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AttachmentUpdate = (props: IAttachmentUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { attachmentEntity, loading, updating } = props;

  const { file, fileContentType } = attachmentEntity;

  const handleClose = () => {
    props.history.push('/attachment');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  const onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => props.setBlob(name, data, contentType), isAnImage);
  };

  const clearBlob = name => () => {
    props.setBlob(name, undefined, undefined);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...attachmentEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ronyApp.attachment.home.createOrEditLabel">
            <Translate contentKey="ronyApp.attachment.home.createOrEditLabel">Create or edit a Attachment</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : attachmentEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="attachment-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="attachment-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <AvGroup>
                  <Label id="fileLabel" for="file">
                    <Translate contentKey="ronyApp.attachment.file">File</Translate>
                  </Label>
                  <br />
                  {file ? (
                    <div>
                      {fileContentType ? (
                        <a onClick={openFile(fileContentType, file)}>
                          <Translate contentKey="entity.action.open">Open</Translate>
                        </a>
                      ) : null}
                      <br />
                      <Row>
                        <Col md="11">
                          <span>
                            {fileContentType}, {byteSize(file)}
                          </span>
                        </Col>
                        <Col md="1">
                          <Button color="danger" onClick={clearBlob('file')}>
                            <FontAwesomeIcon icon="times-circle" />
                          </Button>
                        </Col>
                      </Row>
                    </div>
                  ) : null}
                  <input id="file_file" type="file" onChange={onBlobChange(false, 'file')} />
                  <AvInput type="hidden" name="file" value={file} />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <Label id="typeLabel" for="attachment-type">
                  <Translate contentKey="ronyApp.attachment.type">Type</Translate>
                </Label>
                <AvField id="attachment-type" type="text" name="type" />
              </AvGroup>
              <AvGroup>
                <Label id="uriLabel" for="attachment-uri">
                  <Translate contentKey="ronyApp.attachment.uri">Uri</Translate>
                </Label>
                <AvField id="attachment-uri" type="text" name="uri" />
              </AvGroup>
              <AvGroup>
                <Label id="unicodeLabel" for="attachment-unicode">
                  <Translate contentKey="ronyApp.attachment.unicode">Unicode</Translate>
                </Label>
                <AvField id="attachment-unicode" type="text" name="unicode" />
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="attachment-name">
                  <Translate contentKey="ronyApp.attachment.name">Name</Translate>
                </Label>
                <AvField id="attachment-name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="entityNameLabel" for="attachment-entityName">
                  <Translate contentKey="ronyApp.attachment.entityName">Entity Name</Translate>
                </Label>
                <AvField id="attachment-entityName" type="text" name="entityName" />
              </AvGroup>
              <AvGroup>
                <Label id="entityIdLabel" for="attachment-entityId">
                  <Translate contentKey="ronyApp.attachment.entityId">Entity Id</Translate>
                </Label>
                <AvField id="attachment-entityId" type="string" className="form-control" name="entityId" />
              </AvGroup>
              <AvGroup check>
                <Label id="flagDeletedLabel">
                  <AvInput id="attachment-flagDeleted" type="checkbox" className="form-check-input" name="flagDeleted" />
                  <Translate contentKey="ronyApp.attachment.flagDeleted">Flag Deleted</Translate>
                </Label>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/attachment" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  attachmentEntity: storeState.attachment.entity,
  loading: storeState.attachment.loading,
  updating: storeState.attachment.updating,
  updateSuccess: storeState.attachment.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AttachmentUpdate);
