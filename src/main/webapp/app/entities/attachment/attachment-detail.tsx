import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './attachment.reducer';
import { IAttachment } from 'app/shared/model/attachment.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAttachmentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AttachmentDetail = (props: IAttachmentDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { attachmentEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.attachment.detail.title">Attachment</Translate> [<b>{attachmentEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="file">
              <Translate contentKey="ronyApp.attachment.file">File</Translate>
            </span>
          </dt>
          <dd>
            {attachmentEntity.file ? (
              <div>
                {attachmentEntity.fileContentType ? (
                  <a onClick={openFile(attachmentEntity.fileContentType, attachmentEntity.file)}>
                    <Translate contentKey="entity.action.open">Open</Translate>&nbsp;
                  </a>
                ) : null}
                <span>
                  {attachmentEntity.fileContentType}, {byteSize(attachmentEntity.file)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>
            <span id="type">
              <Translate contentKey="ronyApp.attachment.type">Type</Translate>
            </span>
          </dt>
          <dd>{attachmentEntity.type}</dd>
          <dt>
            <span id="uri">
              <Translate contentKey="ronyApp.attachment.uri">Uri</Translate>
            </span>
          </dt>
          <dd>{attachmentEntity.uri}</dd>
          <dt>
            <span id="unicode">
              <Translate contentKey="ronyApp.attachment.unicode">Unicode</Translate>
            </span>
          </dt>
          <dd>{attachmentEntity.unicode}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="ronyApp.attachment.name">Name</Translate>
            </span>
          </dt>
          <dd>{attachmentEntity.name}</dd>
          <dt>
            <span id="entityName">
              <Translate contentKey="ronyApp.attachment.entityName">Entity Name</Translate>
            </span>
          </dt>
          <dd>{attachmentEntity.entityName}</dd>
          <dt>
            <span id="entityId">
              <Translate contentKey="ronyApp.attachment.entityId">Entity Id</Translate>
            </span>
          </dt>
          <dd>{attachmentEntity.entityId}</dd>
          <dt>
            <span id="flagDeleted">
              <Translate contentKey="ronyApp.attachment.flagDeleted">Flag Deleted</Translate>
            </span>
          </dt>
          <dd>{attachmentEntity.flagDeleted ? 'true' : 'false'}</dd>
        </dl>
        <Button tag={Link} to="/attachment" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/attachment/${attachmentEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ attachment }: IRootState) => ({
  attachmentEntity: attachment.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AttachmentDetail);
