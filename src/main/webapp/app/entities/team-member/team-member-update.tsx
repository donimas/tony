import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { IProjectTeam } from 'app/shared/model/project-team.model';
import { getEntities as getProjectTeams } from 'app/entities/project-team/project-team.reducer';
import { getEntity, updateEntity, createEntity, reset } from './team-member.reducer';
import { ITeamMember } from 'app/shared/model/team-member.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITeamMemberUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TeamMemberUpdate = (props: ITeamMemberUpdateProps) => {
  const [employeeId, setEmployeeId] = useState('0');
  const [teamId, setTeamId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { teamMemberEntity, users, projectTeams, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/team-member');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }

    props.getUsers();
    props.getProjectTeams();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.startDate = convertDateTimeToServer(values.startDate);
    values.finishDate = convertDateTimeToServer(values.finishDate);
    values.createDate = convertDateTimeToServer(values.createDate);

    if (errors.length === 0) {
      const entity = {
        ...teamMemberEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ronyApp.teamMember.home.createOrEditLabel">
            <Translate contentKey="ronyApp.teamMember.home.createOrEditLabel">Create or edit a TeamMember</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : teamMemberEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="team-member-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="team-member-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="involvementLabel" for="team-member-involvement">
                  <Translate contentKey="ronyApp.teamMember.involvement">Involvement</Translate>
                </Label>
                <AvField id="team-member-involvement" type="text" name="involvement" />
              </AvGroup>
              <AvGroup>
                <Label id="startDateLabel" for="team-member-startDate">
                  <Translate contentKey="ronyApp.teamMember.startDate">Start Date</Translate>
                </Label>
                <AvInput
                  id="team-member-startDate"
                  type="datetime-local"
                  className="form-control"
                  name="startDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.teamMemberEntity.startDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="finishDateLabel" for="team-member-finishDate">
                  <Translate contentKey="ronyApp.teamMember.finishDate">Finish Date</Translate>
                </Label>
                <AvInput
                  id="team-member-finishDate"
                  type="datetime-local"
                  className="form-control"
                  name="finishDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.teamMemberEntity.finishDate)}
                />
              </AvGroup>
              <AvGroup check>
                <Label id="flagLeftLabel">
                  <AvInput id="team-member-flagLeft" type="checkbox" className="form-check-input" name="flagLeft" />
                  <Translate contentKey="ronyApp.teamMember.flagLeft">Flag Left</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label id="createDateLabel" for="team-member-createDate">
                  <Translate contentKey="ronyApp.teamMember.createDate">Create Date</Translate>
                </Label>
                <AvInput
                  id="team-member-createDate"
                  type="datetime-local"
                  className="form-control"
                  name="createDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.teamMemberEntity.createDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label for="team-member-employee">
                  <Translate contentKey="ronyApp.teamMember.employee">Employee</Translate>
                </Label>
                <AvInput id="team-member-employee" type="select" className="form-control" name="employeeId">
                  <option value="" key="0" />
                  {users
                    ? users.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="team-member-team">
                  <Translate contentKey="ronyApp.teamMember.team">Team</Translate>
                </Label>
                <AvInput id="team-member-team" type="select" className="form-control" name="teamId">
                  <option value="" key="0" />
                  {projectTeams
                    ? projectTeams.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/team-member" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  users: storeState.userManagement.users,
  projectTeams: storeState.projectTeam.entities,
  teamMemberEntity: storeState.teamMember.entity,
  loading: storeState.teamMember.loading,
  updating: storeState.teamMember.updating,
  updateSuccess: storeState.teamMember.updateSuccess,
});

const mapDispatchToProps = {
  getUsers,
  getProjectTeams,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TeamMemberUpdate);
