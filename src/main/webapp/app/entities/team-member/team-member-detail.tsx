import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './team-member.reducer';
import { ITeamMember } from 'app/shared/model/team-member.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITeamMemberDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TeamMemberDetail = (props: ITeamMemberDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { teamMemberEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.teamMember.detail.title">TeamMember</Translate> [<b>{teamMemberEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="involvement">
              <Translate contentKey="ronyApp.teamMember.involvement">Involvement</Translate>
            </span>
          </dt>
          <dd>{teamMemberEntity.involvement}</dd>
          <dt>
            <span id="startDate">
              <Translate contentKey="ronyApp.teamMember.startDate">Start Date</Translate>
            </span>
          </dt>
          <dd>
            {teamMemberEntity.startDate ? <TextFormat value={teamMemberEntity.startDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="finishDate">
              <Translate contentKey="ronyApp.teamMember.finishDate">Finish Date</Translate>
            </span>
          </dt>
          <dd>
            {teamMemberEntity.finishDate ? <TextFormat value={teamMemberEntity.finishDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="flagLeft">
              <Translate contentKey="ronyApp.teamMember.flagLeft">Flag Left</Translate>
            </span>
          </dt>
          <dd>{teamMemberEntity.flagLeft ? 'true' : 'false'}</dd>
          <dt>
            <span id="createDate">
              <Translate contentKey="ronyApp.teamMember.createDate">Create Date</Translate>
            </span>
          </dt>
          <dd>
            {teamMemberEntity.createDate ? <TextFormat value={teamMemberEntity.createDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <Translate contentKey="ronyApp.teamMember.employee">Employee</Translate>
          </dt>
          <dd>{teamMemberEntity.employeeId ? teamMemberEntity.employeeId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.teamMember.team">Team</Translate>
          </dt>
          <dd>{teamMemberEntity.teamId ? teamMemberEntity.teamId : ''}</dd>
        </dl>
        <Button tag={Link} to="/team-member" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/team-member/${teamMemberEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ teamMember }: IRootState) => ({
  teamMemberEntity: teamMember.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TeamMemberDetail);
