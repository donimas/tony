import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Col, Label} from 'reactstrap';
import {AvForm, AvGroup, AvInput, AvField} from 'availity-reactstrap-validation';
import {Translate, translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';

import {getTopLevel as getStructures} from 'app/entities/structure/structure.reducer';
import {getEntity, updateEntity, createEntity, reset} from './project.reducer';
import {convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime} from 'app/shared/util/date-utils';
import {mapIdList} from 'app/shared/util/entity-utils';

import Select from 'react-select';
import FindSelect from "app/shared/form/find-select";
import {DICT_PROJECT_STATUS_HISTORY, DICT_STACK, ENTITY_NAME_USER} from "app/shared/util/constants";
import CreateDictForm from "app/shared/form/create-dict";

export interface IProjectUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const ProjectUpdate = (props: IProjectUpdateProps) => {
	const [idstechStack, setIdstechStack] = useState([]);
	const [statusHistoryId, setStatusHistoryId] = useState('0');
	const [directionId, setDirectionId] = useState(null);
	const [teamId, setTeamId] = useState('0');
	const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);
	const [statusDictId, setStatusDictId] = useState(null);
	const [teamLeadId, setTeamLeadId] = useState(null);
	
	const {
		projectEntity, structures, loading, updating
	} = props;
	
	const handleClose = () => {
		props.history.push('/project' + props.location.search);
	};
	
	useEffect(() => {
		if (isNew) {
			props.reset();
		} else {
			props.getEntity(props.match.params.id);
		}
		
		props.getStructures();
	}, []);
	
	useEffect(() => {
		if (props.updateSuccess) {
			handleClose();
		}
	}, [props.updateSuccess]);
	
	const saveEntity = (event, errors, values) => {
		values.startDate = convertDateTimeToServer(values.startDate);
		values.finishDate = convertDateTimeToServer(values.finishDate);
		values.createDate = convertDateTimeToServer(values.createDate);
		
		let techStackIds = [];
		if(setIdstechStack.length > 0) {
			techStackIds = mapIdList(idstechStack.map(e => e.id))
		}
		
		if (errors.length === 0) {
			const entity = {
				...projectEntity,
				...values,
				directionId,
				techStacks: techStackIds,
				statusDictId,
				teamLeadId
			};
			
			if (isNew) {
				props.createEntity(entity);
			} else {
				props.updateEntity(entity);
			}
		}
	};
	
	return (
		
		<div>
			<Row className="justify-content-center">
				<Col md="8">
					<h2 id="ronyApp.project.home.createOrEditLabel">
						<Translate contentKey="ronyApp.project.home.createOrEditLabel">Create or edit a
							Project</Translate>
					</h2>
				</Col>
			</Row>
			<Row className="justify-content-center">
				<Col md="8">
					{loading ? (
						<p>Loading...</p>
					) : (
						<AvForm model={isNew ? {} : projectEntity} onSubmit={saveEntity}>
							{!isNew ? (
								<AvGroup>
									<Label for="project-id">
										<Translate contentKey="global.field.id">ID</Translate>
									</Label>
									<AvInput id="project-id" type="text" className="form-control" name="id" required
											 readOnly/>
								</AvGroup>
							) : null}
							<AvGroup>
								<Label id="nameLabel" for="project-name">
									<Translate contentKey="ronyApp.project.name">Name</Translate>
								</Label>
								<AvField
									id="project-name"
									type="text"
									name="name"
									validate={{
										required: {value: true, errorMessage: translate('entity.validation.required')},
									}}
								/>
							</AvGroup>
							<Row>
								<Col md="6">
									<AvGroup>
										<Label id="descriptionLabel" for="project-description">
											<Translate contentKey="ronyApp.project.description">Description</Translate>
										</Label>
										<AvField id="project-description" type="text" name="description"/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label id="goalLabel" for="project-goal">
											<Translate contentKey="ronyApp.project.goal">Goal</Translate>
										</Label>
										<AvField id="project-goal" type="text" name="goal"/>
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<Row>
										<Col md="6">
											<AvGroup>
												<Label id="startDateLabel" for="project-startDate">
													<Translate contentKey="ronyApp.project.startDate">Start
														Date</Translate>
												</Label>
												<AvInput
													id="project-startDate"
													type="datetime-local"
													className="form-control"
													name="startDate"
													placeholder={'YYYY-MM-DD HH:mm'}
													value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.projectEntity.startDate)}
												/>
											</AvGroup>
										</Col>
										<Col md="6">
											<AvGroup>
												<Label id="finishDateLabel" for="project-finishDate">
													<Translate contentKey="ronyApp.project.finishDate">Finish
														Date</Translate>
												</Label>
												<AvInput
													id="project-finishDate"
													type="datetime-local"
													className="form-control"
													name="finishDate"
													placeholder={'YYYY-MM-DD HH:mm'}
													value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.projectEntity.finishDate)}
												/>
											</AvGroup>
										</Col>
									</Row>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label id="uriLabel" for="project-uri">
											<Translate contentKey="ronyApp.project.uri">Uri</Translate>
										</Label>
										<AvField id="project-uri" type="text" name="uri"/>
									</AvGroup>
								</Col>
							</Row>
							
							<Row>
								<Col md="6">
									<AvGroup>
										<Label for="project-direction">
											<Translate contentKey="ronyApp.project.direction">Direction</Translate>
										</Label>
										
										<Select
											isClearable={true}
											name="directionId"
											options={structures}
											getOptionLabel={(option) => option.name}
											getOptionValue={(option) => option.id}
											getOptionKey={(option) => option.id}
											onChange={(option) => setDirectionId(option ? option.id : null)}
										/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label for="project-team">
											Team lead
										</Label>
										
										<FindSelect
											entityName={ENTITY_NAME_USER}
											onChange={(option) => setTeamLeadId(option ? option.id : null)}
										/>
										
									</AvGroup>
								</Col>
							</Row>
							<Row>
								<Col md="6">
									<AvGroup>
										<Label for="status">
											Статус
										</Label>
										
										<CreateDictForm entityName={DICT_PROJECT_STATUS_HISTORY}
														onChange={(option) => option ? setStatusDictId(option.id) : setStatusDictId(null)}/>
									</AvGroup>
								</Col>
								<Col md="6">
									<AvGroup>
										<Label for="project-techStack">
											<Translate contentKey="ronyApp.project.techStack">Tech Stack</Translate>
										</Label>
										<CreateDictForm
											entityName={DICT_STACK}
											onChange={(option) => option ? setIdstechStack(option) : setIdstechStack([])}
											isMulty={true}
										/>
										
									</AvGroup>
								</Col>
							</Row>
							
							<Button tag={Link} id="cancel-save" to="/project" replace color="info">
								<FontAwesomeIcon icon="arrow-left"/>
								&nbsp;
								<span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
							</Button>
							&nbsp;
							<Button color="primary" id="save-entity" type="submit" disabled={updating}>
								<FontAwesomeIcon icon="save"/>
								&nbsp;
								<Translate contentKey="entity.action.save">Save</Translate>
							</Button>
						</AvForm>
					)}
				</Col>
			</Row>
		</div>
	);
};

const mapStateToProps = (storeState: IRootState) => ({
	structures: storeState.structure.entities,
	projectEntity: storeState.project.entity,
	loading: storeState.project.loading,
	updating: storeState.project.updating,
	updateSuccess: storeState.project.updateSuccess
});

const mapDispatchToProps = {
	getStructures,
	getEntity,
	updateEntity,
	createEntity,
	reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProjectUpdate);
