import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Col, Nav, NavItem, NavLink, TabContent, TabPane, Collapse, Card, CardBody} from 'reactstrap';
import {Translate, ICrudGetAction, TextFormat} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntity} from './project.reducer';
import {IProject} from 'app/shared/model/project.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT} from 'app/config/constants';
import classnames from 'classnames';

export interface IProjectDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const ProjectDetail = (props: IProjectDetailProps) => {
	useEffect(() => {
		props.getEntity(props.match.params.id);
	}, []);
	
	const {projectEntity} = props;
	
	const [activeTab, setActiveTab] = useState('1');
	
	const toggle = tab => {
		if (activeTab !== tab) setActiveTab(tab);
	};
	
	const [isTeamOpen, setIsTeamOpen] = useState(true);
	const [isCustomerOpen, setIsCustomerOpen] = useState(false);
	const [isContractOpen, setIsContractOpen] = useState(false);
	const [isTransactionOpen, setIsTransactionOpen] = useState(false);
	
	const toggleTeam = () => setIsTeamOpen(!isTeamOpen);
	const toggleCustomer = () => setIsCustomerOpen(!isCustomerOpen);
	const toggleContract = () => setIsContractOpen(!isContractOpen);
	const toggleTransaction = () => setIsTransactionOpen(!isTransactionOpen);
	
	return (
		<Row className="justify-content-center">
			<Col md="8">
				<h2>
					<b>{projectEntity.name}</b>
				</h2>
				<Row>
					<Col md="3">
						<span id="description">
						  <b><Translate contentKey="ronyApp.project.description">Description</Translate>:</b>
						</span>
					</Col>
					<Col md="9">
						<span>{projectEntity.description}</span>
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<span id="goal">
						  <b><Translate contentKey="ronyApp.project.goal">Goal</Translate>:</b>
						</span>
					</Col>
					<Col md="9">
						<span>{projectEntity.goal}</span>
					</Col>
				</Row>
				<Row>
					<Col md="3">
						<span id="startDate">
						  <b><Translate contentKey="ronyApp.project.startDate">Start Date</Translate>:</b>
						</span>
					</Col>
					<Col md="9">
            			<span>{projectEntity.startDate ?
							<TextFormat value={projectEntity.startDate} type="date" format={APP_DATE_FORMAT}/> : null}
            			</span> -
						<span> {projectEntity.finishDate ?
							<TextFormat value={projectEntity.finishDate} type="date"
										format={APP_DATE_FORMAT}/> : null}
						</span>
					</Col>
				</Row>
				<Row>
					<Col md="3"><b><Translate contentKey="ronyApp.project.uri">Uri</Translate>:</b></Col>
					<Col md="9">
						<span>{projectEntity.uri}</span>
					</Col>
				</Row>
				<Row>
					<Col md="3"><b><Translate contentKey="ronyApp.project.statusHistory">Status History</Translate>:</b></Col>
					<Col md="9">
						<span>{projectEntity.statusHistoryId ? projectEntity.statusDictLabel : ''}</span>
					</Col>
				</Row>
				<Row>
					<Col md="3"><b><Translate contentKey="ronyApp.project.direction">Direction</Translate>:</b></Col>
					<Col md="9">
						<span>{projectEntity.directionId ? projectEntity.directionDictLabel : ''}</span>
					</Col>
				</Row>
				<Row>
					<Col md="3"><b><Translate contentKey="ronyApp.project.techStack">Tech Stack</Translate>:</b></Col>
					<Col md="9">
						{projectEntity.techStacks
							? projectEntity.techStacks.map((val, i) => (
								<span key={val.id}>
                    <a>{val.label}</a>
									{projectEntity.techStacks && i === projectEntity.techStacks.length - 1 ? '' : ', '}
                  </span>
							))
							: null}
					</Col>
				</Row>
				
				{/*<Row>
          <Col md="3"><b><Translate contentKey="ronyApp.project.team">Team</Translate>:</b></Col>
          <Col md="9">{projectEntity.teamId ? projectEntity.teamId : ''}</Col>
        </Row>*/}
				
				<Row>
					<Col md="12">
						<Button color="secondary" onClick={toggleTeam} style={{marginTop: '1rem'}}><Translate
							contentKey="ronyApp.project.team">Team</Translate>:</Button>
						<Collapse isOpen={isTeamOpen}>
							<Card>
								<CardBody>
									Team
								</CardBody>
							</Card>
						</Collapse>
					</Col>
				</Row>
				<Row>
					<Col md="12">
						<Button color="secondary" onClick={toggleCustomer}
								style={{marginTop: '1rem'}}>Заказчик:</Button>
						<Collapse isOpen={isCustomerOpen}>
							<Card>
								<CardBody>
									Customer
								</CardBody>
							</Card>
						</Collapse>
					</Col>
				</Row>
				<Row>
					<Col md="12">
						<Button color="secondary" onClick={toggleContract} style={{marginTop: '1rem'}}>Договор:</Button>
						<Collapse isOpen={isContractOpen}>
							<Card>
								<CardBody>
									Team
								</CardBody>
							</Card>
						</Collapse>
					</Col>
				</Row>
				<Row>
					<Col md="12">
						<Button color="secondary" onClick={toggleTransaction}
								style={{marginTop: '1rem'}}>Транзакции:</Button>
						<Collapse isOpen={isTransactionOpen}>
							<Card>
								<CardBody>
									Team
								</CardBody>
							</Card>
						</Collapse>
					</Col>
				</Row>
				
				<Row className="detail-buttons">
					<Col md="12">
						<Button tag={Link} to="/project" replace color="info">
							<FontAwesomeIcon icon="arrow-left"/>{' '}
							<span className="d-none d-md-inline">
								<Translate contentKey="entity.action.back">Back</Translate>
							</span>
						</Button>
						&nbsp;
						<Button tag={Link} to={`/project/${projectEntity.id}/edit`} replace color="primary">
							<FontAwesomeIcon icon="pencil-alt"/>{' '}
							<span className="d-none d-md-inline">
								<Translate contentKey="entity.action.edit">Edit</Translate>
							</span>
						</Button>
					</Col>
				</Row>
			</Col>
		</Row>
	);
};

const mapStateToProps = ({project}: IRootState) => ({
	projectEntity: project.entity,
});

const mapDispatchToProps = {getEntity};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetail);
