import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDictionary } from 'app/shared/model/dictionary.model';
import { getEntities as getDictionaries } from 'app/entities/dictionary/dictionary.reducer';
import { ICompanyInfo } from 'app/shared/model/company-info.model';
import { getEntities as getCompanyInfos } from 'app/entities/company-info/company-info.reducer';
import { IContract } from 'app/shared/model/contract.model';
import { getEntities as getContracts } from 'app/entities/contract/contract.reducer';
import { getEntity, updateEntity, createEntity, reset } from './partnership.reducer';
import { IPartnership } from 'app/shared/model/partnership.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPartnershipUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PartnershipUpdate = (props: IPartnershipUpdateProps) => {
  const [typeId, setTypeId] = useState('0');
  const [contractorId, setContractorId] = useState('0');
  const [contractId, setContractId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { partnershipEntity, dictionaries, companyInfos, contracts, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/partnership');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getDictionaries();
    props.getCompanyInfos();
    props.getContracts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...partnershipEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ronyApp.partnership.home.createOrEditLabel">
            <Translate contentKey="ronyApp.partnership.home.createOrEditLabel">Create or edit a Partnership</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : partnershipEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="partnership-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="partnership-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label for="partnership-type">
                  <Translate contentKey="ronyApp.partnership.type">Type</Translate>
                </Label>
                <AvInput id="partnership-type" type="select" className="form-control" name="typeId">
                  <option value="" key="0" />
                  {dictionaries
                    ? dictionaries.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="partnership-contractor">
                  <Translate contentKey="ronyApp.partnership.contractor">Contractor</Translate>
                </Label>
                <AvInput id="partnership-contractor" type="select" className="form-control" name="contractorId">
                  <option value="" key="0" />
                  {companyInfos
                    ? companyInfos.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="partnership-contract">
                  <Translate contentKey="ronyApp.partnership.contract">Contract</Translate>
                </Label>
                <AvInput id="partnership-contract" type="select" className="form-control" name="contractId">
                  <option value="" key="0" />
                  {contracts
                    ? contracts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/partnership" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  dictionaries: storeState.dictionary.entities,
  companyInfos: storeState.companyInfo.entities,
  contracts: storeState.contract.entities,
  partnershipEntity: storeState.partnership.entity,
  loading: storeState.partnership.loading,
  updating: storeState.partnership.updating,
  updateSuccess: storeState.partnership.updateSuccess,
});

const mapDispatchToProps = {
  getDictionaries,
  getCompanyInfos,
  getContracts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PartnershipUpdate);
