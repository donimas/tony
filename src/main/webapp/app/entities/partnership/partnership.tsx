import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './partnership.reducer';
import { IPartnership } from 'app/shared/model/partnership.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPartnershipProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Partnership = (props: IPartnershipProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { partnershipList, match, loading } = props;
  return (
    <div>
      <h2 id="partnership-heading">
        <Translate contentKey="ronyApp.partnership.home.title">Partnerships</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="ronyApp.partnership.home.createLabel">Create new Partnership</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {partnershipList && partnershipList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.partnership.type">Type</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.partnership.contractor">Contractor</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.partnership.contract">Contract</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {partnershipList.map((partnership, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${partnership.id}`} color="link" size="sm">
                      {partnership.id}
                    </Button>
                  </td>
                  <td>{partnership.typeId ? <Link to={`dictionary/${partnership.typeId}`}>{partnership.typeId}</Link> : ''}</td>
                  <td>
                    {partnership.contractorId ? (
                      <Link to={`company-info/${partnership.contractorId}`}>{partnership.contractorId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>{partnership.contractId ? <Link to={`contract/${partnership.contractId}`}>{partnership.contractId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${partnership.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${partnership.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${partnership.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="ronyApp.partnership.home.notFound">No Partnerships found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ partnership }: IRootState) => ({
  partnershipList: partnership.entities,
  loading: partnership.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Partnership);
