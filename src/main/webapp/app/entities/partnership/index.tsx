import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Partnership from './partnership';
import PartnershipDetail from './partnership-detail';
import PartnershipUpdate from './partnership-update';
import PartnershipDeleteDialog from './partnership-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PartnershipUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PartnershipUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PartnershipDetail} />
      <ErrorBoundaryRoute path={match.url} component={Partnership} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PartnershipDeleteDialog} />
  </>
);

export default Routes;
