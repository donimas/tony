import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPartnership, defaultValue } from 'app/shared/model/partnership.model';

export const ACTION_TYPES = {
  FETCH_PARTNERSHIP_LIST: 'partnership/FETCH_PARTNERSHIP_LIST',
  FETCH_PARTNERSHIP: 'partnership/FETCH_PARTNERSHIP',
  CREATE_PARTNERSHIP: 'partnership/CREATE_PARTNERSHIP',
  UPDATE_PARTNERSHIP: 'partnership/UPDATE_PARTNERSHIP',
  DELETE_PARTNERSHIP: 'partnership/DELETE_PARTNERSHIP',
  RESET: 'partnership/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPartnership>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PartnershipState = Readonly<typeof initialState>;

// Reducer

export default (state: PartnershipState = initialState, action): PartnershipState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PARTNERSHIP_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PARTNERSHIP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PARTNERSHIP):
    case REQUEST(ACTION_TYPES.UPDATE_PARTNERSHIP):
    case REQUEST(ACTION_TYPES.DELETE_PARTNERSHIP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PARTNERSHIP_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PARTNERSHIP):
    case FAILURE(ACTION_TYPES.CREATE_PARTNERSHIP):
    case FAILURE(ACTION_TYPES.UPDATE_PARTNERSHIP):
    case FAILURE(ACTION_TYPES.DELETE_PARTNERSHIP):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PARTNERSHIP_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PARTNERSHIP):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PARTNERSHIP):
    case SUCCESS(ACTION_TYPES.UPDATE_PARTNERSHIP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PARTNERSHIP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/partnerships';

// Actions

export const getEntities: ICrudGetAllAction<IPartnership> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PARTNERSHIP_LIST,
  payload: axios.get<IPartnership>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IPartnership> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PARTNERSHIP,
    payload: axios.get<IPartnership>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPartnership> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PARTNERSHIP,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPartnership> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PARTNERSHIP,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPartnership> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PARTNERSHIP,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
