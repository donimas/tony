import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './partnership.reducer';
import { IPartnership } from 'app/shared/model/partnership.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPartnershipDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PartnershipDetail = (props: IPartnershipDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { partnershipEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.partnership.detail.title">Partnership</Translate> [<b>{partnershipEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <Translate contentKey="ronyApp.partnership.type">Type</Translate>
          </dt>
          <dd>{partnershipEntity.typeId ? partnershipEntity.typeId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.partnership.contractor">Contractor</Translate>
          </dt>
          <dd>{partnershipEntity.contractorId ? partnershipEntity.contractorId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.partnership.contract">Contract</Translate>
          </dt>
          <dd>{partnershipEntity.contractId ? partnershipEntity.contractId : ''}</dd>
        </dl>
        <Button tag={Link} to="/partnership" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/partnership/${partnershipEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ partnership }: IRootState) => ({
  partnershipEntity: partnership.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PartnershipDetail);
