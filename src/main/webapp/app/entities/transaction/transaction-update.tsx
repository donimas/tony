import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ICurrency } from 'app/shared/model/currency.model';
import { getEntities as getCurrencies } from 'app/entities/currency/currency.reducer';
import { IContract } from 'app/shared/model/contract.model';
import { getEntities as getContracts } from 'app/entities/contract/contract.reducer';
import { getEntity, updateEntity, createEntity, reset } from './transaction.reducer';
import { ITransaction } from 'app/shared/model/transaction.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITransactionUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TransactionUpdate = (props: ITransactionUpdateProps) => {
  const [currencyId, setCurrencyId] = useState('0');
  const [contractId, setContractId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { transactionEntity, currencies, contracts, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/transaction' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getCurrencies();
    props.getContracts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.payDate = convertDateTimeToServer(values.payDate);
    values.createDate = convertDateTimeToServer(values.createDate);

    if (errors.length === 0) {
      const entity = {
        ...transactionEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ronyApp.transaction.home.createOrEditLabel">
            <Translate contentKey="ronyApp.transaction.home.createOrEditLabel">Create or edit a Transaction</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : transactionEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="transaction-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="transaction-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="planningSumLabel" for="transaction-planningSum">
                  <Translate contentKey="ronyApp.transaction.planningSum">Planning Sum</Translate>
                </Label>
                <AvField id="transaction-planningSum" type="text" name="planningSum" />
              </AvGroup>
              <AvGroup>
                <Label id="actualSumLabel" for="transaction-actualSum">
                  <Translate contentKey="ronyApp.transaction.actualSum">Actual Sum</Translate>
                </Label>
                <AvField id="transaction-actualSum" type="text" name="actualSum" />
              </AvGroup>
              <AvGroup>
                <Label id="convertedSumLabel" for="transaction-convertedSum">
                  <Translate contentKey="ronyApp.transaction.convertedSum">Converted Sum</Translate>
                </Label>
                <AvField id="transaction-convertedSum" type="text" name="convertedSum" />
              </AvGroup>
              <AvGroup>
                <Label id="payDateLabel" for="transaction-payDate">
                  <Translate contentKey="ronyApp.transaction.payDate">Pay Date</Translate>
                </Label>
                <AvInput
                  id="transaction-payDate"
                  type="datetime-local"
                  className="form-control"
                  name="payDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.transactionEntity.payDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="noteLabel" for="transaction-note">
                  <Translate contentKey="ronyApp.transaction.note">Note</Translate>
                </Label>
                <AvField id="transaction-note" type="text" name="note" />
              </AvGroup>
              <AvGroup>
                <Label id="statusLabel" for="transaction-status">
                  <Translate contentKey="ronyApp.transaction.status">Status</Translate>
                </Label>
                <AvInput
                  id="transaction-status"
                  type="select"
                  className="form-control"
                  name="status"
                  value={(!isNew && transactionEntity.status) || 'PLANNING'}
                >
                  <option value="PLANNING">{translate('ronyApp.TransactionStatus.PLANNING')}</option>
                  <option value="DECLINED">{translate('ronyApp.TransactionStatus.DECLINED')}</option>
                  <option value="PAID">{translate('ronyApp.TransactionStatus.PAID')}</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="directionLabel" for="transaction-direction">
                  <Translate contentKey="ronyApp.transaction.direction">Direction</Translate>
                </Label>
                <AvInput
                  id="transaction-direction"
                  type="select"
                  className="form-control"
                  name="direction"
                  value={(!isNew && transactionEntity.direction) || 'EMPTY'}
                >
                  <option value="EMPTY">{translate('ronyApp.DirectionType.EMPTY')}</option>
                  <option value="INCOME">{translate('ronyApp.DirectionType.INCOME')}</option>
                  <option value="EXPENSE">{translate('ronyApp.DirectionType.EXPENSE')}</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="createDateLabel" for="transaction-createDate">
                  <Translate contentKey="ronyApp.transaction.createDate">Create Date</Translate>
                </Label>
                <AvInput
                  id="transaction-createDate"
                  type="datetime-local"
                  className="form-control"
                  name="createDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.transactionEntity.createDate)}
                />
              </AvGroup>
              <AvGroup check>
                <Label id="flagDeletedLabel">
                  <AvInput id="transaction-flagDeleted" type="checkbox" className="form-check-input" name="flagDeleted" />
                  <Translate contentKey="ronyApp.transaction.flagDeleted">Flag Deleted</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label for="transaction-currency">
                  <Translate contentKey="ronyApp.transaction.currency">Currency</Translate>
                </Label>
                <AvInput id="transaction-currency" type="select" className="form-control" name="currencyId">
                  <option value="" key="0" />
                  {currencies
                    ? currencies.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="transaction-contract">
                  <Translate contentKey="ronyApp.transaction.contract">Contract</Translate>
                </Label>
                <AvInput id="transaction-contract" type="select" className="form-control" name="contractId">
                  <option value="" key="0" />
                  {contracts
                    ? contracts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/transaction" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  currencies: storeState.currency.entities,
  contracts: storeState.contract.entities,
  transactionEntity: storeState.transaction.entity,
  loading: storeState.transaction.loading,
  updating: storeState.transaction.updating,
  updateSuccess: storeState.transaction.updateSuccess,
});

const mapDispatchToProps = {
  getCurrencies,
  getContracts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TransactionUpdate);
