import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './transaction.reducer';
import { ITransaction } from 'app/shared/model/transaction.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITransactionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TransactionDetail = (props: ITransactionDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { transactionEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.transaction.detail.title">Transaction</Translate> [<b>{transactionEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="planningSum">
              <Translate contentKey="ronyApp.transaction.planningSum">Planning Sum</Translate>
            </span>
          </dt>
          <dd>{transactionEntity.planningSum}</dd>
          <dt>
            <span id="actualSum">
              <Translate contentKey="ronyApp.transaction.actualSum">Actual Sum</Translate>
            </span>
          </dt>
          <dd>{transactionEntity.actualSum}</dd>
          <dt>
            <span id="convertedSum">
              <Translate contentKey="ronyApp.transaction.convertedSum">Converted Sum</Translate>
            </span>
          </dt>
          <dd>{transactionEntity.convertedSum}</dd>
          <dt>
            <span id="payDate">
              <Translate contentKey="ronyApp.transaction.payDate">Pay Date</Translate>
            </span>
          </dt>
          <dd>
            {transactionEntity.payDate ? <TextFormat value={transactionEntity.payDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="note">
              <Translate contentKey="ronyApp.transaction.note">Note</Translate>
            </span>
          </dt>
          <dd>{transactionEntity.note}</dd>
          <dt>
            <span id="status">
              <Translate contentKey="ronyApp.transaction.status">Status</Translate>
            </span>
          </dt>
          <dd>{transactionEntity.status}</dd>
          <dt>
            <span id="direction">
              <Translate contentKey="ronyApp.transaction.direction">Direction</Translate>
            </span>
          </dt>
          <dd>{transactionEntity.direction}</dd>
          <dt>
            <span id="createDate">
              <Translate contentKey="ronyApp.transaction.createDate">Create Date</Translate>
            </span>
          </dt>
          <dd>
            {transactionEntity.createDate ? <TextFormat value={transactionEntity.createDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="flagDeleted">
              <Translate contentKey="ronyApp.transaction.flagDeleted">Flag Deleted</Translate>
            </span>
          </dt>
          <dd>{transactionEntity.flagDeleted ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="ronyApp.transaction.currency">Currency</Translate>
          </dt>
          <dd>{transactionEntity.currencyId ? transactionEntity.currencyId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.transaction.contract">Contract</Translate>
          </dt>
          <dd>{transactionEntity.contractId ? transactionEntity.contractId : ''}</dd>
        </dl>
        <Button tag={Link} to="/transaction" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/transaction/${transactionEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ transaction }: IRootState) => ({
  transactionEntity: transaction.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TransactionDetail);
