import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProjectTeam from './project-team';
import ProjectTeamDetail from './project-team-detail';
import ProjectTeamUpdate from './project-team-update';
import ProjectTeamDeleteDialog from './project-team-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProjectTeamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProjectTeamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProjectTeamDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProjectTeam} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ProjectTeamDeleteDialog} />
  </>
);

export default Routes;
