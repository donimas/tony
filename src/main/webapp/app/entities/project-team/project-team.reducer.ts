import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProjectTeam, defaultValue } from 'app/shared/model/project-team.model';

export const ACTION_TYPES = {
  FETCH_PROJECTTEAM_LIST: 'projectTeam/FETCH_PROJECTTEAM_LIST',
  FETCH_PROJECTTEAM: 'projectTeam/FETCH_PROJECTTEAM',
  CREATE_PROJECTTEAM: 'projectTeam/CREATE_PROJECTTEAM',
  UPDATE_PROJECTTEAM: 'projectTeam/UPDATE_PROJECTTEAM',
  DELETE_PROJECTTEAM: 'projectTeam/DELETE_PROJECTTEAM',
  RESET: 'projectTeam/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProjectTeam>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ProjectTeamState = Readonly<typeof initialState>;

// Reducer

export default (state: ProjectTeamState = initialState, action): ProjectTeamState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROJECTTEAM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROJECTTEAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PROJECTTEAM):
    case REQUEST(ACTION_TYPES.UPDATE_PROJECTTEAM):
    case REQUEST(ACTION_TYPES.DELETE_PROJECTTEAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PROJECTTEAM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROJECTTEAM):
    case FAILURE(ACTION_TYPES.CREATE_PROJECTTEAM):
    case FAILURE(ACTION_TYPES.UPDATE_PROJECTTEAM):
    case FAILURE(ACTION_TYPES.DELETE_PROJECTTEAM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROJECTTEAM_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROJECTTEAM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROJECTTEAM):
    case SUCCESS(ACTION_TYPES.UPDATE_PROJECTTEAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROJECTTEAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/project-teams';

// Actions

export const getEntities: ICrudGetAllAction<IProjectTeam> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROJECTTEAM_LIST,
    payload: axios.get<IProjectTeam>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IProjectTeam> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROJECTTEAM,
    payload: axios.get<IProjectTeam>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IProjectTeam> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROJECTTEAM,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProjectTeam> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROJECTTEAM,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProjectTeam> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROJECTTEAM,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
