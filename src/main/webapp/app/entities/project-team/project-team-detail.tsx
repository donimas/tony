import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './project-team.reducer';
import { IProjectTeam } from 'app/shared/model/project-team.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProjectTeamDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProjectTeamDetail = (props: IProjectTeamDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { projectTeamEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.projectTeam.detail.title">ProjectTeam</Translate> [<b>{projectTeamEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="flagDeleted">
              <Translate contentKey="ronyApp.projectTeam.flagDeleted">Flag Deleted</Translate>
            </span>
          </dt>
          <dd>{projectTeamEntity.flagDeleted ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="ronyApp.projectTeam.lead">Lead</Translate>
          </dt>
          <dd>{projectTeamEntity.leadId ? projectTeamEntity.leadId : ''}</dd>
        </dl>
        <Button tag={Link} to="/project-team" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/project-team/${projectTeamEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ projectTeam }: IRootState) => ({
  projectTeamEntity: projectTeam.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProjectTeamDetail);
