import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './project-status-history.reducer';
import { IProjectStatusHistory } from 'app/shared/model/project-status-history.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProjectStatusHistoryDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProjectStatusHistoryDetail = (props: IProjectStatusHistoryDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { projectStatusHistoryEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.projectStatusHistory.detail.title">ProjectStatusHistory</Translate> [
          <b>{projectStatusHistoryEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="comment">
              <Translate contentKey="ronyApp.projectStatusHistory.comment">Comment</Translate>
            </span>
          </dt>
          <dd>{projectStatusHistoryEntity.comment}</dd>
          <dt>
            <span id="createDate">
              <Translate contentKey="ronyApp.projectStatusHistory.createDate">Create Date</Translate>
            </span>
          </dt>
          <dd>
            {projectStatusHistoryEntity.createDate ? (
              <TextFormat value={projectStatusHistoryEntity.createDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="ronyApp.projectStatusHistory.status">Status</Translate>
          </dt>
          <dd>{projectStatusHistoryEntity.statusId ? projectStatusHistoryEntity.statusId : ''}</dd>
          <dt>
            <Translate contentKey="ronyApp.projectStatusHistory.project">Project</Translate>
          </dt>
          <dd>{projectStatusHistoryEntity.projectId ? projectStatusHistoryEntity.projectId : ''}</dd>
        </dl>
        <Button tag={Link} to="/project-status-history" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/project-status-history/${projectStatusHistoryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ projectStatusHistory }: IRootState) => ({
  projectStatusHistoryEntity: projectStatusHistory.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProjectStatusHistoryDetail);
