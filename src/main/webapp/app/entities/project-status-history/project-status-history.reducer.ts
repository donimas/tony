import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProjectStatusHistory, defaultValue } from 'app/shared/model/project-status-history.model';

export const ACTION_TYPES = {
  FETCH_PROJECTSTATUSHISTORY_LIST: 'projectStatusHistory/FETCH_PROJECTSTATUSHISTORY_LIST',
  FETCH_PROJECTSTATUSHISTORY: 'projectStatusHistory/FETCH_PROJECTSTATUSHISTORY',
  CREATE_PROJECTSTATUSHISTORY: 'projectStatusHistory/CREATE_PROJECTSTATUSHISTORY',
  UPDATE_PROJECTSTATUSHISTORY: 'projectStatusHistory/UPDATE_PROJECTSTATUSHISTORY',
  DELETE_PROJECTSTATUSHISTORY: 'projectStatusHistory/DELETE_PROJECTSTATUSHISTORY',
  RESET: 'projectStatusHistory/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProjectStatusHistory>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ProjectStatusHistoryState = Readonly<typeof initialState>;

// Reducer

export default (state: ProjectStatusHistoryState = initialState, action): ProjectStatusHistoryState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROJECTSTATUSHISTORY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROJECTSTATUSHISTORY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PROJECTSTATUSHISTORY):
    case REQUEST(ACTION_TYPES.UPDATE_PROJECTSTATUSHISTORY):
    case REQUEST(ACTION_TYPES.DELETE_PROJECTSTATUSHISTORY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PROJECTSTATUSHISTORY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROJECTSTATUSHISTORY):
    case FAILURE(ACTION_TYPES.CREATE_PROJECTSTATUSHISTORY):
    case FAILURE(ACTION_TYPES.UPDATE_PROJECTSTATUSHISTORY):
    case FAILURE(ACTION_TYPES.DELETE_PROJECTSTATUSHISTORY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROJECTSTATUSHISTORY_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROJECTSTATUSHISTORY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROJECTSTATUSHISTORY):
    case SUCCESS(ACTION_TYPES.UPDATE_PROJECTSTATUSHISTORY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROJECTSTATUSHISTORY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/project-status-histories';

// Actions

export const getEntities: ICrudGetAllAction<IProjectStatusHistory> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PROJECTSTATUSHISTORY_LIST,
  payload: axios.get<IProjectStatusHistory>(`${apiUrl}/own?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IProjectStatusHistory> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROJECTSTATUSHISTORY,
    payload: axios.get<IProjectStatusHistory>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IProjectStatusHistory> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROJECTSTATUSHISTORY,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProjectStatusHistory> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROJECTSTATUSHISTORY,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProjectStatusHistory> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROJECTSTATUSHISTORY,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
