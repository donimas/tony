import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './project-status-history.reducer';
import { IProjectStatusHistory } from 'app/shared/model/project-status-history.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProjectStatusHistoryProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const ProjectStatusHistory = (props: IProjectStatusHistoryProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { projectStatusHistoryList, match, loading } = props;
  return (
    <div>
      <h2 id="project-status-history-heading">
        <Translate contentKey="ronyApp.projectStatusHistory.home.title">Project Status Histories</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="ronyApp.projectStatusHistory.home.createLabel">Create new Project Status History</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {projectStatusHistoryList && projectStatusHistoryList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.projectStatusHistory.comment">Comment</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.projectStatusHistory.createDate">Create Date</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.projectStatusHistory.status">Status</Translate>
                </th>
                <th>
                  <Translate contentKey="ronyApp.projectStatusHistory.project">Project</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {projectStatusHistoryList.map((projectStatusHistory, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${projectStatusHistory.id}`} color="link" size="sm">
                      {projectStatusHistory.id}
                    </Button>
                  </td>
                  <td>{projectStatusHistory.comment}</td>
                  <td>
                    {projectStatusHistory.createDate ? (
                      <TextFormat type="date" value={projectStatusHistory.createDate} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {projectStatusHistory.statusId ? (
                      <Link to={`dictionary/${projectStatusHistory.statusId}`}>{projectStatusHistory.statusType}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {projectStatusHistory.projectId ? (
                      <Link to={`project/${projectStatusHistory.projectId}`}>{projectStatusHistory.projectId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${projectStatusHistory.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${projectStatusHistory.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${projectStatusHistory.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="ronyApp.projectStatusHistory.home.notFound">No Project Status Histories found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ projectStatusHistory }: IRootState) => ({
  projectStatusHistoryList: projectStatusHistory.entities,
  loading: projectStatusHistory.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProjectStatusHistory);
