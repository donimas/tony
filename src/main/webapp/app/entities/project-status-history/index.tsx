import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProjectStatusHistory from './project-status-history';
import ProjectStatusHistoryDetail from './project-status-history-detail';
import ProjectStatusHistoryUpdate from './project-status-history-update';
import ProjectStatusHistoryDeleteDialog from './project-status-history-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProjectStatusHistoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProjectStatusHistoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProjectStatusHistoryDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProjectStatusHistory} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ProjectStatusHistoryDeleteDialog} />
  </>
);

export default Routes;
