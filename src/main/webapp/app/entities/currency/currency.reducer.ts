import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ICurrency, defaultValue } from 'app/shared/model/currency.model';
import { CURRENCY_API_KEY, CURRENCY_API_URL } from 'app/shared/util/constants';

export const ACTION_TYPES = {
  FETCH_CURRENCY_LIST: 'currency/FETCH_CURRENCY_LIST',
  FETCH_CURRENCY_SYMBOLS: 'currency/FETCH_CURRENCY_SYMBOLS',
  FETCH_CURRENCY_RATES: 'currency/FETCH_CURRENCY_RATES',
  FETCH_CURRENCY: 'currency/FETCH_CURRENCY',
  CREATE_CURRENCY: 'currency/CREATE_CURRENCY',
  UPDATE_CURRENCY: 'currency/UPDATE_CURRENCY',
  DELETE_CURRENCY: 'currency/DELETE_CURRENCY',
  RESET: 'currency/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICurrency>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,

  symbols: [],
  rates: [],
};

export type CurrencyState = Readonly<typeof initialState>;

// Reducer

export default (state: CurrencyState = initialState, action): CurrencyState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CURRENCY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CURRENCY_SYMBOLS):
    case REQUEST(ACTION_TYPES.FETCH_CURRENCY_RATES):
    case REQUEST(ACTION_TYPES.FETCH_CURRENCY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CURRENCY):
    case REQUEST(ACTION_TYPES.UPDATE_CURRENCY):
    case REQUEST(ACTION_TYPES.DELETE_CURRENCY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CURRENCY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CURRENCY_SYMBOLS):
    case FAILURE(ACTION_TYPES.FETCH_CURRENCY_RATES):
    case FAILURE(ACTION_TYPES.FETCH_CURRENCY):
    case FAILURE(ACTION_TYPES.CREATE_CURRENCY):
    case FAILURE(ACTION_TYPES.UPDATE_CURRENCY):
    case FAILURE(ACTION_TYPES.DELETE_CURRENCY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CURRENCY_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_CURRENCY_SYMBOLS):
      return {
        ...state,
        loading: false,
        symbols: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CURRENCY_RATES):
      return {
        ...state,
        loading: false,
        rates: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CURRENCY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CURRENCY):
    case SUCCESS(ACTION_TYPES.UPDATE_CURRENCY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CURRENCY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/currencies';
const currencyApiUrl = 'http://data.fixer.io/api/symbols';

// Actions

export const getEntities: ICrudGetAllAction<ICurrency> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CURRENCY_LIST,
    payload: axios.get<ICurrency>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};
export const getAllSymbols: any = () => {
  const requestUrl = `${apiUrl}/symbols`;
  return {
    type: ACTION_TYPES.FETCH_CURRENCY_SYMBOLS,
    payload: axios.get<any>(`${requestUrl}`),
  };
};
export const getCurrencyRates: any = (base: string, symbols: []) => {
  const toSymbols = symbols.map(s => s).join(',');
  const requestUrl = `${CURRENCY_API_URL}/latest?access_key=${CURRENCY_API_KEY}&base=${base}&symbols=${toSymbols}`;
  return {
    type: ACTION_TYPES.FETCH_CURRENCY_RATES,
    payload: axios.get<any>(`${requestUrl}`),
  };
};

export const getEntity: ICrudGetAction<ICurrency> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CURRENCY,
    payload: axios.get<ICurrency>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ICurrency> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CURRENCY,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};
export const createCurrency = entity => {
  return {
    type: ACTION_TYPES.CREATE_CURRENCY,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  };
};

export const updateEntity: ICrudPutAction<ICurrency> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CURRENCY,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICurrency> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CURRENCY,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
