import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './currency.reducer';
import { ICurrency } from 'app/shared/model/currency.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICurrencyDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CurrencyDetail = (props: ICurrencyDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { currencyEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.currency.detail.title">Currency</Translate> [<b>{currencyEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="formula">
              <Translate contentKey="ronyApp.currency.formula">Formula</Translate>
            </span>
          </dt>
          <dd>{currencyEntity.formula}</dd>
          <dt>
            <Translate contentKey="ronyApp.currency.title">Title</Translate>
          </dt>
          <dd>{currencyEntity.titleId ? currencyEntity.titleId : ''}</dd>
        </dl>
        <Button tag={Link} to="/currency" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/currency/${currencyEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ currency }: IRootState) => ({
  currencyEntity: currency.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyDetail);
