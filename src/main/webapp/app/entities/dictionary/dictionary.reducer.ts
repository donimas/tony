import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IDictionary, defaultValue } from 'app/shared/model/dictionary.model';

export const ACTION_TYPES = {
  FETCH_DICTIONARY_LIST: 'dictionary/FETCH_DICTIONARY_LIST',
  FETCH_DICTIONARY_LIST_PROJECT_STATUS: 'dictionary/FETCH_DICTIONARY_LIST_PROJECT_STATUS',
  FETCH_DICTIONARY_LIST_TECH_STACK: 'dictionary/FETCH_DICTIONARY_LIST_TECH_STACK',
  FETCH_DICTIONARY_LIST_AGREEMENT: 'dictionary/FETCH_DICTIONARY_LIST_AGREEMENT',
  FETCH_DICTIONARY_LIST_CONTRACT_STATUS: 'dictionary/FETCH_DICTIONARY_LIST_CONTRACT_STATUS',
  FETCH_DICTIONARY_LIST_CONTRACT_SALES_TYPE: 'dictionary/FETCH_DICTIONARY_LIST_CONTRACT_SALES_TYPE',
  FETCH_DICTIONARY_LIST_CURRENCY: 'dictionary/FETCH_DICTIONARY_LIST_CURRENCY',
  FETCH_DICTIONARY_LIST_STATUS: 'dictionary/FETCH_DICTIONARY_LIST_STATUS',
  FETCH_DICTIONARY: 'dictionary/FETCH_DICTIONARY',
  CREATE_DICTIONARY: 'dictionary/CREATE_DICTIONARY',
  UPDATE_DICTIONARY: 'dictionary/UPDATE_DICTIONARY',
  DELETE_DICTIONARY: 'dictionary/DELETE_DICTIONARY',
  RESET: 'dictionary/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IDictionary>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
  techStackDictionary: [],
  agreementDictionary: [],

  contractStatus: [],
  contractSalesType: [],
  currencies: [],
  items: [],
};

export type DictionaryState = Readonly<typeof initialState>;

// Reducer

export default (state: DictionaryState = initialState, action): DictionaryState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_DICTIONARY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_DICTIONARY_LIST_PROJECT_STATUS):
    case REQUEST(ACTION_TYPES.FETCH_DICTIONARY_LIST_TECH_STACK):
    case REQUEST(ACTION_TYPES.FETCH_DICTIONARY_LIST_AGREEMENT):
    case REQUEST(ACTION_TYPES.FETCH_DICTIONARY_LIST_CONTRACT_SALES_TYPE):
    case REQUEST(ACTION_TYPES.FETCH_DICTIONARY_LIST_CONTRACT_STATUS):
    case REQUEST(ACTION_TYPES.FETCH_DICTIONARY_LIST_CURRENCY):
    case REQUEST(ACTION_TYPES.FETCH_DICTIONARY_LIST_STATUS):
    case REQUEST(ACTION_TYPES.FETCH_DICTIONARY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_DICTIONARY):
    case REQUEST(ACTION_TYPES.UPDATE_DICTIONARY):
    case REQUEST(ACTION_TYPES.DELETE_DICTIONARY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_DICTIONARY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_DICTIONARY_LIST_PROJECT_STATUS):
    case FAILURE(ACTION_TYPES.FETCH_DICTIONARY_LIST_TECH_STACK):
    case FAILURE(ACTION_TYPES.FETCH_DICTIONARY_LIST_AGREEMENT):
    case FAILURE(ACTION_TYPES.FETCH_DICTIONARY_LIST_CONTRACT_SALES_TYPE):
    case FAILURE(ACTION_TYPES.FETCH_DICTIONARY_LIST_CONTRACT_STATUS):
    case FAILURE(ACTION_TYPES.FETCH_DICTIONARY_LIST_CURRENCY):
    case FAILURE(ACTION_TYPES.FETCH_DICTIONARY_LIST_STATUS):
    case FAILURE(ACTION_TYPES.FETCH_DICTIONARY):
    case FAILURE(ACTION_TYPES.CREATE_DICTIONARY):
    case FAILURE(ACTION_TYPES.UPDATE_DICTIONARY):
    case FAILURE(ACTION_TYPES.DELETE_DICTIONARY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICTIONARY_LIST):
    case SUCCESS(ACTION_TYPES.FETCH_DICTIONARY_LIST_PROJECT_STATUS):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICTIONARY_LIST_TECH_STACK):
      return {
        ...state,
        loading: false,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
        techStackDictionary: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICTIONARY_LIST_AGREEMENT):
      return {
        ...state,
        loading: false,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
        agreementDictionary: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICTIONARY_LIST_CONTRACT_STATUS):
      return {
        ...state,
        loading: false,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
        contractStatus: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICTIONARY_LIST_CONTRACT_SALES_TYPE):
      return {
        ...state,
        loading: false,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
        contractSalesType: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICTIONARY_LIST_CURRENCY):
      return {
        ...state,
        loading: false,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
        currencies: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICTIONARY_LIST_STATUS):
      return {
        ...state,
        loading: false,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
        items: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICTIONARY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_DICTIONARY):
    case SUCCESS(ACTION_TYPES.UPDATE_DICTIONARY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_DICTIONARY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/dictionaries';

// Actions

export const getEntities: ICrudGetAllAction<IDictionary> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_DICTIONARY_LIST,
    payload: axios.get<IDictionary>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};
export const getEntitiesProjectStatus: ICrudGetAllAction<IDictionary> = (page, size, sort) => {
  const requestUrl = `${apiUrl.replace('dictionaries', 'dict')}/projectStatus`;
  return {
    type: ACTION_TYPES.FETCH_DICTIONARY_LIST_PROJECT_STATUS,
    payload: axios.get<IDictionary>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};
export const getEntitiesTechStack: ICrudGetAllAction<IDictionary> = (page, size, sort) => {
  const requestUrl = `${apiUrl.replace('dictionaries', 'dict')}/techStack`;
  return {
    type: ACTION_TYPES.FETCH_DICTIONARY_LIST_TECH_STACK,
    payload: axios.get<IDictionary>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};
export const getEntitiesAgreement: ICrudGetAllAction<IDictionary> = (page, size, sort) => {
  const requestUrl = `${apiUrl.replace('dictionaries', 'dict')}/agreementStatus`;
  return {
    type: ACTION_TYPES.FETCH_DICTIONARY_LIST_AGREEMENT,
    payload: axios.get<IDictionary>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};
export const getEntitiesContractStatus = () => {
  const requestUrl = `${apiUrl.replace('dictionaries', 'dict')}/contractStatus`;
  return {
    type: ACTION_TYPES.FETCH_DICTIONARY_LIST_CONTRACT_STATUS,
    payload: axios.get<IDictionary>(`${requestUrl}?cacheBuster=${new Date().getTime()}`),
  };
};
export const getEntitiesContractSalesType: ICrudGetAllAction<IDictionary> = (page, size, sort) => {
  const requestUrl = `${apiUrl.replace('dictionaries', 'dict')}/contractSalesType`;
  return {
    type: ACTION_TYPES.FETCH_DICTIONARY_LIST_CONTRACT_SALES_TYPE,
    payload: axios.get<IDictionary>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};
export const getEntitiesCurrency: ICrudGetAllAction<IDictionary> = (page, size, sort) => {
  const requestUrl = `${apiUrl.replace('dictionaries', 'dict')}/currency`;
  return {
    type: ACTION_TYPES.FETCH_DICTIONARY_LIST_CURRENCY,
    payload: axios.get<IDictionary>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};
export const getEntitiesByName = (entityName: string) => {
  const requestUrl = `${apiUrl.replace('dictionaries', 'dict')}/name/${entityName}`;
  return {
    type: ACTION_TYPES.FETCH_DICTIONARY_LIST_STATUS,
    payload: axios.get<IDictionary>(`${requestUrl}?cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IDictionary> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_DICTIONARY,
    payload: axios.get<IDictionary>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IDictionary> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_DICTIONARY,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};
export const createDict = entity => {
  return {
    type: ACTION_TYPES.CREATE_DICTIONARY,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  };
};

export const updateEntity: ICrudPutAction<IDictionary> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_DICTIONARY,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IDictionary> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_DICTIONARY,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
