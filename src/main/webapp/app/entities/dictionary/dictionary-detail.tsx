import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './dictionary.reducer';
import { IDictionary } from 'app/shared/model/dictionary.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDictionaryDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DictionaryDetail = (props: IDictionaryDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { dictionaryEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ronyApp.dictionary.detail.title">Dictionary</Translate> [<b>{dictionaryEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="code">
              <Translate contentKey="ronyApp.dictionary.code">Code</Translate>
            </span>
          </dt>
          <dd>{dictionaryEntity.code}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="ronyApp.dictionary.name">Name</Translate>
            </span>
          </dt>
          <dd>{dictionaryEntity.name}</dd>
          <dt>
            <span id="entityName">
              <Translate contentKey="ronyApp.dictionary.entityName">Entity Name</Translate>
            </span>
          </dt>
          <dd>{dictionaryEntity.entityName}</dd>
          <dt>
            <span id="flagDeleted">
              <Translate contentKey="ronyApp.dictionary.flagDeleted">Flag Deleted</Translate>
            </span>
          </dt>
          <dd>{dictionaryEntity.flagDeleted ? 'true' : 'false'}</dd>
        </dl>
        <Button tag={Link} to="/dictionary" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/dictionary/${dictionaryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ dictionary }: IRootState) => ({
  dictionaryEntity: dictionary.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DictionaryDetail);
