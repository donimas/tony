import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Col, Label} from 'reactstrap';
import {AvFeedback, AvForm, AvGroup, AvInput, AvField} from 'availity-reactstrap-validation';
import {Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';

import {getEntity, updateEntity, createEntity, reset} from './dictionary.reducer';
import {IDictionary} from 'app/shared/model/dictionary.model';
import {convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime} from 'app/shared/util/date-utils';
import {mapIdList} from 'app/shared/util/entity-utils';

import Select from 'react-select';

export interface IDictionaryUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const DictionaryUpdate = (props: IDictionaryUpdateProps) => {
	const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);
	const [entityName, setEntityName] = useState('');
	
	const {dictionaryEntity, loading, updating} = props;
	
	const handleClose = () => {
		props.history.push('/dictionary' + props.location.search);
	};
	
	useEffect(() => {
		
		if (isNew) {
			props.reset();
		} else {
			props.getEntity(props.match.params.id);
		}
	}, []);
	
	useEffect(() => {
		if (props.updateSuccess) {
			handleClose();
		}
	}, [props.updateSuccess]);
	
	const saveEntity = (event, errors, values) => {
		if (errors.length === 0) {
			const entity = {
				...dictionaryEntity,
				...values,
				'entityName': entityName['value']
			};
			
			if (isNew) {
				props.createEntity(entity);
			} else {
				props.updateEntity(entity);
			}
		}
	};
	
	const options = [
		{value: 'ProjectStatusHistory', label: 'Статус проекта'},
		{value: 'AgreementStatusHistory', label: 'Статус соглашения'},
		{value: 'ContractStatusHistory', label: 'Статус договора'},
		{value: 'Currency', label: 'Валюта'},
		{value: 'TechStack', label: 'Тех. стэк'}
	];
	
	return (
		<div>
			<Row className="justify-content-center">
				<Col md="8">
					<h2 id="ronyApp.dictionary.home.createOrEditLabel">
						<Translate contentKey="ronyApp.dictionary.home.createOrEditLabel">Create or edit a
							Dictionary</Translate>
					</h2>
				</Col>
			</Row>
			<Row className="justify-content-center">
				<Col md="8">
					{loading ? (
						<p>Loading...</p>
					) : (
						<AvForm model={isNew ? {} : dictionaryEntity} onSubmit={saveEntity}>
							{!isNew ? (
								<AvGroup>
									<Label for="dictionary-id">
										<Translate contentKey="global.field.id">ID</Translate>
									</Label>
									<AvInput id="dictionary-id" type="text" className="form-control" name="id" required
											 readOnly/>
								</AvGroup>
							) : null}
							<AvGroup>
								<Label id="codeLabel" for="dictionary-code">
									<Translate contentKey="ronyApp.dictionary.code">Code</Translate>
								</Label>
								<AvField id="dictionary-code" type="text" name="code"/>
							</AvGroup>
							<AvGroup>
								<Label id="nameLabel" for="dictionary-name">
									<Translate contentKey="ronyApp.dictionary.name">Name</Translate>
								</Label>
								<AvField
									id="dictionary-name"
									type="text"
									name="name"
									validate={{
										required: {value: true, errorMessage: translate('entity.validation.required')},
									}}
								/>
							</AvGroup>
							<AvGroup>
								<Label id="entityNameLabel" for="dictionary-entityName">
									<Translate contentKey="ronyApp.dictionary.entityName">Entity Name</Translate>
								</Label>
								{/*<AvField id="dictionary-entityName" type="text" name="entityName" />*/}
								<Select
									value={entityName}
									onChange={setEntityName}
									options={options}
								/>
							</AvGroup>
							<AvGroup check>
								<Label id="flagDeletedLabel">
									<AvInput id="dictionary-flagDeleted" type="checkbox" className="form-check-input"
											 name="flagDeleted"/>
									<Translate contentKey="ronyApp.dictionary.flagDeleted">Flag Deleted</Translate>
								</Label>
							</AvGroup>
							<Button tag={Link} id="cancel-save" to="/dictionary" replace color="info">
								<FontAwesomeIcon icon="arrow-left"/>
								&nbsp;
								<span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
							</Button>
							&nbsp;
							<Button color="primary" id="save-entity" type="submit" disabled={updating}>
								<FontAwesomeIcon icon="save"/>
								&nbsp;
								<Translate contentKey="entity.action.save">Save</Translate>
							</Button>
						</AvForm>
					)}
				</Col>
			</Row>
		</div>
	);
};

const mapStateToProps = (storeState: IRootState) => ({
	dictionaryEntity: storeState.dictionary.entity,
	loading: storeState.dictionary.loading,
	updating: storeState.dictionary.updating,
	updateSuccess: storeState.dictionary.updateSuccess,
});

const mapDispatchToProps = {
	getEntity,
	updateEntity,
	createEntity,
	reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DictionaryUpdate);
