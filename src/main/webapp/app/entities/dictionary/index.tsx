import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Dictionary from './dictionary';
import DictionaryDetail from './dictionary-detail';
import DictionaryUpdate from './dictionary-update';
import DictionaryDeleteDialog from './dictionary-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DictionaryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DictionaryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DictionaryDetail} />
      <ErrorBoundaryRoute path={match.url} component={Dictionary} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={DictionaryDeleteDialog} />
  </>
);

export default Routes;
