package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.ProjectService
import kz.donimas.rony.service.dto.ProjectDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException
import java.time.ZonedDateTime
import javax.validation.Valid

private const val ENTITY_NAME = "project"
/**
 * REST controller for managing [kz.donimas.rony.domain.Project].
 */
@RestController
@RequestMapping("/api")
class ProjectResource(
    private val projectService: ProjectService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /projects` : Create a new project.
     *
     * @param projectDTO the projectDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new projectDTO, or with status `400 (Bad Request)` if the project has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/projects")
    fun createProject(@Valid @RequestBody projectDTO: ProjectDTO): ResponseEntity<ProjectDTO> {
        log.debug("REST request to save Project : $projectDTO")
        if (projectDTO.id != null) {
            throw BadRequestAlertException(
                "A new project cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        projectDTO.createDate = ZonedDateTime.now()
        val result = projectService.save(projectDTO)
        return ResponseEntity.created(URI("/api/projects/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /projects` : Updates an existing project.
     *
     * @param projectDTO the projectDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated projectDTO,
     * or with status `400 (Bad Request)` if the projectDTO is not valid,
     * or with status `500 (Internal Server Error)` if the projectDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/projects")
    fun updateProject(@Valid @RequestBody projectDTO: ProjectDTO): ResponseEntity<ProjectDTO> {
        log.debug("REST request to update Project : $projectDTO")
        if (projectDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = projectService.save(projectDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    projectDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /projects` : get all the projects.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the [ResponseEntity] with status `200 (OK)` and the list of projects in body.
     */
    @GetMapping("/projects")
    fun getAllProjects(pageable: Pageable, @RequestParam(required = false, defaultValue = "false") eagerload: Boolean): ResponseEntity<List<ProjectDTO>> {
        log.debug("REST request to get a page of Projects")
        val page: Page<ProjectDTO> = if (eagerload) {
            projectService.findAllWithEagerRelationships(pageable)
        } else {
            projectService.findAll(pageable)
        }
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /projects/:id` : get the "id" project.
     *
     * @param id the id of the projectDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the projectDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/projects/{id}")
    fun getProject(@PathVariable id: Long): ResponseEntity<ProjectDTO> {
        log.debug("REST request to get Project : $id")
        val projectDTO = projectService.findOne(id)
        return ResponseUtil.wrapOrNotFound(projectDTO)
    }
    /**
     *  `DELETE  /projects/:id` : delete the "id" project.
     *
     * @param id the id of the projectDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/projects/{id}")
    fun deleteProject(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Project : $id")

        projectService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
