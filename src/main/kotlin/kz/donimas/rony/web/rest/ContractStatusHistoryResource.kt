package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.ContractStatusHistoryService
import kz.donimas.rony.service.dto.ContractStatusHistoryDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "contractStatusHistory"
/**
 * REST controller for managing [kz.donimas.rony.domain.ContractStatusHistory].
 */
@RestController
@RequestMapping("/api")
class ContractStatusHistoryResource(
    private val contractStatusHistoryService: ContractStatusHistoryService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /contract-status-histories` : Create a new contractStatusHistory.
     *
     * @param contractStatusHistoryDTO the contractStatusHistoryDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new contractStatusHistoryDTO, or with status `400 (Bad Request)` if the contractStatusHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contract-status-histories")
    fun createContractStatusHistory(@RequestBody contractStatusHistoryDTO: ContractStatusHistoryDTO): ResponseEntity<ContractStatusHistoryDTO> {
        log.debug("REST request to save ContractStatusHistory : $contractStatusHistoryDTO")
        if (contractStatusHistoryDTO.id != null) {
            throw BadRequestAlertException(
                "A new contractStatusHistory cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = contractStatusHistoryService.save(contractStatusHistoryDTO)
        return ResponseEntity.created(URI("/api/contract-status-histories/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /contract-status-histories` : Updates an existing contractStatusHistory.
     *
     * @param contractStatusHistoryDTO the contractStatusHistoryDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated contractStatusHistoryDTO,
     * or with status `400 (Bad Request)` if the contractStatusHistoryDTO is not valid,
     * or with status `500 (Internal Server Error)` if the contractStatusHistoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contract-status-histories")
    fun updateContractStatusHistory(@RequestBody contractStatusHistoryDTO: ContractStatusHistoryDTO): ResponseEntity<ContractStatusHistoryDTO> {
        log.debug("REST request to update ContractStatusHistory : $contractStatusHistoryDTO")
        if (contractStatusHistoryDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = contractStatusHistoryService.save(contractStatusHistoryDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    contractStatusHistoryDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /contract-status-histories` : get all the contractStatusHistories.
     *

     * @return the [ResponseEntity] with status `200 (OK)` and the list of contractStatusHistories in body.
     */
    @GetMapping("/contract-status-histories")
    fun getAllContractStatusHistories(): MutableList<ContractStatusHistoryDTO> {
        log.debug("REST request to get all ContractStatusHistories")

        return contractStatusHistoryService.findAll()
    }

    /**
     * `GET  /contract-status-histories/:id` : get the "id" contractStatusHistory.
     *
     * @param id the id of the contractStatusHistoryDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the contractStatusHistoryDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/contract-status-histories/{id}")
    fun getContractStatusHistory(@PathVariable id: Long): ResponseEntity<ContractStatusHistoryDTO> {
        log.debug("REST request to get ContractStatusHistory : $id")
        val contractStatusHistoryDTO = contractStatusHistoryService.findOne(id)
        return ResponseUtil.wrapOrNotFound(contractStatusHistoryDTO)
    }
    /**
     *  `DELETE  /contract-status-histories/:id` : delete the "id" contractStatusHistory.
     *
     * @param id the id of the contractStatusHistoryDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/contract-status-histories/{id}")
    fun deleteContractStatusHistory(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete ContractStatusHistory : $id")

        contractStatusHistoryService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
