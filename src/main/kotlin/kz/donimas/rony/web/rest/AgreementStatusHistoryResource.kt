package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.AgreementStatusHistoryService
import kz.donimas.rony.service.dto.AgreementStatusHistoryDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "agreementStatusHistory"
/**
 * REST controller for managing [kz.donimas.rony.domain.AgreementStatusHistory].
 */
@RestController
@RequestMapping("/api")
class AgreementStatusHistoryResource(
    private val agreementStatusHistoryService: AgreementStatusHistoryService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /agreement-status-histories` : Create a new agreementStatusHistory.
     *
     * @param agreementStatusHistoryDTO the agreementStatusHistoryDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new agreementStatusHistoryDTO, or with status `400 (Bad Request)` if the agreementStatusHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agreement-status-histories")
    fun createAgreementStatusHistory(@RequestBody agreementStatusHistoryDTO: AgreementStatusHistoryDTO): ResponseEntity<AgreementStatusHistoryDTO> {
        log.debug("REST request to save AgreementStatusHistory : $agreementStatusHistoryDTO")
        if (agreementStatusHistoryDTO.id != null) {
            throw BadRequestAlertException(
                "A new agreementStatusHistory cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = agreementStatusHistoryService.save(agreementStatusHistoryDTO)
        return ResponseEntity.created(URI("/api/agreement-status-histories/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /agreement-status-histories` : Updates an existing agreementStatusHistory.
     *
     * @param agreementStatusHistoryDTO the agreementStatusHistoryDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated agreementStatusHistoryDTO,
     * or with status `400 (Bad Request)` if the agreementStatusHistoryDTO is not valid,
     * or with status `500 (Internal Server Error)` if the agreementStatusHistoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agreement-status-histories")
    fun updateAgreementStatusHistory(@RequestBody agreementStatusHistoryDTO: AgreementStatusHistoryDTO): ResponseEntity<AgreementStatusHistoryDTO> {
        log.debug("REST request to update AgreementStatusHistory : $agreementStatusHistoryDTO")
        if (agreementStatusHistoryDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = agreementStatusHistoryService.save(agreementStatusHistoryDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    agreementStatusHistoryDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /agreement-status-histories` : get all the agreementStatusHistories.
     *

     * @return the [ResponseEntity] with status `200 (OK)` and the list of agreementStatusHistories in body.
     */
    @GetMapping("/agreement-status-histories")
    fun getAllAgreementStatusHistories(): MutableList<AgreementStatusHistoryDTO> {
        log.debug("REST request to get all AgreementStatusHistories")

        return agreementStatusHistoryService.findAll()
    }

    /**
     * `GET  /agreement-status-histories/:id` : get the "id" agreementStatusHistory.
     *
     * @param id the id of the agreementStatusHistoryDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the agreementStatusHistoryDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/agreement-status-histories/{id}")
    fun getAgreementStatusHistory(@PathVariable id: Long): ResponseEntity<AgreementStatusHistoryDTO> {
        log.debug("REST request to get AgreementStatusHistory : $id")
        val agreementStatusHistoryDTO = agreementStatusHistoryService.findOne(id)
        return ResponseUtil.wrapOrNotFound(agreementStatusHistoryDTO)
    }
    /**
     *  `DELETE  /agreement-status-histories/:id` : delete the "id" agreementStatusHistory.
     *
     * @param id the id of the agreementStatusHistoryDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/agreement-status-histories/{id}")
    fun deleteAgreementStatusHistory(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete AgreementStatusHistory : $id")

        agreementStatusHistoryService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
