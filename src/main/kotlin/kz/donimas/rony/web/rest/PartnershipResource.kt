package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.PartnershipService
import kz.donimas.rony.service.dto.PartnershipDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "partnership"
/**
 * REST controller for managing [kz.donimas.rony.domain.Partnership].
 */
@RestController
@RequestMapping("/api")
class PartnershipResource(
    private val partnershipService: PartnershipService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /partnerships` : Create a new partnership.
     *
     * @param partnershipDTO the partnershipDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new partnershipDTO, or with status `400 (Bad Request)` if the partnership has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/partnerships")
    fun createPartnership(@RequestBody partnershipDTO: PartnershipDTO): ResponseEntity<PartnershipDTO> {
        log.debug("REST request to save Partnership : $partnershipDTO")
        if (partnershipDTO.id != null) {
            throw BadRequestAlertException(
                "A new partnership cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = partnershipService.save(partnershipDTO)
        return ResponseEntity.created(URI("/api/partnerships/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /partnerships` : Updates an existing partnership.
     *
     * @param partnershipDTO the partnershipDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated partnershipDTO,
     * or with status `400 (Bad Request)` if the partnershipDTO is not valid,
     * or with status `500 (Internal Server Error)` if the partnershipDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/partnerships")
    fun updatePartnership(@RequestBody partnershipDTO: PartnershipDTO): ResponseEntity<PartnershipDTO> {
        log.debug("REST request to update Partnership : $partnershipDTO")
        if (partnershipDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = partnershipService.save(partnershipDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    partnershipDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /partnerships` : get all the partnerships.
     *

     * @return the [ResponseEntity] with status `200 (OK)` and the list of partnerships in body.
     */
    @GetMapping("/partnerships")
    fun getAllPartnerships(): MutableList<PartnershipDTO> {
        log.debug("REST request to get all Partnerships")

        return partnershipService.findAll()
    }

    /**
     * `GET  /partnerships/:id` : get the "id" partnership.
     *
     * @param id the id of the partnershipDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the partnershipDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/partnerships/{id}")
    fun getPartnership(@PathVariable id: Long): ResponseEntity<PartnershipDTO> {
        log.debug("REST request to get Partnership : $id")
        val partnershipDTO = partnershipService.findOne(id)
        return ResponseUtil.wrapOrNotFound(partnershipDTO)
    }
    /**
     *  `DELETE  /partnerships/:id` : delete the "id" partnership.
     *
     * @param id the id of the partnershipDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/partnerships/{id}")
    fun deletePartnership(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Partnership : $id")

        partnershipService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
