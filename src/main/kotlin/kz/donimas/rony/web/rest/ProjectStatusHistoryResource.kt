package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.ProjectStatusHistoryService
import kz.donimas.rony.service.dto.ProjectStatusHistoryDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "projectStatusHistory"
/**
 * REST controller for managing [kz.donimas.rony.domain.ProjectStatusHistory].
 */
@RestController
@RequestMapping("/api")
class ProjectStatusHistoryResource(
    private val projectStatusHistoryService: ProjectStatusHistoryService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /project-status-histories` : Create a new projectStatusHistory.
     *
     * @param projectStatusHistoryDTO the projectStatusHistoryDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new projectStatusHistoryDTO, or with status `400 (Bad Request)` if the projectStatusHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/project-status-histories")
    fun createProjectStatusHistory(@RequestBody projectStatusHistoryDTO: ProjectStatusHistoryDTO): ResponseEntity<ProjectStatusHistoryDTO> {
        log.debug("REST request to save ProjectStatusHistory : $projectStatusHistoryDTO")
        if (projectStatusHistoryDTO.id != null) {
            throw BadRequestAlertException(
                "A new projectStatusHistory cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = projectStatusHistoryService.save(projectStatusHistoryDTO)
        return ResponseEntity.created(URI("/api/project-status-histories/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /project-status-histories` : Updates an existing projectStatusHistory.
     *
     * @param projectStatusHistoryDTO the projectStatusHistoryDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated projectStatusHistoryDTO,
     * or with status `400 (Bad Request)` if the projectStatusHistoryDTO is not valid,
     * or with status `500 (Internal Server Error)` if the projectStatusHistoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/project-status-histories")
    fun updateProjectStatusHistory(@RequestBody projectStatusHistoryDTO: ProjectStatusHistoryDTO): ResponseEntity<ProjectStatusHistoryDTO> {
        log.debug("REST request to update ProjectStatusHistory : $projectStatusHistoryDTO")
        if (projectStatusHistoryDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = projectStatusHistoryService.save(projectStatusHistoryDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    projectStatusHistoryDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /project-status-histories` : get all the projectStatusHistories.
     *

     * @return the [ResponseEntity] with status `200 (OK)` and the list of projectStatusHistories in body.
     */
    @GetMapping("/project-status-histories")
    fun getAllProjectStatusHistories(): MutableList<ProjectStatusHistoryDTO> {
        log.debug("REST request to get all ProjectStatusHistories")

        return projectStatusHistoryService.findAll()
    }

    /**
     * `GET  /project-status-histories/:id` : get the "id" projectStatusHistory.
     *
     * @param id the id of the projectStatusHistoryDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the projectStatusHistoryDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/project-status-histories/{id}")
    fun getProjectStatusHistory(@PathVariable id: Long): ResponseEntity<ProjectStatusHistoryDTO> {
        log.debug("REST request to get ProjectStatusHistory : $id")
        val projectStatusHistoryDTO = projectStatusHistoryService.findOne(id)
        return ResponseUtil.wrapOrNotFound(projectStatusHistoryDTO)
    }
    /**
     *  `DELETE  /project-status-histories/:id` : delete the "id" projectStatusHistory.
     *
     * @param id the id of the projectStatusHistoryDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/project-status-histories/{id}")
    fun deleteProjectStatusHistory(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete ProjectStatusHistory : $id")

        projectStatusHistoryService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }

    @GetMapping("/project-status-histories/own")
    fun getAllOwnProjectStatusHistories(): MutableList<ProjectStatusHistoryDTO> {
        log.debug("REST request to get all own ProjectStatusHistories")

        return projectStatusHistoryService.findAllOwn()
    }
}
