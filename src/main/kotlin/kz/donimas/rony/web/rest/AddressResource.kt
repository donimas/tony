package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.AddressService
import kz.donimas.rony.service.dto.AddressDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "address"
/**
 * REST controller for managing [kz.donimas.rony.domain.Address].
 */
@RestController
@RequestMapping("/api")
class AddressResource(
    private val addressService: AddressService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /addresses` : Create a new address.
     *
     * @param addressDTO the addressDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new addressDTO, or with status `400 (Bad Request)` if the address has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/addresses")
    fun createAddress(@RequestBody addressDTO: AddressDTO): ResponseEntity<AddressDTO> {
        log.debug("REST request to save Address : $addressDTO")
        if (addressDTO.id != null) {
            throw BadRequestAlertException(
                "A new address cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = addressService.save(addressDTO)
        return ResponseEntity.created(URI("/api/addresses/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /addresses` : Updates an existing address.
     *
     * @param addressDTO the addressDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated addressDTO,
     * or with status `400 (Bad Request)` if the addressDTO is not valid,
     * or with status `500 (Internal Server Error)` if the addressDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/addresses")
    fun updateAddress(@RequestBody addressDTO: AddressDTO): ResponseEntity<AddressDTO> {
        log.debug("REST request to update Address : $addressDTO")
        if (addressDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = addressService.save(addressDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    addressDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /addresses` : get all the addresses.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of addresses in body.
     */
    @GetMapping("/addresses")
    fun getAllAddresses(pageable: Pageable): ResponseEntity<List<AddressDTO>> {
        log.debug("REST request to get a page of Addresses")
        val page = addressService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /addresses/:id` : get the "id" address.
     *
     * @param id the id of the addressDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the addressDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/addresses/{id}")
    fun getAddress(@PathVariable id: Long): ResponseEntity<AddressDTO> {
        log.debug("REST request to get Address : $id")
        val addressDTO = addressService.findOne(id)
        return ResponseUtil.wrapOrNotFound(addressDTO)
    }
    /**
     *  `DELETE  /addresses/:id` : delete the "id" address.
     *
     * @param id the id of the addressDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/addresses/{id}")
    fun deleteAddress(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Address : $id")

        addressService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
