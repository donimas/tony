package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.ProjectTeamService
import kz.donimas.rony.service.dto.ProjectTeamDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "projectTeam"
/**
 * REST controller for managing [kz.donimas.rony.domain.ProjectTeam].
 */
@RestController
@RequestMapping("/api")
class ProjectTeamResource(
    private val projectTeamService: ProjectTeamService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /project-teams` : Create a new projectTeam.
     *
     * @param projectTeamDTO the projectTeamDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new projectTeamDTO, or with status `400 (Bad Request)` if the projectTeam has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/project-teams")
    fun createProjectTeam(@RequestBody projectTeamDTO: ProjectTeamDTO): ResponseEntity<ProjectTeamDTO> {
        log.debug("REST request to save ProjectTeam : $projectTeamDTO")
        if (projectTeamDTO.id != null) {
            throw BadRequestAlertException(
                "A new projectTeam cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = projectTeamService.save(projectTeamDTO)
        return ResponseEntity.created(URI("/api/project-teams/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /project-teams` : Updates an existing projectTeam.
     *
     * @param projectTeamDTO the projectTeamDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated projectTeamDTO,
     * or with status `400 (Bad Request)` if the projectTeamDTO is not valid,
     * or with status `500 (Internal Server Error)` if the projectTeamDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/project-teams")
    fun updateProjectTeam(@RequestBody projectTeamDTO: ProjectTeamDTO): ResponseEntity<ProjectTeamDTO> {
        log.debug("REST request to update ProjectTeam : $projectTeamDTO")
        if (projectTeamDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = projectTeamService.save(projectTeamDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    projectTeamDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /project-teams` : get all the projectTeams.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of projectTeams in body.
     */
    @GetMapping("/project-teams")
    fun getAllProjectTeams(pageable: Pageable): ResponseEntity<List<ProjectTeamDTO>> {
        log.debug("REST request to get a page of ProjectTeams")
        val page = projectTeamService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /project-teams/:id` : get the "id" projectTeam.
     *
     * @param id the id of the projectTeamDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the projectTeamDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/project-teams/{id}")
    fun getProjectTeam(@PathVariable id: Long): ResponseEntity<ProjectTeamDTO> {
        log.debug("REST request to get ProjectTeam : $id")
        val projectTeamDTO = projectTeamService.findOne(id)
        return ResponseUtil.wrapOrNotFound(projectTeamDTO)
    }
    /**
     *  `DELETE  /project-teams/:id` : delete the "id" projectTeam.
     *
     * @param id the id of the projectTeamDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/project-teams/{id}")
    fun deleteProjectTeam(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete ProjectTeam : $id")

        projectTeamService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
