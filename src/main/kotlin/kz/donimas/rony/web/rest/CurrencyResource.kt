package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.CurrencyService
import kz.donimas.rony.service.dto.CurrencyDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "currency"
/**
 * REST controller for managing [kz.donimas.rony.domain.Currency].
 */
@RestController
@RequestMapping("/api")
class CurrencyResource(
    private val currencyService: CurrencyService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /currencies` : Create a new currency.
     *
     * @param currencyDTO the currencyDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new currencyDTO, or with status `400 (Bad Request)` if the currency has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/currencies")
    fun createCurrency(@RequestBody currencyDTO: CurrencyDTO): ResponseEntity<CurrencyDTO> {
        log.debug("REST request to save Currency : $currencyDTO")
        if (currencyDTO.id != null) {
            throw BadRequestAlertException(
                "A new currency cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        if (currencyDTO.formula.isNullOrEmpty()) {
            throw BadRequestAlertException(
                "Formula is empty",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = currencyService.save(currencyDTO)
        return ResponseEntity.created(URI("/api/currencies/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /currencies` : Updates an existing currency.
     *
     * @param currencyDTO the currencyDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated currencyDTO,
     * or with status `400 (Bad Request)` if the currencyDTO is not valid,
     * or with status `500 (Internal Server Error)` if the currencyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/currencies")
    fun updateCurrency(@RequestBody currencyDTO: CurrencyDTO): ResponseEntity<CurrencyDTO> {
        log.debug("REST request to update Currency : $currencyDTO")
        if (currencyDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = currencyService.save(currencyDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    currencyDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /currencies` : get all the currencies.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of currencies in body.
     */
    @GetMapping("/currencies")
    fun getAllCurrencies(pageable: Pageable): ResponseEntity<List<CurrencyDTO>> {
        log.debug("REST request to get a page of Currencies")
        val page = currencyService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    @GetMapping("/currencies/symbols")
    fun getAllCurrencies(): MutableList<CurrencyDTO> {
        log.debug("REST request to get currency symbols")
        return currencyService.findAllSymbols()
    }

    /**
     * `GET  /currencies/:id` : get the "id" currency.
     *
     * @param id the id of the currencyDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the currencyDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/currencies/{id}")
    fun getCurrency(@PathVariable id: Long): ResponseEntity<CurrencyDTO> {
        log.debug("REST request to get Currency : $id")
        val currencyDTO = currencyService.findOne(id)
        return ResponseUtil.wrapOrNotFound(currencyDTO)
    }
    /**
     *  `DELETE  /currencies/:id` : delete the "id" currency.
     *
     * @param id the id of the currencyDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/currencies/{id}")
    fun deleteCurrency(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Currency : $id")

        currencyService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
