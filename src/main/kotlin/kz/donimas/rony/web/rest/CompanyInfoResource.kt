package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.CompanyInfoService
import kz.donimas.rony.service.dto.CompanyInfoDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException
import javax.validation.Valid

private const val ENTITY_NAME = "companyInfo"
/**
 * REST controller for managing [kz.donimas.rony.domain.CompanyInfo].
 */
@RestController
@RequestMapping("/api")
class CompanyInfoResource(
    private val companyInfoService: CompanyInfoService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /company-infos` : Create a new companyInfo.
     *
     * @param companyInfoDTO the companyInfoDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new companyInfoDTO, or with status `400 (Bad Request)` if the companyInfo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-infos")
    fun createCompanyInfo(@Valid @RequestBody companyInfoDTO: CompanyInfoDTO): ResponseEntity<CompanyInfoDTO> {
        log.debug("REST request to save CompanyInfo : $companyInfoDTO")
        if (companyInfoDTO.id != null) {
            throw BadRequestAlertException(
                "A new companyInfo cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = companyInfoService.save(companyInfoDTO)
        return ResponseEntity.created(URI("/api/company-infos/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /company-infos` : Updates an existing companyInfo.
     *
     * @param companyInfoDTO the companyInfoDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated companyInfoDTO,
     * or with status `400 (Bad Request)` if the companyInfoDTO is not valid,
     * or with status `500 (Internal Server Error)` if the companyInfoDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-infos")
    fun updateCompanyInfo(@Valid @RequestBody companyInfoDTO: CompanyInfoDTO): ResponseEntity<CompanyInfoDTO> {
        log.debug("REST request to update CompanyInfo : $companyInfoDTO")
        if (companyInfoDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = companyInfoService.save(companyInfoDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    companyInfoDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /company-infos` : get all the companyInfos.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of companyInfos in body.
     */
    @GetMapping("/company-infos")
    fun getAllCompanyInfos(pageable: Pageable): ResponseEntity<List<CompanyInfoDTO>> {
        log.debug("REST request to get a page of CompanyInfos")
        val page = companyInfoService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /company-infos/:id` : get the "id" companyInfo.
     *
     * @param id the id of the companyInfoDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the companyInfoDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/company-infos/{id}")
    fun getCompanyInfo(@PathVariable id: Long): ResponseEntity<CompanyInfoDTO> {
        log.debug("REST request to get CompanyInfo : $id")
        val companyInfoDTO = companyInfoService.findOne(id)
        return ResponseUtil.wrapOrNotFound(companyInfoDTO)
    }
    /**
     *  `DELETE  /company-infos/:id` : delete the "id" companyInfo.
     *
     * @param id the id of the companyInfoDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/company-infos/{id}")
    fun deleteCompanyInfo(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete CompanyInfo : $id")

        companyInfoService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
