package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.MainContactService
import kz.donimas.rony.service.dto.MainContactDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException
import javax.validation.Valid

private const val ENTITY_NAME = "mainContact"
/**
 * REST controller for managing [kz.donimas.rony.domain.MainContact].
 */
@RestController
@RequestMapping("/api")
class MainContactResource(
    private val mainContactService: MainContactService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /main-contacts` : Create a new mainContact.
     *
     * @param mainContactDTO the mainContactDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new mainContactDTO, or with status `400 (Bad Request)` if the mainContact has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/main-contacts")
    fun createMainContact(@Valid @RequestBody mainContactDTO: MainContactDTO): ResponseEntity<MainContactDTO> {
        log.debug("REST request to save MainContact : $mainContactDTO")
        if (mainContactDTO.id != null) {
            throw BadRequestAlertException(
                "A new mainContact cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        if (mainContactDTO.companyId == null) {
            throw BadRequestAlertException(
                "Company is empty",
                ENTITY_NAME,
                "companyIsEmpty"
            )
        }
        val result = mainContactService.save(mainContactDTO)
        return ResponseEntity.created(URI("/api/main-contacts/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /main-contacts` : Updates an existing mainContact.
     *
     * @param mainContactDTO the mainContactDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated mainContactDTO,
     * or with status `400 (Bad Request)` if the mainContactDTO is not valid,
     * or with status `500 (Internal Server Error)` if the mainContactDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/main-contacts")
    fun updateMainContact(@Valid @RequestBody mainContactDTO: MainContactDTO): ResponseEntity<MainContactDTO> {
        log.debug("REST request to update MainContact : $mainContactDTO")
        if (mainContactDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = mainContactService.save(mainContactDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    mainContactDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /main-contacts` : get all the mainContacts.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of mainContacts in body.
     */
    @GetMapping("/main-contacts")
    fun getAllMainContacts(pageable: Pageable): ResponseEntity<List<MainContactDTO>> {
        log.debug("REST request to get a page of MainContacts")
        val page = mainContactService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /main-contacts/:id` : get the "id" mainContact.
     *
     * @param id the id of the mainContactDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the mainContactDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/main-contacts/{id}")
    fun getMainContact(@PathVariable id: Long): ResponseEntity<MainContactDTO> {
        log.debug("REST request to get MainContact : $id")
        val mainContactDTO = mainContactService.findOne(id)
        return ResponseUtil.wrapOrNotFound(mainContactDTO)
    }
    /**
     *  `DELETE  /main-contacts/:id` : delete the "id" mainContact.
     *
     * @param id the id of the mainContactDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/main-contacts/{id}")
    fun deleteMainContact(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete MainContact : $id")

        mainContactService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
