package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.AgreementService
import kz.donimas.rony.service.dto.AgreementDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "agreement"
/**
 * REST controller for managing [kz.donimas.rony.domain.Agreement].
 */
@RestController
@RequestMapping("/api")
class AgreementResource(
    private val agreementService: AgreementService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /agreements` : Create a new agreement.
     *
     * @param agreementDTO the agreementDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new agreementDTO, or with status `400 (Bad Request)` if the agreement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agreements")
    fun createAgreement(@RequestBody agreementDTO: AgreementDTO): ResponseEntity<AgreementDTO> {
        log.debug("REST request to save Agreement : $agreementDTO")
        if (agreementDTO.id != null) {
            throw BadRequestAlertException(
                "A new agreement cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        if (agreementDTO.projectId == null || agreementDTO.customerId == null) {
            throw BadRequestAlertException(
                "A new agreement doesn't have customer or project id",
                ENTITY_NAME,
                "customerOrProjectEmpty"
            )
        }
        val result = agreementService.save(agreementDTO)
        return ResponseEntity.created(URI("/api/agreements/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /agreements` : Updates an existing agreement.
     *
     * @param agreementDTO the agreementDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated agreementDTO,
     * or with status `400 (Bad Request)` if the agreementDTO is not valid,
     * or with status `500 (Internal Server Error)` if the agreementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agreements")
    fun updateAgreement(@RequestBody agreementDTO: AgreementDTO): ResponseEntity<AgreementDTO> {
        log.debug("REST request to update Agreement : $agreementDTO")
        if (agreementDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = agreementService.save(agreementDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    agreementDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /agreements` : get all the agreements.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the [ResponseEntity] with status `200 (OK)` and the list of agreements in body.
     */
    @GetMapping("/agreements")
    fun getAllAgreements(pageable: Pageable, @RequestParam(required = false, defaultValue = "false") eagerload: Boolean): List<AgreementDTO> {
        log.debug("REST request to get a page of Agreements")
        val list: List<AgreementDTO>
        if (eagerload) {
            list = agreementService.findAllWithEagerRelationships()
        } else {
            list = agreementService.findAll(pageable).content
        }
        return list
    }

    @GetMapping("/agreements/findByName/{query}")
    fun searchAgreement(@PathVariable query: String): MutableList<AgreementDTO> {
        log.debug("REST request to find agreement: $query")

        return agreementService.search(query)
    }

    /**
     * `GET  /agreements/:id` : get the "id" agreement.
     *
     * @param id the id of the agreementDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the agreementDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/agreements/{id}")
    fun getAgreement(@PathVariable id: Long): ResponseEntity<AgreementDTO> {
        log.debug("REST request to get Agreement : $id")
        val agreementDTO = agreementService.findOne(id)
        return ResponseUtil.wrapOrNotFound(agreementDTO)
    }
    /**
     *  `DELETE  /agreements/:id` : delete the "id" agreement.
     *
     * @param id the id of the agreementDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/agreements/{id}")
    fun deleteAgreement(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Agreement : $id")

        agreementService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
