package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.ContractService
import kz.donimas.rony.service.dto.ContractDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException
import java.time.ZonedDateTime

private const val ENTITY_NAME = "contract"
/**
 * REST controller for managing [kz.donimas.rony.domain.Contract].
 */
@RestController
@RequestMapping("/api")
class ContractResource(
    private val contractService: ContractService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /contracts` : Create a new contract.
     *
     * @param contractDTO the contractDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new contractDTO, or with status `400 (Bad Request)` if the contract has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contracts")
    fun createContract(@RequestBody contractDTO: ContractDTO): ResponseEntity<ContractDTO> {
        log.debug("REST request to save Contract : $contractDTO")
        if (contractDTO.id != null) {
            throw BadRequestAlertException(
                "A new contract cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        contractDTO.createDate = ZonedDateTime.now()
        val result = contractService.save(contractDTO)
        return ResponseEntity.created(URI("/api/contracts/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /contracts` : Updates an existing contract.
     *
     * @param contractDTO the contractDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated contractDTO,
     * or with status `400 (Bad Request)` if the contractDTO is not valid,
     * or with status `500 (Internal Server Error)` if the contractDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contracts")
    fun updateContract(@RequestBody contractDTO: ContractDTO): ResponseEntity<ContractDTO> {
        log.debug("REST request to update Contract : $contractDTO")
        if (contractDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = contractService.save(contractDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    contractDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /contracts` : get all the contracts.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of contracts in body.
     */
    @GetMapping("/contracts")
    fun getAllContracts(pageable: Pageable): ResponseEntity<List<ContractDTO>> {
        log.debug("REST request to get a page of Contracts")
        val page = contractService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /contracts/:id` : get the "id" contract.
     *
     * @param id the id of the contractDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the contractDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/contracts/{id}")
    fun getContract(@PathVariable id: Long): ResponseEntity<ContractDTO> {
        log.debug("REST request to get Contract : $id")
        val contractDTO = contractService.findOne(id)
        return ResponseUtil.wrapOrNotFound(contractDTO)
    }
    /**
     *  `DELETE  /contracts/:id` : delete the "id" contract.
     *
     * @param id the id of the contractDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/contracts/{id}")
    fun deleteContract(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Contract : $id")

        contractService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
