package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.TeamMemberService
import kz.donimas.rony.service.dto.TeamMemberDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "teamMember"
/**
 * REST controller for managing [kz.donimas.rony.domain.TeamMember].
 */
@RestController
@RequestMapping("/api")
class TeamMemberResource(
    private val teamMemberService: TeamMemberService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /team-members` : Create a new teamMember.
     *
     * @param teamMemberDTO the teamMemberDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new teamMemberDTO, or with status `400 (Bad Request)` if the teamMember has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/team-members")
    fun createTeamMember(@RequestBody teamMemberDTO: TeamMemberDTO): ResponseEntity<TeamMemberDTO> {
        log.debug("REST request to save TeamMember : $teamMemberDTO")
        if (teamMemberDTO.id != null) {
            throw BadRequestAlertException(
                "A new teamMember cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = teamMemberService.save(teamMemberDTO)
        return ResponseEntity.created(URI("/api/team-members/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /team-members` : Updates an existing teamMember.
     *
     * @param teamMemberDTO the teamMemberDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated teamMemberDTO,
     * or with status `400 (Bad Request)` if the teamMemberDTO is not valid,
     * or with status `500 (Internal Server Error)` if the teamMemberDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/team-members")
    fun updateTeamMember(@RequestBody teamMemberDTO: TeamMemberDTO): ResponseEntity<TeamMemberDTO> {
        log.debug("REST request to update TeamMember : $teamMemberDTO")
        if (teamMemberDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = teamMemberService.save(teamMemberDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    teamMemberDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /team-members` : get all the teamMembers.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of teamMembers in body.
     */
    @GetMapping("/team-members")
    fun getAllTeamMembers(pageable: Pageable): ResponseEntity<List<TeamMemberDTO>> {
        log.debug("REST request to get a page of TeamMembers")
        val page = teamMemberService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /team-members/:id` : get the "id" teamMember.
     *
     * @param id the id of the teamMemberDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the teamMemberDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/team-members/{id}")
    fun getTeamMember(@PathVariable id: Long): ResponseEntity<TeamMemberDTO> {
        log.debug("REST request to get TeamMember : $id")
        val teamMemberDTO = teamMemberService.findOne(id)
        return ResponseUtil.wrapOrNotFound(teamMemberDTO)
    }
    /**
     *  `DELETE  /team-members/:id` : delete the "id" teamMember.
     *
     * @param id the id of the teamMemberDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/team-members/{id}")
    fun deleteTeamMember(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete TeamMember : $id")

        teamMemberService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
