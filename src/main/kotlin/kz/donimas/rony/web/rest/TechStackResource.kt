package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.TechStackService
import kz.donimas.rony.service.dto.TechStackDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "techStack"
/**
 * REST controller for managing [kz.donimas.rony.domain.TechStack].
 */
@RestController
@RequestMapping("/api")
class TechStackResource(
    private val techStackService: TechStackService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /tech-stacks` : Create a new techStack.
     *
     * @param techStackDTO the techStackDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new techStackDTO, or with status `400 (Bad Request)` if the techStack has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tech-stacks")
    fun createTechStack(@RequestBody techStackDTO: TechStackDTO): ResponseEntity<TechStackDTO> {
        log.debug("REST request to save TechStack : $techStackDTO")
        if (techStackDTO.id != null) {
            throw BadRequestAlertException(
                "A new techStack cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        if (techStackDTO.label?.trim().isNullOrEmpty()) {
            throw BadRequestAlertException(
                "There is no label",
                ENTITY_NAME,
                "emptyLabel"
            )
        }
        val result = techStackService.create(techStackDTO)
        return ResponseEntity.created(URI("/api/tech-stacks/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /tech-stacks` : Updates an existing techStack.
     *
     * @param techStackDTO the techStackDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated techStackDTO,
     * or with status `400 (Bad Request)` if the techStackDTO is not valid,
     * or with status `500 (Internal Server Error)` if the techStackDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tech-stacks")
    fun updateTechStack(@RequestBody techStackDTO: TechStackDTO): ResponseEntity<TechStackDTO> {
        log.debug("REST request to update TechStack : $techStackDTO")
        if (techStackDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = techStackService.save(techStackDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    techStackDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /tech-stacks` : get all the techStacks.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of techStacks in body.
     */
    @GetMapping("/tech-stacks")
    fun getAllTechStacks(pageable: Pageable): ResponseEntity<List<TechStackDTO>> {
        log.debug("REST request to get a page of TechStacks")
        val page = techStackService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /tech-stacks/:id` : get the "id" techStack.
     *
     * @param id the id of the techStackDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the techStackDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/tech-stacks/{id}")
    fun getTechStack(@PathVariable id: Long): ResponseEntity<TechStackDTO> {
        log.debug("REST request to get TechStack : $id")
        val techStackDTO = techStackService.findOne(id)
        return ResponseUtil.wrapOrNotFound(techStackDTO)
    }
    /**
     *  `DELETE  /tech-stacks/:id` : delete the "id" techStack.
     *
     * @param id the id of the techStackDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/tech-stacks/{id}")
    fun deleteTechStack(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete TechStack : $id")

        techStackService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
