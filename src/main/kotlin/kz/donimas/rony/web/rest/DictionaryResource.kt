package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.DictionaryService
import kz.donimas.rony.service.catalog.*
import kz.donimas.rony.service.dto.DictionaryDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException
import javax.validation.Valid

private const val ENTITY_NAME = "dictionary"
/**
 * REST controller for managing [kz.donimas.rony.domain.Dictionary].
 */
@RestController
@RequestMapping("/api")
class DictionaryResource(
    private val dictionaryService: DictionaryService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /dictionaries` : Create a new dictionary.
     *
     * @param dictionaryDTO the dictionaryDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new dictionaryDTO, or with status `400 (Bad Request)` if the dictionary has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dictionaries")
    fun createDictionary(@Valid @RequestBody dictionaryDTO: DictionaryDTO): ResponseEntity<DictionaryDTO> {
        log.debug("REST request to save Dictionary : $dictionaryDTO")
        if (dictionaryDTO.id != null) {
            throw BadRequestAlertException(
                "A new dictionary cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = dictionaryService.save(dictionaryDTO)
        return ResponseEntity.created(URI("/api/dictionaries/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /dictionaries` : Updates an existing dictionary.
     *
     * @param dictionaryDTO the dictionaryDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated dictionaryDTO,
     * or with status `400 (Bad Request)` if the dictionaryDTO is not valid,
     * or with status `500 (Internal Server Error)` if the dictionaryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dictionaries")
    fun updateDictionary(@Valid @RequestBody dictionaryDTO: DictionaryDTO): ResponseEntity<DictionaryDTO> {
        log.debug("REST request to update Dictionary : $dictionaryDTO")
        if (dictionaryDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = dictionaryService.save(dictionaryDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    dictionaryDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /dictionaries` : get all the dictionaries.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of dictionaries in body.
     */
    @GetMapping("/dictionaries")
    fun getAllDictionaries(pageable: Pageable): ResponseEntity<List<DictionaryDTO>> {
        log.debug("REST request to get a page of Dictionaries")
        val page = dictionaryService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /dictionaries/:id` : get the "id" dictionary.
     *
     * @param id the id of the dictionaryDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the dictionaryDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/dictionaries/{id}")
    fun getDictionary(@PathVariable id: Long): ResponseEntity<DictionaryDTO> {
        log.debug("REST request to get Dictionary : $id")
        val dictionaryDTO = dictionaryService.findOne(id)
        return ResponseUtil.wrapOrNotFound(dictionaryDTO)
    }
    /**
     *  `DELETE  /dictionaries/:id` : delete the "id" dictionary.
     *
     * @param id the id of the dictionaryDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/dictionaries/{id}")
    fun deleteDictionary(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Dictionary : $id")

        dictionaryService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }

    @GetMapping("dict/name/{entityName}")
    fun getStatusDict(@PathVariable entityName: String): MutableList<DictionaryDTO> {
        log.debug("REST request to get status dict: $entityName")

        return dictionaryService.findByEntityName(entityName)
    }

    @GetMapping("dict/projectStatus")
    fun getProjectStatusDict(): MutableList<DictionaryDTO> {
        log.debug("REST request to get project status dict")

        return dictionaryService.findByEntityName(DICT_PROJECT_STATUS_HISTORY)
    }
    @GetMapping("dict/techStack")
    fun getTechStackDict(): MutableList<DictionaryDTO> {
        log.debug("REST request to get tech stack dictionary")

        return dictionaryService.findByEntityName(DICT_STACK)
    }
    @GetMapping("dict/agreementStatus")
    fun getAgreementDict(): MutableList<DictionaryDTO> {
        log.debug("REST request to get agreement dictionary")

        return dictionaryService.findByEntityName(DICT_AGREEMENT_STATUS_HISTORY)
    }
    @GetMapping("dict/contractSalesType")
    fun getContractSalesTypeDict(): MutableList<DictionaryDTO> {
        log.debug("REST request to get contract sales type dictionary")

        return dictionaryService.findByEntityName(DICT_CONTRACT_SALES_TYPE)
    }
    @GetMapping("dict/contractStatus")
    fun getContractStatus(): MutableList<DictionaryDTO> {
        log.debug("REST request to get contract status dictionary")

        return dictionaryService.findByEntityName(DICT_CONTRACT_STATUS_HISTORY)
    }
    @GetMapping("dict/currency")
    fun getCurrency(): MutableList<DictionaryDTO> {
        log.debug("REST request to get currency dictionary")

        return dictionaryService.findByEntityName(DICT_CURRENCY)
    }
}
