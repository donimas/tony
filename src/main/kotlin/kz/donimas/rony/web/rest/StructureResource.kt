package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.StructureService
import kz.donimas.rony.service.dto.StructureDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException
import javax.validation.Valid

private const val ENTITY_NAME = "structure"
/**
 * REST controller for managing [kz.donimas.rony.domain.Structure].
 */
@RestController
@RequestMapping("/api")
class StructureResource(
    private val structureService: StructureService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /structures` : Create a new structure.
     *
     * @param structureDTO the structureDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new structureDTO, or with status `400 (Bad Request)` if the structure has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/structures")
    fun createStructure(@Valid @RequestBody structureDTO: StructureDTO): ResponseEntity<StructureDTO> {
        log.debug("REST request to save Structure : $structureDTO")
        if (structureDTO.id != null) {
            throw BadRequestAlertException(
                "A new structure cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = structureService.save(structureDTO)
        return ResponseEntity.created(URI("/api/structures/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /structures` : Updates an existing structure.
     *
     * @param structureDTO the structureDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated structureDTO,
     * or with status `400 (Bad Request)` if the structureDTO is not valid,
     * or with status `500 (Internal Server Error)` if the structureDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/structures")
    fun updateStructure(@Valid @RequestBody structureDTO: StructureDTO): ResponseEntity<StructureDTO> {
        log.debug("REST request to update Structure : $structureDTO")
        if (structureDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = structureService.save(structureDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    structureDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /structures` : get all the structures.
     *
     * @param pageable the pagination information.

     * @return the [ResponseEntity] with status `200 (OK)` and the list of structures in body.
     */
    @GetMapping("/structures")
    fun getAllStructures(pageable: Pageable): ResponseEntity<List<StructureDTO>> {
        log.debug("REST request to get a page of Structures")
        val page = structureService.findAll(pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /structures/:id` : get the "id" structure.
     *
     * @param id the id of the structureDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the structureDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/structures/{id}")
    fun getStructure(@PathVariable id: Long): ResponseEntity<StructureDTO> {
        log.debug("REST request to get Structure : $id")
        val structureDTO = structureService.findOne(id)
        return ResponseUtil.wrapOrNotFound(structureDTO)
    }
    /**
     *  `DELETE  /structures/:id` : delete the "id" structure.
     *
     * @param id the id of the structureDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/structures/{id}")
    fun deleteStructure(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Structure : $id")

        structureService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }

    @GetMapping("/structures/top")
    fun getAllTopStructure(): MutableList<StructureDTO> {
        log.debug("Rest request to get all top structure")

        return structureService.findTopStructure()
    }
}
