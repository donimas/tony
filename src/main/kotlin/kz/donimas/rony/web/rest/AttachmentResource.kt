package kz.donimas.rony.web.rest

import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.ResponseUtil
import kz.donimas.rony.service.AttachmentService
import kz.donimas.rony.service.dto.AttachmentDTO
import kz.donimas.rony.web.rest.errors.BadRequestAlertException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.net.URISyntaxException

private const val ENTITY_NAME = "attachment"
/**
 * REST controller for managing [kz.donimas.rony.domain.Attachment].
 */
@RestController
@RequestMapping("/api")
class AttachmentResource(
    private val attachmentService: AttachmentService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /attachments` : Create a new attachment.
     *
     * @param attachmentDTO the attachmentDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new attachmentDTO, or with status `400 (Bad Request)` if the attachment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/attachments")
    fun createAttachment(@RequestBody attachmentDTO: AttachmentDTO): ResponseEntity<AttachmentDTO> {
        log.debug("REST request to save Attachment : $attachmentDTO")
        if (attachmentDTO.id != null) {
            throw BadRequestAlertException(
                "A new attachment cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            )
        }
        val result = attachmentService.save(attachmentDTO)
        return ResponseEntity.created(URI("/api/attachments/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /attachments` : Updates an existing attachment.
     *
     * @param attachmentDTO the attachmentDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated attachmentDTO,
     * or with status `400 (Bad Request)` if the attachmentDTO is not valid,
     * or with status `500 (Internal Server Error)` if the attachmentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/attachments")
    fun updateAttachment(@RequestBody attachmentDTO: AttachmentDTO): ResponseEntity<AttachmentDTO> {
        log.debug("REST request to update Attachment : $attachmentDTO")
        if (attachmentDTO.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = attachmentService.save(attachmentDTO)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    true,
                    ENTITY_NAME,
                    attachmentDTO.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /attachments` : get all the attachments.
     *

     * @return the [ResponseEntity] with status `200 (OK)` and the list of attachments in body.
     */
    @GetMapping("/attachments")
    fun getAllAttachments(): MutableList<AttachmentDTO> {
        log.debug("REST request to get all Attachments")

        return attachmentService.findAll()
    }

    /**
     * `GET  /attachments/:id` : get the "id" attachment.
     *
     * @param id the id of the attachmentDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the attachmentDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/attachments/{id}")
    fun getAttachment(@PathVariable id: Long): ResponseEntity<AttachmentDTO> {
        log.debug("REST request to get Attachment : $id")
        val attachmentDTO = attachmentService.findOne(id)
        return ResponseUtil.wrapOrNotFound(attachmentDTO)
    }
    /**
     *  `DELETE  /attachments/:id` : delete the "id" attachment.
     *
     * @param id the id of the attachmentDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/attachments/{id}")
    fun deleteAttachment(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Attachment : $id")

        attachmentService.delete(id)
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build()
    }
}
