package kz.donimas.rony.service
import kz.donimas.rony.domain.Structure
import kz.donimas.rony.repository.StructureRepository
import kz.donimas.rony.service.dto.StructureDTO
import kz.donimas.rony.service.mapper.StructureMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [Structure].
 */
@Service
@Transactional
class StructureService(
    private val structureRepository: StructureRepository,
    private val structureMapper: StructureMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a structure.
     *
     * @param structureDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(structureDTO: StructureDTO): StructureDTO {
        log.debug("Request to save Structure : $structureDTO")

        var structure = structureMapper.toEntity(structureDTO)
        structure = structureRepository.save(structure)
        return structureMapper.toDto(structure)
    }

    /**
     * Get all the structures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<StructureDTO> {
        log.debug("Request to get all Structures")
        return structureRepository.findAllByFlagDeletedFalse(pageable)
            .map(structureMapper::toDto)
    }

    /**
     * Get one structure by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<StructureDTO> {
        log.debug("Request to get Structure : $id")
        return structureRepository.findById(id)
            .map(structureMapper::toDto)
    }

    /**
     * Delete the structure by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Structure : $id")

        structureRepository.deleteById(id)
    }

    fun findTopStructure(): MutableList<StructureDTO> {
        log.debug("Request to get top structure")

        return structureRepository.findAllByParentIsNullAndFlagDeletedIsFalse()
            .mapTo(mutableListOf(), structureMapper::toDto)
    }
}
