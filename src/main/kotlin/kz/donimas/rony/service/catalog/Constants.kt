package kz.donimas.rony.service.catalog

const val DICT_PROJECT_STATUS_HISTORY = "ProjectStatusHistory"
const val DICT_AGREEMENT_STATUS_HISTORY = "AgreementStatusHistory"
const val DICT_CONTRACT_STATUS_HISTORY = "ContractStatusHistory"
const val DICT_CURRENCY = "Currency"
const val DICT_STACK = "TechStack"
const val DICT_CONTRACT_SALES_TYPE = "ContractSalesType"
const val DICT_PARTNERSHIP_TYPE = "PartnershipType"
