package kz.donimas.rony.service
import kz.donimas.rony.domain.Attachment
import kz.donimas.rony.repository.AttachmentRepository
import kz.donimas.rony.service.dto.AttachmentDTO
import kz.donimas.rony.service.mapper.AttachmentMapper
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [Attachment].
 */
@Service
@Transactional
class AttachmentService(
    private val attachmentRepository: AttachmentRepository,
    private val attachmentMapper: AttachmentMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a attachment.
     *
     * @param attachmentDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(attachmentDTO: AttachmentDTO): AttachmentDTO {
        log.debug("Request to save Attachment : $attachmentDTO")

        var attachment = attachmentMapper.toEntity(attachmentDTO)
        attachment = attachmentRepository.save(attachment)
        return attachmentMapper.toDto(attachment)
    }

    /**
     * Get all the attachments.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(): MutableList<AttachmentDTO> {
        log.debug("Request to get all Attachments")
        return attachmentRepository.findAll()
            .mapTo(mutableListOf(), attachmentMapper::toDto)
    }

    /**
     * Get one attachment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<AttachmentDTO> {
        log.debug("Request to get Attachment : $id")
        return attachmentRepository.findById(id)
            .map(attachmentMapper::toDto)
    }

    /**
     * Delete the attachment by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Attachment : $id")

        attachmentRepository.deleteById(id)
    }
}
