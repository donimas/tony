package kz.donimas.rony.service
import kz.donimas.rony.domain.Agreement
import kz.donimas.rony.domain.AgreementStatusHistory
import kz.donimas.rony.repository.AgreementRepository
import kz.donimas.rony.repository.AgreementStatusHistoryRepository
import kz.donimas.rony.service.catalog.DICT_AGREEMENT_STATUS_HISTORY
import kz.donimas.rony.service.dto.AgreementStatusHistoryDTO
import kz.donimas.rony.service.mapper.AgreementStatusHistoryMapper
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.Optional

/**
 * Service Implementation for managing [AgreementStatusHistory].
 */
@Service
@Transactional
class AgreementStatusHistoryService(
    private val agreementStatusHistoryRepository: AgreementStatusHistoryRepository,
    private val agreementStatusHistoryMapper: AgreementStatusHistoryMapper,
    private val agreementRepository: AgreementRepository
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a agreementStatusHistory.
     *
     * @param agreementStatusHistoryDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(agreementStatusHistoryDTO: AgreementStatusHistoryDTO): AgreementStatusHistoryDTO {
        log.debug("Request to save AgreementStatusHistory : $agreementStatusHistoryDTO")

        var agreementStatusHistory = agreementStatusHistoryMapper.toEntity(agreementStatusHistoryDTO)
        agreementStatusHistory.createDate = ZonedDateTime.now()
        agreementStatusHistory = agreementStatusHistoryRepository.save(agreementStatusHistory)

        val agreement: Agreement? = agreementStatusHistory.agreement
        log.debug("Gonna change status of agreement -> $agreement")
        if (agreement != null) {
            agreement.statusHistory = agreementStatusHistory
            agreementRepository.save(agreement)
        }

        return agreementStatusHistoryMapper.toDto(agreementStatusHistory)
    }
    fun create(agreementStatusHistoryDTO: AgreementStatusHistoryDTO): AgreementStatusHistory {
        log.debug("Request to create AgreementStatusHistory : $agreementStatusHistoryDTO")

        var agreementStatusHistory = agreementStatusHistoryMapper.toEntity(agreementStatusHistoryDTO)
        agreementStatusHistory.createDate = ZonedDateTime.now()
        agreementStatusHistory = agreementStatusHistoryRepository.save(agreementStatusHistory)

        return agreementStatusHistory
    }

    /**
     * Get all the agreementStatusHistories.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(): MutableList<AgreementStatusHistoryDTO> {
        log.debug("Request to get all AgreementStatusHistories")
        return agreementStatusHistoryRepository.findAllByStatusEntityName(DICT_AGREEMENT_STATUS_HISTORY)
            .mapTo(mutableListOf(), agreementStatusHistoryMapper::toDto)
    }

    /**
     * Get one agreementStatusHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<AgreementStatusHistoryDTO> {
        log.debug("Request to get AgreementStatusHistory : $id")
        return agreementStatusHistoryRepository.findById(id)
            .map(agreementStatusHistoryMapper::toDto)
    }

    /**
     * Delete the agreementStatusHistory by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete AgreementStatusHistory : $id")

        agreementStatusHistoryRepository.deleteById(id)
    }
}
