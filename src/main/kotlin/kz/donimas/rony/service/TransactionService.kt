package kz.donimas.rony.service
import kz.donimas.rony.domain.Transaction
import kz.donimas.rony.repository.TransactionRepository
import kz.donimas.rony.service.dto.TransactionDTO
import kz.donimas.rony.service.mapper.TransactionMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [Transaction].
 */
@Service
@Transactional
class TransactionService(
    private val transactionRepository: TransactionRepository,
    private val transactionMapper: TransactionMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a transaction.
     *
     * @param transactionDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(transactionDTO: TransactionDTO): TransactionDTO {
        log.debug("Request to save Transaction : $transactionDTO")

        var transaction = transactionMapper.toEntity(transactionDTO)
        transaction = transactionRepository.save(transaction)
        return transactionMapper.toDto(transaction)
    }

    /**
     * Get all the transactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<TransactionDTO> {
        log.debug("Request to get all Transactions")
        return transactionRepository.findAll(pageable)
            .map(transactionMapper::toDto)
    }

    /**
     * Get one transaction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<TransactionDTO> {
        log.debug("Request to get Transaction : $id")
        return transactionRepository.findById(id)
            .map(transactionMapper::toDto)
    }

    /**
     * Delete the transaction by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Transaction : $id")

        transactionRepository.deleteById(id)
    }
}
