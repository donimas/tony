package kz.donimas.rony.service
import kz.donimas.rony.domain.TeamMember
import kz.donimas.rony.repository.TeamMemberRepository
import kz.donimas.rony.service.dto.TeamMemberDTO
import kz.donimas.rony.service.mapper.TeamMemberMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [TeamMember].
 */
@Service
@Transactional
class TeamMemberService(
    private val teamMemberRepository: TeamMemberRepository,
    private val teamMemberMapper: TeamMemberMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a teamMember.
     *
     * @param teamMemberDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(teamMemberDTO: TeamMemberDTO): TeamMemberDTO {
        log.debug("Request to save TeamMember : $teamMemberDTO")

        var teamMember = teamMemberMapper.toEntity(teamMemberDTO)
        teamMember = teamMemberRepository.save(teamMember)
        return teamMemberMapper.toDto(teamMember)
    }

    /**
     * Get all the teamMembers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<TeamMemberDTO> {
        log.debug("Request to get all TeamMembers")
        return teamMemberRepository.findAll(pageable)
            .map(teamMemberMapper::toDto)
    }

    /**
     * Get one teamMember by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<TeamMemberDTO> {
        log.debug("Request to get TeamMember : $id")
        return teamMemberRepository.findById(id)
            .map(teamMemberMapper::toDto)
    }

    /**
     * Delete the teamMember by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete TeamMember : $id")

        teamMemberRepository.deleteById(id)
    }
}
