package kz.donimas.rony.service
import kz.donimas.rony.domain.ProjectTeam
import kz.donimas.rony.repository.ProjectTeamRepository
import kz.donimas.rony.service.dto.ProjectTeamDTO
import kz.donimas.rony.service.mapper.ProjectTeamMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [ProjectTeam].
 */
@Service
@Transactional
class ProjectTeamService(
    private val projectTeamRepository: ProjectTeamRepository,
    private val projectTeamMapper: ProjectTeamMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a projectTeam.
     *
     * @param projectTeamDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(projectTeamDTO: ProjectTeamDTO): ProjectTeamDTO {
        log.debug("Request to save ProjectTeam : $projectTeamDTO")

        var projectTeam = projectTeamMapper.toEntity(projectTeamDTO)
        projectTeam = projectTeamRepository.save(projectTeam)
        return projectTeamMapper.toDto(projectTeam)
    }

    /**
     * Get all the projectTeams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<ProjectTeamDTO> {
        log.debug("Request to get all ProjectTeams")
        return projectTeamRepository.findAll(pageable)
            .map(projectTeamMapper::toDto)
    }

    /**
     * Get one projectTeam by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<ProjectTeamDTO> {
        log.debug("Request to get ProjectTeam : $id")
        return projectTeamRepository.findById(id)
            .map(projectTeamMapper::toDto)
    }

    /**
     * Delete the projectTeam by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete ProjectTeam : $id")

        projectTeamRepository.deleteById(id)
    }
}
