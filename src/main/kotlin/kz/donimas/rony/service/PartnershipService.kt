package kz.donimas.rony.service
import kz.donimas.rony.domain.Partnership
import kz.donimas.rony.repository.PartnershipRepository
import kz.donimas.rony.service.dto.PartnershipDTO
import kz.donimas.rony.service.mapper.PartnershipMapper
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [Partnership].
 */
@Service
@Transactional
class PartnershipService(
    private val partnershipRepository: PartnershipRepository,
    private val partnershipMapper: PartnershipMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a partnership.
     *
     * @param partnershipDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(partnershipDTO: PartnershipDTO): PartnershipDTO {
        log.debug("Request to save Partnership : $partnershipDTO")

        var partnership = partnershipMapper.toEntity(partnershipDTO)
        partnership = partnershipRepository.save(partnership)
        return partnershipMapper.toDto(partnership)
    }

    /**
     * Get all the partnerships.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(): MutableList<PartnershipDTO> {
        log.debug("Request to get all Partnerships")
        return partnershipRepository.findAll()
            .mapTo(mutableListOf(), partnershipMapper::toDto)
    }

    /**
     * Get one partnership by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<PartnershipDTO> {
        log.debug("Request to get Partnership : $id")
        return partnershipRepository.findById(id)
            .map(partnershipMapper::toDto)
    }

    /**
     * Delete the partnership by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Partnership : $id")

        partnershipRepository.deleteById(id)
    }
}
