package kz.donimas.rony.service
import kz.donimas.rony.domain.TechStack
import kz.donimas.rony.repository.TechStackRepository
import kz.donimas.rony.service.catalog.DICT_STACK
import kz.donimas.rony.service.dto.DictionaryDTO
import kz.donimas.rony.service.dto.TechStackDTO
import kz.donimas.rony.service.mapper.TechStackMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [TechStack].
 */
@Service
@Transactional
class TechStackService(
    private val techStackRepository: TechStackRepository,
    private val techStackMapper: TechStackMapper,
    private val dictionaryService: DictionaryService
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a techStack.
     *
     * @param techStackDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(techStackDTO: TechStackDTO): TechStackDTO {
        log.debug("Request to save TechStack : $techStackDTO")

        var techStack = techStackMapper.toEntity(techStackDTO)
        techStack = techStackRepository.save(techStack)
        return techStackMapper.toDto(techStack)
    }

    fun create(techStackDTO: TechStackDTO): TechStackDTO {
        log.debug("Request to create tech stack: $techStackDTO")

        val dictionaryDTO = DictionaryDTO()
        dictionaryDTO.entityName = DICT_STACK
        dictionaryDTO.name = techStackDTO.label
        val techStack = TechStack()
        techStack.title = dictionaryService.saveAsEntity(dictionaryDTO)
        techStackRepository.save(techStack)

        return techStackMapper.toDto(techStack)
    }

    /**
     * Get all the techStacks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<TechStackDTO> {
        log.debug("Request to get all TechStacks")
        return techStackRepository.findAllByTitleEntityName(DICT_STACK, pageable)
            .map(techStackMapper::toDto)
    }

    /**
     * Get one techStack by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<TechStackDTO> {
        log.debug("Request to get TechStack : $id")
        return techStackRepository.findById(id)
            .map(techStackMapper::toDto)
    }

    /**
     * Delete the techStack by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete TechStack : $id")

        techStackRepository.deleteById(id)
    }
}
