package kz.donimas.rony.service
import kz.donimas.rony.domain.Project
import kz.donimas.rony.domain.ProjectStatusHistory
import kz.donimas.rony.repository.ProjectRepository
import kz.donimas.rony.service.dto.ProjectDTO
import kz.donimas.rony.service.dto.ProjectStatusHistoryDTO
import kz.donimas.rony.service.dto.ProjectTeamDTO
import kz.donimas.rony.service.mapper.ProjectMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.Optional

/**
 * Service Implementation for managing [Project].
 */
@Service
@Transactional
class ProjectService(
    private val projectRepository: ProjectRepository,
    private val projectMapper: ProjectMapper,
    private val projectStatusHistoryService: ProjectStatusHistoryService,
    private val projectTeamService: ProjectTeamService
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a project.
     *
     * @param projectDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(projectDTO: ProjectDTO): ProjectDTO {
        log.debug("Request to save Project : $projectDTO")

        // teamLeadId appears only on first creating
        if (projectDTO.teamLeadId != null) {
            val teamDto = ProjectTeamDTO()
            teamDto.leadId = projectDTO.teamLeadId

            val projectTeam = projectTeamService.save(teamDto)
            log.debug("Project team created: $projectTeam")
            projectDTO.teamId = projectTeam.id
        }

        var project = projectMapper.toEntity(projectDTO)
        project = projectRepository.save(project)

        // statusDictId appears only on first creating
        if (projectDTO.statusDictId != null) {
            val statusHistoryDTO = ProjectStatusHistoryDTO()
            statusHistoryDTO.projectId = project.id
            statusHistoryDTO.statusId = projectDTO.statusDictId
            statusHistoryDTO.createDate = ZonedDateTime.now()

            val statusHistory: ProjectStatusHistory = projectStatusHistoryService.create(statusHistoryDTO)
            project.statusHistory = statusHistory
            project = projectRepository.save(project)
        }

        return projectMapper.toDto(project)
    }

    /**
     * Get all the projects.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<ProjectDTO> {
        log.debug("Request to get all Projects")
        return projectRepository.findAllByFlagDeletedIsFalse(pageable)
            .map(projectMapper::toDto)
    }

    /**
     * Get all the projects with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    fun findAllWithEagerRelationships(pageable: Pageable) =
        projectRepository.findAllWithEagerRelationships(pageable).map(projectMapper::toDto)

    /**
     * Get one project by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<ProjectDTO> {
        log.debug("Request to get Project : $id")
        return projectRepository.findOneWithEagerRelationships(id)
            .map(projectMapper::toDto)
    }

    /**
     * Delete the project by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Project : $id")

        projectRepository.deleteById(id)
    }
}
