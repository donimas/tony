package kz.donimas.rony.service
import kz.donimas.rony.domain.CompanyInfo
import kz.donimas.rony.repository.CompanyInfoRepository
import kz.donimas.rony.service.dto.CompanyInfoDTO
import kz.donimas.rony.service.mapper.CompanyInfoMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [CompanyInfo].
 */
@Service
@Transactional
class CompanyInfoService(
    private val companyInfoRepository: CompanyInfoRepository,
    private val companyInfoMapper: CompanyInfoMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a companyInfo.
     *
     * @param companyInfoDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(companyInfoDTO: CompanyInfoDTO): CompanyInfoDTO {
        log.debug("Request to save CompanyInfo : $companyInfoDTO")

        var companyInfo = companyInfoMapper.toEntity(companyInfoDTO)
        companyInfo = companyInfoRepository.save(companyInfo)
        return companyInfoMapper.toDto(companyInfo)
    }

    /**
     * Get all the companyInfos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<CompanyInfoDTO> {
        log.debug("Request to get all CompanyInfos")
        return companyInfoRepository.findAllByFlagDeletedIsFalse(pageable)
            .map(companyInfoMapper::toDto)
    }

    /**
     * Get one companyInfo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<CompanyInfoDTO> {
        log.debug("Request to get CompanyInfo : $id")
        return companyInfoRepository.findById(id)
            .map(companyInfoMapper::toDto)
    }

    /**
     * Delete the companyInfo by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete CompanyInfo : $id")

        companyInfoRepository.deleteById(id)
    }
}
