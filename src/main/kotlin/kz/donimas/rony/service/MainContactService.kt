package kz.donimas.rony.service
import kz.donimas.rony.domain.MainContact
import kz.donimas.rony.repository.MainContactRepository
import kz.donimas.rony.service.dto.MainContactDTO
import kz.donimas.rony.service.mapper.MainContactMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [MainContact].
 */
@Service
@Transactional
class MainContactService(
    private val mainContactRepository: MainContactRepository,
    private val mainContactMapper: MainContactMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a mainContact.
     *
     * @param mainContactDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(mainContactDTO: MainContactDTO): MainContactDTO {
        log.debug("Request to save MainContact : $mainContactDTO")

        var mainContact = mainContactMapper.toEntity(mainContactDTO)
        mainContact = mainContactRepository.save(mainContact)
        return mainContactMapper.toDto(mainContact)
    }

    /**
     * Get all the mainContacts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<MainContactDTO> {
        log.debug("Request to get all MainContacts")
        return mainContactRepository.findAllByFlagDeletedIsFalse(pageable)
            .map(mainContactMapper::toDto)
    }

    /**
     * Get one mainContact by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<MainContactDTO> {
        log.debug("Request to get MainContact : $id")
        return mainContactRepository.findById(id)
            .map(mainContactMapper::toDto)
    }

    /**
     * Delete the mainContact by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete MainContact : $id")

        mainContactRepository.deleteById(id)
    }
}
