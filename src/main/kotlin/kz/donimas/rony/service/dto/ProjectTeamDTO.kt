package kz.donimas.rony.service.dto

import java.io.Serializable

/**
 * A DTO for the [kz.donimas.rony.domain.ProjectTeam] entity.
 */
data class ProjectTeamDTO(

    var id: Long? = null,

    var flagDeleted: Boolean? = null,

    var leadId: Long? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ProjectTeamDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
