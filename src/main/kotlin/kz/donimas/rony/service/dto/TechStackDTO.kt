package kz.donimas.rony.service.dto

import java.io.Serializable

/**
 * A DTO for the [kz.donimas.rony.domain.TechStack] entity.
 */
data class TechStackDTO(

    var id: Long? = null,

    var titleId: Long? = null,

    // for dto mapper
    var label: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TechStackDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
