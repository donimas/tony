package kz.donimas.rony.service.dto

import java.io.Serializable
import javax.persistence.Lob

/**
 * A DTO for the [kz.donimas.rony.domain.Attachment] entity.
 */
data class AttachmentDTO(

    var id: Long? = null,

    @Lob
    var file: ByteArray? = null,
    var fileContentType: String? = null,

    var type: String? = null,

    var uri: String? = null,

    var unicode: String? = null,

    var name: String? = null,

    var entityName: String? = null,

    var entityId: Long? = null,

    var flagDeleted: Boolean? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AttachmentDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
