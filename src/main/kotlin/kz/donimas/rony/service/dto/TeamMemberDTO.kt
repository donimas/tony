package kz.donimas.rony.service.dto

import java.io.Serializable
import java.math.BigDecimal
import java.time.Instant
import java.time.ZonedDateTime

/**
 * A DTO for the [kz.donimas.rony.domain.TeamMember] entity.
 */
data class TeamMemberDTO(

    var id: Long? = null,

    var involvement: BigDecimal? = null,

    var startDate: Instant? = null,

    var finishDate: Instant? = null,

    var flagLeft: Boolean? = null,

    var createDate: ZonedDateTime? = null,

    var employeeId: Long? = null,

    var teamId: Long? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TeamMemberDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
