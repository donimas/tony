package kz.donimas.rony.service.dto

import kz.donimas.rony.domain.enumeration.DirectionType
import kz.donimas.rony.domain.enumeration.TransactionStatus
import java.io.Serializable
import java.math.BigDecimal
import java.time.ZonedDateTime

/**
 * A DTO for the [kz.donimas.rony.domain.Transaction] entity.
 */
data class TransactionDTO(

    var id: Long? = null,

    var planningSum: BigDecimal? = null,

    var actualSum: BigDecimal? = null,

    var convertedSum: BigDecimal? = null,

    var payDate: ZonedDateTime? = null,

    var note: String? = null,

    var status: TransactionStatus? = null,

    var direction: DirectionType? = null,

    var createDate: ZonedDateTime? = null,

    var flagDeleted: Boolean? = null,

    var currencyId: Long? = null,

    var contractId: Long? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TransactionDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
