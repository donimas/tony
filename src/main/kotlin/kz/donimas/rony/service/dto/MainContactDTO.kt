package kz.donimas.rony.service.dto

import java.io.Serializable
import java.time.Instant
import javax.validation.constraints.NotNull

/**
 * A DTO for the [kz.donimas.rony.domain.MainContact] entity.
 */
data class MainContactDTO(

    var id: Long? = null,

    @get: NotNull
    var fullName: String? = null,

    var telephone: String? = null,

    var email: String? = null,

    var dob: Instant? = null,

    var position: String? = null,

    var comment: String? = null,

    var flagDeleted: Boolean? = null,

    var companyId: Long? = null,

    // to dto
    var companyLabel: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MainContactDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
