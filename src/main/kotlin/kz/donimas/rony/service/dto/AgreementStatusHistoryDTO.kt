package kz.donimas.rony.service.dto

import java.io.Serializable
import java.time.ZonedDateTime

/**
 * A DTO for the [kz.donimas.rony.domain.AgreementStatusHistory] entity.
 */
data class AgreementStatusHistoryDTO(

    var id: Long? = null,

    var createDate: ZonedDateTime? = null,

    var comment: String? = null,

    var statusId: Long? = null,

    var agreementId: Long? = null,

    var statusDictLabel: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AgreementStatusHistoryDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
