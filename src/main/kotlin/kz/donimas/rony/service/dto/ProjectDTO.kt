package kz.donimas.rony.service.dto

import java.io.Serializable
import java.time.Instant
import java.time.ZonedDateTime
import javax.validation.constraints.NotNull

/**
 * A DTO for the [kz.donimas.rony.domain.Project] entity.
 */
data class ProjectDTO(

    var id: Long? = null,

    @get: NotNull
    var name: String? = null,

    var description: String? = null,

    var goal: String? = null,

    var startDate: Instant? = null,

    var finishDate: Instant? = null,

    var uri: String? = null,

    var createDate: ZonedDateTime? = null,

    var flagDeleted: Boolean? = null,

    var statusHistoryId: Long? = null,

    var directionId: Long? = null,

    var techStacks: MutableSet<TechStackDTO> = mutableSetOf(),

    var teamId: Long? = null,

    // for creation new project with first status
    var statusDictId: Long? = null,

    // mapper to DTO
    var statusDictLabel: String? = null,

    // user id for creating team
    var teamLeadId: Long? = null,

    // mapper to DTO
    var directionDictLabel: String? = null,

    // mapper to DTO
    var teamLeader: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ProjectDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
