package kz.donimas.rony.service.dto

import java.io.Serializable
import javax.validation.constraints.NotNull

/**
 * A DTO for the [kz.donimas.rony.domain.Dictionary] entity.
 */
data class DictionaryDTO(

    var id: Long? = null,

    var code: String? = null,

    @get: NotNull
    var name: String? = null,

    var entityName: String? = null,

    var flagDeleted: Boolean? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DictionaryDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
