package kz.donimas.rony.service.dto

import java.io.Serializable
import java.math.BigDecimal
import java.time.Instant
import java.time.ZonedDateTime

/**
 * A DTO for the [kz.donimas.rony.domain.Contract] entity.
 */
data class ContractDTO(

    var id: Long? = null,

    var name: String? = null,

    var number: String? = null,

    var sum: BigDecimal? = null,

    var signDate: Instant? = null,

    var finishDate: Instant? = null,

    var comment: String? = null,

    var createDate: ZonedDateTime? = null,

    var flagDeleted: Boolean? = null,

    var responsiblePmId: Long? = null,

    var salesTypeId: Long? = null,

    var statusHistoryId: Long? = null,

    var vendorId: Long? = null,

    var customerId: Long? = null,

    var currencyId: Long? = null,

    var agreementId: Long? = null,

    var projectId: Long? = null,

    var mainContacts: MutableSet<MainContactDTO> = mutableSetOf(),

    var partnerships: MutableSet<PartnershipDTO> = mutableSetOf(),

    // first status history create dict id
    var statusDictId: Long? = null,

    // to dto view mappers
    var responsiblePmLabel: String? = null,
    var salesTypeLabel: String? = null,
    var statusHistoryLabel: String? = null,
    var vendorLabel: String? = null,
    var customerLabel: String? = null,
    var currencyLabel: String? = null,
    var agreementLabel: String? = null,
    var projectLabel: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ContractDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
