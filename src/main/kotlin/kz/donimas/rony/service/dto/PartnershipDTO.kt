package kz.donimas.rony.service.dto

import kz.donimas.rony.domain.enumeration.PartnerShareType
import java.io.Serializable
import java.math.BigDecimal

/**
 * A DTO for the [kz.donimas.rony.domain.Partnership] entity.
 */
data class PartnershipDTO(

    var id: Long? = null,

    var typeId: Long? = null,

    var contractorId: Long? = null,

    var contractId: Long? = null,

    var share: BigDecimal? = null,

    var shareType: PartnerShareType? = null,

    var workTitle: String? = null,

    var contractorLabel: String? = null,
    var typeLabel: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is PartnershipDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
