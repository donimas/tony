package kz.donimas.rony.service.dto

import java.io.Serializable
import java.time.ZonedDateTime

/**
 * A DTO for the [kz.donimas.rony.domain.ProjectStatusHistory] entity.
 */
data class ProjectStatusHistoryDTO(

    var id: Long? = null,

    var comment: String? = null,

    var createDate: ZonedDateTime? = null,

    var statusId: Long? = null,

    var projectId: Long? = null,

    var statusType: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ProjectStatusHistoryDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
