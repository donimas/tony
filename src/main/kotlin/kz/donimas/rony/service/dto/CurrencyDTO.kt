package kz.donimas.rony.service.dto

import java.io.Serializable

/**
 * A DTO for the [kz.donimas.rony.domain.Currency] entity.
 */
data class CurrencyDTO(

    var id: Long? = null,

    var formula: String? = null,

    var titleId: Long? = null,

    // to dto mapper
    var titleLabel: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CurrencyDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
