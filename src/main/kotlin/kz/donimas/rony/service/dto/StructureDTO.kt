package kz.donimas.rony.service.dto

import java.io.Serializable
import javax.validation.constraints.NotNull

/**
 * A DTO for the [kz.donimas.rony.domain.Structure] entity.
 */
data class StructureDTO(

    var id: Long? = null,

    var code: String? = null,

    @get: NotNull
    var name: String? = null,

    var description: String? = null,

    var flagDeleted: Boolean? = null,

    var parentId: Long? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is StructureDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
