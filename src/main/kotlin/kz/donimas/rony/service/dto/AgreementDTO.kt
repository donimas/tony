package kz.donimas.rony.service.dto

import java.io.Serializable
import java.math.BigDecimal
import java.time.Instant
import java.time.ZonedDateTime

/**
 * A DTO for the [kz.donimas.rony.domain.Agreement] entity.
 */
data class AgreementDTO(

    var id: Long? = null,

    var createDate: ZonedDateTime? = null,

    var comment: String? = null,

    var progressBar: BigDecimal? = null,

    var startDate: Instant? = null,

    var finishDate: Instant? = null,

    var flagDeleted: Boolean? = null,

    var statusHistoryId: Long? = null,

    var customerId: Long? = null,

    var mainContacts: MutableSet<MainContactDTO> = mutableSetOf(),

    var projectId: Long? = null,

    var statusDictId: Long? = null,

    // to dto mapper
    var statusDictLabel: String? = null,

    // to dto mapper
    var customerLabel: String? = null,

    // to dto mapper
    var projectLabel: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AgreementDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
