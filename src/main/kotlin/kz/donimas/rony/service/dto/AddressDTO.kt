package kz.donimas.rony.service.dto

import java.io.Serializable

/**
 * A DTO for the [kz.donimas.rony.domain.Address] entity.
 */
data class AddressDTO(

    var id: Long? = null,

    var country: String? = null,

    var city: String? = null,

    var fullAddress: String? = null,

    var cabinet: String? = null,

    var lat: String? = null,

    var lon: String? = null,

    var postalCode: String? = null,

    var flagDeleted: Boolean? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AddressDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
