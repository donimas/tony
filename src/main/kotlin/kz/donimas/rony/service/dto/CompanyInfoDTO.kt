package kz.donimas.rony.service.dto

import java.io.Serializable
import javax.validation.constraints.NotNull

/**
 * A DTO for the [kz.donimas.rony.domain.CompanyInfo] entity.
 */
data class CompanyInfoDTO(

    var id: Long? = null,

    @get: NotNull
    var name: String? = null,

    var website: String? = null,

    var requisites: String? = null,

    var flagDeleted: Boolean? = null,

    var addressId: Long? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CompanyInfoDTO) return false
        return id != null && id == other.id
    }

    override fun hashCode() = 31
}
