package kz.donimas.rony.service
import kz.donimas.rony.domain.Address
import kz.donimas.rony.repository.AddressRepository
import kz.donimas.rony.service.dto.AddressDTO
import kz.donimas.rony.service.mapper.AddressMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [Address].
 */
@Service
@Transactional
class AddressService(
    private val addressRepository: AddressRepository,
    private val addressMapper: AddressMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a address.
     *
     * @param addressDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(addressDTO: AddressDTO): AddressDTO {
        log.debug("Request to save Address : $addressDTO")

        var address = addressMapper.toEntity(addressDTO)
        address = addressRepository.save(address)
        return addressMapper.toDto(address)
    }

    /**
     * Get all the addresses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<AddressDTO> {
        log.debug("Request to get all Addresses")
        return addressRepository.findAll(pageable)
            .map(addressMapper::toDto)
    }

    /**
     * Get one address by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<AddressDTO> {
        log.debug("Request to get Address : $id")
        return addressRepository.findById(id)
            .map(addressMapper::toDto)
    }

    /**
     * Delete the address by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Address : $id")

        addressRepository.deleteById(id)
    }
}
