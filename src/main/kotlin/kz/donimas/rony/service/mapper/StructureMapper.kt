package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Structure
import kz.donimas.rony.service.dto.StructureDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Structure] and its DTO [StructureDTO].
 */
@Mapper(componentModel = "spring", uses = [])
interface StructureMapper :
    EntityMapper<StructureDTO, Structure> {

    @Mappings(
        Mapping(source = "parent.id", target = "parentId")
    )
    override fun toDto(structure: Structure): StructureDTO

    @Mappings(
        Mapping(source = "parentId", target = "parent")
    )
    override fun toEntity(structureDTO: StructureDTO): Structure

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val structure = Structure()
        structure.id = id
        structure
    }
}
