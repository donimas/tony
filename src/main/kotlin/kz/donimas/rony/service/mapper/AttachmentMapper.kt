package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Attachment
import kz.donimas.rony.service.dto.AttachmentDTO
import org.mapstruct.Mapper

/**
 * Mapper for the entity [Attachment] and its DTO [AttachmentDTO].
 */
@Mapper(componentModel = "spring", uses = [])
interface AttachmentMapper :
    EntityMapper<AttachmentDTO, Attachment> {

    override fun toEntity(attachmentDTO: AttachmentDTO): Attachment

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val attachment = Attachment()
        attachment.id = id
        attachment
    }
}
