package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Transaction
import kz.donimas.rony.service.dto.TransactionDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Transaction] and its DTO [TransactionDTO].
 */
@Mapper(componentModel = "spring", uses = [CurrencyMapper::class, ContractMapper::class])
interface TransactionMapper :
    EntityMapper<TransactionDTO, Transaction> {

    @Mappings(
        Mapping(source = "currency.id", target = "currencyId"),
        Mapping(source = "contract.id", target = "contractId")
    )
    override fun toDto(transaction: Transaction): TransactionDTO

    @Mappings(
        Mapping(source = "currencyId", target = "currency"),
        Mapping(source = "contractId", target = "contract")
    )
    override fun toEntity(transactionDTO: TransactionDTO): Transaction

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val transaction = Transaction()
        transaction.id = id
        transaction
    }
}
