package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Currency
import kz.donimas.rony.service.dto.CurrencyDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Currency] and its DTO [CurrencyDTO].
 */
@Mapper(componentModel = "spring", uses = [DictionaryMapper::class])
interface CurrencyMapper :
    EntityMapper<CurrencyDTO, Currency> {

    @Mappings(
        Mapping(source = "title.id", target = "titleId"),
        Mapping(source = "title.name", target = "titleLabel")
    )
    override fun toDto(currency: Currency): CurrencyDTO

    @Mappings(
        Mapping(source = "titleId", target = "title")
    )
    override fun toEntity(currencyDTO: CurrencyDTO): Currency

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val currency = Currency()
        currency.id = id
        currency
    }
}
