package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.CompanyInfo
import kz.donimas.rony.service.dto.CompanyInfoDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [CompanyInfo] and its DTO [CompanyInfoDTO].
 */
@Mapper(componentModel = "spring", uses = [AddressMapper::class])
interface CompanyInfoMapper :
    EntityMapper<CompanyInfoDTO, CompanyInfo> {

    @Mappings(
        Mapping(source = "address.id", target = "addressId")
    )
    override fun toDto(companyInfo: CompanyInfo): CompanyInfoDTO

    @Mappings(
        Mapping(source = "addressId", target = "address")
    )
    override fun toEntity(companyInfoDTO: CompanyInfoDTO): CompanyInfo

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val companyInfo = CompanyInfo()
        companyInfo.id = id
        companyInfo
    }
}
