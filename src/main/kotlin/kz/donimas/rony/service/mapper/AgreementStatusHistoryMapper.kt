package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.AgreementStatusHistory
import kz.donimas.rony.service.dto.AgreementStatusHistoryDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [AgreementStatusHistory] and its DTO [AgreementStatusHistoryDTO].
 */
@Mapper(componentModel = "spring", uses = [DictionaryMapper::class, AgreementMapper::class])
interface AgreementStatusHistoryMapper :
    EntityMapper<AgreementStatusHistoryDTO, AgreementStatusHistory> {

    @Mappings(
        Mapping(source = "status.id", target = "statusId"),
        Mapping(source = "agreement.id", target = "agreementId"),
        Mapping(source = "status.name", target = "statusDictLabel")
    )
    override fun toDto(agreementStatusHistory: AgreementStatusHistory): AgreementStatusHistoryDTO

    @Mappings(
        Mapping(source = "statusId", target = "status"),
        Mapping(source = "agreementId", target = "agreement")
    )
    override fun toEntity(agreementStatusHistoryDTO: AgreementStatusHistoryDTO): AgreementStatusHistory

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val agreementStatusHistory = AgreementStatusHistory()
        agreementStatusHistory.id = id
        agreementStatusHistory
    }
}
