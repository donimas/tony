package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Partnership
import kz.donimas.rony.service.dto.PartnershipDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Partnership] and its DTO [PartnershipDTO].
 */
@Mapper(componentModel = "spring", uses = [DictionaryMapper::class, CompanyInfoMapper::class, ContractMapper::class])
interface PartnershipMapper :
    EntityMapper<PartnershipDTO, Partnership> {

    @Mappings(
        Mapping(source = "type.id", target = "typeId"),
        Mapping(source = "contractor.id", target = "contractorId"),
        Mapping(source = "contract.id", target = "contractId"),
        Mapping(source = "contractor.name", target = "contractorLabel"),
        Mapping(source = "type.name", target = "typeLabel")
    )
    override fun toDto(partnership: Partnership): PartnershipDTO

    @Mappings(
        Mapping(source = "typeId", target = "type"),
        Mapping(source = "contractorId", target = "contractor"),
        Mapping(source = "contractId", target = "contract")
    )
    override fun toEntity(partnershipDTO: PartnershipDTO): Partnership

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val partnership = Partnership()
        partnership.id = id
        partnership
    }
}
