package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.ContractStatusHistory
import kz.donimas.rony.service.dto.ContractStatusHistoryDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [ContractStatusHistory] and its DTO [ContractStatusHistoryDTO].
 */
@Mapper(componentModel = "spring", uses = [DictionaryMapper::class, ContractMapper::class])
interface ContractStatusHistoryMapper :
    EntityMapper<ContractStatusHistoryDTO, ContractStatusHistory> {

    @Mappings(
        Mapping(source = "status.id", target = "statusId"),
        Mapping(source = "contract.id", target = "contractId")
    )
    override fun toDto(contractStatusHistory: ContractStatusHistory): ContractStatusHistoryDTO

    @Mappings(
        Mapping(source = "statusId", target = "status"),
        Mapping(source = "contractId", target = "contract")
    )
    override fun toEntity(contractStatusHistoryDTO: ContractStatusHistoryDTO): ContractStatusHistory

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val contractStatusHistory = ContractStatusHistory()
        contractStatusHistory.id = id
        contractStatusHistory
    }
}
