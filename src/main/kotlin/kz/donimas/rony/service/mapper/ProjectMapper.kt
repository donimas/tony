package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Project
import kz.donimas.rony.service.dto.ProjectDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Project] and its DTO [ProjectDTO].
 */
@Mapper(componentModel = "spring", uses = [ProjectStatusHistoryMapper::class, StructureMapper::class, TechStackMapper::class, ProjectTeamMapper::class])
interface ProjectMapper :
    EntityMapper<ProjectDTO, Project> {

    @Mappings(
        Mapping(source = "statusHistory.id", target = "statusHistoryId"),
        Mapping(source = "statusHistory.status.name", target = "statusDictLabel"),
        Mapping(source = "direction.id", target = "directionId"),
        Mapping(source = "direction.name", target = "directionDictLabel"),
        Mapping(source = "team.lead.login", target = "teamLeader"),
        Mapping(source = "team.lead.id", target = "teamLeadId"),
        Mapping(source = "team.id", target = "teamId")
    )
    override fun toDto(project: Project): ProjectDTO

    @Mappings(
        Mapping(target = "agreements", ignore = true),
        Mapping(target = "removeAgreement", ignore = true),
        Mapping(target = "deals", ignore = true),
        Mapping(target = "removeDeal", ignore = true),
        // Mapping(source = "statusHistoryId", ignore = true),
        Mapping(source = "directionId", target = "direction"),
        Mapping(target = "removeTechStack", ignore = true),
        Mapping(source = "teamId", target = "team")
    )
    override fun toEntity(projectDTO: ProjectDTO): Project

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val project = Project()
        project.id = id
        project
    }
}
