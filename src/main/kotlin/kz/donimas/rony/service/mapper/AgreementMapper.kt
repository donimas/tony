package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Agreement
import kz.donimas.rony.service.dto.AgreementDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Agreement] and its DTO [AgreementDTO].
 */
@Mapper(componentModel = "spring", uses = [AgreementStatusHistoryMapper::class, CompanyInfoMapper::class, MainContactMapper::class, ProjectMapper::class])
interface AgreementMapper :
    EntityMapper<AgreementDTO, Agreement> {

    @Mappings(
        Mapping(source = "statusHistory.id", target = "statusHistoryId"),
        Mapping(source = "customer.id", target = "customerId"),
        Mapping(source = "project.id", target = "projectId"),
        Mapping(source = "project.name", target = "projectLabel"),
        Mapping(source = "customer.name", target = "customerLabel"),
        Mapping(source = "statusHistory.status.name", target = "statusDictLabel")
    )
    override fun toDto(entity: Agreement): AgreementDTO

    @Mappings(
        Mapping(target = "contracts", ignore = true),
        Mapping(target = "removeContract", ignore = true),
        Mapping(source = "statusHistoryId", target = "statusHistory"),
        Mapping(source = "customerId", target = "customer"),
        Mapping(target = "removeMainContact", ignore = true),
        Mapping(source = "projectId", target = "project")
    )
    override fun toEntity(agreementDTO: AgreementDTO): Agreement

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val agreement = Agreement()
        agreement.id = id
        agreement
    }
}
