package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Contract
import kz.donimas.rony.service.dto.ContractDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Contract] and its DTO [ContractDTO].
 */
@Mapper(
    componentModel = "spring",
    uses = [
        UserMapper::class, DictionaryMapper::class,
        ContractStatusHistoryMapper::class, CompanyInfoMapper::class, CurrencyMapper::class, AgreementMapper::class,
        ProjectMapper::class, PartnershipMapper::class
    ]
)
interface ContractMapper :
    EntityMapper<ContractDTO, Contract> {

    @Mappings(
        Mapping(source = "responsiblePm.id", target = "responsiblePmId"),
        Mapping(source = "salesType.id", target = "salesTypeId"),
        Mapping(source = "statusHistory.id", target = "statusHistoryId"),
        Mapping(source = "vendor.id", target = "vendorId"),
        Mapping(source = "customer.id", target = "customerId"),
        Mapping(source = "currency.id", target = "currencyId"),
        Mapping(source = "agreement.id", target = "agreementId"),
        Mapping(source = "project.id", target = "projectId"),

        Mapping(source = "responsiblePm.login", target = "responsiblePmLabel"),
        Mapping(source = "salesType.name", target = "salesTypeLabel"),
        Mapping(source = "statusHistory.status.name", target = "statusHistoryLabel"),
        Mapping(source = "vendor.name", target = "vendorLabel"),
        Mapping(source = "customer.name", target = "customerLabel"),
        // Mapping(source = "currency.title.name", target = "currencyLabel"),
        Mapping(source = "currency.formula", target = "currencyLabel"),
        Mapping(source = "agreement.project.name", target = "agreementLabel"),
        Mapping(source = "project.name", target = "projectLabel"),

        Mapping(source = "partnerships", target = "partnerships")
    )
    override fun toDto(contract: Contract): ContractDTO

    @Mappings(
        Mapping(target = "partnerships", ignore = true),
        Mapping(target = "removePartnership", ignore = true),
        Mapping(target = "transactions", ignore = true),
        Mapping(target = "removeTransaction", ignore = true),
        Mapping(source = "responsiblePmId", target = "responsiblePm"),
        Mapping(source = "salesTypeId", target = "salesType"),
        Mapping(source = "statusHistoryId", target = "statusHistory"),
        Mapping(source = "vendorId", target = "vendor"),
        Mapping(source = "customerId", target = "customer"),
        Mapping(source = "currencyId", target = "currency"),
        Mapping(source = "agreementId", target = "agreement"),
        Mapping(target = "removeMainContact", ignore = true),
        Mapping(source = "projectId", target = "project")
    )
    override fun toEntity(contractDTO: ContractDTO): Contract

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val contract = Contract()
        contract.id = id
        contract
    }
}
