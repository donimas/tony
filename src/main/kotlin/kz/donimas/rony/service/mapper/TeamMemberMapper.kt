package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.TeamMember
import kz.donimas.rony.service.dto.TeamMemberDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [TeamMember] and its DTO [TeamMemberDTO].
 */
@Mapper(componentModel = "spring", uses = [UserMapper::class, ProjectTeamMapper::class])
interface TeamMemberMapper :
    EntityMapper<TeamMemberDTO, TeamMember> {

    @Mappings(
        Mapping(source = "employee.id", target = "employeeId"),
        Mapping(source = "team.id", target = "teamId")
    )
    override fun toDto(teamMember: TeamMember): TeamMemberDTO

    @Mappings(
        Mapping(source = "employeeId", target = "employee"),
        Mapping(source = "teamId", target = "team")
    )
    override fun toEntity(teamMemberDTO: TeamMemberDTO): TeamMember

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val teamMember = TeamMember()
        teamMember.id = id
        teamMember
    }
}
