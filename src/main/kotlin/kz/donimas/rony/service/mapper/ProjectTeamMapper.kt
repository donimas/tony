package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.ProjectTeam
import kz.donimas.rony.service.dto.ProjectTeamDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [ProjectTeam] and its DTO [ProjectTeamDTO].
 */
@Mapper(componentModel = "spring", uses = [UserMapper::class])
interface ProjectTeamMapper :
    EntityMapper<ProjectTeamDTO, ProjectTeam> {

    @Mappings(
        Mapping(source = "lead.id", target = "leadId")
    )
    override fun toDto(projectTeam: ProjectTeam): ProjectTeamDTO

    @Mappings(
        Mapping(target = "projects", ignore = true),
        Mapping(target = "removeProject", ignore = true),
        Mapping(target = "teamMembers", ignore = true),
        Mapping(target = "removeTeamMember", ignore = true),
        Mapping(source = "leadId", target = "lead")
    )
    override fun toEntity(projectTeamDTO: ProjectTeamDTO): ProjectTeam

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val projectTeam = ProjectTeam()
        projectTeam.id = id
        projectTeam
    }
}
