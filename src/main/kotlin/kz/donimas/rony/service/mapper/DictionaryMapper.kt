package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Dictionary
import kz.donimas.rony.service.dto.DictionaryDTO
import org.mapstruct.Mapper

/**
 * Mapper for the entity [Dictionary] and its DTO [DictionaryDTO].
 */
@Mapper(componentModel = "spring", uses = [])
interface DictionaryMapper :
    EntityMapper<DictionaryDTO, Dictionary> {

    override fun toEntity(dictionaryDTO: DictionaryDTO): Dictionary

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val dictionary = Dictionary()
        dictionary.id = id
        dictionary
    }
}
