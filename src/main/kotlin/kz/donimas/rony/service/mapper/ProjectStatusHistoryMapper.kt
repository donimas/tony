package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.ProjectStatusHistory
import kz.donimas.rony.service.dto.ProjectStatusHistoryDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [ProjectStatusHistory] and its DTO [ProjectStatusHistoryDTO].
 */
@Mapper(componentModel = "spring", uses = [DictionaryMapper::class, ProjectMapper::class])
interface ProjectStatusHistoryMapper :
    EntityMapper<ProjectStatusHistoryDTO, ProjectStatusHistory> {

    @Mappings(
        Mapping(source = "status.id", target = "statusId"),
        Mapping(source = "project.id", target = "projectId"),
        Mapping(source = "status.name", target = "statusType")
    )
    override fun toDto(projectStatusHistory: ProjectStatusHistory): ProjectStatusHistoryDTO

    @Mappings(
        Mapping(source = "statusId", target = "status"),
        Mapping(source = "projectId", target = "project")
    )
    override fun toEntity(projectStatusHistoryDTO: ProjectStatusHistoryDTO): ProjectStatusHistory

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val projectStatusHistory = ProjectStatusHistory()
        projectStatusHistory.id = id
        projectStatusHistory
    }
}
