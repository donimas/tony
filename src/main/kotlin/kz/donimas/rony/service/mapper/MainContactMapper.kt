package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.MainContact
import kz.donimas.rony.service.dto.MainContactDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [MainContact] and its DTO [MainContactDTO].
 */
@Mapper(componentModel = "spring", uses = [CompanyInfoMapper::class])
interface MainContactMapper :
    EntityMapper<MainContactDTO, MainContact> {

    @Mappings(
        Mapping(source = "company.id", target = "companyId"),
        Mapping(source = "company.name", target = "companyLabel")
    )
    override fun toDto(mainContact: MainContact): MainContactDTO

    @Mappings(
        Mapping(source = "companyId", target = "company"),
        Mapping(target = "agreements", ignore = true),
        Mapping(target = "removeAgreement", ignore = true),
        Mapping(target = "contracts", ignore = true),
        Mapping(target = "removeContract", ignore = true)
    )
    override fun toEntity(mainContactDTO: MainContactDTO): MainContact

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val mainContact = MainContact()
        mainContact.id = id
        mainContact
    }
}
