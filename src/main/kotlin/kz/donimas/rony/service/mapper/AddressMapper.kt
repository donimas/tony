package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.Address
import kz.donimas.rony.service.dto.AddressDTO
import org.mapstruct.Mapper

/**
 * Mapper for the entity [Address] and its DTO [AddressDTO].
 */
@Mapper(componentModel = "spring", uses = [])
interface AddressMapper :
    EntityMapper<AddressDTO, Address> {

    override fun toEntity(addressDTO: AddressDTO): Address

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val address = Address()
        address.id = id
        address
    }
}
