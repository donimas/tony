package kz.donimas.rony.service.mapper

import kz.donimas.rony.domain.TechStack
import kz.donimas.rony.service.dto.TechStackDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [TechStack] and its DTO [TechStackDTO].
 */
@Mapper(componentModel = "spring", uses = [DictionaryMapper::class])
interface TechStackMapper :
    EntityMapper<TechStackDTO, TechStack> {

    @Mappings(
        Mapping(source = "title.id", target = "titleId"),
        Mapping(source = "title.name", target = "label")
    )
    override fun toDto(techStack: TechStack): TechStackDTO

    @Mappings(
        Mapping(source = "titleId", target = "title"),
        Mapping(target = "projects", ignore = true),
        Mapping(target = "removeProject", ignore = true)
    )
    override fun toEntity(techStackDTO: TechStackDTO): TechStack

    @JvmDefault
    fun fromId(id: Long?) = id?.let {
        val techStack = TechStack()
        techStack.id = id
        techStack
    }
}
