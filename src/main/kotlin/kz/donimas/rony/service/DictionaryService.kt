package kz.donimas.rony.service
import kz.donimas.rony.domain.Dictionary
import kz.donimas.rony.repository.DictionaryRepository
import kz.donimas.rony.service.dto.DictionaryDTO
import kz.donimas.rony.service.mapper.DictionaryMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [Dictionary].
 */
@Service
@Transactional
class DictionaryService(
    private val dictionaryRepository: DictionaryRepository,
    private val dictionaryMapper: DictionaryMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a dictionary.
     *
     * @param dictionaryDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(dictionaryDTO: DictionaryDTO): DictionaryDTO {
        log.debug("Request to save Dictionary : $dictionaryDTO")

        var dictionary = dictionaryMapper.toEntity(dictionaryDTO)
        dictionary = dictionaryRepository.save(dictionary)
        return dictionaryMapper.toDto(dictionary)
    }

    fun saveAsEntity(dictionaryDTO: DictionaryDTO): Dictionary {
        log.debug("Request to create dictionary as entity: $dictionaryDTO")
        val dictionary = dictionaryMapper.toEntity(dictionaryDTO)
        dictionary.flagDeleted = false
        return dictionaryRepository.save(dictionary)
    }

    /**
     * Get all the dictionaries.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<DictionaryDTO> {
        log.debug("Request to get all Dictionaries")
        return dictionaryRepository.findAllByFlagDeletedIsFalse(pageable)
            .map(dictionaryMapper::toDto)
    }

    /**
     * Get one dictionary by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<DictionaryDTO> {
        log.debug("Request to get Dictionary : $id")
        return dictionaryRepository.findById(id)
            .map(dictionaryMapper::toDto)
    }

    /**
     * Delete the dictionary by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Dictionary : $id")

        dictionaryRepository.deleteById(id)
    }

    @Transactional(readOnly = true)
    fun findByEntityName(entityName: String): MutableList<DictionaryDTO> {
        log.debug("Request to find by entity name : $entityName")

        return dictionaryRepository.findAllByEntityNameAndFlagDeletedIsFalse(entityName)
            .mapTo(mutableListOf(), dictionaryMapper::toDto)
    }
}
