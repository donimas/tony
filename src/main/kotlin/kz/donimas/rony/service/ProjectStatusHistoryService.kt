package kz.donimas.rony.service
import kz.donimas.rony.domain.Project
import kz.donimas.rony.domain.ProjectStatusHistory
import kz.donimas.rony.repository.ProjectRepository
import kz.donimas.rony.repository.ProjectStatusHistoryRepository
import kz.donimas.rony.service.catalog.DICT_PROJECT_STATUS_HISTORY
import kz.donimas.rony.service.dto.ProjectStatusHistoryDTO
import kz.donimas.rony.service.mapper.ProjectStatusHistoryMapper
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.Optional

/**
 * Service Implementation for managing [ProjectStatusHistory].
 */
@Service
@Transactional
class ProjectStatusHistoryService(
    private val projectStatusHistoryRepository: ProjectStatusHistoryRepository,
    private val projectStatusHistoryMapper: ProjectStatusHistoryMapper,
    private val projectRepository: ProjectRepository
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a projectStatusHistory.
     *
     * @param projectStatusHistoryDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(projectStatusHistoryDTO: ProjectStatusHistoryDTO): ProjectStatusHistoryDTO {
        log.debug("Request to save ProjectStatusHistory : $projectStatusHistoryDTO")

        var projectStatusHistory = projectStatusHistoryMapper.toEntity(projectStatusHistoryDTO)
        projectStatusHistory.createDate = ZonedDateTime.now()
        projectStatusHistory = projectStatusHistoryRepository.save(projectStatusHistory)

        val project: Project? = projectStatusHistory.project
        log.debug("Gonna change status of project -> $project")
        if (project != null) {
            project.statusHistory = projectStatusHistory
            projectRepository.save(project)
        }

        return projectStatusHistoryMapper.toDto(projectStatusHistory)
    }

    /**
     * creates from project service for first status to get as not DTO
     */
    fun create(projectStatusHistoryDTO: ProjectStatusHistoryDTO): ProjectStatusHistory {
        log.debug("Request to create ProjectStatusHistory : $projectStatusHistoryDTO")

        val projectStatusHistory = projectStatusHistoryMapper.toEntity(projectStatusHistoryDTO)
        return projectStatusHistoryRepository.save(projectStatusHistory)
    }

    /**
     * Get all the projectStatusHistories.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(): MutableList<ProjectStatusHistoryDTO> {
        log.debug("Request to get all ProjectStatusHistories")
        return projectStatusHistoryRepository.findAll()
            .mapTo(mutableListOf(), projectStatusHistoryMapper::toDto)
    }

    /**
     * Get one projectStatusHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<ProjectStatusHistoryDTO> {
        log.debug("Request to get ProjectStatusHistory : $id")
        return projectStatusHistoryRepository.findById(id)
            .map(projectStatusHistoryMapper::toDto)
    }

    /**
     * Delete the projectStatusHistory by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete ProjectStatusHistory : $id")

        projectStatusHistoryRepository.deleteById(id)
    }

    @Transactional(readOnly = true)
    fun findAllOwn(): MutableList<ProjectStatusHistoryDTO> {
        log.debug("Request to get all ProjectStatusHistories related with Entity name")
        return projectStatusHistoryRepository.findAllByStatus_EntityNameAndStatus_FlagDeleted_IsFalse(DICT_PROJECT_STATUS_HISTORY)
            .mapTo(mutableListOf(), projectStatusHistoryMapper::toDto)
    }
}
