package kz.donimas.rony.service
import kz.donimas.rony.domain.Contract
import kz.donimas.rony.repository.ContractRepository
import kz.donimas.rony.service.dto.ContractDTO
import kz.donimas.rony.service.dto.ContractStatusHistoryDTO
import kz.donimas.rony.service.mapper.ContractMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.Optional

/**
 * Service Implementation for managing [Contract].
 */
@Service
@Transactional
class ContractService(
    private val contractRepository: ContractRepository,
    private val contractMapper: ContractMapper,
    private val contractStatusHistoryService: ContractStatusHistoryService,
    private val partnershipService: PartnershipService
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a contract.
     *
     * @param contractDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(contractDTO: ContractDTO): ContractDTO {
        log.debug("Request to save Contract : $contractDTO")

        var contract = contractMapper.toEntity(contractDTO)
        contract = contractRepository.save(contract)

        // only dict id comes when firstly creates entity
        // then status changes by another way
        if (contractDTO.statusDictId != null) {
            val statusHistoryDTO = ContractStatusHistoryDTO()
            statusHistoryDTO.contractId = contract.id
            statusHistoryDTO.createDate = ZonedDateTime.now()
            statusHistoryDTO.statusId = contractDTO.statusDictId

            contract.statusHistory = contractStatusHistoryService.create(statusHistoryDTO)
            contractRepository.save(contract)
            log.debug("Contract saved with status: $contract")
        }

        if (contractDTO.partnerships.size > 0) {
            for (partnershipDTO in contractDTO.partnerships) {
                partnershipDTO.contractId = contract.id
                partnershipService.save(partnershipDTO)
            }
        }

        return contractMapper.toDto(contract)
    }

    /**
     * Get all the contracts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<ContractDTO> {
        log.debug("Request to get all Contracts")
        return contractRepository.findAllByFlagDeletedIsFalse(pageable)
            .map(contractMapper::toDto)
    }

    /**
     * Get one contract by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<ContractDTO> {
        log.debug("Request to get Contract : $id")
        return contractRepository.findById(id)
            .map(contractMapper::toDto)
    }

    /**
     * Delete the contract by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Contract : $id")

        contractRepository.deleteById(id)
    }
}
