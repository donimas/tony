package kz.donimas.rony.service
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import khttp.get
import khttp.responses.Response
import kz.donimas.rony.domain.Currency
import kz.donimas.rony.repository.CurrencyRepository
import kz.donimas.rony.service.dto.CurrencyDTO
import kz.donimas.rony.service.mapper.CurrencyMapper
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [Currency].
 */
@Service
@Transactional
class CurrencyService(
    private val currencyRepository: CurrencyRepository,
    private val currencyMapper: CurrencyMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a currency.
     *
     * @param currencyDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(currencyDTO: CurrencyDTO): CurrencyDTO {
        log.debug("Request to save Currency : $currencyDTO")

        var currency = currencyMapper.toEntity(currencyDTO)
        val foundSymbol = currency.formula?.let { currencyRepository.findAllByFormula(it) }
        if (foundSymbol == null || foundSymbol.isEmpty()) {
            log.debug("found symbol is empty")
            currency = currencyRepository.save(currency)
        } else {
            currency = foundSymbol.get(0)
            log.debug("found symbol: $currency")
        }

        return currencyMapper.toDto(currency)
    }

    /**
     * Get all the currencies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<CurrencyDTO> {
        log.debug("Request to get all Currencies")
        return currencyRepository.findAll(pageable)
            .map(currencyMapper::toDto)
    }

    @Transactional(readOnly = true)
    fun findAllSymbols(): MutableList<CurrencyDTO> {
        val response: Response = khttp.get(
            url = "http://data.fixer.io/api/symbols",
            params = mapOf("access_key" to "5f81c65224d33e16be8bc77331c402dc")
        )
        val obj: JSONObject = response.jsonObject
        val isSuccess: Boolean = obj["success"] as Boolean
        log.debug("symbols fetch from fixer result is: $isSuccess")
        log.debug("$obj")

        var symbols: MutableMap<Any, Any> = mutableMapOf()
        var result = mutableListOf<CurrencyDTO>()
        if (isSuccess) {
            symbols = ObjectMapper().readValue<MutableMap<Any, Any>>(
                obj.getJSONObject("symbols").toString(),
                object : TypeReference<MutableMap<Any, Any>>() {}
            )
            log.debug("symbols: $symbols")

            result = symbols.map { (index, country) ->
                CurrencyDTO(
                    formula = index as String,
                    titleLabel = country as
                        String
                )
            }.toMutableList()

            log.debug("symbols converted list of object: $result")
        }

        return result
    }

    /**
     * Get one currency by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<CurrencyDTO> {
        log.debug("Request to get Currency : $id")
        return currencyRepository.findById(id)
            .map(currencyMapper::toDto)
    }

    /**
     * Delete the currency by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Currency : $id")

        currencyRepository.deleteById(id)
    }
}
