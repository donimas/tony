package kz.donimas.rony.service
import kz.donimas.rony.domain.ContractStatusHistory
import kz.donimas.rony.repository.ContractStatusHistoryRepository
import kz.donimas.rony.service.dto.ContractStatusHistoryDTO
import kz.donimas.rony.service.mapper.ContractStatusHistoryMapper
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

/**
 * Service Implementation for managing [ContractStatusHistory].
 */
@Service
@Transactional
class ContractStatusHistoryService(
    private val contractStatusHistoryRepository: ContractStatusHistoryRepository,
    private val contractStatusHistoryMapper: ContractStatusHistoryMapper
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a contractStatusHistory.
     *
     * @param contractStatusHistoryDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(contractStatusHistoryDTO: ContractStatusHistoryDTO): ContractStatusHistoryDTO {
        log.debug("Request to save ContractStatusHistory : $contractStatusHistoryDTO")

        var contractStatusHistory = contractStatusHistoryMapper.toEntity(contractStatusHistoryDTO)
        contractStatusHistory = contractStatusHistoryRepository.save(contractStatusHistory)
        return contractStatusHistoryMapper.toDto(contractStatusHistory)
    }

    fun create(contractStatusHistoryDTO: ContractStatusHistoryDTO): ContractStatusHistory {
        log.debug("Request to create ContractStatusHistory: $contractStatusHistoryDTO")

        val contractStatusHistory = contractStatusHistoryMapper.toEntity(contractStatusHistoryDTO)
        return contractStatusHistoryRepository.save(contractStatusHistory)
    }

    /**
     * Get all the contractStatusHistories.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(): MutableList<ContractStatusHistoryDTO> {
        log.debug("Request to get all ContractStatusHistories")
        return contractStatusHistoryRepository.findAll()
            .mapTo(mutableListOf(), contractStatusHistoryMapper::toDto)
    }

    /**
     * Get one contractStatusHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<ContractStatusHistoryDTO> {
        log.debug("Request to get ContractStatusHistory : $id")
        return contractStatusHistoryRepository.findById(id)
            .map(contractStatusHistoryMapper::toDto)
    }

    /**
     * Delete the contractStatusHistory by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete ContractStatusHistory : $id")

        contractStatusHistoryRepository.deleteById(id)
    }
}
