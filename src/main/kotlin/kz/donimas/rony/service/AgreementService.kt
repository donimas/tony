package kz.donimas.rony.service

import kz.donimas.rony.domain.Agreement
import kz.donimas.rony.domain.AgreementStatusHistory
import kz.donimas.rony.repository.AgreementRepository
import kz.donimas.rony.service.dto.AgreementDTO
import kz.donimas.rony.service.dto.AgreementStatusHistoryDTO
import kz.donimas.rony.service.mapper.AgreementMapper
import kz.donimas.rony.service.mapper.AgreementStatusHistoryMapper
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.Optional

/**
 * Service Implementation for managing [Agreement].
 */
@Service
@Transactional
class AgreementService(
    private val agreementRepository: AgreementRepository,
    private val agreementMapper: AgreementMapper,
    private val agreementStatusHistoryMapper: AgreementStatusHistoryMapper,
    private val agreementStatusHistoryService: AgreementStatusHistoryService
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a agreement.
     *
     * @param agreementDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(agreementDTO: AgreementDTO): AgreementDTO {
        log.debug("Request to save Agreement : $agreementDTO")

        var agreement = agreementMapper.toEntity(agreementDTO)
        agreement.createDate = ZonedDateTime.now()
        log.debug("Converted agreement entity: $agreement")
        agreementRepository.save(agreement)

        log.debug("saved agreement: $agreement")

        // statusDictId appears only on first creating
        if (agreementDTO.statusDictId != null) {
            val statusHistoryDTO = AgreementStatusHistoryDTO()
            statusHistoryDTO.agreementId = agreement.id
            statusHistoryDTO.statusId = agreementDTO.statusDictId
            statusHistoryDTO.createDate = ZonedDateTime.now()

            val statusHistory: AgreementStatusHistory = agreementStatusHistoryService.create(statusHistoryDTO)
            agreement.statusHistory = statusHistory

            log.debug("again gonna save for status history: $agreement")

            agreementRepository.save(agreement)

            log.debug("save: $agreement")
        }

        return agreementMapper.toDto(agreement)
    }

    /**
     * Get all the agreements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<AgreementDTO> {
        log.debug("Request to get all Agreements")
        return agreementRepository.findAllByFlagDeletedIsFalse(pageable)
            .map(agreementMapper::toDto)
    }

    @Transactional(readOnly = true)
    fun search(query: String): MutableList<AgreementDTO> {
        log.debug("Request to search agreement: $query")

        return agreementRepository.findAllByFlagDeletedIsFalseAndProjectNameContainingIgnoreCase(query)
            .mapTo(mutableListOf(), agreementMapper::toDto)
    }

    /**
     * Get all the agreements with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    fun findAllWithEagerRelationships(): MutableList<AgreementDTO> {
        // agreementRepository.findAllWithEagerRelationships(pageable).map(agreementMapper::toDto)
        return agreementRepository.findAllWithEagerRelationships()
            .mapTo(mutableListOf(), agreementMapper::toDto)
    }

    /**
     * Get one agreement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<AgreementDTO> {
        log.debug("Request to get Agreement : $id")
        return agreementRepository.findOneWithEagerRelationships(id)
            .map(agreementMapper::toDto)
    }

    /**
     * Delete the agreement by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Agreement : $id")

        agreementRepository.deleteById(id)
    }
}
