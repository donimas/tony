package kz.donimas.rony.config

import io.github.jhipster.config.JHipsterProperties
import io.github.jhipster.config.cache.PrefixedKeyGenerator
import org.ehcache.config.builders.CacheConfigurationBuilder
import org.ehcache.config.builders.ExpiryPolicyBuilder
import org.ehcache.config.builders.ResourcePoolsBuilder
import org.ehcache.jsr107.Eh107Configuration
import org.hibernate.cache.jcache.ConfigSettings
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer
import org.springframework.boot.info.BuildProperties
import org.springframework.boot.info.GitProperties
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Duration

@Configuration
@EnableCaching
class CacheConfiguration(
    @Autowired val gitProperties: GitProperties?,
    @Autowired val buildProperties: BuildProperties?,
    jHipsterProperties: JHipsterProperties
) {

    private val jcacheConfiguration: javax.cache.configuration.Configuration<Any, Any>

    init {
        val ehcache = jHipsterProperties.cache.ehcache

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(
                Any::class.java,
                Any::class.java,
                ResourcePoolsBuilder.heap(ehcache.maxEntries)
            )
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.timeToLiveSeconds.toLong())))
                .build()
        )
    }

    @Bean
    fun hibernatePropertiesCustomizer(cacheManager: javax.cache.CacheManager) = HibernatePropertiesCustomizer {
        hibernateProperties ->
        hibernateProperties[ConfigSettings.CACHE_MANAGER] = cacheManager
    }

    @Bean
    fun cacheManagerCustomizer(): JCacheManagerCustomizer {
        return JCacheManagerCustomizer { cm ->
            createCache(cm, kz.donimas.rony.repository.UserRepository.USERS_BY_LOGIN_CACHE)
            createCache(cm, kz.donimas.rony.repository.UserRepository.USERS_BY_EMAIL_CACHE)
            createCache(cm, kz.donimas.rony.domain.User::class.java.name)
            createCache(cm, kz.donimas.rony.domain.Authority::class.java.name)
            createCache(cm, kz.donimas.rony.domain.User::class.java.name + ".authorities")
            createCache(cm, kz.donimas.rony.domain.Project::class.java.name)
            createCache(cm, kz.donimas.rony.domain.Project::class.java.name + ".agreements")
            createCache(cm, kz.donimas.rony.domain.Project::class.java.name + ".techStacks")
            createCache(cm, kz.donimas.rony.domain.Structure::class.java.name)
            createCache(cm, kz.donimas.rony.domain.CompanyInfo::class.java.name)
            createCache(cm, kz.donimas.rony.domain.MainContact::class.java.name)
            createCache(cm, kz.donimas.rony.domain.MainContact::class.java.name + ".agreements")
            createCache(cm, kz.donimas.rony.domain.Address::class.java.name)
            createCache(cm, kz.donimas.rony.domain.ProjectTeam::class.java.name)
            createCache(cm, kz.donimas.rony.domain.ProjectTeam::class.java.name + ".projects")
            createCache(cm, kz.donimas.rony.domain.ProjectTeam::class.java.name + ".teamMembers")
            createCache(cm, kz.donimas.rony.domain.TeamMember::class.java.name)
            createCache(cm, kz.donimas.rony.domain.Contract::class.java.name)
            createCache(cm, kz.donimas.rony.domain.Contract::class.java.name + ".partnerships")
            createCache(cm, kz.donimas.rony.domain.Contract::class.java.name + ".transactions")
            createCache(cm, kz.donimas.rony.domain.Transaction::class.java.name)
            createCache(cm, kz.donimas.rony.domain.TechStack::class.java.name)
            createCache(cm, kz.donimas.rony.domain.TechStack::class.java.name + ".projects")
            createCache(cm, kz.donimas.rony.domain.Dictionary::class.java.name)
            createCache(cm, kz.donimas.rony.domain.Partnership::class.java.name)
            createCache(cm, kz.donimas.rony.domain.Currency::class.java.name)
            createCache(cm, kz.donimas.rony.domain.Agreement::class.java.name)
            createCache(cm, kz.donimas.rony.domain.Agreement::class.java.name + ".contracts")
            createCache(cm, kz.donimas.rony.domain.Agreement::class.java.name + ".mainContacts")
            createCache(cm, kz.donimas.rony.domain.AgreementStatusHistory::class.java.name)
            createCache(cm, kz.donimas.rony.domain.ProjectStatusHistory::class.java.name)
            createCache(cm, kz.donimas.rony.domain.ContractStatusHistory::class.java.name)
            createCache(cm, kz.donimas.rony.domain.Attachment::class.java.name)
            // jhipster-needle-ehcache-add-entry
        }
    }

    private fun createCache(cm: javax.cache.CacheManager, cacheName: String) {
        val cache: javax.cache.Cache<Any, Any>? = cm.getCache(cacheName)
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration)
        }
    }

    @Bean
    fun keyGenerator() = PrefixedKeyGenerator(gitProperties, buildProperties)
}
