package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import java.time.Instant
import javax.persistence.*
import javax.validation.constraints.*

/**
 * A MainContact.
 */
@Entity
@Table(name = "main_contact")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class MainContact(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @get: NotNull
    @Column(name = "full_name", nullable = false)
    var fullName: String? = null,

    @Column(name = "telephone")
    var telephone: String? = null,

    @Column(name = "email")
    var email: String? = null,

    @Column(name = "dob")
    var dob: Instant? = null,

    @Column(name = "position")
    var position: String? = null,

    @Column(name = "comment")
    var comment: String? = null,

    @Column(name = "flag_deleted")
    var flagDeleted: Boolean? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["mainContacts"], allowSetters = true)
    var company: CompanyInfo? = null,

    @ManyToMany(mappedBy = "mainContacts")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    var agreements: MutableSet<Agreement> = mutableSetOf(),

    @ManyToMany(mappedBy = "mainContacts")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    var contracts: MutableSet<Contract> = mutableSetOf()

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {

    fun addAgreement(agreement: Agreement): MainContact {
        this.agreements.add(agreement)
        agreement.mainContacts.add(this)
        return this
    }

    fun removeAgreement(agreement: Agreement): MainContact {
        this.agreements.remove(agreement)
        agreement.mainContacts.remove(this)
        return this
    }

    fun addContract(contract: Contract): MainContact {
        this.contracts.add(contract)
        contract.mainContacts.add(this)
        return this
    }

    fun removeContract(contract: Contract): MainContact {
        this.contracts.remove(contract)
        contract.mainContacts.remove(this)
        return this
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MainContact) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "MainContact{" +
        "id=$id" +
        ", fullName='$fullName'" +
        ", telephone='$telephone'" +
        ", email='$email'" +
        ", dob='$dob'" +
        ", position='$position'" +
        ", comment='$comment'" +
        ", flagDeleted='$flagDeleted'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
