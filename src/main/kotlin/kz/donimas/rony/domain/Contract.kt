package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import java.math.BigDecimal
import java.time.Instant
import java.time.ZonedDateTime
import javax.persistence.*

/**
 * A Contract.
 */
@Entity
@Table(name = "contract")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class Contract(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @Column(name = "name")
    var name: String? = null,

    @Column(name = "number")
    var number: String? = null,

    @Column(name = "sum", precision = 21, scale = 2)
    var sum: BigDecimal? = null,

    @Column(name = "sign_date")
    var signDate: Instant? = null,

    @Column(name = "finish_date")
    var finishDate: Instant? = null,

    @Column(name = "comment")
    var comment: String? = null,

    @Column(name = "create_date")
    var createDate: ZonedDateTime? = null,

    @Column(name = "flag_deleted")
    var flagDeleted: Boolean? = null,

    @OneToMany(mappedBy = "contract")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    var partnerships: MutableSet<Partnership> = mutableSetOf(),

    @OneToMany(mappedBy = "contract")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    var transactions: MutableSet<Transaction> = mutableSetOf(),

    @ManyToOne @JsonIgnoreProperties(value = ["contracts"], allowSetters = true)
    var responsiblePm: User? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["contracts"], allowSetters = true)
    var salesType: Dictionary? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["contracts"], allowSetters = true)
    var statusHistory: ContractStatusHistory? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["contracts"], allowSetters = true)
    var vendor: CompanyInfo? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["contracts"], allowSetters = true)
    var customer: CompanyInfo? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["contracts"], allowSetters = true)
    var currency: Currency? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["contracts"], allowSetters = true)
    var agreement: Agreement? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["deals"], allowSetters = true)
    var project: Project? = null,

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(
        name = "contract_main_contact",
        joinColumns = [JoinColumn(name = "contract_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "main_contact_id", referencedColumnName = "id")]
    )
    var mainContacts: MutableSet<MainContact> = mutableSetOf()

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {

    fun addPartnership(partnership: Partnership): Contract {
        this.partnerships.add(partnership)
        partnership.contract = this
        return this
    }

    fun removePartnership(partnership: Partnership): Contract {
        this.partnerships.remove(partnership)
        partnership.contract = null
        return this
    }

    fun addTransaction(transaction: Transaction): Contract {
        this.transactions.add(transaction)
        transaction.contract = this
        return this
    }

    fun removeTransaction(transaction: Transaction): Contract {
        this.transactions.remove(transaction)
        transaction.contract = null
        return this
    }

    fun addMainContact(mainContact: MainContact): Contract {
        this.mainContacts.add(mainContact)
        return this
    }

    fun removeMainContact(mainContact: MainContact): Contract {
        this.mainContacts.remove(mainContact)
        return this
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Contract) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "Contract{" +
        "id=$id" +
        ", name='$name'" +
        ", number='$number'" +
        ", sum=$sum" +
        ", signDate='$signDate'" +
        ", finishDate='$finishDate'" +
        ", comment='$comment'" +
        ", createDate='$createDate'" +
        ", flagDeleted='$flagDeleted'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
