package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import kz.donimas.rony.domain.enumeration.DirectionType
import kz.donimas.rony.domain.enumeration.TransactionStatus
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.*

/**
 * A Transaction.
 */
@Entity
@Table(name = "transaction")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class Transaction(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @Column(name = "planning_sum", precision = 21, scale = 2)
    var planningSum: BigDecimal? = null,

    @Column(name = "actual_sum", precision = 21, scale = 2)
    var actualSum: BigDecimal? = null,

    @Column(name = "converted_sum", precision = 21, scale = 2)
    var convertedSum: BigDecimal? = null,

    @Column(name = "pay_date")
    var payDate: ZonedDateTime? = null,

    @Column(name = "note")
    var note: String? = null,

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    var status: TransactionStatus? = null,

    @Enumerated(EnumType.STRING)
    @Column(name = "direction")
    var direction: DirectionType? = null,

    @Column(name = "create_date")
    var createDate: ZonedDateTime? = null,

    @Column(name = "flag_deleted")
    var flagDeleted: Boolean? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["transactions"], allowSetters = true)
    var currency: Currency? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["transactions"], allowSetters = true)
    var contract: Contract? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Transaction) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "Transaction{" +
        "id=$id" +
        ", planningSum=$planningSum" +
        ", actualSum=$actualSum" +
        ", convertedSum=$convertedSum" +
        ", payDate='$payDate'" +
        ", note='$note'" +
        ", status='$status'" +
        ", direction='$direction'" +
        ", createDate='$createDate'" +
        ", flagDeleted='$flagDeleted'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
