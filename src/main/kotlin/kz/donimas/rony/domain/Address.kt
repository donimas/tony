package kz.donimas.rony.domain

import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import javax.persistence.*

/**
 * A Address.
 */
@Entity
@Table(name = "address")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class Address(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @Column(name = "country")
    var country: String? = null,

    @Column(name = "city")
    var city: String? = null,

    @Column(name = "full_address")
    var fullAddress: String? = null,

    @Column(name = "cabinet")
    var cabinet: String? = null,

    @Column(name = "lat")
    var lat: String? = null,

    @Column(name = "lon")
    var lon: String? = null,

    @Column(name = "postal_code")
    var postalCode: String? = null,

    @Column(name = "flag_deleted")
    var flagDeleted: Boolean? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Address) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "Address{" +
        "id=$id" +
        ", country='$country'" +
        ", city='$city'" +
        ", fullAddress='$fullAddress'" +
        ", cabinet='$cabinet'" +
        ", lat='$lat'" +
        ", lon='$lon'" +
        ", postalCode='$postalCode'" +
        ", flagDeleted='$flagDeleted'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
