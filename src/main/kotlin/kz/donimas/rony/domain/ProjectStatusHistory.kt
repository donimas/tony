package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import java.time.ZonedDateTime
import javax.persistence.*

/**
 * A ProjectStatusHistory.
 */
@Entity
@Table(name = "project_status_history")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class ProjectStatusHistory(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @Column(name = "comment")
    var comment: String? = null,

    @Column(name = "create_date")
    var createDate: ZonedDateTime? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["projectStatusHistories"], allowSetters = true)
    var status: Dictionary? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["projectStatusHistories"], allowSetters = true)
    var project: Project? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ProjectStatusHistory) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "ProjectStatusHistory{" +
        "id=$id" +
        ", comment='$comment'" +
        ", createDate='$createDate'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
