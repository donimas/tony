package kz.donimas.rony.domain

import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import javax.persistence.*

/**
 * A Attachment.
 */
@Entity
@Table(name = "attachment")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class Attachment(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @Lob
    @Column(name = "file")
    var file: ByteArray? = null,

    @Column(name = "file_content_type")
    var fileContentType: String? = null,

    @Column(name = "type")
    var type: String? = null,

    @Column(name = "uri")
    var uri: String? = null,

    @Column(name = "unicode")
    var unicode: String? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "entity_name")
    var entityName: String? = null,

    @Column(name = "entity_id")
    var entityId: Long? = null,

    @Column(name = "flag_deleted")
    var flagDeleted: Boolean? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Attachment) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "Attachment{" +
        "id=$id" +
        ", file='$file'" +
        ", fileContentType='$fileContentType'" +
        ", type='$type'" +
        ", uri='$uri'" +
        ", unicode='$unicode'" +
        ", name='$name'" +
        ", entityName='$entityName'" +
        ", entityId=$entityId" +
        ", flagDeleted='$flagDeleted'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
