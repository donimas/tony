package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import javax.persistence.*

/**
 * A ProjectTeam.
 */
@Entity
@Table(name = "project_team")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class ProjectTeam(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @Column(name = "flag_deleted")
    var flagDeleted: Boolean? = null,

    @OneToMany(mappedBy = "team")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    var projects: MutableSet<Project> = mutableSetOf(),

    @OneToMany(mappedBy = "team")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    var teamMembers: MutableSet<TeamMember> = mutableSetOf(),

    @ManyToOne @JsonIgnoreProperties(value = ["projectTeams"], allowSetters = true)
    var lead: User? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {

    fun addProject(project: Project): ProjectTeam {
        this.projects.add(project)
        project.team = this
        return this
    }

    fun removeProject(project: Project): ProjectTeam {
        this.projects.remove(project)
        project.team = null
        return this
    }

    fun addTeamMember(teamMember: TeamMember): ProjectTeam {
        this.teamMembers.add(teamMember)
        teamMember.team = this
        return this
    }

    fun removeTeamMember(teamMember: TeamMember): ProjectTeam {
        this.teamMembers.remove(teamMember)
        teamMember.team = null
        return this
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ProjectTeam) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "ProjectTeam{" +
        "id=$id" +
        ", flagDeleted='$flagDeleted'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
