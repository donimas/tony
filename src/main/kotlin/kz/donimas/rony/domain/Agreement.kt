package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import java.math.BigDecimal
import java.time.Instant
import java.time.ZonedDateTime
import javax.persistence.*

/**
 * A Agreement.
 */
@Entity
@Table(name = "agreement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class Agreement(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @Column(name = "create_date")
    var createDate: ZonedDateTime? = null,

    @Column(name = "comment")
    var comment: String? = null,

    @Column(name = "progress_bar", precision = 21, scale = 2)
    var progressBar: BigDecimal? = null,

    @Column(name = "start_date")
    var startDate: Instant? = null,

    @Column(name = "finish_date")
    var finishDate: Instant? = null,

    @Column(name = "flag_deleted")
    var flagDeleted: Boolean? = null,

    @OneToMany(mappedBy = "agreement")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    var contracts: MutableSet<Contract> = mutableSetOf(),

    @ManyToOne @JsonIgnoreProperties(value = ["agreements"], allowSetters = true)
    var statusHistory: AgreementStatusHistory? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["agreements"], allowSetters = true)
    var customer: CompanyInfo? = null,

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(
        name = "agreement_main_contact",
        joinColumns = [JoinColumn(name = "agreement_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "main_contact_id", referencedColumnName = "id")]
    )
    var mainContacts: MutableSet<MainContact> = mutableSetOf(),

    @ManyToOne @JsonIgnoreProperties(value = ["agreements"], allowSetters = true)
    var project: Project? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {

    fun addContract(contract: Contract): Agreement {
        this.contracts.add(contract)
        contract.agreement = this
        return this
    }

    fun removeContract(contract: Contract): Agreement {
        this.contracts.remove(contract)
        contract.agreement = null
        return this
    }

    fun addMainContact(mainContact: MainContact): Agreement {
        this.mainContacts.add(mainContact)
        return this
    }

    fun removeMainContact(mainContact: MainContact): Agreement {
        this.mainContacts.remove(mainContact)
        return this
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Agreement) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "Agreement{" +
        "id=$id" +
        ", createDate='$createDate'" +
        ", comment='$comment'" +
        ", progressBar=$progressBar" +
        ", startDate='$startDate'" +
        ", finishDate='$finishDate'" +
        ", flagDeleted='$flagDeleted'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
