package kz.donimas.rony.domain.enumeration

/**
 * The TransactionStatus enumeration.
 */
enum class TransactionStatus {
    PLANNING, DECLINED, PAID
}
