package kz.donimas.rony.domain.enumeration

/**
 * The PartnerShareType enumeration.
 */
enum class PartnerShareType {
    PERCENTAGE_FROM_CONTRACT, SUM_FROM_CONTRACT
}
