package kz.donimas.rony.domain.enumeration

/**
 * The DirectionType enumeration.
 */
enum class DirectionType {
    EMPTY, INCOME, EXPENSE
}
