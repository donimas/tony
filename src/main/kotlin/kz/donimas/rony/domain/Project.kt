package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import java.time.Instant
import java.time.ZonedDateTime
import javax.persistence.*
import javax.validation.constraints.*

/**
 * A Project.
 */
@Entity
@Table(name = "project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class Project(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @get: NotNull
    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "goal")
    var goal: String? = null,

    @Column(name = "start_date")
    var startDate: Instant? = null,

    @Column(name = "finish_date")
    var finishDate: Instant? = null,

    @Column(name = "uri")
    var uri: String? = null,

    @Column(name = "create_date")
    var createDate: ZonedDateTime? = null,

    @Column(name = "flag_deleted")
    var flagDeleted: Boolean? = null,

    @OneToMany(mappedBy = "project")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    var agreements: MutableSet<Agreement> = mutableSetOf(),

    @OneToMany(mappedBy = "project")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    var deals: MutableSet<Contract> = mutableSetOf(),

    @ManyToOne @JsonIgnoreProperties(value = ["projects"], allowSetters = true)
    var statusHistory: ProjectStatusHistory? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["projects"], allowSetters = true)
    var direction: Structure? = null,

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(
        name = "project_tech_stack",
        joinColumns = [JoinColumn(name = "project_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "tech_stack_id", referencedColumnName = "id")]
    )
    var techStacks: MutableSet<TechStack> = mutableSetOf(),

    @ManyToOne @JsonIgnoreProperties(value = ["projects"], allowSetters = true)
    var team: ProjectTeam? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {

    fun addAgreement(agreement: Agreement): Project {
        this.agreements.add(agreement)
        agreement.project = this
        return this
    }

    fun removeAgreement(agreement: Agreement): Project {
        this.agreements.remove(agreement)
        agreement.project = null
        return this
    }

    fun addDeal(deal: Contract): Project {
        this.deals.add(deal)
        deal.project = this
        return this
    }

    fun removeDeal(deal: Contract): Project {
        this.deals.remove(deal)
        deal.project = null
        return this
    }

    fun addTechStack(techStack: TechStack): Project {
        this.techStacks.add(techStack)
        return this
    }

    fun removeTechStack(techStack: TechStack): Project {
        this.techStacks.remove(techStack)
        return this
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Project) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "Project{" +
        "id=$id" +
        ", name='$name'" +
        ", description='$description'" +
        ", goal='$goal'" +
        ", startDate='$startDate'" +
        ", finishDate='$finishDate'" +
        ", uri='$uri'" +
        ", createDate='$createDate'" +
        ", flagDeleted='$flagDeleted'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
