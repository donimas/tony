package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import javax.persistence.*

/**
 * A TechStack.
 */
@Entity
@Table(name = "tech_stack")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class TechStack(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @ManyToOne @JsonIgnoreProperties(value = ["techStacks"], allowSetters = true)
    var title: Dictionary? = null,

    @ManyToMany(mappedBy = "techStacks")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    var projects: MutableSet<Project> = mutableSetOf()

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {

    fun addProject(project: Project): TechStack {
        this.projects.add(project)
        project.techStacks.add(this)
        return this
    }

    fun removeProject(project: Project): TechStack {
        this.projects.remove(project)
        project.techStacks.remove(this)
        return this
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TechStack) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "TechStack{" +
        "id=$id" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
