package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import javax.persistence.*
import javax.validation.constraints.*

/**
 * A CompanyInfo.
 */
@Entity
@Table(name = "company_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class CompanyInfo(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @get: NotNull
    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "website")
    var website: String? = null,

    @Column(name = "requisites")
    var requisites: String? = null,

    @Column(name = "flag_deleted")
    var flagDeleted: Boolean? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["companyInfos"], allowSetters = true)
    var address: Address? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CompanyInfo) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "CompanyInfo{" +
        "id=$id" +
        ", name='$name'" +
        ", website='$website'" +
        ", requisites='$requisites'" +
        ", flagDeleted='$flagDeleted'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
