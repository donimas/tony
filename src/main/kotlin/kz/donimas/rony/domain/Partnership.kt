package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import kz.donimas.rony.domain.enumeration.PartnerShareType
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.*

/**
 * A Partnership.
 */
@Entity
@Table(name = "partnership")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class Partnership(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["partnerships"], allowSetters = true)
    var type: Dictionary? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["partnerships"], allowSetters = true)
    var contractor: CompanyInfo? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["partnerships"], allowSetters = true)
    var contract: Contract? = null,

    @Column(name = "share", precision = 21, scale = 2)
    var share: BigDecimal? = null,

    @Enumerated(EnumType.STRING)
    @Column(name = "share_type")
    var shareType: PartnerShareType? = null,

    var workTitle: String? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Partnership) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "Partnership{" +
        "id=$id" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
