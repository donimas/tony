package kz.donimas.rony.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import java.io.Serializable
import java.math.BigDecimal
import java.time.Instant
import java.time.ZonedDateTime
import javax.persistence.*

/**
 * A TeamMember.
 */
@Entity
@Table(name = "team_member")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class TeamMember(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @Column(name = "involvement", precision = 21, scale = 2)
    var involvement: BigDecimal? = null,

    @Column(name = "start_date")
    var startDate: Instant? = null,

    @Column(name = "finish_date")
    var finishDate: Instant? = null,

    @Column(name = "flag_left")
    var flagLeft: Boolean? = null,

    @Column(name = "create_date")
    var createDate: ZonedDateTime? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["teamMembers"], allowSetters = true)
    var employee: User? = null,

    @ManyToOne @JsonIgnoreProperties(value = ["teamMembers"], allowSetters = true)
    var team: ProjectTeam? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TeamMember) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "TeamMember{" +
        "id=$id" +
        ", involvement=$involvement" +
        ", startDate='$startDate'" +
        ", finishDate='$finishDate'" +
        ", flagLeft='$flagLeft'" +
        ", createDate='$createDate'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
