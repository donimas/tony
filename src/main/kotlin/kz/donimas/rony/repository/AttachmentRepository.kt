package kz.donimas.rony.repository

import kz.donimas.rony.domain.Attachment
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Attachment] entity.
 */
@Suppress("unused")
@Repository
interface AttachmentRepository : JpaRepository<Attachment, Long>
