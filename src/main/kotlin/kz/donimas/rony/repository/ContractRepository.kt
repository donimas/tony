package kz.donimas.rony.repository

import kz.donimas.rony.domain.Contract
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Contract] entity.
 */
@Suppress("unused")
@Repository
interface ContractRepository : JpaRepository<Contract, Long> {

    @Query("select contract from Contract contract where contract.responsiblePm.login = ?#{principal.username}")
    fun findByResponsiblePmIsCurrentUser(): MutableList<Contract>

    fun findAllByFlagDeletedIsFalse(pageable: Pageable): Page<Contract>
}
