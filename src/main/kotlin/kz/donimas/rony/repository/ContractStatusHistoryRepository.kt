package kz.donimas.rony.repository

import kz.donimas.rony.domain.ContractStatusHistory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [ContractStatusHistory] entity.
 */
@Suppress("unused")
@Repository
interface ContractStatusHistoryRepository : JpaRepository<ContractStatusHistory, Long>
