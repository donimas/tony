package kz.donimas.rony.repository

import kz.donimas.rony.domain.Partnership
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Partnership] entity.
 */
@Suppress("unused")
@Repository
interface PartnershipRepository : JpaRepository<Partnership, Long>
