package kz.donimas.rony.repository

import kz.donimas.rony.domain.AgreementStatusHistory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [AgreementStatusHistory] entity.
 */
@Suppress("unused")
@Repository
interface AgreementStatusHistoryRepository : JpaRepository<AgreementStatusHistory, Long> {

    fun findAllByStatusEntityName(entityName: String): MutableList<AgreementStatusHistory>
}
