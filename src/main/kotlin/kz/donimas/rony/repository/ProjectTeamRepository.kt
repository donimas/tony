package kz.donimas.rony.repository

import kz.donimas.rony.domain.ProjectTeam
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [ProjectTeam] entity.
 */
@Suppress("unused")
@Repository
interface ProjectTeamRepository : JpaRepository<ProjectTeam, Long> {

    @Query("select projectTeam from ProjectTeam projectTeam where projectTeam.lead.login = ?#{principal.username}")
    fun findByLeadIsCurrentUser(): MutableList<ProjectTeam>
}
