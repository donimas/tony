package kz.donimas.rony.repository

import kz.donimas.rony.domain.Currency
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Currency] entity.
 */
@Suppress("unused")
@Repository
interface CurrencyRepository : JpaRepository<Currency, Long> {

    fun findAllByFormula(formula: String): MutableList<Currency>
}
