package kz.donimas.rony.repository

import kz.donimas.rony.domain.Agreement
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.Optional

/**
 * Spring Data  repository for the [Agreement] entity.
 */
@Repository
interface AgreementRepository : JpaRepository<Agreement, Long> {

    @Query(
        value = "select distinct agreement from Agreement agreement left join fetch agreement.mainContacts",
        countQuery = "select count(distinct agreement) from Agreement agreement"
    )
    fun findAllWithEagerRelationships(pageable: Pageable): Page<Agreement>

    fun findAllByFlagDeletedIsFalse(pageable: Pageable): Page<Agreement>

    @Query("select distinct agreement from Agreement agreement left join fetch agreement.mainContacts")
    fun findAllWithEagerRelationships(): MutableList<Agreement>

    @Query("select agreement from Agreement agreement left join fetch agreement.mainContacts where agreement.id =:id")
    fun findOneWithEagerRelationships(@Param("id") id: Long): Optional<Agreement>

    fun findAllByFlagDeletedIsFalseAndProjectNameContainingIgnoreCase(query: String): MutableList<Agreement>
}
