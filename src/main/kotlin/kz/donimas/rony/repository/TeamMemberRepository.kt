package kz.donimas.rony.repository

import kz.donimas.rony.domain.TeamMember
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [TeamMember] entity.
 */
@Suppress("unused")
@Repository
interface TeamMemberRepository : JpaRepository<TeamMember, Long> {

    @Query("select teamMember from TeamMember teamMember where teamMember.employee.login = ?#{principal.username}")
    fun findByEmployeeIsCurrentUser(): MutableList<TeamMember>
}
