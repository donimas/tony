package kz.donimas.rony.repository

import kz.donimas.rony.domain.Dictionary
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Dictionary] entity.
 */
@Suppress("unused")
@Repository
interface DictionaryRepository : JpaRepository<Dictionary, Long> {

    fun findAllByEntityNameAndFlagDeletedIsFalse(entityName: String): MutableList<Dictionary>

    fun findAllByFlagDeletedIsFalse(pageable: Pageable): Page<Dictionary>
}
