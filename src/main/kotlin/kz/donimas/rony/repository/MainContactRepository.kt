package kz.donimas.rony.repository

import kz.donimas.rony.domain.MainContact
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [MainContact] entity.
 */
@Suppress("unused")
@Repository
interface MainContactRepository : JpaRepository<MainContact, Long> {

    fun findAllByFlagDeletedIsFalse(pageable: Pageable): Page<MainContact>
}
