package kz.donimas.rony.repository

import kz.donimas.rony.domain.CompanyInfo
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [CompanyInfo] entity.
 */
@Suppress("unused")
@Repository
interface CompanyInfoRepository : JpaRepository<CompanyInfo, Long> {

    fun findAllByFlagDeletedIsFalse(pageable: Pageable): Page<CompanyInfo>
}
