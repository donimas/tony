package kz.donimas.rony.repository

import kz.donimas.rony.domain.TechStack
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [TechStack] entity.
 */
@Suppress("unused")
@Repository
interface TechStackRepository : JpaRepository<TechStack, Long> {

    fun findAllByTitleEntityName(entityName: String, pageable: Pageable): Page<TechStack>
}
