package kz.donimas.rony.repository

import kz.donimas.rony.domain.Project
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.Optional

/**
 * Spring Data  repository for the [Project] entity.
 */
@Repository
interface ProjectRepository : JpaRepository<Project, Long> {

    @Query(
        value = "select distinct project from Project project left join fetch project.techStacks",
        countQuery = "select count(distinct project) from Project project"
    )
    fun findAllWithEagerRelationships(pageable: Pageable): Page<Project>

    @Query("select distinct project from Project project left join fetch project.techStacks")
    fun findAllWithEagerRelationships(): MutableList<Project>

    @Query("select project from Project project left join fetch project.techStacks where project.id =:id")
    fun findOneWithEagerRelationships(@Param("id") id: Long): Optional<Project>

    fun findAllByFlagDeletedIsFalse(pageable: Pageable): Page<Project>
}
