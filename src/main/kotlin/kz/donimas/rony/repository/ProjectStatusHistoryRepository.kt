package kz.donimas.rony.repository

import kz.donimas.rony.domain.ProjectStatusHistory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [ProjectStatusHistory] entity.
 */
@Suppress("unused")
@Repository
interface ProjectStatusHistoryRepository : JpaRepository<ProjectStatusHistory, Long> {

    fun findAllByStatus_EntityNameAndStatus_FlagDeleted_IsFalse(entityName: String): MutableList<ProjectStatusHistory>
}
