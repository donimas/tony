package kz.donimas.rony.repository

import kz.donimas.rony.domain.Address
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Address] entity.
 */
@Suppress("unused")
@Repository
interface AddressRepository : JpaRepository<Address, Long>
