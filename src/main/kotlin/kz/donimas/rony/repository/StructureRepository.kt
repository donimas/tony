package kz.donimas.rony.repository

import kz.donimas.rony.domain.Structure
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Structure] entity.
 */
@Suppress("unused")
@Repository
interface StructureRepository : JpaRepository<Structure, Long> {

    fun findAllByParentIsNullAndFlagDeletedIsFalse(): MutableList<Structure>

    fun findAllByFlagDeletedFalse(pageable: Pageable): Page<Structure>
}
