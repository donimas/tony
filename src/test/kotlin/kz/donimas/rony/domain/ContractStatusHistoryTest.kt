package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ContractStatusHistoryTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(ContractStatusHistory::class)
        val contractStatusHistory1 = ContractStatusHistory()
        contractStatusHistory1.id = 1L
        val contractStatusHistory2 = ContractStatusHistory()
        contractStatusHistory2.id = contractStatusHistory1.id
        assertThat(contractStatusHistory1).isEqualTo(contractStatusHistory2)
        contractStatusHistory2.id = 2L
        assertThat(contractStatusHistory1).isNotEqualTo(contractStatusHistory2)
        contractStatusHistory1.id = null
        assertThat(contractStatusHistory1).isNotEqualTo(contractStatusHistory2)
    }
}
