package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CompanyInfoTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(CompanyInfo::class)
        val companyInfo1 = CompanyInfo()
        companyInfo1.id = 1L
        val companyInfo2 = CompanyInfo()
        companyInfo2.id = companyInfo1.id
        assertThat(companyInfo1).isEqualTo(companyInfo2)
        companyInfo2.id = 2L
        assertThat(companyInfo1).isNotEqualTo(companyInfo2)
        companyInfo1.id = null
        assertThat(companyInfo1).isNotEqualTo(companyInfo2)
    }
}
