package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ProjectTeamTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(ProjectTeam::class)
        val projectTeam1 = ProjectTeam()
        projectTeam1.id = 1L
        val projectTeam2 = ProjectTeam()
        projectTeam2.id = projectTeam1.id
        assertThat(projectTeam1).isEqualTo(projectTeam2)
        projectTeam2.id = 2L
        assertThat(projectTeam1).isNotEqualTo(projectTeam2)
        projectTeam1.id = null
        assertThat(projectTeam1).isNotEqualTo(projectTeam2)
    }
}
