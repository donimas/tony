package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class MainContactTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(MainContact::class)
        val mainContact1 = MainContact()
        mainContact1.id = 1L
        val mainContact2 = MainContact()
        mainContact2.id = mainContact1.id
        assertThat(mainContact1).isEqualTo(mainContact2)
        mainContact2.id = 2L
        assertThat(mainContact1).isNotEqualTo(mainContact2)
        mainContact1.id = null
        assertThat(mainContact1).isNotEqualTo(mainContact2)
    }
}
