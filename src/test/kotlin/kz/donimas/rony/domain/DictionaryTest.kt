package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class DictionaryTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(Dictionary::class)
        val dictionary1 = Dictionary()
        dictionary1.id = 1L
        val dictionary2 = Dictionary()
        dictionary2.id = dictionary1.id
        assertThat(dictionary1).isEqualTo(dictionary2)
        dictionary2.id = 2L
        assertThat(dictionary1).isNotEqualTo(dictionary2)
        dictionary1.id = null
        assertThat(dictionary1).isNotEqualTo(dictionary2)
    }
}
