package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AgreementTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(Agreement::class)
        val agreement1 = Agreement()
        agreement1.id = 1L
        val agreement2 = Agreement()
        agreement2.id = agreement1.id
        assertThat(agreement1).isEqualTo(agreement2)
        agreement2.id = 2L
        assertThat(agreement1).isNotEqualTo(agreement2)
        agreement1.id = null
        assertThat(agreement1).isNotEqualTo(agreement2)
    }
}
