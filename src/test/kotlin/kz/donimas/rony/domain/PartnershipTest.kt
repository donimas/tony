package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class PartnershipTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(Partnership::class)
        val partnership1 = Partnership()
        partnership1.id = 1L
        val partnership2 = Partnership()
        partnership2.id = partnership1.id
        assertThat(partnership1).isEqualTo(partnership2)
        partnership2.id = 2L
        assertThat(partnership1).isNotEqualTo(partnership2)
        partnership1.id = null
        assertThat(partnership1).isNotEqualTo(partnership2)
    }
}
