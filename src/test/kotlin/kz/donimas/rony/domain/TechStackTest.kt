package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TechStackTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(TechStack::class)
        val techStack1 = TechStack()
        techStack1.id = 1L
        val techStack2 = TechStack()
        techStack2.id = techStack1.id
        assertThat(techStack1).isEqualTo(techStack2)
        techStack2.id = 2L
        assertThat(techStack1).isNotEqualTo(techStack2)
        techStack1.id = null
        assertThat(techStack1).isNotEqualTo(techStack2)
    }
}
