package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ContractTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(Contract::class)
        val contract1 = Contract()
        contract1.id = 1L
        val contract2 = Contract()
        contract2.id = contract1.id
        assertThat(contract1).isEqualTo(contract2)
        contract2.id = 2L
        assertThat(contract1).isNotEqualTo(contract2)
        contract1.id = null
        assertThat(contract1).isNotEqualTo(contract2)
    }
}
