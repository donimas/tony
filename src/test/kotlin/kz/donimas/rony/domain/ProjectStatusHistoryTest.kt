package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ProjectStatusHistoryTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(ProjectStatusHistory::class)
        val projectStatusHistory1 = ProjectStatusHistory()
        projectStatusHistory1.id = 1L
        val projectStatusHistory2 = ProjectStatusHistory()
        projectStatusHistory2.id = projectStatusHistory1.id
        assertThat(projectStatusHistory1).isEqualTo(projectStatusHistory2)
        projectStatusHistory2.id = 2L
        assertThat(projectStatusHistory1).isNotEqualTo(projectStatusHistory2)
        projectStatusHistory1.id = null
        assertThat(projectStatusHistory1).isNotEqualTo(projectStatusHistory2)
    }
}
