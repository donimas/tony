package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class StructureTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(Structure::class)
        val structure1 = Structure()
        structure1.id = 1L
        val structure2 = Structure()
        structure2.id = structure1.id
        assertThat(structure1).isEqualTo(structure2)
        structure2.id = 2L
        assertThat(structure1).isNotEqualTo(structure2)
        structure1.id = null
        assertThat(structure1).isNotEqualTo(structure2)
    }
}
