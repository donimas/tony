package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AttachmentTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(Attachment::class)
        val attachment1 = Attachment()
        attachment1.id = 1L
        val attachment2 = Attachment()
        attachment2.id = attachment1.id
        assertThat(attachment1).isEqualTo(attachment2)
        attachment2.id = 2L
        assertThat(attachment1).isNotEqualTo(attachment2)
        attachment1.id = null
        assertThat(attachment1).isNotEqualTo(attachment2)
    }
}
