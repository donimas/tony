package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TeamMemberTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(TeamMember::class)
        val teamMember1 = TeamMember()
        teamMember1.id = 1L
        val teamMember2 = TeamMember()
        teamMember2.id = teamMember1.id
        assertThat(teamMember1).isEqualTo(teamMember2)
        teamMember2.id = 2L
        assertThat(teamMember1).isNotEqualTo(teamMember2)
        teamMember1.id = null
        assertThat(teamMember1).isNotEqualTo(teamMember2)
    }
}
