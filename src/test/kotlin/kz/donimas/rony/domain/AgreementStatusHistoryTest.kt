package kz.donimas.rony.domain

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AgreementStatusHistoryTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(AgreementStatusHistory::class)
        val agreementStatusHistory1 = AgreementStatusHistory()
        agreementStatusHistory1.id = 1L
        val agreementStatusHistory2 = AgreementStatusHistory()
        agreementStatusHistory2.id = agreementStatusHistory1.id
        assertThat(agreementStatusHistory1).isEqualTo(agreementStatusHistory2)
        agreementStatusHistory2.id = 2L
        assertThat(agreementStatusHistory1).isNotEqualTo(agreementStatusHistory2)
        agreementStatusHistory1.id = null
        assertThat(agreementStatusHistory1).isNotEqualTo(agreementStatusHistory2)
    }
}
