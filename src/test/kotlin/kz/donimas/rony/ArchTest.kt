package kz.donimas.rony

import com.tngtech.archunit.core.importer.ClassFileImporter
import com.tngtech.archunit.core.importer.ImportOption
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses
import org.junit.jupiter.api.Test

class ArchTest {

    @Test
    fun servicesAndRepositoriesShouldNotDependOnWebLayer() {

        val importedClasses = ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("kz.donimas.rony")

        noClasses()
            .that()
            .resideInAnyPackage("kz.donimas.rony.service..")
            .or()
            .resideInAnyPackage("kz.donimas.rony.repository..")
            .should().dependOnClassesThat()
            .resideInAnyPackage("..kz.donimas.rony.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses)
    }
}
