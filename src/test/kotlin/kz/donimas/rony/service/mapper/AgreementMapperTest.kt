package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class AgreementMapperTest {

    private lateinit var agreementMapper: AgreementMapper

    @BeforeEach
    fun setUp() {
        agreementMapper = AgreementMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(agreementMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(agreementMapper.fromId(null)).isNull()
    }
}
