package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class AddressMapperTest {

    private lateinit var addressMapper: AddressMapper

    @BeforeEach
    fun setUp() {
        addressMapper = AddressMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(addressMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(addressMapper.fromId(null)).isNull()
    }
}
