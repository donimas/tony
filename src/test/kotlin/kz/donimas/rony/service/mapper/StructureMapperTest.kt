package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class StructureMapperTest {

    private lateinit var structureMapper: StructureMapper

    @BeforeEach
    fun setUp() {
        structureMapper = StructureMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(structureMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(structureMapper.fromId(null)).isNull()
    }
}
