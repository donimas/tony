package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ProjectMapperTest {

    private lateinit var projectMapper: ProjectMapper

    @BeforeEach
    fun setUp() {
        projectMapper = ProjectMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(projectMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(projectMapper.fromId(null)).isNull()
    }
}
