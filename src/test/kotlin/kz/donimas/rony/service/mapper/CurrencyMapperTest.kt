package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class CurrencyMapperTest {

    private lateinit var currencyMapper: CurrencyMapper

    @BeforeEach
    fun setUp() {
        currencyMapper = CurrencyMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(currencyMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(currencyMapper.fromId(null)).isNull()
    }
}
