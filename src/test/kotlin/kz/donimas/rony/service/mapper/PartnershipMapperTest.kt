package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class PartnershipMapperTest {

    private lateinit var partnershipMapper: PartnershipMapper

    @BeforeEach
    fun setUp() {
        partnershipMapper = PartnershipMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(partnershipMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(partnershipMapper.fromId(null)).isNull()
    }
}
