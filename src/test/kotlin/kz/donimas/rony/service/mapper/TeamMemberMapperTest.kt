package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TeamMemberMapperTest {

    private lateinit var teamMemberMapper: TeamMemberMapper

    @BeforeEach
    fun setUp() {
        teamMemberMapper = TeamMemberMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(teamMemberMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(teamMemberMapper.fromId(null)).isNull()
    }
}
