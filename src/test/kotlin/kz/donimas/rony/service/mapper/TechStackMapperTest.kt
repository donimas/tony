package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TechStackMapperTest {

    private lateinit var techStackMapper: TechStackMapper

    @BeforeEach
    fun setUp() {
        techStackMapper = TechStackMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(techStackMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(techStackMapper.fromId(null)).isNull()
    }
}
