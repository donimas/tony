package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ContractStatusHistoryMapperTest {

    private lateinit var contractStatusHistoryMapper: ContractStatusHistoryMapper

    @BeforeEach
    fun setUp() {
        contractStatusHistoryMapper = ContractStatusHistoryMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(contractStatusHistoryMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(contractStatusHistoryMapper.fromId(null)).isNull()
    }
}
