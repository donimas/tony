package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class CompanyInfoMapperTest {

    private lateinit var companyInfoMapper: CompanyInfoMapper

    @BeforeEach
    fun setUp() {
        companyInfoMapper = CompanyInfoMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(companyInfoMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(companyInfoMapper.fromId(null)).isNull()
    }
}
