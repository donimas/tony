package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ContractMapperTest {

    private lateinit var contractMapper: ContractMapper

    @BeforeEach
    fun setUp() {
        contractMapper = ContractMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(contractMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(contractMapper.fromId(null)).isNull()
    }
}
