package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ProjectTeamMapperTest {

    private lateinit var projectTeamMapper: ProjectTeamMapper

    @BeforeEach
    fun setUp() {
        projectTeamMapper = ProjectTeamMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(projectTeamMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(projectTeamMapper.fromId(null)).isNull()
    }
}
