package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class AgreementStatusHistoryMapperTest {

    private lateinit var agreementStatusHistoryMapper: AgreementStatusHistoryMapper

    @BeforeEach
    fun setUp() {
        agreementStatusHistoryMapper = AgreementStatusHistoryMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(agreementStatusHistoryMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(agreementStatusHistoryMapper.fromId(null)).isNull()
    }
}
