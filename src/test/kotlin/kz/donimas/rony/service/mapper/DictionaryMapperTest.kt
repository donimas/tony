package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class DictionaryMapperTest {

    private lateinit var dictionaryMapper: DictionaryMapper

    @BeforeEach
    fun setUp() {
        dictionaryMapper = DictionaryMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(dictionaryMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(dictionaryMapper.fromId(null)).isNull()
    }
}
