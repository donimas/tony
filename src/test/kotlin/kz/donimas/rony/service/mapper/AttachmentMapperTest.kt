package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class AttachmentMapperTest {

    private lateinit var attachmentMapper: AttachmentMapper

    @BeforeEach
    fun setUp() {
        attachmentMapper = AttachmentMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(attachmentMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(attachmentMapper.fromId(null)).isNull()
    }
}
