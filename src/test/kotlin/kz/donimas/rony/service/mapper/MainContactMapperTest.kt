package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MainContactMapperTest {

    private lateinit var mainContactMapper: MainContactMapper

    @BeforeEach
    fun setUp() {
        mainContactMapper = MainContactMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(mainContactMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(mainContactMapper.fromId(null)).isNull()
    }
}
