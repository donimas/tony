package kz.donimas.rony.service.mapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ProjectStatusHistoryMapperTest {

    private lateinit var projectStatusHistoryMapper: ProjectStatusHistoryMapper

    @BeforeEach
    fun setUp() {
        projectStatusHistoryMapper = ProjectStatusHistoryMapperImpl()
    }

    @Test
    fun testEntityFromId() {
        val id = 1L
        assertThat(projectStatusHistoryMapper.fromId(id)?.id).isEqualTo(id)
        assertThat(projectStatusHistoryMapper.fromId(null)).isNull()
    }
}
