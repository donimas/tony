package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class PartnershipDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(PartnershipDTO::class)
        val partnershipDTO1 = PartnershipDTO()
        partnershipDTO1.id = 1L
        val partnershipDTO2 = PartnershipDTO()
        assertThat(partnershipDTO1).isNotEqualTo(partnershipDTO2)
        partnershipDTO2.id = partnershipDTO1.id
        assertThat(partnershipDTO1).isEqualTo(partnershipDTO2)
        partnershipDTO2.id = 2L
        assertThat(partnershipDTO1).isNotEqualTo(partnershipDTO2)
        partnershipDTO1.id = null
        assertThat(partnershipDTO1).isNotEqualTo(partnershipDTO2)
    }
}
