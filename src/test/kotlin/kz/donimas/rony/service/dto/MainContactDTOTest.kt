package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class MainContactDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(MainContactDTO::class)
        val mainContactDTO1 = MainContactDTO()
        mainContactDTO1.id = 1L
        val mainContactDTO2 = MainContactDTO()
        assertThat(mainContactDTO1).isNotEqualTo(mainContactDTO2)
        mainContactDTO2.id = mainContactDTO1.id
        assertThat(mainContactDTO1).isEqualTo(mainContactDTO2)
        mainContactDTO2.id = 2L
        assertThat(mainContactDTO1).isNotEqualTo(mainContactDTO2)
        mainContactDTO1.id = null
        assertThat(mainContactDTO1).isNotEqualTo(mainContactDTO2)
    }
}
