package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AgreementStatusHistoryDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(AgreementStatusHistoryDTO::class)
        val agreementStatusHistoryDTO1 = AgreementStatusHistoryDTO()
        agreementStatusHistoryDTO1.id = 1L
        val agreementStatusHistoryDTO2 = AgreementStatusHistoryDTO()
        assertThat(agreementStatusHistoryDTO1).isNotEqualTo(agreementStatusHistoryDTO2)
        agreementStatusHistoryDTO2.id = agreementStatusHistoryDTO1.id
        assertThat(agreementStatusHistoryDTO1).isEqualTo(agreementStatusHistoryDTO2)
        agreementStatusHistoryDTO2.id = 2L
        assertThat(agreementStatusHistoryDTO1).isNotEqualTo(agreementStatusHistoryDTO2)
        agreementStatusHistoryDTO1.id = null
        assertThat(agreementStatusHistoryDTO1).isNotEqualTo(agreementStatusHistoryDTO2)
    }
}
