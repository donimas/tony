package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CurrencyDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(CurrencyDTO::class)
        val currencyDTO1 = CurrencyDTO()
        currencyDTO1.id = 1L
        val currencyDTO2 = CurrencyDTO()
        assertThat(currencyDTO1).isNotEqualTo(currencyDTO2)
        currencyDTO2.id = currencyDTO1.id
        assertThat(currencyDTO1).isEqualTo(currencyDTO2)
        currencyDTO2.id = 2L
        assertThat(currencyDTO1).isNotEqualTo(currencyDTO2)
        currencyDTO1.id = null
        assertThat(currencyDTO1).isNotEqualTo(currencyDTO2)
    }
}
