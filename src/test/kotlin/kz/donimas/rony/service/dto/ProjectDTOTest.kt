package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ProjectDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(ProjectDTO::class)
        val projectDTO1 = ProjectDTO()
        projectDTO1.id = 1L
        val projectDTO2 = ProjectDTO()
        assertThat(projectDTO1).isNotEqualTo(projectDTO2)
        projectDTO2.id = projectDTO1.id
        assertThat(projectDTO1).isEqualTo(projectDTO2)
        projectDTO2.id = 2L
        assertThat(projectDTO1).isNotEqualTo(projectDTO2)
        projectDTO1.id = null
        assertThat(projectDTO1).isNotEqualTo(projectDTO2)
    }
}
