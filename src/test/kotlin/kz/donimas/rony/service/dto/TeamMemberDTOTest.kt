package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TeamMemberDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(TeamMemberDTO::class)
        val teamMemberDTO1 = TeamMemberDTO()
        teamMemberDTO1.id = 1L
        val teamMemberDTO2 = TeamMemberDTO()
        assertThat(teamMemberDTO1).isNotEqualTo(teamMemberDTO2)
        teamMemberDTO2.id = teamMemberDTO1.id
        assertThat(teamMemberDTO1).isEqualTo(teamMemberDTO2)
        teamMemberDTO2.id = 2L
        assertThat(teamMemberDTO1).isNotEqualTo(teamMemberDTO2)
        teamMemberDTO1.id = null
        assertThat(teamMemberDTO1).isNotEqualTo(teamMemberDTO2)
    }
}
