package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ProjectTeamDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(ProjectTeamDTO::class)
        val projectTeamDTO1 = ProjectTeamDTO()
        projectTeamDTO1.id = 1L
        val projectTeamDTO2 = ProjectTeamDTO()
        assertThat(projectTeamDTO1).isNotEqualTo(projectTeamDTO2)
        projectTeamDTO2.id = projectTeamDTO1.id
        assertThat(projectTeamDTO1).isEqualTo(projectTeamDTO2)
        projectTeamDTO2.id = 2L
        assertThat(projectTeamDTO1).isNotEqualTo(projectTeamDTO2)
        projectTeamDTO1.id = null
        assertThat(projectTeamDTO1).isNotEqualTo(projectTeamDTO2)
    }
}
