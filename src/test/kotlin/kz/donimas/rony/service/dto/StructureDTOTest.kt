package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class StructureDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(StructureDTO::class)
        val structureDTO1 = StructureDTO()
        structureDTO1.id = 1L
        val structureDTO2 = StructureDTO()
        assertThat(structureDTO1).isNotEqualTo(structureDTO2)
        structureDTO2.id = structureDTO1.id
        assertThat(structureDTO1).isEqualTo(structureDTO2)
        structureDTO2.id = 2L
        assertThat(structureDTO1).isNotEqualTo(structureDTO2)
        structureDTO1.id = null
        assertThat(structureDTO1).isNotEqualTo(structureDTO2)
    }
}
