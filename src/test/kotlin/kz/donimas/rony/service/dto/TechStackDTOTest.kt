package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class TechStackDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(TechStackDTO::class)
        val techStackDTO1 = TechStackDTO()
        techStackDTO1.id = 1L
        val techStackDTO2 = TechStackDTO()
        assertThat(techStackDTO1).isNotEqualTo(techStackDTO2)
        techStackDTO2.id = techStackDTO1.id
        assertThat(techStackDTO1).isEqualTo(techStackDTO2)
        techStackDTO2.id = 2L
        assertThat(techStackDTO1).isNotEqualTo(techStackDTO2)
        techStackDTO1.id = null
        assertThat(techStackDTO1).isNotEqualTo(techStackDTO2)
    }
}
