package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class DictionaryDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(DictionaryDTO::class)
        val dictionaryDTO1 = DictionaryDTO()
        dictionaryDTO1.id = 1L
        val dictionaryDTO2 = DictionaryDTO()
        assertThat(dictionaryDTO1).isNotEqualTo(dictionaryDTO2)
        dictionaryDTO2.id = dictionaryDTO1.id
        assertThat(dictionaryDTO1).isEqualTo(dictionaryDTO2)
        dictionaryDTO2.id = 2L
        assertThat(dictionaryDTO1).isNotEqualTo(dictionaryDTO2)
        dictionaryDTO1.id = null
        assertThat(dictionaryDTO1).isNotEqualTo(dictionaryDTO2)
    }
}
