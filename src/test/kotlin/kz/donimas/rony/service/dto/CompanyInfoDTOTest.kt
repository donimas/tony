package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CompanyInfoDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(CompanyInfoDTO::class)
        val companyInfoDTO1 = CompanyInfoDTO()
        companyInfoDTO1.id = 1L
        val companyInfoDTO2 = CompanyInfoDTO()
        assertThat(companyInfoDTO1).isNotEqualTo(companyInfoDTO2)
        companyInfoDTO2.id = companyInfoDTO1.id
        assertThat(companyInfoDTO1).isEqualTo(companyInfoDTO2)
        companyInfoDTO2.id = 2L
        assertThat(companyInfoDTO1).isNotEqualTo(companyInfoDTO2)
        companyInfoDTO1.id = null
        assertThat(companyInfoDTO1).isNotEqualTo(companyInfoDTO2)
    }
}
