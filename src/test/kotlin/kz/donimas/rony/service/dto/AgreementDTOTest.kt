package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AgreementDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(AgreementDTO::class)
        val agreementDTO1 = AgreementDTO()
        agreementDTO1.id = 1L
        val agreementDTO2 = AgreementDTO()
        assertThat(agreementDTO1).isNotEqualTo(agreementDTO2)
        agreementDTO2.id = agreementDTO1.id
        assertThat(agreementDTO1).isEqualTo(agreementDTO2)
        agreementDTO2.id = 2L
        assertThat(agreementDTO1).isNotEqualTo(agreementDTO2)
        agreementDTO1.id = null
        assertThat(agreementDTO1).isNotEqualTo(agreementDTO2)
    }
}
