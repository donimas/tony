package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ProjectStatusHistoryDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(ProjectStatusHistoryDTO::class)
        val projectStatusHistoryDTO1 = ProjectStatusHistoryDTO()
        projectStatusHistoryDTO1.id = 1L
        val projectStatusHistoryDTO2 = ProjectStatusHistoryDTO()
        assertThat(projectStatusHistoryDTO1).isNotEqualTo(projectStatusHistoryDTO2)
        projectStatusHistoryDTO2.id = projectStatusHistoryDTO1.id
        assertThat(projectStatusHistoryDTO1).isEqualTo(projectStatusHistoryDTO2)
        projectStatusHistoryDTO2.id = 2L
        assertThat(projectStatusHistoryDTO1).isNotEqualTo(projectStatusHistoryDTO2)
        projectStatusHistoryDTO1.id = null
        assertThat(projectStatusHistoryDTO1).isNotEqualTo(projectStatusHistoryDTO2)
    }
}
