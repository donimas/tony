package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ContractDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(ContractDTO::class)
        val contractDTO1 = ContractDTO()
        contractDTO1.id = 1L
        val contractDTO2 = ContractDTO()
        assertThat(contractDTO1).isNotEqualTo(contractDTO2)
        contractDTO2.id = contractDTO1.id
        assertThat(contractDTO1).isEqualTo(contractDTO2)
        contractDTO2.id = 2L
        assertThat(contractDTO1).isNotEqualTo(contractDTO2)
        contractDTO1.id = null
        assertThat(contractDTO1).isNotEqualTo(contractDTO2)
    }
}
