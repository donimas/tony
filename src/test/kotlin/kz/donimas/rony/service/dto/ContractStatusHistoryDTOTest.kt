package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ContractStatusHistoryDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(ContractStatusHistoryDTO::class)
        val contractStatusHistoryDTO1 = ContractStatusHistoryDTO()
        contractStatusHistoryDTO1.id = 1L
        val contractStatusHistoryDTO2 = ContractStatusHistoryDTO()
        assertThat(contractStatusHistoryDTO1).isNotEqualTo(contractStatusHistoryDTO2)
        contractStatusHistoryDTO2.id = contractStatusHistoryDTO1.id
        assertThat(contractStatusHistoryDTO1).isEqualTo(contractStatusHistoryDTO2)
        contractStatusHistoryDTO2.id = 2L
        assertThat(contractStatusHistoryDTO1).isNotEqualTo(contractStatusHistoryDTO2)
        contractStatusHistoryDTO1.id = null
        assertThat(contractStatusHistoryDTO1).isNotEqualTo(contractStatusHistoryDTO2)
    }
}
