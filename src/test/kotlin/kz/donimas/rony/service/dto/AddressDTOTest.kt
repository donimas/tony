package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AddressDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(AddressDTO::class)
        val addressDTO1 = AddressDTO()
        addressDTO1.id = 1L
        val addressDTO2 = AddressDTO()
        assertThat(addressDTO1).isNotEqualTo(addressDTO2)
        addressDTO2.id = addressDTO1.id
        assertThat(addressDTO1).isEqualTo(addressDTO2)
        addressDTO2.id = 2L
        assertThat(addressDTO1).isNotEqualTo(addressDTO2)
        addressDTO1.id = null
        assertThat(addressDTO1).isNotEqualTo(addressDTO2)
    }
}
