package kz.donimas.rony.service.dto

import kz.donimas.rony.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AttachmentDTOTest {

    @Test
    fun dtoEqualsVerifier() {
        equalsVerifier(AttachmentDTO::class)
        val attachmentDTO1 = AttachmentDTO()
        attachmentDTO1.id = 1L
        val attachmentDTO2 = AttachmentDTO()
        assertThat(attachmentDTO1).isNotEqualTo(attachmentDTO2)
        attachmentDTO2.id = attachmentDTO1.id
        assertThat(attachmentDTO1).isEqualTo(attachmentDTO2)
        attachmentDTO2.id = 2L
        assertThat(attachmentDTO1).isNotEqualTo(attachmentDTO2)
        attachmentDTO1.id = null
        assertThat(attachmentDTO1).isNotEqualTo(attachmentDTO2)
    }
}
