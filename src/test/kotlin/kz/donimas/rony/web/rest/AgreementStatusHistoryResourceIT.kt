package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.AgreementStatusHistory
import kz.donimas.rony.repository.AgreementStatusHistoryRepository
import kz.donimas.rony.service.AgreementStatusHistoryService
import kz.donimas.rony.service.mapper.AgreementStatusHistoryMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [AgreementStatusHistoryResource] REST controller.
 *
 * @see AgreementStatusHistoryResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class AgreementStatusHistoryResourceIT {

    @Autowired
    private lateinit var agreementStatusHistoryRepository: AgreementStatusHistoryRepository

    @Autowired
    private lateinit var agreementStatusHistoryMapper: AgreementStatusHistoryMapper

    @Autowired
    private lateinit var agreementStatusHistoryService: AgreementStatusHistoryService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restAgreementStatusHistoryMockMvc: MockMvc

    private lateinit var agreementStatusHistory: AgreementStatusHistory

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val agreementStatusHistoryResource = AgreementStatusHistoryResource(agreementStatusHistoryService)
        this.restAgreementStatusHistoryMockMvc = MockMvcBuilders.standaloneSetup(agreementStatusHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        agreementStatusHistory = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createAgreementStatusHistory() {
        val databaseSizeBeforeCreate = agreementStatusHistoryRepository.findAll().size

        // Create the AgreementStatusHistory
        val agreementStatusHistoryDTO = agreementStatusHistoryMapper.toDto(agreementStatusHistory)
        restAgreementStatusHistoryMockMvc.perform(
            post("/api/agreement-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(agreementStatusHistoryDTO))
        ).andExpect(status().isCreated)

        // Validate the AgreementStatusHistory in the database
        val agreementStatusHistoryList = agreementStatusHistoryRepository.findAll()
        assertThat(agreementStatusHistoryList).hasSize(databaseSizeBeforeCreate + 1)
        val testAgreementStatusHistory = agreementStatusHistoryList[agreementStatusHistoryList.size - 1]
        assertThat(testAgreementStatusHistory.createDate).isEqualTo(DEFAULT_CREATE_DATE)
        assertThat(testAgreementStatusHistory.comment).isEqualTo(DEFAULT_COMMENT)
    }

    @Test
    @Transactional
    fun createAgreementStatusHistoryWithExistingId() {
        val databaseSizeBeforeCreate = agreementStatusHistoryRepository.findAll().size

        // Create the AgreementStatusHistory with an existing ID
        agreementStatusHistory.id = 1L
        val agreementStatusHistoryDTO = agreementStatusHistoryMapper.toDto(agreementStatusHistory)

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgreementStatusHistoryMockMvc.perform(
            post("/api/agreement-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(agreementStatusHistoryDTO))
        ).andExpect(status().isBadRequest)

        // Validate the AgreementStatusHistory in the database
        val agreementStatusHistoryList = agreementStatusHistoryRepository.findAll()
        assertThat(agreementStatusHistoryList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllAgreementStatusHistories() {
        // Initialize the database
        agreementStatusHistoryRepository.saveAndFlush(agreementStatusHistory)

        // Get all the agreementStatusHistoryList
        restAgreementStatusHistoryMockMvc.perform(get("/api/agreement-status-histories?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agreementStatusHistory.id?.toInt())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAgreementStatusHistory() {
        // Initialize the database
        agreementStatusHistoryRepository.saveAndFlush(agreementStatusHistory)

        val id = agreementStatusHistory.id
        assertNotNull(id)

        // Get the agreementStatusHistory
        restAgreementStatusHistoryMockMvc.perform(get("/api/agreement-status-histories/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(agreementStatusHistory.id?.toInt()))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingAgreementStatusHistory() {
        // Get the agreementStatusHistory
        restAgreementStatusHistoryMockMvc.perform(get("/api/agreement-status-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateAgreementStatusHistory() {
        // Initialize the database
        agreementStatusHistoryRepository.saveAndFlush(agreementStatusHistory)

        val databaseSizeBeforeUpdate = agreementStatusHistoryRepository.findAll().size

        // Update the agreementStatusHistory
        val id = agreementStatusHistory.id
        assertNotNull(id)
        val updatedAgreementStatusHistory = agreementStatusHistoryRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedAgreementStatusHistory are not directly saved in db
        em.detach(updatedAgreementStatusHistory)
        updatedAgreementStatusHistory.createDate = UPDATED_CREATE_DATE
        updatedAgreementStatusHistory.comment = UPDATED_COMMENT
        val agreementStatusHistoryDTO = agreementStatusHistoryMapper.toDto(updatedAgreementStatusHistory)

        restAgreementStatusHistoryMockMvc.perform(
            put("/api/agreement-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(agreementStatusHistoryDTO))
        ).andExpect(status().isOk)

        // Validate the AgreementStatusHistory in the database
        val agreementStatusHistoryList = agreementStatusHistoryRepository.findAll()
        assertThat(agreementStatusHistoryList).hasSize(databaseSizeBeforeUpdate)
        val testAgreementStatusHistory = agreementStatusHistoryList[agreementStatusHistoryList.size - 1]
        assertThat(testAgreementStatusHistory.createDate).isEqualTo(UPDATED_CREATE_DATE)
        assertThat(testAgreementStatusHistory.comment).isEqualTo(UPDATED_COMMENT)
    }

    @Test
    @Transactional
    fun updateNonExistingAgreementStatusHistory() {
        val databaseSizeBeforeUpdate = agreementStatusHistoryRepository.findAll().size

        // Create the AgreementStatusHistory
        val agreementStatusHistoryDTO = agreementStatusHistoryMapper.toDto(agreementStatusHistory)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgreementStatusHistoryMockMvc.perform(
            put("/api/agreement-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(agreementStatusHistoryDTO))
        ).andExpect(status().isBadRequest)

        // Validate the AgreementStatusHistory in the database
        val agreementStatusHistoryList = agreementStatusHistoryRepository.findAll()
        assertThat(agreementStatusHistoryList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteAgreementStatusHistory() {
        // Initialize the database
        agreementStatusHistoryRepository.saveAndFlush(agreementStatusHistory)

        val databaseSizeBeforeDelete = agreementStatusHistoryRepository.findAll().size

        // Delete the agreementStatusHistory
        restAgreementStatusHistoryMockMvc.perform(
            delete("/api/agreement-status-histories/{id}", agreementStatusHistory.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val agreementStatusHistoryList = agreementStatusHistoryRepository.findAll()
        assertThat(agreementStatusHistoryList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private val DEFAULT_CREATE_DATE: ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC)
        private val UPDATED_CREATE_DATE: ZonedDateTime = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0)

        private const val DEFAULT_COMMENT = "AAAAAAAAAA"
        private const val UPDATED_COMMENT = "BBBBBBBBBB"

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): AgreementStatusHistory {
            val agreementStatusHistory = AgreementStatusHistory(
                createDate = DEFAULT_CREATE_DATE,
                comment = DEFAULT_COMMENT
            )

            return agreementStatusHistory
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): AgreementStatusHistory {
            val agreementStatusHistory = AgreementStatusHistory(
                createDate = UPDATED_CREATE_DATE,
                comment = UPDATED_COMMENT
            )

            return agreementStatusHistory
        }
    }
}
