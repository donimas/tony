package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.ContractStatusHistory
import kz.donimas.rony.repository.ContractStatusHistoryRepository
import kz.donimas.rony.service.ContractStatusHistoryService
import kz.donimas.rony.service.mapper.ContractStatusHistoryMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [ContractStatusHistoryResource] REST controller.
 *
 * @see ContractStatusHistoryResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class ContractStatusHistoryResourceIT {

    @Autowired
    private lateinit var contractStatusHistoryRepository: ContractStatusHistoryRepository

    @Autowired
    private lateinit var contractStatusHistoryMapper: ContractStatusHistoryMapper

    @Autowired
    private lateinit var contractStatusHistoryService: ContractStatusHistoryService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restContractStatusHistoryMockMvc: MockMvc

    private lateinit var contractStatusHistory: ContractStatusHistory

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val contractStatusHistoryResource = ContractStatusHistoryResource(contractStatusHistoryService)
        this.restContractStatusHistoryMockMvc = MockMvcBuilders.standaloneSetup(contractStatusHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        contractStatusHistory = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createContractStatusHistory() {
        val databaseSizeBeforeCreate = contractStatusHistoryRepository.findAll().size

        // Create the ContractStatusHistory
        val contractStatusHistoryDTO = contractStatusHistoryMapper.toDto(contractStatusHistory)
        restContractStatusHistoryMockMvc.perform(
            post("/api/contract-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(contractStatusHistoryDTO))
        ).andExpect(status().isCreated)

        // Validate the ContractStatusHistory in the database
        val contractStatusHistoryList = contractStatusHistoryRepository.findAll()
        assertThat(contractStatusHistoryList).hasSize(databaseSizeBeforeCreate + 1)
        val testContractStatusHistory = contractStatusHistoryList[contractStatusHistoryList.size - 1]
        assertThat(testContractStatusHistory.comment).isEqualTo(DEFAULT_COMMENT)
        assertThat(testContractStatusHistory.createDate).isEqualTo(DEFAULT_CREATE_DATE)
    }

    @Test
    @Transactional
    fun createContractStatusHistoryWithExistingId() {
        val databaseSizeBeforeCreate = contractStatusHistoryRepository.findAll().size

        // Create the ContractStatusHistory with an existing ID
        contractStatusHistory.id = 1L
        val contractStatusHistoryDTO = contractStatusHistoryMapper.toDto(contractStatusHistory)

        // An entity with an existing ID cannot be created, so this API call must fail
        restContractStatusHistoryMockMvc.perform(
            post("/api/contract-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(contractStatusHistoryDTO))
        ).andExpect(status().isBadRequest)

        // Validate the ContractStatusHistory in the database
        val contractStatusHistoryList = contractStatusHistoryRepository.findAll()
        assertThat(contractStatusHistoryList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllContractStatusHistories() {
        // Initialize the database
        contractStatusHistoryRepository.saveAndFlush(contractStatusHistory)

        // Get all the contractStatusHistoryList
        restContractStatusHistoryMockMvc.perform(get("/api/contract-status-histories?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contractStatusHistory.id?.toInt())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getContractStatusHistory() {
        // Initialize the database
        contractStatusHistoryRepository.saveAndFlush(contractStatusHistory)

        val id = contractStatusHistory.id
        assertNotNull(id)

        // Get the contractStatusHistory
        restContractStatusHistoryMockMvc.perform(get("/api/contract-status-histories/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contractStatusHistory.id?.toInt()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingContractStatusHistory() {
        // Get the contractStatusHistory
        restContractStatusHistoryMockMvc.perform(get("/api/contract-status-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateContractStatusHistory() {
        // Initialize the database
        contractStatusHistoryRepository.saveAndFlush(contractStatusHistory)

        val databaseSizeBeforeUpdate = contractStatusHistoryRepository.findAll().size

        // Update the contractStatusHistory
        val id = contractStatusHistory.id
        assertNotNull(id)
        val updatedContractStatusHistory = contractStatusHistoryRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedContractStatusHistory are not directly saved in db
        em.detach(updatedContractStatusHistory)
        updatedContractStatusHistory.comment = UPDATED_COMMENT
        updatedContractStatusHistory.createDate = UPDATED_CREATE_DATE
        val contractStatusHistoryDTO = contractStatusHistoryMapper.toDto(updatedContractStatusHistory)

        restContractStatusHistoryMockMvc.perform(
            put("/api/contract-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(contractStatusHistoryDTO))
        ).andExpect(status().isOk)

        // Validate the ContractStatusHistory in the database
        val contractStatusHistoryList = contractStatusHistoryRepository.findAll()
        assertThat(contractStatusHistoryList).hasSize(databaseSizeBeforeUpdate)
        val testContractStatusHistory = contractStatusHistoryList[contractStatusHistoryList.size - 1]
        assertThat(testContractStatusHistory.comment).isEqualTo(UPDATED_COMMENT)
        assertThat(testContractStatusHistory.createDate).isEqualTo(UPDATED_CREATE_DATE)
    }

    @Test
    @Transactional
    fun updateNonExistingContractStatusHistory() {
        val databaseSizeBeforeUpdate = contractStatusHistoryRepository.findAll().size

        // Create the ContractStatusHistory
        val contractStatusHistoryDTO = contractStatusHistoryMapper.toDto(contractStatusHistory)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContractStatusHistoryMockMvc.perform(
            put("/api/contract-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(contractStatusHistoryDTO))
        ).andExpect(status().isBadRequest)

        // Validate the ContractStatusHistory in the database
        val contractStatusHistoryList = contractStatusHistoryRepository.findAll()
        assertThat(contractStatusHistoryList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteContractStatusHistory() {
        // Initialize the database
        contractStatusHistoryRepository.saveAndFlush(contractStatusHistory)

        val databaseSizeBeforeDelete = contractStatusHistoryRepository.findAll().size

        // Delete the contractStatusHistory
        restContractStatusHistoryMockMvc.perform(
            delete("/api/contract-status-histories/{id}", contractStatusHistory.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val contractStatusHistoryList = contractStatusHistoryRepository.findAll()
        assertThat(contractStatusHistoryList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_COMMENT = "AAAAAAAAAA"
        private const val UPDATED_COMMENT = "BBBBBBBBBB"

        private val DEFAULT_CREATE_DATE: ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC)
        private val UPDATED_CREATE_DATE: ZonedDateTime = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0)

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): ContractStatusHistory {
            val contractStatusHistory = ContractStatusHistory(
                comment = DEFAULT_COMMENT,
                createDate = DEFAULT_CREATE_DATE
            )

            return contractStatusHistory
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): ContractStatusHistory {
            val contractStatusHistory = ContractStatusHistory(
                comment = UPDATED_COMMENT,
                createDate = UPDATED_CREATE_DATE
            )

            return contractStatusHistory
        }
    }
}
