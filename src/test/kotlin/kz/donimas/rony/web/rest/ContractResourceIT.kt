package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.Contract
import kz.donimas.rony.repository.ContractRepository
import kz.donimas.rony.service.ContractService
import kz.donimas.rony.service.mapper.ContractMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import java.math.BigDecimal
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [ContractResource] REST controller.
 *
 * @see ContractResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class ContractResourceIT {

    @Autowired
    private lateinit var contractRepository: ContractRepository

    @Autowired
    private lateinit var contractMapper: ContractMapper

    @Autowired
    private lateinit var contractService: ContractService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restContractMockMvc: MockMvc

    private lateinit var contract: Contract

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val contractResource = ContractResource(contractService)
        this.restContractMockMvc = MockMvcBuilders.standaloneSetup(contractResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        contract = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createContract() {
        val databaseSizeBeforeCreate = contractRepository.findAll().size

        // Create the Contract
        val contractDTO = contractMapper.toDto(contract)
        restContractMockMvc.perform(
            post("/api/contracts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(contractDTO))
        ).andExpect(status().isCreated)

        // Validate the Contract in the database
        val contractList = contractRepository.findAll()
        assertThat(contractList).hasSize(databaseSizeBeforeCreate + 1)
        val testContract = contractList[contractList.size - 1]
        assertThat(testContract.name).isEqualTo(DEFAULT_NAME)
        assertThat(testContract.number).isEqualTo(DEFAULT_NUMBER)
        assertThat(testContract.sum).isEqualTo(DEFAULT_SUM)
        assertThat(testContract.signDate).isEqualTo(DEFAULT_SIGN_DATE)
        assertThat(testContract.finishDate).isEqualTo(DEFAULT_FINISH_DATE)
        assertThat(testContract.comment).isEqualTo(DEFAULT_COMMENT)
        assertThat(testContract.createDate).isEqualTo(DEFAULT_CREATE_DATE)
        assertThat(testContract.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createContractWithExistingId() {
        val databaseSizeBeforeCreate = contractRepository.findAll().size

        // Create the Contract with an existing ID
        contract.id = 1L
        val contractDTO = contractMapper.toDto(contract)

        // An entity with an existing ID cannot be created, so this API call must fail
        restContractMockMvc.perform(
            post("/api/contracts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(contractDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Contract in the database
        val contractList = contractRepository.findAll()
        assertThat(contractList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllContracts() {
        // Initialize the database
        contractRepository.saveAndFlush(contract)

        // Get all the contractList
        restContractMockMvc.perform(get("/api/contracts?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contract.id?.toInt())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].sum").value(hasItem(DEFAULT_SUM?.toInt())))
            .andExpect(jsonPath("$.[*].signDate").value(hasItem(DEFAULT_SIGN_DATE.toString())))
            .andExpect(jsonPath("$.[*].finishDate").value(hasItem(DEFAULT_FINISH_DATE.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getContract() {
        // Initialize the database
        contractRepository.saveAndFlush(contract)

        val id = contract.id
        assertNotNull(id)

        // Get the contract
        restContractMockMvc.perform(get("/api/contracts/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contract.id?.toInt()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER))
            .andExpect(jsonPath("$.sum").value(DEFAULT_SUM?.toInt()))
            .andExpect(jsonPath("$.signDate").value(DEFAULT_SIGN_DATE.toString()))
            .andExpect(jsonPath("$.finishDate").value(DEFAULT_FINISH_DATE.toString()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingContract() {
        // Get the contract
        restContractMockMvc.perform(get("/api/contracts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateContract() {
        // Initialize the database
        contractRepository.saveAndFlush(contract)

        val databaseSizeBeforeUpdate = contractRepository.findAll().size

        // Update the contract
        val id = contract.id
        assertNotNull(id)
        val updatedContract = contractRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedContract are not directly saved in db
        em.detach(updatedContract)
        updatedContract.name = UPDATED_NAME
        updatedContract.number = UPDATED_NUMBER
        updatedContract.sum = UPDATED_SUM
        updatedContract.signDate = UPDATED_SIGN_DATE
        updatedContract.finishDate = UPDATED_FINISH_DATE
        updatedContract.comment = UPDATED_COMMENT
        updatedContract.createDate = UPDATED_CREATE_DATE
        updatedContract.flagDeleted = UPDATED_FLAG_DELETED
        val contractDTO = contractMapper.toDto(updatedContract)

        restContractMockMvc.perform(
            put("/api/contracts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(contractDTO))
        ).andExpect(status().isOk)

        // Validate the Contract in the database
        val contractList = contractRepository.findAll()
        assertThat(contractList).hasSize(databaseSizeBeforeUpdate)
        val testContract = contractList[contractList.size - 1]
        assertThat(testContract.name).isEqualTo(UPDATED_NAME)
        assertThat(testContract.number).isEqualTo(UPDATED_NUMBER)
        assertThat(testContract.sum).isEqualTo(UPDATED_SUM)
        assertThat(testContract.signDate).isEqualTo(UPDATED_SIGN_DATE)
        assertThat(testContract.finishDate).isEqualTo(UPDATED_FINISH_DATE)
        assertThat(testContract.comment).isEqualTo(UPDATED_COMMENT)
        assertThat(testContract.createDate).isEqualTo(UPDATED_CREATE_DATE)
        assertThat(testContract.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingContract() {
        val databaseSizeBeforeUpdate = contractRepository.findAll().size

        // Create the Contract
        val contractDTO = contractMapper.toDto(contract)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContractMockMvc.perform(
            put("/api/contracts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(contractDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Contract in the database
        val contractList = contractRepository.findAll()
        assertThat(contractList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteContract() {
        // Initialize the database
        contractRepository.saveAndFlush(contract)

        val databaseSizeBeforeDelete = contractRepository.findAll().size

        // Delete the contract
        restContractMockMvc.perform(
            delete("/api/contracts/{id}", contract.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val contractList = contractRepository.findAll()
        assertThat(contractList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_NAME = "AAAAAAAAAA"
        private const val UPDATED_NAME = "BBBBBBBBBB"

        private const val DEFAULT_NUMBER = "AAAAAAAAAA"
        private const val UPDATED_NUMBER = "BBBBBBBBBB"

        private val DEFAULT_SUM: BigDecimal = BigDecimal(1)
        private val UPDATED_SUM: BigDecimal = BigDecimal(2)

        private val DEFAULT_SIGN_DATE: Instant = Instant.ofEpochMilli(0L)
        private val UPDATED_SIGN_DATE: Instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

        private val DEFAULT_FINISH_DATE: Instant = Instant.ofEpochMilli(0L)
        private val UPDATED_FINISH_DATE: Instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

        private const val DEFAULT_COMMENT = "AAAAAAAAAA"
        private const val UPDATED_COMMENT = "BBBBBBBBBB"

        private val DEFAULT_CREATE_DATE: ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC)
        private val UPDATED_CREATE_DATE: ZonedDateTime = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0)

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Contract {
            val contract = Contract(
                name = DEFAULT_NAME,
                number = DEFAULT_NUMBER,
                sum = DEFAULT_SUM,
                signDate = DEFAULT_SIGN_DATE,
                finishDate = DEFAULT_FINISH_DATE,
                comment = DEFAULT_COMMENT,
                createDate = DEFAULT_CREATE_DATE,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return contract
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Contract {
            val contract = Contract(
                name = UPDATED_NAME,
                number = UPDATED_NUMBER,
                sum = UPDATED_SUM,
                signDate = UPDATED_SIGN_DATE,
                finishDate = UPDATED_FINISH_DATE,
                comment = UPDATED_COMMENT,
                createDate = UPDATED_CREATE_DATE,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return contract
        }
    }
}
