package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.TeamMember
import kz.donimas.rony.repository.TeamMemberRepository
import kz.donimas.rony.service.TeamMemberService
import kz.donimas.rony.service.mapper.TeamMemberMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import java.math.BigDecimal
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [TeamMemberResource] REST controller.
 *
 * @see TeamMemberResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class TeamMemberResourceIT {

    @Autowired
    private lateinit var teamMemberRepository: TeamMemberRepository

    @Autowired
    private lateinit var teamMemberMapper: TeamMemberMapper

    @Autowired
    private lateinit var teamMemberService: TeamMemberService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restTeamMemberMockMvc: MockMvc

    private lateinit var teamMember: TeamMember

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val teamMemberResource = TeamMemberResource(teamMemberService)
        this.restTeamMemberMockMvc = MockMvcBuilders.standaloneSetup(teamMemberResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        teamMember = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createTeamMember() {
        val databaseSizeBeforeCreate = teamMemberRepository.findAll().size

        // Create the TeamMember
        val teamMemberDTO = teamMemberMapper.toDto(teamMember)
        restTeamMemberMockMvc.perform(
            post("/api/team-members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(teamMemberDTO))
        ).andExpect(status().isCreated)

        // Validate the TeamMember in the database
        val teamMemberList = teamMemberRepository.findAll()
        assertThat(teamMemberList).hasSize(databaseSizeBeforeCreate + 1)
        val testTeamMember = teamMemberList[teamMemberList.size - 1]
        assertThat(testTeamMember.involvement).isEqualTo(DEFAULT_INVOLVEMENT)
        assertThat(testTeamMember.startDate).isEqualTo(DEFAULT_START_DATE)
        assertThat(testTeamMember.finishDate).isEqualTo(DEFAULT_FINISH_DATE)
        assertThat(testTeamMember.flagLeft).isEqualTo(DEFAULT_FLAG_LEFT)
        assertThat(testTeamMember.createDate).isEqualTo(DEFAULT_CREATE_DATE)
    }

    @Test
    @Transactional
    fun createTeamMemberWithExistingId() {
        val databaseSizeBeforeCreate = teamMemberRepository.findAll().size

        // Create the TeamMember with an existing ID
        teamMember.id = 1L
        val teamMemberDTO = teamMemberMapper.toDto(teamMember)

        // An entity with an existing ID cannot be created, so this API call must fail
        restTeamMemberMockMvc.perform(
            post("/api/team-members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(teamMemberDTO))
        ).andExpect(status().isBadRequest)

        // Validate the TeamMember in the database
        val teamMemberList = teamMemberRepository.findAll()
        assertThat(teamMemberList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllTeamMembers() {
        // Initialize the database
        teamMemberRepository.saveAndFlush(teamMember)

        // Get all the teamMemberList
        restTeamMemberMockMvc.perform(get("/api/team-members?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(teamMember.id?.toInt())))
            .andExpect(jsonPath("$.[*].involvement").value(hasItem(DEFAULT_INVOLVEMENT?.toInt())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].finishDate").value(hasItem(DEFAULT_FINISH_DATE.toString())))
            .andExpect(jsonPath("$.[*].flagLeft").value(hasItem(DEFAULT_FLAG_LEFT)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getTeamMember() {
        // Initialize the database
        teamMemberRepository.saveAndFlush(teamMember)

        val id = teamMember.id
        assertNotNull(id)

        // Get the teamMember
        restTeamMemberMockMvc.perform(get("/api/team-members/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(teamMember.id?.toInt()))
            .andExpect(jsonPath("$.involvement").value(DEFAULT_INVOLVEMENT?.toInt()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.finishDate").value(DEFAULT_FINISH_DATE.toString()))
            .andExpect(jsonPath("$.flagLeft").value(DEFAULT_FLAG_LEFT))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingTeamMember() {
        // Get the teamMember
        restTeamMemberMockMvc.perform(get("/api/team-members/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateTeamMember() {
        // Initialize the database
        teamMemberRepository.saveAndFlush(teamMember)

        val databaseSizeBeforeUpdate = teamMemberRepository.findAll().size

        // Update the teamMember
        val id = teamMember.id
        assertNotNull(id)
        val updatedTeamMember = teamMemberRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedTeamMember are not directly saved in db
        em.detach(updatedTeamMember)
        updatedTeamMember.involvement = UPDATED_INVOLVEMENT
        updatedTeamMember.startDate = UPDATED_START_DATE
        updatedTeamMember.finishDate = UPDATED_FINISH_DATE
        updatedTeamMember.flagLeft = UPDATED_FLAG_LEFT
        updatedTeamMember.createDate = UPDATED_CREATE_DATE
        val teamMemberDTO = teamMemberMapper.toDto(updatedTeamMember)

        restTeamMemberMockMvc.perform(
            put("/api/team-members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(teamMemberDTO))
        ).andExpect(status().isOk)

        // Validate the TeamMember in the database
        val teamMemberList = teamMemberRepository.findAll()
        assertThat(teamMemberList).hasSize(databaseSizeBeforeUpdate)
        val testTeamMember = teamMemberList[teamMemberList.size - 1]
        assertThat(testTeamMember.involvement).isEqualTo(UPDATED_INVOLVEMENT)
        assertThat(testTeamMember.startDate).isEqualTo(UPDATED_START_DATE)
        assertThat(testTeamMember.finishDate).isEqualTo(UPDATED_FINISH_DATE)
        assertThat(testTeamMember.flagLeft).isEqualTo(UPDATED_FLAG_LEFT)
        assertThat(testTeamMember.createDate).isEqualTo(UPDATED_CREATE_DATE)
    }

    @Test
    @Transactional
    fun updateNonExistingTeamMember() {
        val databaseSizeBeforeUpdate = teamMemberRepository.findAll().size

        // Create the TeamMember
        val teamMemberDTO = teamMemberMapper.toDto(teamMember)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTeamMemberMockMvc.perform(
            put("/api/team-members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(teamMemberDTO))
        ).andExpect(status().isBadRequest)

        // Validate the TeamMember in the database
        val teamMemberList = teamMemberRepository.findAll()
        assertThat(teamMemberList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteTeamMember() {
        // Initialize the database
        teamMemberRepository.saveAndFlush(teamMember)

        val databaseSizeBeforeDelete = teamMemberRepository.findAll().size

        // Delete the teamMember
        restTeamMemberMockMvc.perform(
            delete("/api/team-members/{id}", teamMember.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val teamMemberList = teamMemberRepository.findAll()
        assertThat(teamMemberList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private val DEFAULT_INVOLVEMENT: BigDecimal = BigDecimal(1)
        private val UPDATED_INVOLVEMENT: BigDecimal = BigDecimal(2)

        private val DEFAULT_START_DATE: Instant = Instant.ofEpochMilli(0L)
        private val UPDATED_START_DATE: Instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

        private val DEFAULT_FINISH_DATE: Instant = Instant.ofEpochMilli(0L)
        private val UPDATED_FINISH_DATE: Instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

        private const val DEFAULT_FLAG_LEFT: Boolean = false
        private const val UPDATED_FLAG_LEFT: Boolean = true

        private val DEFAULT_CREATE_DATE: ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC)
        private val UPDATED_CREATE_DATE: ZonedDateTime = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0)

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): TeamMember {
            val teamMember = TeamMember(
                involvement = DEFAULT_INVOLVEMENT,
                startDate = DEFAULT_START_DATE,
                finishDate = DEFAULT_FINISH_DATE,
                flagLeft = DEFAULT_FLAG_LEFT,
                createDate = DEFAULT_CREATE_DATE
            )

            return teamMember
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): TeamMember {
            val teamMember = TeamMember(
                involvement = UPDATED_INVOLVEMENT,
                startDate = UPDATED_START_DATE,
                finishDate = UPDATED_FINISH_DATE,
                flagLeft = UPDATED_FLAG_LEFT,
                createDate = UPDATED_CREATE_DATE
            )

            return teamMember
        }
    }
}
