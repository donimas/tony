package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.Address
import kz.donimas.rony.repository.AddressRepository
import kz.donimas.rony.service.AddressService
import kz.donimas.rony.service.mapper.AddressMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [AddressResource] REST controller.
 *
 * @see AddressResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class AddressResourceIT {

    @Autowired
    private lateinit var addressRepository: AddressRepository

    @Autowired
    private lateinit var addressMapper: AddressMapper

    @Autowired
    private lateinit var addressService: AddressService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restAddressMockMvc: MockMvc

    private lateinit var address: Address

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val addressResource = AddressResource(addressService)
        this.restAddressMockMvc = MockMvcBuilders.standaloneSetup(addressResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        address = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createAddress() {
        val databaseSizeBeforeCreate = addressRepository.findAll().size

        // Create the Address
        val addressDTO = addressMapper.toDto(address)
        restAddressMockMvc.perform(
            post("/api/addresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(addressDTO))
        ).andExpect(status().isCreated)

        // Validate the Address in the database
        val addressList = addressRepository.findAll()
        assertThat(addressList).hasSize(databaseSizeBeforeCreate + 1)
        val testAddress = addressList[addressList.size - 1]
        assertThat(testAddress.country).isEqualTo(DEFAULT_COUNTRY)
        assertThat(testAddress.city).isEqualTo(DEFAULT_CITY)
        assertThat(testAddress.fullAddress).isEqualTo(DEFAULT_FULL_ADDRESS)
        assertThat(testAddress.cabinet).isEqualTo(DEFAULT_CABINET)
        assertThat(testAddress.lat).isEqualTo(DEFAULT_LAT)
        assertThat(testAddress.lon).isEqualTo(DEFAULT_LON)
        assertThat(testAddress.postalCode).isEqualTo(DEFAULT_POSTAL_CODE)
        assertThat(testAddress.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createAddressWithExistingId() {
        val databaseSizeBeforeCreate = addressRepository.findAll().size

        // Create the Address with an existing ID
        address.id = 1L
        val addressDTO = addressMapper.toDto(address)

        // An entity with an existing ID cannot be created, so this API call must fail
        restAddressMockMvc.perform(
            post("/api/addresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(addressDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Address in the database
        val addressList = addressRepository.findAll()
        assertThat(addressList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllAddresses() {
        // Initialize the database
        addressRepository.saveAndFlush(address)

        // Get all the addressList
        restAddressMockMvc.perform(get("/api/addresses?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(address.id?.toInt())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].fullAddress").value(hasItem(DEFAULT_FULL_ADDRESS)))
            .andExpect(jsonPath("$.[*].cabinet").value(hasItem(DEFAULT_CABINET)))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT)))
            .andExpect(jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAddress() {
        // Initialize the database
        addressRepository.saveAndFlush(address)

        val id = address.id
        assertNotNull(id)

        // Get the address
        restAddressMockMvc.perform(get("/api/addresses/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(address.id?.toInt()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.fullAddress").value(DEFAULT_FULL_ADDRESS))
            .andExpect(jsonPath("$.cabinet").value(DEFAULT_CABINET))
            .andExpect(jsonPath("$.lat").value(DEFAULT_LAT))
            .andExpect(jsonPath("$.lon").value(DEFAULT_LON))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingAddress() {
        // Get the address
        restAddressMockMvc.perform(get("/api/addresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateAddress() {
        // Initialize the database
        addressRepository.saveAndFlush(address)

        val databaseSizeBeforeUpdate = addressRepository.findAll().size

        // Update the address
        val id = address.id
        assertNotNull(id)
        val updatedAddress = addressRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedAddress are not directly saved in db
        em.detach(updatedAddress)
        updatedAddress.country = UPDATED_COUNTRY
        updatedAddress.city = UPDATED_CITY
        updatedAddress.fullAddress = UPDATED_FULL_ADDRESS
        updatedAddress.cabinet = UPDATED_CABINET
        updatedAddress.lat = UPDATED_LAT
        updatedAddress.lon = UPDATED_LON
        updatedAddress.postalCode = UPDATED_POSTAL_CODE
        updatedAddress.flagDeleted = UPDATED_FLAG_DELETED
        val addressDTO = addressMapper.toDto(updatedAddress)

        restAddressMockMvc.perform(
            put("/api/addresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(addressDTO))
        ).andExpect(status().isOk)

        // Validate the Address in the database
        val addressList = addressRepository.findAll()
        assertThat(addressList).hasSize(databaseSizeBeforeUpdate)
        val testAddress = addressList[addressList.size - 1]
        assertThat(testAddress.country).isEqualTo(UPDATED_COUNTRY)
        assertThat(testAddress.city).isEqualTo(UPDATED_CITY)
        assertThat(testAddress.fullAddress).isEqualTo(UPDATED_FULL_ADDRESS)
        assertThat(testAddress.cabinet).isEqualTo(UPDATED_CABINET)
        assertThat(testAddress.lat).isEqualTo(UPDATED_LAT)
        assertThat(testAddress.lon).isEqualTo(UPDATED_LON)
        assertThat(testAddress.postalCode).isEqualTo(UPDATED_POSTAL_CODE)
        assertThat(testAddress.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingAddress() {
        val databaseSizeBeforeUpdate = addressRepository.findAll().size

        // Create the Address
        val addressDTO = addressMapper.toDto(address)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAddressMockMvc.perform(
            put("/api/addresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(addressDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Address in the database
        val addressList = addressRepository.findAll()
        assertThat(addressList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteAddress() {
        // Initialize the database
        addressRepository.saveAndFlush(address)

        val databaseSizeBeforeDelete = addressRepository.findAll().size

        // Delete the address
        restAddressMockMvc.perform(
            delete("/api/addresses/{id}", address.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val addressList = addressRepository.findAll()
        assertThat(addressList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_COUNTRY = "AAAAAAAAAA"
        private const val UPDATED_COUNTRY = "BBBBBBBBBB"

        private const val DEFAULT_CITY = "AAAAAAAAAA"
        private const val UPDATED_CITY = "BBBBBBBBBB"

        private const val DEFAULT_FULL_ADDRESS = "AAAAAAAAAA"
        private const val UPDATED_FULL_ADDRESS = "BBBBBBBBBB"

        private const val DEFAULT_CABINET = "AAAAAAAAAA"
        private const val UPDATED_CABINET = "BBBBBBBBBB"

        private const val DEFAULT_LAT = "AAAAAAAAAA"
        private const val UPDATED_LAT = "BBBBBBBBBB"

        private const val DEFAULT_LON = "AAAAAAAAAA"
        private const val UPDATED_LON = "BBBBBBBBBB"

        private const val DEFAULT_POSTAL_CODE = "AAAAAAAAAA"
        private const val UPDATED_POSTAL_CODE = "BBBBBBBBBB"

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Address {
            val address = Address(
                country = DEFAULT_COUNTRY,
                city = DEFAULT_CITY,
                fullAddress = DEFAULT_FULL_ADDRESS,
                cabinet = DEFAULT_CABINET,
                lat = DEFAULT_LAT,
                lon = DEFAULT_LON,
                postalCode = DEFAULT_POSTAL_CODE,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return address
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Address {
            val address = Address(
                country = UPDATED_COUNTRY,
                city = UPDATED_CITY,
                fullAddress = UPDATED_FULL_ADDRESS,
                cabinet = UPDATED_CABINET,
                lat = UPDATED_LAT,
                lon = UPDATED_LON,
                postalCode = UPDATED_POSTAL_CODE,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return address
        }
    }
}
