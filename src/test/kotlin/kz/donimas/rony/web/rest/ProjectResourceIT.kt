package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.Project
import kz.donimas.rony.repository.ProjectRepository
import kz.donimas.rony.service.ProjectService
import kz.donimas.rony.service.mapper.ProjectMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.mockito.ArgumentMatchers.*
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [ProjectResource] REST controller.
 *
 * @see ProjectResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
@Extensions(
    ExtendWith(MockitoExtension::class)
)
class ProjectResourceIT {

    @Autowired
    private lateinit var projectRepository: ProjectRepository

    @Mock
    private lateinit var projectRepositoryMock: ProjectRepository

    @Autowired
    private lateinit var projectMapper: ProjectMapper

    @Mock
    private lateinit var projectServiceMock: ProjectService

    @Autowired
    private lateinit var projectService: ProjectService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restProjectMockMvc: MockMvc

    private lateinit var project: Project

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val projectResource = ProjectResource(projectService)
        this.restProjectMockMvc = MockMvcBuilders.standaloneSetup(projectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        project = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createProject() {
        val databaseSizeBeforeCreate = projectRepository.findAll().size

        // Create the Project
        val projectDTO = projectMapper.toDto(project)
        restProjectMockMvc.perform(
            post("/api/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectDTO))
        ).andExpect(status().isCreated)

        // Validate the Project in the database
        val projectList = projectRepository.findAll()
        assertThat(projectList).hasSize(databaseSizeBeforeCreate + 1)
        val testProject = projectList[projectList.size - 1]
        assertThat(testProject.name).isEqualTo(DEFAULT_NAME)
        assertThat(testProject.description).isEqualTo(DEFAULT_DESCRIPTION)
        assertThat(testProject.goal).isEqualTo(DEFAULT_GOAL)
        assertThat(testProject.startDate).isEqualTo(DEFAULT_START_DATE)
        assertThat(testProject.finishDate).isEqualTo(DEFAULT_FINISH_DATE)
        assertThat(testProject.uri).isEqualTo(DEFAULT_URI)
        assertThat(testProject.createDate).isEqualTo(DEFAULT_CREATE_DATE)
        assertThat(testProject.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createProjectWithExistingId() {
        val databaseSizeBeforeCreate = projectRepository.findAll().size

        // Create the Project with an existing ID
        project.id = 1L
        val projectDTO = projectMapper.toDto(project)

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjectMockMvc.perform(
            post("/api/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Project in the database
        val projectList = projectRepository.findAll()
        assertThat(projectList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    fun checkNameIsRequired() {
        val databaseSizeBeforeTest = projectRepository.findAll().size
        // set the field null
        project.name = null

        // Create the Project, which fails.
        val projectDTO = projectMapper.toDto(project)

        restProjectMockMvc.perform(
            post("/api/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectDTO))
        ).andExpect(status().isBadRequest)

        val projectList = projectRepository.findAll()
        assertThat(projectList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllProjects() {
        // Initialize the database
        projectRepository.saveAndFlush(project)

        // Get all the projectList
        restProjectMockMvc.perform(get("/api/projects?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(project.id?.toInt())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].goal").value(hasItem(DEFAULT_GOAL)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].finishDate").value(hasItem(DEFAULT_FINISH_DATE.toString())))
            .andExpect(jsonPath("$.[*].uri").value(hasItem(DEFAULT_URI)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Suppress("unchecked")
    @Throws(Exception::class)
    fun getAllProjectsWithEagerRelationshipsIsEnabled() {
        val projectResource = ProjectResource(projectServiceMock)
        `when`(projectServiceMock.findAllWithEagerRelationships(any())).thenReturn(PageImpl(mutableListOf()))

        val restProjectMockMvc = MockMvcBuilders.standaloneSetup(projectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build()

        restProjectMockMvc.perform(get("/api/projects?eagerload=true"))
            .andExpect(status().isOk)

        verify(projectServiceMock, times(1)).findAllWithEagerRelationships(any())
    }

    @Suppress("unchecked")
    @Throws(Exception::class)
    fun getAllProjectsWithEagerRelationshipsIsNotEnabled() {
        val projectResource = ProjectResource(projectServiceMock)
        `when`(projectServiceMock.findAllWithEagerRelationships(any())).thenReturn(PageImpl(mutableListOf()))

        val restProjectMockMvc = MockMvcBuilders.standaloneSetup(projectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build()

        restProjectMockMvc.perform(get("/api/projects?eagerload=true"))
            .andExpect(status().isOk)

        verify(projectServiceMock, times(1)).findAllWithEagerRelationships(any())
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getProject() {
        // Initialize the database
        projectRepository.saveAndFlush(project)

        val id = project.id
        assertNotNull(id)

        // Get the project
        restProjectMockMvc.perform(get("/api/projects/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(project.id?.toInt()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.goal").value(DEFAULT_GOAL))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.finishDate").value(DEFAULT_FINISH_DATE.toString()))
            .andExpect(jsonPath("$.uri").value(DEFAULT_URI))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingProject() {
        // Get the project
        restProjectMockMvc.perform(get("/api/projects/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateProject() {
        // Initialize the database
        projectRepository.saveAndFlush(project)

        val databaseSizeBeforeUpdate = projectRepository.findAll().size

        // Update the project
        val id = project.id
        assertNotNull(id)
        val updatedProject = projectRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedProject are not directly saved in db
        em.detach(updatedProject)
        updatedProject.name = UPDATED_NAME
        updatedProject.description = UPDATED_DESCRIPTION
        updatedProject.goal = UPDATED_GOAL
        updatedProject.startDate = UPDATED_START_DATE
        updatedProject.finishDate = UPDATED_FINISH_DATE
        updatedProject.uri = UPDATED_URI
        updatedProject.createDate = UPDATED_CREATE_DATE
        updatedProject.flagDeleted = UPDATED_FLAG_DELETED
        val projectDTO = projectMapper.toDto(updatedProject)

        restProjectMockMvc.perform(
            put("/api/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectDTO))
        ).andExpect(status().isOk)

        // Validate the Project in the database
        val projectList = projectRepository.findAll()
        assertThat(projectList).hasSize(databaseSizeBeforeUpdate)
        val testProject = projectList[projectList.size - 1]
        assertThat(testProject.name).isEqualTo(UPDATED_NAME)
        assertThat(testProject.description).isEqualTo(UPDATED_DESCRIPTION)
        assertThat(testProject.goal).isEqualTo(UPDATED_GOAL)
        assertThat(testProject.startDate).isEqualTo(UPDATED_START_DATE)
        assertThat(testProject.finishDate).isEqualTo(UPDATED_FINISH_DATE)
        assertThat(testProject.uri).isEqualTo(UPDATED_URI)
        assertThat(testProject.createDate).isEqualTo(UPDATED_CREATE_DATE)
        assertThat(testProject.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingProject() {
        val databaseSizeBeforeUpdate = projectRepository.findAll().size

        // Create the Project
        val projectDTO = projectMapper.toDto(project)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProjectMockMvc.perform(
            put("/api/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Project in the database
        val projectList = projectRepository.findAll()
        assertThat(projectList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteProject() {
        // Initialize the database
        projectRepository.saveAndFlush(project)

        val databaseSizeBeforeDelete = projectRepository.findAll().size

        // Delete the project
        restProjectMockMvc.perform(
            delete("/api/projects/{id}", project.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val projectList = projectRepository.findAll()
        assertThat(projectList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_NAME = "AAAAAAAAAA"
        private const val UPDATED_NAME = "BBBBBBBBBB"

        private const val DEFAULT_DESCRIPTION = "AAAAAAAAAA"
        private const val UPDATED_DESCRIPTION = "BBBBBBBBBB"

        private const val DEFAULT_GOAL = "AAAAAAAAAA"
        private const val UPDATED_GOAL = "BBBBBBBBBB"

        private val DEFAULT_START_DATE: Instant = Instant.ofEpochMilli(0L)
        private val UPDATED_START_DATE: Instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

        private val DEFAULT_FINISH_DATE: Instant = Instant.ofEpochMilli(0L)
        private val UPDATED_FINISH_DATE: Instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

        private const val DEFAULT_URI = "AAAAAAAAAA"
        private const val UPDATED_URI = "BBBBBBBBBB"

        private val DEFAULT_CREATE_DATE: ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC)
        private val UPDATED_CREATE_DATE: ZonedDateTime = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0)

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Project {
            val project = Project(
                name = DEFAULT_NAME,
                description = DEFAULT_DESCRIPTION,
                goal = DEFAULT_GOAL,
                startDate = DEFAULT_START_DATE,
                finishDate = DEFAULT_FINISH_DATE,
                uri = DEFAULT_URI,
                createDate = DEFAULT_CREATE_DATE,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return project
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Project {
            val project = Project(
                name = UPDATED_NAME,
                description = UPDATED_DESCRIPTION,
                goal = UPDATED_GOAL,
                startDate = UPDATED_START_DATE,
                finishDate = UPDATED_FINISH_DATE,
                uri = UPDATED_URI,
                createDate = UPDATED_CREATE_DATE,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return project
        }
    }
}
