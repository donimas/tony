package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.Structure
import kz.donimas.rony.repository.StructureRepository
import kz.donimas.rony.service.StructureService
import kz.donimas.rony.service.mapper.StructureMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [StructureResource] REST controller.
 *
 * @see StructureResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class StructureResourceIT {

    @Autowired
    private lateinit var structureRepository: StructureRepository

    @Autowired
    private lateinit var structureMapper: StructureMapper

    @Autowired
    private lateinit var structureService: StructureService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restStructureMockMvc: MockMvc

    private lateinit var structure: Structure

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val structureResource = StructureResource(structureService)
        this.restStructureMockMvc = MockMvcBuilders.standaloneSetup(structureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        structure = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createStructure() {
        val databaseSizeBeforeCreate = structureRepository.findAll().size

        // Create the Structure
        val structureDTO = structureMapper.toDto(structure)
        restStructureMockMvc.perform(
            post("/api/structures")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(structureDTO))
        ).andExpect(status().isCreated)

        // Validate the Structure in the database
        val structureList = structureRepository.findAll()
        assertThat(structureList).hasSize(databaseSizeBeforeCreate + 1)
        val testStructure = structureList[structureList.size - 1]
        assertThat(testStructure.code).isEqualTo(DEFAULT_CODE)
        assertThat(testStructure.name).isEqualTo(DEFAULT_NAME)
        assertThat(testStructure.description).isEqualTo(DEFAULT_DESCRIPTION)
        assertThat(testStructure.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createStructureWithExistingId() {
        val databaseSizeBeforeCreate = structureRepository.findAll().size

        // Create the Structure with an existing ID
        structure.id = 1L
        val structureDTO = structureMapper.toDto(structure)

        // An entity with an existing ID cannot be created, so this API call must fail
        restStructureMockMvc.perform(
            post("/api/structures")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(structureDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Structure in the database
        val structureList = structureRepository.findAll()
        assertThat(structureList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    fun checkNameIsRequired() {
        val databaseSizeBeforeTest = structureRepository.findAll().size
        // set the field null
        structure.name = null

        // Create the Structure, which fails.
        val structureDTO = structureMapper.toDto(structure)

        restStructureMockMvc.perform(
            post("/api/structures")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(structureDTO))
        ).andExpect(status().isBadRequest)

        val structureList = structureRepository.findAll()
        assertThat(structureList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllStructures() {
        // Initialize the database
        structureRepository.saveAndFlush(structure)

        // Get all the structureList
        restStructureMockMvc.perform(get("/api/structures?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(structure.id?.toInt())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getStructure() {
        // Initialize the database
        structureRepository.saveAndFlush(structure)

        val id = structure.id
        assertNotNull(id)

        // Get the structure
        restStructureMockMvc.perform(get("/api/structures/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(structure.id?.toInt()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingStructure() {
        // Get the structure
        restStructureMockMvc.perform(get("/api/structures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateStructure() {
        // Initialize the database
        structureRepository.saveAndFlush(structure)

        val databaseSizeBeforeUpdate = structureRepository.findAll().size

        // Update the structure
        val id = structure.id
        assertNotNull(id)
        val updatedStructure = structureRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedStructure are not directly saved in db
        em.detach(updatedStructure)
        updatedStructure.code = UPDATED_CODE
        updatedStructure.name = UPDATED_NAME
        updatedStructure.description = UPDATED_DESCRIPTION
        updatedStructure.flagDeleted = UPDATED_FLAG_DELETED
        val structureDTO = structureMapper.toDto(updatedStructure)

        restStructureMockMvc.perform(
            put("/api/structures")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(structureDTO))
        ).andExpect(status().isOk)

        // Validate the Structure in the database
        val structureList = structureRepository.findAll()
        assertThat(structureList).hasSize(databaseSizeBeforeUpdate)
        val testStructure = structureList[structureList.size - 1]
        assertThat(testStructure.code).isEqualTo(UPDATED_CODE)
        assertThat(testStructure.name).isEqualTo(UPDATED_NAME)
        assertThat(testStructure.description).isEqualTo(UPDATED_DESCRIPTION)
        assertThat(testStructure.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingStructure() {
        val databaseSizeBeforeUpdate = structureRepository.findAll().size

        // Create the Structure
        val structureDTO = structureMapper.toDto(structure)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStructureMockMvc.perform(
            put("/api/structures")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(structureDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Structure in the database
        val structureList = structureRepository.findAll()
        assertThat(structureList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteStructure() {
        // Initialize the database
        structureRepository.saveAndFlush(structure)

        val databaseSizeBeforeDelete = structureRepository.findAll().size

        // Delete the structure
        restStructureMockMvc.perform(
            delete("/api/structures/{id}", structure.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val structureList = structureRepository.findAll()
        assertThat(structureList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_CODE = "AAAAAAAAAA"
        private const val UPDATED_CODE = "BBBBBBBBBB"

        private const val DEFAULT_NAME = "AAAAAAAAAA"
        private const val UPDATED_NAME = "BBBBBBBBBB"

        private const val DEFAULT_DESCRIPTION = "AAAAAAAAAA"
        private const val UPDATED_DESCRIPTION = "BBBBBBBBBB"

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Structure {
            val structure = Structure(
                code = DEFAULT_CODE,
                name = DEFAULT_NAME,
                description = DEFAULT_DESCRIPTION,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return structure
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Structure {
            val structure = Structure(
                code = UPDATED_CODE,
                name = UPDATED_NAME,
                description = UPDATED_DESCRIPTION,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return structure
        }
    }
}
