package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.TechStack
import kz.donimas.rony.repository.TechStackRepository
import kz.donimas.rony.service.TechStackService
import kz.donimas.rony.service.mapper.TechStackMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [TechStackResource] REST controller.
 *
 * @see TechStackResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class TechStackResourceIT {

    @Autowired
    private lateinit var techStackRepository: TechStackRepository

    @Autowired
    private lateinit var techStackMapper: TechStackMapper

    @Autowired
    private lateinit var techStackService: TechStackService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restTechStackMockMvc: MockMvc

    private lateinit var techStack: TechStack

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val techStackResource = TechStackResource(techStackService)
        this.restTechStackMockMvc = MockMvcBuilders.standaloneSetup(techStackResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        techStack = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createTechStack() {
        val databaseSizeBeforeCreate = techStackRepository.findAll().size

        // Create the TechStack
        val techStackDTO = techStackMapper.toDto(techStack)
        restTechStackMockMvc.perform(
            post("/api/tech-stacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(techStackDTO))
        ).andExpect(status().isCreated)

        // Validate the TechStack in the database
        val techStackList = techStackRepository.findAll()
        assertThat(techStackList).hasSize(databaseSizeBeforeCreate + 1)
    }

    @Test
    @Transactional
    fun createTechStackWithExistingId() {
        val databaseSizeBeforeCreate = techStackRepository.findAll().size

        // Create the TechStack with an existing ID
        techStack.id = 1L
        val techStackDTO = techStackMapper.toDto(techStack)

        // An entity with an existing ID cannot be created, so this API call must fail
        restTechStackMockMvc.perform(
            post("/api/tech-stacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(techStackDTO))
        ).andExpect(status().isBadRequest)

        // Validate the TechStack in the database
        val techStackList = techStackRepository.findAll()
        assertThat(techStackList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllTechStacks() {
        // Initialize the database
        techStackRepository.saveAndFlush(techStack)

        // Get all the techStackList
        restTechStackMockMvc.perform(get("/api/tech-stacks?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(techStack.id?.toInt())))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getTechStack() {
        // Initialize the database
        techStackRepository.saveAndFlush(techStack)

        val id = techStack.id
        assertNotNull(id)

        // Get the techStack
        restTechStackMockMvc.perform(get("/api/tech-stacks/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(techStack.id?.toInt()))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingTechStack() {
        // Get the techStack
        restTechStackMockMvc.perform(get("/api/tech-stacks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateTechStack() {
        // Initialize the database
        techStackRepository.saveAndFlush(techStack)

        val databaseSizeBeforeUpdate = techStackRepository.findAll().size

        // Update the techStack
        val id = techStack.id
        assertNotNull(id)
        val updatedTechStack = techStackRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedTechStack are not directly saved in db
        em.detach(updatedTechStack)
        val techStackDTO = techStackMapper.toDto(updatedTechStack)

        restTechStackMockMvc.perform(
            put("/api/tech-stacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(techStackDTO))
        ).andExpect(status().isOk)

        // Validate the TechStack in the database
        val techStackList = techStackRepository.findAll()
        assertThat(techStackList).hasSize(databaseSizeBeforeUpdate)
        val testTechStack = techStackList[techStackList.size - 1]
    }

    @Test
    @Transactional
    fun updateNonExistingTechStack() {
        val databaseSizeBeforeUpdate = techStackRepository.findAll().size

        // Create the TechStack
        val techStackDTO = techStackMapper.toDto(techStack)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTechStackMockMvc.perform(
            put("/api/tech-stacks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(techStackDTO))
        ).andExpect(status().isBadRequest)

        // Validate the TechStack in the database
        val techStackList = techStackRepository.findAll()
        assertThat(techStackList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteTechStack() {
        // Initialize the database
        techStackRepository.saveAndFlush(techStack)

        val databaseSizeBeforeDelete = techStackRepository.findAll().size

        // Delete the techStack
        restTechStackMockMvc.perform(
            delete("/api/tech-stacks/{id}", techStack.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val techStackList = techStackRepository.findAll()
        assertThat(techStackList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): TechStack {
            val techStack = TechStack()

            return techStack
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): TechStack {
            val techStack = TechStack()

            return techStack
        }
    }
}
