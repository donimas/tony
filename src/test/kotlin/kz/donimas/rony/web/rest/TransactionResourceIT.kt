package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.Transaction
import kz.donimas.rony.domain.enumeration.DirectionType
import kz.donimas.rony.domain.enumeration.TransactionStatus
import kz.donimas.rony.repository.TransactionRepository
import kz.donimas.rony.service.TransactionService
import kz.donimas.rony.service.mapper.TransactionMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import java.math.BigDecimal
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [TransactionResource] REST controller.
 *
 * @see TransactionResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class TransactionResourceIT {

    @Autowired
    private lateinit var transactionRepository: TransactionRepository

    @Autowired
    private lateinit var transactionMapper: TransactionMapper

    @Autowired
    private lateinit var transactionService: TransactionService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restTransactionMockMvc: MockMvc

    private lateinit var transaction: Transaction

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val transactionResource = TransactionResource(transactionService)
        this.restTransactionMockMvc = MockMvcBuilders.standaloneSetup(transactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        transaction = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createTransaction() {
        val databaseSizeBeforeCreate = transactionRepository.findAll().size

        // Create the Transaction
        val transactionDTO = transactionMapper.toDto(transaction)
        restTransactionMockMvc.perform(
            post("/api/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(transactionDTO))
        ).andExpect(status().isCreated)

        // Validate the Transaction in the database
        val transactionList = transactionRepository.findAll()
        assertThat(transactionList).hasSize(databaseSizeBeforeCreate + 1)
        val testTransaction = transactionList[transactionList.size - 1]
        assertThat(testTransaction.planningSum).isEqualTo(DEFAULT_PLANNING_SUM)
        assertThat(testTransaction.actualSum).isEqualTo(DEFAULT_ACTUAL_SUM)
        assertThat(testTransaction.convertedSum).isEqualTo(DEFAULT_CONVERTED_SUM)
        assertThat(testTransaction.payDate).isEqualTo(DEFAULT_PAY_DATE)
        assertThat(testTransaction.note).isEqualTo(DEFAULT_NOTE)
        assertThat(testTransaction.status).isEqualTo(DEFAULT_STATUS)
        assertThat(testTransaction.direction).isEqualTo(DEFAULT_DIRECTION)
        assertThat(testTransaction.createDate).isEqualTo(DEFAULT_CREATE_DATE)
        assertThat(testTransaction.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createTransactionWithExistingId() {
        val databaseSizeBeforeCreate = transactionRepository.findAll().size

        // Create the Transaction with an existing ID
        transaction.id = 1L
        val transactionDTO = transactionMapper.toDto(transaction)

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactionMockMvc.perform(
            post("/api/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(transactionDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Transaction in the database
        val transactionList = transactionRepository.findAll()
        assertThat(transactionList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllTransactions() {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction)

        // Get all the transactionList
        restTransactionMockMvc.perform(get("/api/transactions?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transaction.id?.toInt())))
            .andExpect(jsonPath("$.[*].planningSum").value(hasItem(DEFAULT_PLANNING_SUM?.toInt())))
            .andExpect(jsonPath("$.[*].actualSum").value(hasItem(DEFAULT_ACTUAL_SUM?.toInt())))
            .andExpect(jsonPath("$.[*].convertedSum").value(hasItem(DEFAULT_CONVERTED_SUM?.toInt())))
            .andExpect(jsonPath("$.[*].payDate").value(hasItem(sameInstant(DEFAULT_PAY_DATE))))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].direction").value(hasItem(DEFAULT_DIRECTION.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getTransaction() {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction)

        val id = transaction.id
        assertNotNull(id)

        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(transaction.id?.toInt()))
            .andExpect(jsonPath("$.planningSum").value(DEFAULT_PLANNING_SUM?.toInt()))
            .andExpect(jsonPath("$.actualSum").value(DEFAULT_ACTUAL_SUM?.toInt()))
            .andExpect(jsonPath("$.convertedSum").value(DEFAULT_CONVERTED_SUM?.toInt()))
            .andExpect(jsonPath("$.payDate").value(sameInstant(DEFAULT_PAY_DATE)))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.direction").value(DEFAULT_DIRECTION.toString()))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingTransaction() {
        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateTransaction() {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction)

        val databaseSizeBeforeUpdate = transactionRepository.findAll().size

        // Update the transaction
        val id = transaction.id
        assertNotNull(id)
        val updatedTransaction = transactionRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedTransaction are not directly saved in db
        em.detach(updatedTransaction)
        updatedTransaction.planningSum = UPDATED_PLANNING_SUM
        updatedTransaction.actualSum = UPDATED_ACTUAL_SUM
        updatedTransaction.convertedSum = UPDATED_CONVERTED_SUM
        updatedTransaction.payDate = UPDATED_PAY_DATE
        updatedTransaction.note = UPDATED_NOTE
        updatedTransaction.status = UPDATED_STATUS
        updatedTransaction.direction = UPDATED_DIRECTION
        updatedTransaction.createDate = UPDATED_CREATE_DATE
        updatedTransaction.flagDeleted = UPDATED_FLAG_DELETED
        val transactionDTO = transactionMapper.toDto(updatedTransaction)

        restTransactionMockMvc.perform(
            put("/api/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(transactionDTO))
        ).andExpect(status().isOk)

        // Validate the Transaction in the database
        val transactionList = transactionRepository.findAll()
        assertThat(transactionList).hasSize(databaseSizeBeforeUpdate)
        val testTransaction = transactionList[transactionList.size - 1]
        assertThat(testTransaction.planningSum).isEqualTo(UPDATED_PLANNING_SUM)
        assertThat(testTransaction.actualSum).isEqualTo(UPDATED_ACTUAL_SUM)
        assertThat(testTransaction.convertedSum).isEqualTo(UPDATED_CONVERTED_SUM)
        assertThat(testTransaction.payDate).isEqualTo(UPDATED_PAY_DATE)
        assertThat(testTransaction.note).isEqualTo(UPDATED_NOTE)
        assertThat(testTransaction.status).isEqualTo(UPDATED_STATUS)
        assertThat(testTransaction.direction).isEqualTo(UPDATED_DIRECTION)
        assertThat(testTransaction.createDate).isEqualTo(UPDATED_CREATE_DATE)
        assertThat(testTransaction.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingTransaction() {
        val databaseSizeBeforeUpdate = transactionRepository.findAll().size

        // Create the Transaction
        val transactionDTO = transactionMapper.toDto(transaction)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionMockMvc.perform(
            put("/api/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(transactionDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Transaction in the database
        val transactionList = transactionRepository.findAll()
        assertThat(transactionList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteTransaction() {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction)

        val databaseSizeBeforeDelete = transactionRepository.findAll().size

        // Delete the transaction
        restTransactionMockMvc.perform(
            delete("/api/transactions/{id}", transaction.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val transactionList = transactionRepository.findAll()
        assertThat(transactionList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private val DEFAULT_PLANNING_SUM: BigDecimal = BigDecimal(1)
        private val UPDATED_PLANNING_SUM: BigDecimal = BigDecimal(2)

        private val DEFAULT_ACTUAL_SUM: BigDecimal = BigDecimal(1)
        private val UPDATED_ACTUAL_SUM: BigDecimal = BigDecimal(2)

        private val DEFAULT_CONVERTED_SUM: BigDecimal = BigDecimal(1)
        private val UPDATED_CONVERTED_SUM: BigDecimal = BigDecimal(2)

        private val DEFAULT_PAY_DATE: ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC)
        private val UPDATED_PAY_DATE: ZonedDateTime = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0)

        private const val DEFAULT_NOTE = "AAAAAAAAAA"
        private const val UPDATED_NOTE = "BBBBBBBBBB"

        private val DEFAULT_STATUS: TransactionStatus = TransactionStatus.PLANNING
        private val UPDATED_STATUS: TransactionStatus = TransactionStatus.DECLINED

        private val DEFAULT_DIRECTION: DirectionType = DirectionType.EMPTY
        private val UPDATED_DIRECTION: DirectionType = DirectionType.INCOME

        private val DEFAULT_CREATE_DATE: ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC)
        private val UPDATED_CREATE_DATE: ZonedDateTime = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0)

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Transaction {
            val transaction = Transaction(
                planningSum = DEFAULT_PLANNING_SUM,
                actualSum = DEFAULT_ACTUAL_SUM,
                convertedSum = DEFAULT_CONVERTED_SUM,
                payDate = DEFAULT_PAY_DATE,
                note = DEFAULT_NOTE,
                status = DEFAULT_STATUS,
                direction = DEFAULT_DIRECTION,
                createDate = DEFAULT_CREATE_DATE,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return transaction
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Transaction {
            val transaction = Transaction(
                planningSum = UPDATED_PLANNING_SUM,
                actualSum = UPDATED_ACTUAL_SUM,
                convertedSum = UPDATED_CONVERTED_SUM,
                payDate = UPDATED_PAY_DATE,
                note = UPDATED_NOTE,
                status = UPDATED_STATUS,
                direction = UPDATED_DIRECTION,
                createDate = UPDATED_CREATE_DATE,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return transaction
        }
    }
}
