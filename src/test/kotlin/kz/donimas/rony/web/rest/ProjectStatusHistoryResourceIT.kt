package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.ProjectStatusHistory
import kz.donimas.rony.repository.ProjectStatusHistoryRepository
import kz.donimas.rony.service.ProjectStatusHistoryService
import kz.donimas.rony.service.mapper.ProjectStatusHistoryMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [ProjectStatusHistoryResource] REST controller.
 *
 * @see ProjectStatusHistoryResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class ProjectStatusHistoryResourceIT {

    @Autowired
    private lateinit var projectStatusHistoryRepository: ProjectStatusHistoryRepository

    @Autowired
    private lateinit var projectStatusHistoryMapper: ProjectStatusHistoryMapper

    @Autowired
    private lateinit var projectStatusHistoryService: ProjectStatusHistoryService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restProjectStatusHistoryMockMvc: MockMvc

    private lateinit var projectStatusHistory: ProjectStatusHistory

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val projectStatusHistoryResource = ProjectStatusHistoryResource(projectStatusHistoryService)
        this.restProjectStatusHistoryMockMvc = MockMvcBuilders.standaloneSetup(projectStatusHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        projectStatusHistory = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createProjectStatusHistory() {
        val databaseSizeBeforeCreate = projectStatusHistoryRepository.findAll().size

        // Create the ProjectStatusHistory
        val projectStatusHistoryDTO = projectStatusHistoryMapper.toDto(projectStatusHistory)
        restProjectStatusHistoryMockMvc.perform(
            post("/api/project-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectStatusHistoryDTO))
        ).andExpect(status().isCreated)

        // Validate the ProjectStatusHistory in the database
        val projectStatusHistoryList = projectStatusHistoryRepository.findAll()
        assertThat(projectStatusHistoryList).hasSize(databaseSizeBeforeCreate + 1)
        val testProjectStatusHistory = projectStatusHistoryList[projectStatusHistoryList.size - 1]
        assertThat(testProjectStatusHistory.comment).isEqualTo(DEFAULT_COMMENT)
        assertThat(testProjectStatusHistory.createDate).isEqualTo(DEFAULT_CREATE_DATE)
    }

    @Test
    @Transactional
    fun createProjectStatusHistoryWithExistingId() {
        val databaseSizeBeforeCreate = projectStatusHistoryRepository.findAll().size

        // Create the ProjectStatusHistory with an existing ID
        projectStatusHistory.id = 1L
        val projectStatusHistoryDTO = projectStatusHistoryMapper.toDto(projectStatusHistory)

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjectStatusHistoryMockMvc.perform(
            post("/api/project-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectStatusHistoryDTO))
        ).andExpect(status().isBadRequest)

        // Validate the ProjectStatusHistory in the database
        val projectStatusHistoryList = projectStatusHistoryRepository.findAll()
        assertThat(projectStatusHistoryList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllProjectStatusHistories() {
        // Initialize the database
        projectStatusHistoryRepository.saveAndFlush(projectStatusHistory)

        // Get all the projectStatusHistoryList
        restProjectStatusHistoryMockMvc.perform(get("/api/project-status-histories?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projectStatusHistory.id?.toInt())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getProjectStatusHistory() {
        // Initialize the database
        projectStatusHistoryRepository.saveAndFlush(projectStatusHistory)

        val id = projectStatusHistory.id
        assertNotNull(id)

        // Get the projectStatusHistory
        restProjectStatusHistoryMockMvc.perform(get("/api/project-status-histories/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(projectStatusHistory.id?.toInt()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingProjectStatusHistory() {
        // Get the projectStatusHistory
        restProjectStatusHistoryMockMvc.perform(get("/api/project-status-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateProjectStatusHistory() {
        // Initialize the database
        projectStatusHistoryRepository.saveAndFlush(projectStatusHistory)

        val databaseSizeBeforeUpdate = projectStatusHistoryRepository.findAll().size

        // Update the projectStatusHistory
        val id = projectStatusHistory.id
        assertNotNull(id)
        val updatedProjectStatusHistory = projectStatusHistoryRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedProjectStatusHistory are not directly saved in db
        em.detach(updatedProjectStatusHistory)
        updatedProjectStatusHistory.comment = UPDATED_COMMENT
        updatedProjectStatusHistory.createDate = UPDATED_CREATE_DATE
        val projectStatusHistoryDTO = projectStatusHistoryMapper.toDto(updatedProjectStatusHistory)

        restProjectStatusHistoryMockMvc.perform(
            put("/api/project-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectStatusHistoryDTO))
        ).andExpect(status().isOk)

        // Validate the ProjectStatusHistory in the database
        val projectStatusHistoryList = projectStatusHistoryRepository.findAll()
        assertThat(projectStatusHistoryList).hasSize(databaseSizeBeforeUpdate)
        val testProjectStatusHistory = projectStatusHistoryList[projectStatusHistoryList.size - 1]
        assertThat(testProjectStatusHistory.comment).isEqualTo(UPDATED_COMMENT)
        assertThat(testProjectStatusHistory.createDate).isEqualTo(UPDATED_CREATE_DATE)
    }

    @Test
    @Transactional
    fun updateNonExistingProjectStatusHistory() {
        val databaseSizeBeforeUpdate = projectStatusHistoryRepository.findAll().size

        // Create the ProjectStatusHistory
        val projectStatusHistoryDTO = projectStatusHistoryMapper.toDto(projectStatusHistory)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProjectStatusHistoryMockMvc.perform(
            put("/api/project-status-histories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectStatusHistoryDTO))
        ).andExpect(status().isBadRequest)

        // Validate the ProjectStatusHistory in the database
        val projectStatusHistoryList = projectStatusHistoryRepository.findAll()
        assertThat(projectStatusHistoryList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteProjectStatusHistory() {
        // Initialize the database
        projectStatusHistoryRepository.saveAndFlush(projectStatusHistory)

        val databaseSizeBeforeDelete = projectStatusHistoryRepository.findAll().size

        // Delete the projectStatusHistory
        restProjectStatusHistoryMockMvc.perform(
            delete("/api/project-status-histories/{id}", projectStatusHistory.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val projectStatusHistoryList = projectStatusHistoryRepository.findAll()
        assertThat(projectStatusHistoryList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_COMMENT = "AAAAAAAAAA"
        private const val UPDATED_COMMENT = "BBBBBBBBBB"

        private val DEFAULT_CREATE_DATE: ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC)
        private val UPDATED_CREATE_DATE: ZonedDateTime = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0)

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): ProjectStatusHistory {
            val projectStatusHistory = ProjectStatusHistory(
                comment = DEFAULT_COMMENT,
                createDate = DEFAULT_CREATE_DATE
            )

            return projectStatusHistory
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): ProjectStatusHistory {
            val projectStatusHistory = ProjectStatusHistory(
                comment = UPDATED_COMMENT,
                createDate = UPDATED_CREATE_DATE
            )

            return projectStatusHistory
        }
    }
}
