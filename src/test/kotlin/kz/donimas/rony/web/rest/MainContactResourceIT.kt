package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.MainContact
import kz.donimas.rony.repository.MainContactRepository
import kz.donimas.rony.service.MainContactService
import kz.donimas.rony.service.mapper.MainContactMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import java.time.Instant
import java.time.temporal.ChronoUnit
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [MainContactResource] REST controller.
 *
 * @see MainContactResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class MainContactResourceIT {

    @Autowired
    private lateinit var mainContactRepository: MainContactRepository

    @Autowired
    private lateinit var mainContactMapper: MainContactMapper

    @Autowired
    private lateinit var mainContactService: MainContactService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restMainContactMockMvc: MockMvc

    private lateinit var mainContact: MainContact

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val mainContactResource = MainContactResource(mainContactService)
        this.restMainContactMockMvc = MockMvcBuilders.standaloneSetup(mainContactResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        mainContact = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createMainContact() {
        val databaseSizeBeforeCreate = mainContactRepository.findAll().size

        // Create the MainContact
        val mainContactDTO = mainContactMapper.toDto(mainContact)
        restMainContactMockMvc.perform(
            post("/api/main-contacts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(mainContactDTO))
        ).andExpect(status().isCreated)

        // Validate the MainContact in the database
        val mainContactList = mainContactRepository.findAll()
        assertThat(mainContactList).hasSize(databaseSizeBeforeCreate + 1)
        val testMainContact = mainContactList[mainContactList.size - 1]
        assertThat(testMainContact.fullName).isEqualTo(DEFAULT_FULL_NAME)
        assertThat(testMainContact.telephone).isEqualTo(DEFAULT_TELEPHONE)
        assertThat(testMainContact.email).isEqualTo(DEFAULT_EMAIL)
        assertThat(testMainContact.dob).isEqualTo(DEFAULT_DOB)
        assertThat(testMainContact.position).isEqualTo(DEFAULT_POSITION)
        assertThat(testMainContact.comment).isEqualTo(DEFAULT_COMMENT)
        assertThat(testMainContact.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createMainContactWithExistingId() {
        val databaseSizeBeforeCreate = mainContactRepository.findAll().size

        // Create the MainContact with an existing ID
        mainContact.id = 1L
        val mainContactDTO = mainContactMapper.toDto(mainContact)

        // An entity with an existing ID cannot be created, so this API call must fail
        restMainContactMockMvc.perform(
            post("/api/main-contacts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(mainContactDTO))
        ).andExpect(status().isBadRequest)

        // Validate the MainContact in the database
        val mainContactList = mainContactRepository.findAll()
        assertThat(mainContactList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    fun checkFullNameIsRequired() {
        val databaseSizeBeforeTest = mainContactRepository.findAll().size
        // set the field null
        mainContact.fullName = null

        // Create the MainContact, which fails.
        val mainContactDTO = mainContactMapper.toDto(mainContact)

        restMainContactMockMvc.perform(
            post("/api/main-contacts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(mainContactDTO))
        ).andExpect(status().isBadRequest)

        val mainContactList = mainContactRepository.findAll()
        assertThat(mainContactList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllMainContacts() {
        // Initialize the database
        mainContactRepository.saveAndFlush(mainContact)

        // Get all the mainContactList
        restMainContactMockMvc.perform(get("/api/main-contacts?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mainContact.id?.toInt())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].dob").value(hasItem(DEFAULT_DOB.toString())))
            .andExpect(jsonPath("$.[*].position").value(hasItem(DEFAULT_POSITION)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getMainContact() {
        // Initialize the database
        mainContactRepository.saveAndFlush(mainContact)

        val id = mainContact.id
        assertNotNull(id)

        // Get the mainContact
        restMainContactMockMvc.perform(get("/api/main-contacts/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(mainContact.id?.toInt()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.dob").value(DEFAULT_DOB.toString()))
            .andExpect(jsonPath("$.position").value(DEFAULT_POSITION))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingMainContact() {
        // Get the mainContact
        restMainContactMockMvc.perform(get("/api/main-contacts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateMainContact() {
        // Initialize the database
        mainContactRepository.saveAndFlush(mainContact)

        val databaseSizeBeforeUpdate = mainContactRepository.findAll().size

        // Update the mainContact
        val id = mainContact.id
        assertNotNull(id)
        val updatedMainContact = mainContactRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedMainContact are not directly saved in db
        em.detach(updatedMainContact)
        updatedMainContact.fullName = UPDATED_FULL_NAME
        updatedMainContact.telephone = UPDATED_TELEPHONE
        updatedMainContact.email = UPDATED_EMAIL
        updatedMainContact.dob = UPDATED_DOB
        updatedMainContact.position = UPDATED_POSITION
        updatedMainContact.comment = UPDATED_COMMENT
        updatedMainContact.flagDeleted = UPDATED_FLAG_DELETED
        val mainContactDTO = mainContactMapper.toDto(updatedMainContact)

        restMainContactMockMvc.perform(
            put("/api/main-contacts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(mainContactDTO))
        ).andExpect(status().isOk)

        // Validate the MainContact in the database
        val mainContactList = mainContactRepository.findAll()
        assertThat(mainContactList).hasSize(databaseSizeBeforeUpdate)
        val testMainContact = mainContactList[mainContactList.size - 1]
        assertThat(testMainContact.fullName).isEqualTo(UPDATED_FULL_NAME)
        assertThat(testMainContact.telephone).isEqualTo(UPDATED_TELEPHONE)
        assertThat(testMainContact.email).isEqualTo(UPDATED_EMAIL)
        assertThat(testMainContact.dob).isEqualTo(UPDATED_DOB)
        assertThat(testMainContact.position).isEqualTo(UPDATED_POSITION)
        assertThat(testMainContact.comment).isEqualTo(UPDATED_COMMENT)
        assertThat(testMainContact.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingMainContact() {
        val databaseSizeBeforeUpdate = mainContactRepository.findAll().size

        // Create the MainContact
        val mainContactDTO = mainContactMapper.toDto(mainContact)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMainContactMockMvc.perform(
            put("/api/main-contacts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(mainContactDTO))
        ).andExpect(status().isBadRequest)

        // Validate the MainContact in the database
        val mainContactList = mainContactRepository.findAll()
        assertThat(mainContactList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteMainContact() {
        // Initialize the database
        mainContactRepository.saveAndFlush(mainContact)

        val databaseSizeBeforeDelete = mainContactRepository.findAll().size

        // Delete the mainContact
        restMainContactMockMvc.perform(
            delete("/api/main-contacts/{id}", mainContact.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val mainContactList = mainContactRepository.findAll()
        assertThat(mainContactList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_FULL_NAME = "AAAAAAAAAA"
        private const val UPDATED_FULL_NAME = "BBBBBBBBBB"

        private const val DEFAULT_TELEPHONE = "AAAAAAAAAA"
        private const val UPDATED_TELEPHONE = "BBBBBBBBBB"

        private const val DEFAULT_EMAIL = "AAAAAAAAAA"
        private const val UPDATED_EMAIL = "BBBBBBBBBB"

        private val DEFAULT_DOB: Instant = Instant.ofEpochMilli(0L)
        private val UPDATED_DOB: Instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

        private const val DEFAULT_POSITION = "AAAAAAAAAA"
        private const val UPDATED_POSITION = "BBBBBBBBBB"

        private const val DEFAULT_COMMENT = "AAAAAAAAAA"
        private const val UPDATED_COMMENT = "BBBBBBBBBB"

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): MainContact {
            val mainContact = MainContact(
                fullName = DEFAULT_FULL_NAME,
                telephone = DEFAULT_TELEPHONE,
                email = DEFAULT_EMAIL,
                dob = DEFAULT_DOB,
                position = DEFAULT_POSITION,
                comment = DEFAULT_COMMENT,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return mainContact
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): MainContact {
            val mainContact = MainContact(
                fullName = UPDATED_FULL_NAME,
                telephone = UPDATED_TELEPHONE,
                email = UPDATED_EMAIL,
                dob = UPDATED_DOB,
                position = UPDATED_POSITION,
                comment = UPDATED_COMMENT,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return mainContact
        }
    }
}
