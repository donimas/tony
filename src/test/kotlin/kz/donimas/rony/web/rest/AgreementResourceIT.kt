package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.Agreement
import kz.donimas.rony.repository.AgreementRepository
import kz.donimas.rony.service.AgreementService
import kz.donimas.rony.service.mapper.AgreementMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.mockito.ArgumentMatchers.*
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import java.math.BigDecimal
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [AgreementResource] REST controller.
 *
 * @see AgreementResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
@Extensions(
    ExtendWith(MockitoExtension::class)
)
class AgreementResourceIT {

    @Autowired
    private lateinit var agreementRepository: AgreementRepository

    @Mock
    private lateinit var agreementRepositoryMock: AgreementRepository

    @Autowired
    private lateinit var agreementMapper: AgreementMapper

    @Mock
    private lateinit var agreementServiceMock: AgreementService

    @Autowired
    private lateinit var agreementService: AgreementService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restAgreementMockMvc: MockMvc

    private lateinit var agreement: Agreement

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val agreementResource = AgreementResource(agreementService)
        this.restAgreementMockMvc = MockMvcBuilders.standaloneSetup(agreementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        agreement = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createAgreement() {
        val databaseSizeBeforeCreate = agreementRepository.findAll().size

        // Create the Agreement
        val agreementDTO = agreementMapper.toDto(agreement)
        restAgreementMockMvc.perform(
            post("/api/agreements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(agreementDTO))
        ).andExpect(status().isCreated)

        // Validate the Agreement in the database
        val agreementList = agreementRepository.findAll()
        assertThat(agreementList).hasSize(databaseSizeBeforeCreate + 1)
        val testAgreement = agreementList[agreementList.size - 1]
        assertThat(testAgreement.createDate).isEqualTo(DEFAULT_CREATE_DATE)
        assertThat(testAgreement.comment).isEqualTo(DEFAULT_COMMENT)
        assertThat(testAgreement.progressBar).isEqualTo(DEFAULT_PROGRESS_BAR)
        assertThat(testAgreement.startDate).isEqualTo(DEFAULT_START_DATE)
        assertThat(testAgreement.finishDate).isEqualTo(DEFAULT_FINISH_DATE)
        assertThat(testAgreement.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createAgreementWithExistingId() {
        val databaseSizeBeforeCreate = agreementRepository.findAll().size

        // Create the Agreement with an existing ID
        agreement.id = 1L
        val agreementDTO = agreementMapper.toDto(agreement)

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgreementMockMvc.perform(
            post("/api/agreements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(agreementDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Agreement in the database
        val agreementList = agreementRepository.findAll()
        assertThat(agreementList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllAgreements() {
        // Initialize the database
        agreementRepository.saveAndFlush(agreement)

        // Get all the agreementList
        restAgreementMockMvc.perform(get("/api/agreements?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agreement.id?.toInt())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].progressBar").value(hasItem(DEFAULT_PROGRESS_BAR?.toInt())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].finishDate").value(hasItem(DEFAULT_FINISH_DATE.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Suppress("unchecked")
    @Throws(Exception::class)
    fun getAllAgreementsWithEagerRelationshipsIsEnabled() {
        val agreementResource = AgreementResource(agreementServiceMock)
        `when`(agreementServiceMock.findAllWithEagerRelationships(any())).thenReturn(PageImpl(mutableListOf()))

        val restAgreementMockMvc = MockMvcBuilders.standaloneSetup(agreementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build()

        restAgreementMockMvc.perform(get("/api/agreements?eagerload=true"))
            .andExpect(status().isOk)

        verify(agreementServiceMock, times(1)).findAllWithEagerRelationships(any())
    }

    @Suppress("unchecked")
    @Throws(Exception::class)
    fun getAllAgreementsWithEagerRelationshipsIsNotEnabled() {
        val agreementResource = AgreementResource(agreementServiceMock)
        `when`(agreementServiceMock.findAllWithEagerRelationships(any())).thenReturn(PageImpl(mutableListOf()))

        val restAgreementMockMvc = MockMvcBuilders.standaloneSetup(agreementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build()

        restAgreementMockMvc.perform(get("/api/agreements?eagerload=true"))
            .andExpect(status().isOk)

        verify(agreementServiceMock, times(1)).findAllWithEagerRelationships(any())
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAgreement() {
        // Initialize the database
        agreementRepository.saveAndFlush(agreement)

        val id = agreement.id
        assertNotNull(id)

        // Get the agreement
        restAgreementMockMvc.perform(get("/api/agreements/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(agreement.id?.toInt()))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
            .andExpect(jsonPath("$.progressBar").value(DEFAULT_PROGRESS_BAR?.toInt()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.finishDate").value(DEFAULT_FINISH_DATE.toString()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingAgreement() {
        // Get the agreement
        restAgreementMockMvc.perform(get("/api/agreements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateAgreement() {
        // Initialize the database
        agreementRepository.saveAndFlush(agreement)

        val databaseSizeBeforeUpdate = agreementRepository.findAll().size

        // Update the agreement
        val id = agreement.id
        assertNotNull(id)
        val updatedAgreement = agreementRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedAgreement are not directly saved in db
        em.detach(updatedAgreement)
        updatedAgreement.createDate = UPDATED_CREATE_DATE
        updatedAgreement.comment = UPDATED_COMMENT
        updatedAgreement.progressBar = UPDATED_PROGRESS_BAR
        updatedAgreement.startDate = UPDATED_START_DATE
        updatedAgreement.finishDate = UPDATED_FINISH_DATE
        updatedAgreement.flagDeleted = UPDATED_FLAG_DELETED
        val agreementDTO = agreementMapper.toDto(updatedAgreement)

        restAgreementMockMvc.perform(
            put("/api/agreements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(agreementDTO))
        ).andExpect(status().isOk)

        // Validate the Agreement in the database
        val agreementList = agreementRepository.findAll()
        assertThat(agreementList).hasSize(databaseSizeBeforeUpdate)
        val testAgreement = agreementList[agreementList.size - 1]
        assertThat(testAgreement.createDate).isEqualTo(UPDATED_CREATE_DATE)
        assertThat(testAgreement.comment).isEqualTo(UPDATED_COMMENT)
        assertThat(testAgreement.progressBar).isEqualTo(UPDATED_PROGRESS_BAR)
        assertThat(testAgreement.startDate).isEqualTo(UPDATED_START_DATE)
        assertThat(testAgreement.finishDate).isEqualTo(UPDATED_FINISH_DATE)
        assertThat(testAgreement.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingAgreement() {
        val databaseSizeBeforeUpdate = agreementRepository.findAll().size

        // Create the Agreement
        val agreementDTO = agreementMapper.toDto(agreement)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgreementMockMvc.perform(
            put("/api/agreements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(agreementDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Agreement in the database
        val agreementList = agreementRepository.findAll()
        assertThat(agreementList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteAgreement() {
        // Initialize the database
        agreementRepository.saveAndFlush(agreement)

        val databaseSizeBeforeDelete = agreementRepository.findAll().size

        // Delete the agreement
        restAgreementMockMvc.perform(
            delete("/api/agreements/{id}", agreement.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val agreementList = agreementRepository.findAll()
        assertThat(agreementList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private val DEFAULT_CREATE_DATE: ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC)
        private val UPDATED_CREATE_DATE: ZonedDateTime = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0)

        private const val DEFAULT_COMMENT = "AAAAAAAAAA"
        private const val UPDATED_COMMENT = "BBBBBBBBBB"

        private val DEFAULT_PROGRESS_BAR: BigDecimal = BigDecimal(1)
        private val UPDATED_PROGRESS_BAR: BigDecimal = BigDecimal(2)

        private val DEFAULT_START_DATE: Instant = Instant.ofEpochMilli(0L)
        private val UPDATED_START_DATE: Instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

        private val DEFAULT_FINISH_DATE: Instant = Instant.ofEpochMilli(0L)
        private val UPDATED_FINISH_DATE: Instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Agreement {
            val agreement = Agreement(
                createDate = DEFAULT_CREATE_DATE,
                comment = DEFAULT_COMMENT,
                progressBar = DEFAULT_PROGRESS_BAR,
                startDate = DEFAULT_START_DATE,
                finishDate = DEFAULT_FINISH_DATE,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return agreement
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Agreement {
            val agreement = Agreement(
                createDate = UPDATED_CREATE_DATE,
                comment = UPDATED_COMMENT,
                progressBar = UPDATED_PROGRESS_BAR,
                startDate = UPDATED_START_DATE,
                finishDate = UPDATED_FINISH_DATE,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return agreement
        }
    }
}
