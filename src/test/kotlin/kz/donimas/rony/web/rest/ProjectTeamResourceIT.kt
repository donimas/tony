package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.ProjectTeam
import kz.donimas.rony.repository.ProjectTeamRepository
import kz.donimas.rony.service.ProjectTeamService
import kz.donimas.rony.service.mapper.ProjectTeamMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [ProjectTeamResource] REST controller.
 *
 * @see ProjectTeamResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class ProjectTeamResourceIT {

    @Autowired
    private lateinit var projectTeamRepository: ProjectTeamRepository

    @Autowired
    private lateinit var projectTeamMapper: ProjectTeamMapper

    @Autowired
    private lateinit var projectTeamService: ProjectTeamService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restProjectTeamMockMvc: MockMvc

    private lateinit var projectTeam: ProjectTeam

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val projectTeamResource = ProjectTeamResource(projectTeamService)
        this.restProjectTeamMockMvc = MockMvcBuilders.standaloneSetup(projectTeamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        projectTeam = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createProjectTeam() {
        val databaseSizeBeforeCreate = projectTeamRepository.findAll().size

        // Create the ProjectTeam
        val projectTeamDTO = projectTeamMapper.toDto(projectTeam)
        restProjectTeamMockMvc.perform(
            post("/api/project-teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectTeamDTO))
        ).andExpect(status().isCreated)

        // Validate the ProjectTeam in the database
        val projectTeamList = projectTeamRepository.findAll()
        assertThat(projectTeamList).hasSize(databaseSizeBeforeCreate + 1)
        val testProjectTeam = projectTeamList[projectTeamList.size - 1]
        assertThat(testProjectTeam.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createProjectTeamWithExistingId() {
        val databaseSizeBeforeCreate = projectTeamRepository.findAll().size

        // Create the ProjectTeam with an existing ID
        projectTeam.id = 1L
        val projectTeamDTO = projectTeamMapper.toDto(projectTeam)

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjectTeamMockMvc.perform(
            post("/api/project-teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectTeamDTO))
        ).andExpect(status().isBadRequest)

        // Validate the ProjectTeam in the database
        val projectTeamList = projectTeamRepository.findAll()
        assertThat(projectTeamList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllProjectTeams() {
        // Initialize the database
        projectTeamRepository.saveAndFlush(projectTeam)

        // Get all the projectTeamList
        restProjectTeamMockMvc.perform(get("/api/project-teams?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projectTeam.id?.toInt())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getProjectTeam() {
        // Initialize the database
        projectTeamRepository.saveAndFlush(projectTeam)

        val id = projectTeam.id
        assertNotNull(id)

        // Get the projectTeam
        restProjectTeamMockMvc.perform(get("/api/project-teams/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(projectTeam.id?.toInt()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingProjectTeam() {
        // Get the projectTeam
        restProjectTeamMockMvc.perform(get("/api/project-teams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateProjectTeam() {
        // Initialize the database
        projectTeamRepository.saveAndFlush(projectTeam)

        val databaseSizeBeforeUpdate = projectTeamRepository.findAll().size

        // Update the projectTeam
        val id = projectTeam.id
        assertNotNull(id)
        val updatedProjectTeam = projectTeamRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedProjectTeam are not directly saved in db
        em.detach(updatedProjectTeam)
        updatedProjectTeam.flagDeleted = UPDATED_FLAG_DELETED
        val projectTeamDTO = projectTeamMapper.toDto(updatedProjectTeam)

        restProjectTeamMockMvc.perform(
            put("/api/project-teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectTeamDTO))
        ).andExpect(status().isOk)

        // Validate the ProjectTeam in the database
        val projectTeamList = projectTeamRepository.findAll()
        assertThat(projectTeamList).hasSize(databaseSizeBeforeUpdate)
        val testProjectTeam = projectTeamList[projectTeamList.size - 1]
        assertThat(testProjectTeam.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingProjectTeam() {
        val databaseSizeBeforeUpdate = projectTeamRepository.findAll().size

        // Create the ProjectTeam
        val projectTeamDTO = projectTeamMapper.toDto(projectTeam)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProjectTeamMockMvc.perform(
            put("/api/project-teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(projectTeamDTO))
        ).andExpect(status().isBadRequest)

        // Validate the ProjectTeam in the database
        val projectTeamList = projectTeamRepository.findAll()
        assertThat(projectTeamList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteProjectTeam() {
        // Initialize the database
        projectTeamRepository.saveAndFlush(projectTeam)

        val databaseSizeBeforeDelete = projectTeamRepository.findAll().size

        // Delete the projectTeam
        restProjectTeamMockMvc.perform(
            delete("/api/project-teams/{id}", projectTeam.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val projectTeamList = projectTeamRepository.findAll()
        assertThat(projectTeamList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): ProjectTeam {
            val projectTeam = ProjectTeam(
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return projectTeam
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): ProjectTeam {
            val projectTeam = ProjectTeam(
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return projectTeam
        }
    }
}
