package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.Dictionary
import kz.donimas.rony.repository.DictionaryRepository
import kz.donimas.rony.service.DictionaryService
import kz.donimas.rony.service.mapper.DictionaryMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [DictionaryResource] REST controller.
 *
 * @see DictionaryResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class DictionaryResourceIT {

    @Autowired
    private lateinit var dictionaryRepository: DictionaryRepository

    @Autowired
    private lateinit var dictionaryMapper: DictionaryMapper

    @Autowired
    private lateinit var dictionaryService: DictionaryService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restDictionaryMockMvc: MockMvc

    private lateinit var dictionary: Dictionary

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val dictionaryResource = DictionaryResource(dictionaryService)
        this.restDictionaryMockMvc = MockMvcBuilders.standaloneSetup(dictionaryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        dictionary = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createDictionary() {
        val databaseSizeBeforeCreate = dictionaryRepository.findAll().size

        // Create the Dictionary
        val dictionaryDTO = dictionaryMapper.toDto(dictionary)
        restDictionaryMockMvc.perform(
            post("/api/dictionaries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(dictionaryDTO))
        ).andExpect(status().isCreated)

        // Validate the Dictionary in the database
        val dictionaryList = dictionaryRepository.findAll()
        assertThat(dictionaryList).hasSize(databaseSizeBeforeCreate + 1)
        val testDictionary = dictionaryList[dictionaryList.size - 1]
        assertThat(testDictionary.code).isEqualTo(DEFAULT_CODE)
        assertThat(testDictionary.name).isEqualTo(DEFAULT_NAME)
        assertThat(testDictionary.entityName).isEqualTo(DEFAULT_ENTITY_NAME)
        assertThat(testDictionary.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createDictionaryWithExistingId() {
        val databaseSizeBeforeCreate = dictionaryRepository.findAll().size

        // Create the Dictionary with an existing ID
        dictionary.id = 1L
        val dictionaryDTO = dictionaryMapper.toDto(dictionary)

        // An entity with an existing ID cannot be created, so this API call must fail
        restDictionaryMockMvc.perform(
            post("/api/dictionaries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(dictionaryDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Dictionary in the database
        val dictionaryList = dictionaryRepository.findAll()
        assertThat(dictionaryList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    fun checkNameIsRequired() {
        val databaseSizeBeforeTest = dictionaryRepository.findAll().size
        // set the field null
        dictionary.name = null

        // Create the Dictionary, which fails.
        val dictionaryDTO = dictionaryMapper.toDto(dictionary)

        restDictionaryMockMvc.perform(
            post("/api/dictionaries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(dictionaryDTO))
        ).andExpect(status().isBadRequest)

        val dictionaryList = dictionaryRepository.findAll()
        assertThat(dictionaryList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllDictionaries() {
        // Initialize the database
        dictionaryRepository.saveAndFlush(dictionary)

        // Get all the dictionaryList
        restDictionaryMockMvc.perform(get("/api/dictionaries?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dictionary.id?.toInt())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].entityName").value(hasItem(DEFAULT_ENTITY_NAME)))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getDictionary() {
        // Initialize the database
        dictionaryRepository.saveAndFlush(dictionary)

        val id = dictionary.id
        assertNotNull(id)

        // Get the dictionary
        restDictionaryMockMvc.perform(get("/api/dictionaries/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dictionary.id?.toInt()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.entityName").value(DEFAULT_ENTITY_NAME))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingDictionary() {
        // Get the dictionary
        restDictionaryMockMvc.perform(get("/api/dictionaries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateDictionary() {
        // Initialize the database
        dictionaryRepository.saveAndFlush(dictionary)

        val databaseSizeBeforeUpdate = dictionaryRepository.findAll().size

        // Update the dictionary
        val id = dictionary.id
        assertNotNull(id)
        val updatedDictionary = dictionaryRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedDictionary are not directly saved in db
        em.detach(updatedDictionary)
        updatedDictionary.code = UPDATED_CODE
        updatedDictionary.name = UPDATED_NAME
        updatedDictionary.entityName = UPDATED_ENTITY_NAME
        updatedDictionary.flagDeleted = UPDATED_FLAG_DELETED
        val dictionaryDTO = dictionaryMapper.toDto(updatedDictionary)

        restDictionaryMockMvc.perform(
            put("/api/dictionaries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(dictionaryDTO))
        ).andExpect(status().isOk)

        // Validate the Dictionary in the database
        val dictionaryList = dictionaryRepository.findAll()
        assertThat(dictionaryList).hasSize(databaseSizeBeforeUpdate)
        val testDictionary = dictionaryList[dictionaryList.size - 1]
        assertThat(testDictionary.code).isEqualTo(UPDATED_CODE)
        assertThat(testDictionary.name).isEqualTo(UPDATED_NAME)
        assertThat(testDictionary.entityName).isEqualTo(UPDATED_ENTITY_NAME)
        assertThat(testDictionary.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingDictionary() {
        val databaseSizeBeforeUpdate = dictionaryRepository.findAll().size

        // Create the Dictionary
        val dictionaryDTO = dictionaryMapper.toDto(dictionary)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDictionaryMockMvc.perform(
            put("/api/dictionaries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(dictionaryDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Dictionary in the database
        val dictionaryList = dictionaryRepository.findAll()
        assertThat(dictionaryList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteDictionary() {
        // Initialize the database
        dictionaryRepository.saveAndFlush(dictionary)

        val databaseSizeBeforeDelete = dictionaryRepository.findAll().size

        // Delete the dictionary
        restDictionaryMockMvc.perform(
            delete("/api/dictionaries/{id}", dictionary.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val dictionaryList = dictionaryRepository.findAll()
        assertThat(dictionaryList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_CODE = "AAAAAAAAAA"
        private const val UPDATED_CODE = "BBBBBBBBBB"

        private const val DEFAULT_NAME = "AAAAAAAAAA"
        private const val UPDATED_NAME = "BBBBBBBBBB"

        private const val DEFAULT_ENTITY_NAME = "AAAAAAAAAA"
        private const val UPDATED_ENTITY_NAME = "BBBBBBBBBB"

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Dictionary {
            val dictionary = Dictionary(
                code = DEFAULT_CODE,
                name = DEFAULT_NAME,
                entityName = DEFAULT_ENTITY_NAME,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return dictionary
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Dictionary {
            val dictionary = Dictionary(
                code = UPDATED_CODE,
                name = UPDATED_NAME,
                entityName = UPDATED_ENTITY_NAME,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return dictionary
        }
    }
}
