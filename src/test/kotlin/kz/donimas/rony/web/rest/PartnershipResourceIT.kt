package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.Partnership
import kz.donimas.rony.repository.PartnershipRepository
import kz.donimas.rony.service.PartnershipService
import kz.donimas.rony.service.mapper.PartnershipMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [PartnershipResource] REST controller.
 *
 * @see PartnershipResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class PartnershipResourceIT {

    @Autowired
    private lateinit var partnershipRepository: PartnershipRepository

    @Autowired
    private lateinit var partnershipMapper: PartnershipMapper

    @Autowired
    private lateinit var partnershipService: PartnershipService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restPartnershipMockMvc: MockMvc

    private lateinit var partnership: Partnership

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val partnershipResource = PartnershipResource(partnershipService)
        this.restPartnershipMockMvc = MockMvcBuilders.standaloneSetup(partnershipResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        partnership = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createPartnership() {
        val databaseSizeBeforeCreate = partnershipRepository.findAll().size

        // Create the Partnership
        val partnershipDTO = partnershipMapper.toDto(partnership)
        restPartnershipMockMvc.perform(
            post("/api/partnerships")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(partnershipDTO))
        ).andExpect(status().isCreated)

        // Validate the Partnership in the database
        val partnershipList = partnershipRepository.findAll()
        assertThat(partnershipList).hasSize(databaseSizeBeforeCreate + 1)
    }

    @Test
    @Transactional
    fun createPartnershipWithExistingId() {
        val databaseSizeBeforeCreate = partnershipRepository.findAll().size

        // Create the Partnership with an existing ID
        partnership.id = 1L
        val partnershipDTO = partnershipMapper.toDto(partnership)

        // An entity with an existing ID cannot be created, so this API call must fail
        restPartnershipMockMvc.perform(
            post("/api/partnerships")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(partnershipDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Partnership in the database
        val partnershipList = partnershipRepository.findAll()
        assertThat(partnershipList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllPartnerships() {
        // Initialize the database
        partnershipRepository.saveAndFlush(partnership)

        // Get all the partnershipList
        restPartnershipMockMvc.perform(get("/api/partnerships?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partnership.id?.toInt())))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getPartnership() {
        // Initialize the database
        partnershipRepository.saveAndFlush(partnership)

        val id = partnership.id
        assertNotNull(id)

        // Get the partnership
        restPartnershipMockMvc.perform(get("/api/partnerships/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(partnership.id?.toInt()))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingPartnership() {
        // Get the partnership
        restPartnershipMockMvc.perform(get("/api/partnerships/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updatePartnership() {
        // Initialize the database
        partnershipRepository.saveAndFlush(partnership)

        val databaseSizeBeforeUpdate = partnershipRepository.findAll().size

        // Update the partnership
        val id = partnership.id
        assertNotNull(id)
        val updatedPartnership = partnershipRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedPartnership are not directly saved in db
        em.detach(updatedPartnership)
        val partnershipDTO = partnershipMapper.toDto(updatedPartnership)

        restPartnershipMockMvc.perform(
            put("/api/partnerships")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(partnershipDTO))
        ).andExpect(status().isOk)

        // Validate the Partnership in the database
        val partnershipList = partnershipRepository.findAll()
        assertThat(partnershipList).hasSize(databaseSizeBeforeUpdate)
        val testPartnership = partnershipList[partnershipList.size - 1]
    }

    @Test
    @Transactional
    fun updateNonExistingPartnership() {
        val databaseSizeBeforeUpdate = partnershipRepository.findAll().size

        // Create the Partnership
        val partnershipDTO = partnershipMapper.toDto(partnership)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPartnershipMockMvc.perform(
            put("/api/partnerships")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(partnershipDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Partnership in the database
        val partnershipList = partnershipRepository.findAll()
        assertThat(partnershipList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deletePartnership() {
        // Initialize the database
        partnershipRepository.saveAndFlush(partnership)

        val databaseSizeBeforeDelete = partnershipRepository.findAll().size

        // Delete the partnership
        restPartnershipMockMvc.perform(
            delete("/api/partnerships/{id}", partnership.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val partnershipList = partnershipRepository.findAll()
        assertThat(partnershipList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Partnership {
            val partnership = Partnership()

            return partnership
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Partnership {
            val partnership = Partnership()

            return partnership
        }
    }
}
