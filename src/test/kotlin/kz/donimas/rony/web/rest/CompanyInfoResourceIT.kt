package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.CompanyInfo
import kz.donimas.rony.repository.CompanyInfoRepository
import kz.donimas.rony.service.CompanyInfoService
import kz.donimas.rony.service.mapper.CompanyInfoMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [CompanyInfoResource] REST controller.
 *
 * @see CompanyInfoResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class CompanyInfoResourceIT {

    @Autowired
    private lateinit var companyInfoRepository: CompanyInfoRepository

    @Autowired
    private lateinit var companyInfoMapper: CompanyInfoMapper

    @Autowired
    private lateinit var companyInfoService: CompanyInfoService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restCompanyInfoMockMvc: MockMvc

    private lateinit var companyInfo: CompanyInfo

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val companyInfoResource = CompanyInfoResource(companyInfoService)
        this.restCompanyInfoMockMvc = MockMvcBuilders.standaloneSetup(companyInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        companyInfo = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createCompanyInfo() {
        val databaseSizeBeforeCreate = companyInfoRepository.findAll().size

        // Create the CompanyInfo
        val companyInfoDTO = companyInfoMapper.toDto(companyInfo)
        restCompanyInfoMockMvc.perform(
            post("/api/company-infos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(companyInfoDTO))
        ).andExpect(status().isCreated)

        // Validate the CompanyInfo in the database
        val companyInfoList = companyInfoRepository.findAll()
        assertThat(companyInfoList).hasSize(databaseSizeBeforeCreate + 1)
        val testCompanyInfo = companyInfoList[companyInfoList.size - 1]
        assertThat(testCompanyInfo.name).isEqualTo(DEFAULT_NAME)
        assertThat(testCompanyInfo.website).isEqualTo(DEFAULT_WEBSITE)
        assertThat(testCompanyInfo.requisites).isEqualTo(DEFAULT_REQUISITES)
        assertThat(testCompanyInfo.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createCompanyInfoWithExistingId() {
        val databaseSizeBeforeCreate = companyInfoRepository.findAll().size

        // Create the CompanyInfo with an existing ID
        companyInfo.id = 1L
        val companyInfoDTO = companyInfoMapper.toDto(companyInfo)

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyInfoMockMvc.perform(
            post("/api/company-infos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(companyInfoDTO))
        ).andExpect(status().isBadRequest)

        // Validate the CompanyInfo in the database
        val companyInfoList = companyInfoRepository.findAll()
        assertThat(companyInfoList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    fun checkNameIsRequired() {
        val databaseSizeBeforeTest = companyInfoRepository.findAll().size
        // set the field null
        companyInfo.name = null

        // Create the CompanyInfo, which fails.
        val companyInfoDTO = companyInfoMapper.toDto(companyInfo)

        restCompanyInfoMockMvc.perform(
            post("/api/company-infos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(companyInfoDTO))
        ).andExpect(status().isBadRequest)

        val companyInfoList = companyInfoRepository.findAll()
        assertThat(companyInfoList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllCompanyInfos() {
        // Initialize the database
        companyInfoRepository.saveAndFlush(companyInfo)

        // Get all the companyInfoList
        restCompanyInfoMockMvc.perform(get("/api/company-infos?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyInfo.id?.toInt())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE)))
            .andExpect(jsonPath("$.[*].requisites").value(hasItem(DEFAULT_REQUISITES)))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getCompanyInfo() {
        // Initialize the database
        companyInfoRepository.saveAndFlush(companyInfo)

        val id = companyInfo.id
        assertNotNull(id)

        // Get the companyInfo
        restCompanyInfoMockMvc.perform(get("/api/company-infos/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companyInfo.id?.toInt()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.website").value(DEFAULT_WEBSITE))
            .andExpect(jsonPath("$.requisites").value(DEFAULT_REQUISITES))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingCompanyInfo() {
        // Get the companyInfo
        restCompanyInfoMockMvc.perform(get("/api/company-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateCompanyInfo() {
        // Initialize the database
        companyInfoRepository.saveAndFlush(companyInfo)

        val databaseSizeBeforeUpdate = companyInfoRepository.findAll().size

        // Update the companyInfo
        val id = companyInfo.id
        assertNotNull(id)
        val updatedCompanyInfo = companyInfoRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedCompanyInfo are not directly saved in db
        em.detach(updatedCompanyInfo)
        updatedCompanyInfo.name = UPDATED_NAME
        updatedCompanyInfo.website = UPDATED_WEBSITE
        updatedCompanyInfo.requisites = UPDATED_REQUISITES
        updatedCompanyInfo.flagDeleted = UPDATED_FLAG_DELETED
        val companyInfoDTO = companyInfoMapper.toDto(updatedCompanyInfo)

        restCompanyInfoMockMvc.perform(
            put("/api/company-infos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(companyInfoDTO))
        ).andExpect(status().isOk)

        // Validate the CompanyInfo in the database
        val companyInfoList = companyInfoRepository.findAll()
        assertThat(companyInfoList).hasSize(databaseSizeBeforeUpdate)
        val testCompanyInfo = companyInfoList[companyInfoList.size - 1]
        assertThat(testCompanyInfo.name).isEqualTo(UPDATED_NAME)
        assertThat(testCompanyInfo.website).isEqualTo(UPDATED_WEBSITE)
        assertThat(testCompanyInfo.requisites).isEqualTo(UPDATED_REQUISITES)
        assertThat(testCompanyInfo.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingCompanyInfo() {
        val databaseSizeBeforeUpdate = companyInfoRepository.findAll().size

        // Create the CompanyInfo
        val companyInfoDTO = companyInfoMapper.toDto(companyInfo)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyInfoMockMvc.perform(
            put("/api/company-infos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(companyInfoDTO))
        ).andExpect(status().isBadRequest)

        // Validate the CompanyInfo in the database
        val companyInfoList = companyInfoRepository.findAll()
        assertThat(companyInfoList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteCompanyInfo() {
        // Initialize the database
        companyInfoRepository.saveAndFlush(companyInfo)

        val databaseSizeBeforeDelete = companyInfoRepository.findAll().size

        // Delete the companyInfo
        restCompanyInfoMockMvc.perform(
            delete("/api/company-infos/{id}", companyInfo.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val companyInfoList = companyInfoRepository.findAll()
        assertThat(companyInfoList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_NAME = "AAAAAAAAAA"
        private const val UPDATED_NAME = "BBBBBBBBBB"

        private const val DEFAULT_WEBSITE = "AAAAAAAAAA"
        private const val UPDATED_WEBSITE = "BBBBBBBBBB"

        private const val DEFAULT_REQUISITES = "AAAAAAAAAA"
        private const val UPDATED_REQUISITES = "BBBBBBBBBB"

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): CompanyInfo {
            val companyInfo = CompanyInfo(
                name = DEFAULT_NAME,
                website = DEFAULT_WEBSITE,
                requisites = DEFAULT_REQUISITES,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return companyInfo
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): CompanyInfo {
            val companyInfo = CompanyInfo(
                name = UPDATED_NAME,
                website = UPDATED_WEBSITE,
                requisites = UPDATED_REQUISITES,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return companyInfo
        }
    }
}
