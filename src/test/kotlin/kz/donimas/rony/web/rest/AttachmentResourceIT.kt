package kz.donimas.rony.web.rest

import kz.donimas.rony.RonyApp
import kz.donimas.rony.domain.Attachment
import kz.donimas.rony.repository.AttachmentRepository
import kz.donimas.rony.service.AttachmentService
import kz.donimas.rony.service.mapper.AttachmentMapper
import kz.donimas.rony.web.rest.errors.ExceptionTranslator
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.Base64Utils
import org.springframework.validation.Validator
import javax.persistence.EntityManager
import kotlin.test.assertNotNull

/**
 * Integration tests for the [AttachmentResource] REST controller.
 *
 * @see AttachmentResource
 */
@SpringBootTest(classes = [RonyApp::class])
@AutoConfigureMockMvc
@WithMockUser
class AttachmentResourceIT {

    @Autowired
    private lateinit var attachmentRepository: AttachmentRepository

    @Autowired
    private lateinit var attachmentMapper: AttachmentMapper

    @Autowired
    private lateinit var attachmentService: AttachmentService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restAttachmentMockMvc: MockMvc

    private lateinit var attachment: Attachment

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val attachmentResource = AttachmentResource(attachmentService)
        this.restAttachmentMockMvc = MockMvcBuilders.standaloneSetup(attachmentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        attachment = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createAttachment() {
        val databaseSizeBeforeCreate = attachmentRepository.findAll().size

        // Create the Attachment
        val attachmentDTO = attachmentMapper.toDto(attachment)
        restAttachmentMockMvc.perform(
            post("/api/attachments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(attachmentDTO))
        ).andExpect(status().isCreated)

        // Validate the Attachment in the database
        val attachmentList = attachmentRepository.findAll()
        assertThat(attachmentList).hasSize(databaseSizeBeforeCreate + 1)
        val testAttachment = attachmentList[attachmentList.size - 1]
        assertThat(testAttachment.file).isEqualTo(DEFAULT_FILE)
        assertThat(testAttachment.fileContentType).isEqualTo(DEFAULT_FILE_CONTENT_TYPE)
        assertThat(testAttachment.type).isEqualTo(DEFAULT_TYPE)
        assertThat(testAttachment.uri).isEqualTo(DEFAULT_URI)
        assertThat(testAttachment.unicode).isEqualTo(DEFAULT_UNICODE)
        assertThat(testAttachment.name).isEqualTo(DEFAULT_NAME)
        assertThat(testAttachment.entityName).isEqualTo(DEFAULT_ENTITY_NAME)
        assertThat(testAttachment.entityId).isEqualTo(DEFAULT_ENTITY_ID)
        assertThat(testAttachment.flagDeleted).isEqualTo(DEFAULT_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun createAttachmentWithExistingId() {
        val databaseSizeBeforeCreate = attachmentRepository.findAll().size

        // Create the Attachment with an existing ID
        attachment.id = 1L
        val attachmentDTO = attachmentMapper.toDto(attachment)

        // An entity with an existing ID cannot be created, so this API call must fail
        restAttachmentMockMvc.perform(
            post("/api/attachments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(attachmentDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Attachment in the database
        val attachmentList = attachmentRepository.findAll()
        assertThat(attachmentList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllAttachments() {
        // Initialize the database
        attachmentRepository.saveAndFlush(attachment)

        // Get all the attachmentList
        restAttachmentMockMvc.perform(get("/api/attachments?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(attachment.id?.toInt())))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].uri").value(hasItem(DEFAULT_URI)))
            .andExpect(jsonPath("$.[*].unicode").value(hasItem(DEFAULT_UNICODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].entityName").value(hasItem(DEFAULT_ENTITY_NAME)))
            .andExpect(jsonPath("$.[*].entityId").value(hasItem(DEFAULT_ENTITY_ID?.toInt())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED)))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAttachment() {
        // Initialize the database
        attachmentRepository.saveAndFlush(attachment)

        val id = attachment.id
        assertNotNull(id)

        // Get the attachment
        restAttachmentMockMvc.perform(get("/api/attachments/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(attachment.id?.toInt()))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.uri").value(DEFAULT_URI))
            .andExpect(jsonPath("$.unicode").value(DEFAULT_UNICODE))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.entityName").value(DEFAULT_ENTITY_NAME))
            .andExpect(jsonPath("$.entityId").value(DEFAULT_ENTITY_ID?.toInt()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingAttachment() {
        // Get the attachment
        restAttachmentMockMvc.perform(get("/api/attachments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateAttachment() {
        // Initialize the database
        attachmentRepository.saveAndFlush(attachment)

        val databaseSizeBeforeUpdate = attachmentRepository.findAll().size

        // Update the attachment
        val id = attachment.id
        assertNotNull(id)
        val updatedAttachment = attachmentRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedAttachment are not directly saved in db
        em.detach(updatedAttachment)
        updatedAttachment.file = UPDATED_FILE
        updatedAttachment.fileContentType = UPDATED_FILE_CONTENT_TYPE
        updatedAttachment.type = UPDATED_TYPE
        updatedAttachment.uri = UPDATED_URI
        updatedAttachment.unicode = UPDATED_UNICODE
        updatedAttachment.name = UPDATED_NAME
        updatedAttachment.entityName = UPDATED_ENTITY_NAME
        updatedAttachment.entityId = UPDATED_ENTITY_ID
        updatedAttachment.flagDeleted = UPDATED_FLAG_DELETED
        val attachmentDTO = attachmentMapper.toDto(updatedAttachment)

        restAttachmentMockMvc.perform(
            put("/api/attachments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(attachmentDTO))
        ).andExpect(status().isOk)

        // Validate the Attachment in the database
        val attachmentList = attachmentRepository.findAll()
        assertThat(attachmentList).hasSize(databaseSizeBeforeUpdate)
        val testAttachment = attachmentList[attachmentList.size - 1]
        assertThat(testAttachment.file).isEqualTo(UPDATED_FILE)
        assertThat(testAttachment.fileContentType).isEqualTo(UPDATED_FILE_CONTENT_TYPE)
        assertThat(testAttachment.type).isEqualTo(UPDATED_TYPE)
        assertThat(testAttachment.uri).isEqualTo(UPDATED_URI)
        assertThat(testAttachment.unicode).isEqualTo(UPDATED_UNICODE)
        assertThat(testAttachment.name).isEqualTo(UPDATED_NAME)
        assertThat(testAttachment.entityName).isEqualTo(UPDATED_ENTITY_NAME)
        assertThat(testAttachment.entityId).isEqualTo(UPDATED_ENTITY_ID)
        assertThat(testAttachment.flagDeleted).isEqualTo(UPDATED_FLAG_DELETED)
    }

    @Test
    @Transactional
    fun updateNonExistingAttachment() {
        val databaseSizeBeforeUpdate = attachmentRepository.findAll().size

        // Create the Attachment
        val attachmentDTO = attachmentMapper.toDto(attachment)

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAttachmentMockMvc.perform(
            put("/api/attachments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(attachmentDTO))
        ).andExpect(status().isBadRequest)

        // Validate the Attachment in the database
        val attachmentList = attachmentRepository.findAll()
        assertThat(attachmentList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteAttachment() {
        // Initialize the database
        attachmentRepository.saveAndFlush(attachment)

        val databaseSizeBeforeDelete = attachmentRepository.findAll().size

        // Delete the attachment
        restAttachmentMockMvc.perform(
            delete("/api/attachments/{id}", attachment.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val attachmentList = attachmentRepository.findAll()
        assertThat(attachmentList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private val DEFAULT_FILE: ByteArray = createByteArray(1, "0")
        private val UPDATED_FILE: ByteArray = createByteArray(1, "1")
        private const val DEFAULT_FILE_CONTENT_TYPE: String = "image/jpg"
        private const val UPDATED_FILE_CONTENT_TYPE: String = "image/png"

        private const val DEFAULT_TYPE = "AAAAAAAAAA"
        private const val UPDATED_TYPE = "BBBBBBBBBB"

        private const val DEFAULT_URI = "AAAAAAAAAA"
        private const val UPDATED_URI = "BBBBBBBBBB"

        private const val DEFAULT_UNICODE = "AAAAAAAAAA"
        private const val UPDATED_UNICODE = "BBBBBBBBBB"

        private const val DEFAULT_NAME = "AAAAAAAAAA"
        private const val UPDATED_NAME = "BBBBBBBBBB"

        private const val DEFAULT_ENTITY_NAME = "AAAAAAAAAA"
        private const val UPDATED_ENTITY_NAME = "BBBBBBBBBB"

        private const val DEFAULT_ENTITY_ID: Long = 1L
        private const val UPDATED_ENTITY_ID: Long = 2L

        private const val DEFAULT_FLAG_DELETED: Boolean = false
        private const val UPDATED_FLAG_DELETED: Boolean = true

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Attachment {
            val attachment = Attachment(
                file = DEFAULT_FILE,
                fileContentType = DEFAULT_FILE_CONTENT_TYPE,
                type = DEFAULT_TYPE,
                uri = DEFAULT_URI,
                unicode = DEFAULT_UNICODE,
                name = DEFAULT_NAME,
                entityName = DEFAULT_ENTITY_NAME,
                entityId = DEFAULT_ENTITY_ID,
                flagDeleted = DEFAULT_FLAG_DELETED
            )

            return attachment
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Attachment {
            val attachment = Attachment(
                file = UPDATED_FILE,
                fileContentType = UPDATED_FILE_CONTENT_TYPE,
                type = UPDATED_TYPE,
                uri = UPDATED_URI,
                unicode = UPDATED_UNICODE,
                name = UPDATED_NAME,
                entityName = UPDATED_ENTITY_NAME,
                entityId = UPDATED_ENTITY_ID,
                flagDeleted = UPDATED_FLAG_DELETED
            )

            return attachment
        }
    }
}
